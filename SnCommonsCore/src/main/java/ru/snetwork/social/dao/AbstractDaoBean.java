/*
 * Базовый абстрактный класс DAO всех типов данных хранимых в БД.
 * Определяет основные свойства и методы по работе с сущностями модели данных БД,
 * наследующих класс EntityModelBasic.
 */
package ru.snetwork.social.dao;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.commons.collections.list.SetUniqueList;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;
import static ru.snetwork.social.dao.AbstractEntriesDaoBean.ORDER_BY_ID;
import ru.snetwork.social.model.EntityModelBasic;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.model.EntityModelRegistry;
import ru.snetwork.social.dao.query.JoinClauses;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.dao.query.SubqueryClause;
import ru.snetwork.social.dao.query.WhereClause.Operator;
import ru.snetwork.social.dao.query.WhereClauseClass;
import ru.snetwork.social.dao.query.WhereClauseProperty;
import ru.snetwork.social.exception.DaoUnableToCreateObjectException;
import ru.snetwork.social.exception.DaoUnableToDeleteObjectException;

/**
 *
 * @author ky6uHETc
 * @param <T> класс сущности модели данных БД (должен наследовать базовый класс
 * сущности EntityModelBasic)
 */
public abstract class AbstractDaoBean<T extends EntityModelBasic> {

    protected Log log = LogFactory.getLog(getClass());
    protected EntityManager em;
    private Class<T> clazz = null;

    public abstract void setEntityManager(EntityManager entityManager);

    protected void buildQueryWhere(List<WhereClause> where, List<String> qWhere, List<Object> qParam) {
        if (where != null) {
            for (WhereClause clause : where) {
                if (clause instanceof JoinClauses) {
                    JoinClauses join = (JoinClauses) clause;
                    List<String> joinWhere = new LinkedList<>();
                    buildQueryWhere(join.getValue(), joinWhere, qParam);
                    qWhere.add("(" + StringUtils.join(joinWhere, ")" + join.getLogic().getOperand() + " (") + ")");
                } else if (clause instanceof SubqueryClause) {
                    SubqueryClause subquery = (SubqueryClause) clause;
                    String str = subquery.getSubquery();
                    for (int p = 0; p < subquery.getParams().length; p++) {
                        int i = qParam.size();
                        str = str.replaceAll("\\:" + subquery.getParams()[p] + "\\b", "?" + i + "");
                        qParam.add(subquery.getValues()[p]);
                    }
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + "(" + str + ")");

                } else {
                    buildQueryWhereClause(clause, qWhere, qParam);
                }
            }
        }
    }

    protected void buildQueryWhereClause(WhereClause clause, List<String> qWhere, List<Object> qParam) {
        if (clause instanceof WhereClauseClass) {
            WhereClauseClass whereClass = (WhereClauseClass) clause;
            String propertyClass = StringUtils.isBlank(clause.getProperty()) ? "o.class" : clause.getProperty() + ".class";
            if (Operator.ne.equals(whereClass.getOperator())) {
                qWhere.add(propertyClass + " != " + whereClass.getValue().getSimpleName());
            } else {
                qWhere.add(propertyClass + " = " + whereClass.getValue().getSimpleName());
            }
        } else if (clause instanceof WhereClauseProperty) {
            switch (clause.getOperator()) {
                case in:
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + "(" + clause.getValue() + ")");
                    break;
                case objectInElements:
                    qWhere.add(clause.getValue() + clause.getOperator().getOperator() + "(" + clause.getProperty() + ")");
                    break;
                case notIn:
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + "(" + clause.getValue() + ")");
                    break;
                case objectNotInElements:
                    qWhere.add(clause.getValue() + clause.getOperator().getOperator() + "(" + clause.getProperty() + ")");
                    break;
                default:
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + clause.getValue());
                    break;
            }
        } else {
            int i = qParam.size();
            switch (clause.getOperator()) {
                case isNull:
                    qWhere.add(clause.getProperty() + WhereClause.Operator.isNull.getOperator());
                    break;
                case notNull:
                    qWhere.add(clause.getProperty() + WhereClause.Operator.notNull.getOperator());
                    break;
                case isEmpty:
                    qWhere.add(clause.getProperty() + WhereClause.Operator.isEmpty.getOperator());
                    break;
                case notEmpty:
                    qWhere.add(clause.getProperty() + WhereClause.Operator.notEmpty.getOperator());
                    break;
                case exists:
                    qWhere.add(WhereClause.Operator.exists.getOperator() + "(" + clause.getProperty() + ")");
                    break;
                case in:
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + "(?" + i + ")");
                    qParam.add(clause.getValue());
                    break;
                case objectInElements:
                    qWhere.add("?" + i + clause.getOperator().getOperator() + "(" + clause.getProperty() + ")");
                    qParam.add(clause.getValue());
                    break;
                case notIn:
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + "(?" + i + ")");
                    qParam.add(clause.getValue());
                    break;
                case objectNotInElements:
                    qWhere.add("?" + i + clause.getOperator().getOperator() + "(" + clause.getProperty() + ")");
                    qParam.add(clause.getValue());
                    break;
                case ilike:
                    qWhere.add("lower(" + clause.getProperty() + ") " + clause.getOperator().getOperator() + " ?" + i);
                    qParam.add(((String) clause.getValue()).toLowerCase());
                    break;
                default:
                    qWhere.add(clause.getProperty() + clause.getOperator().getOperator() + "?" + i);
                    qParam.add(clause.getValue());
                    break;
            }
        }
    }

    /**
     * Read the entity class from generic class parameters.
     *
     * @return
     */
    public Class<T> objectClass() {
        if (clazz == null) {
            clazz = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return clazz;
    }

    @Transactional
    protected T doSave(T o) throws DaoUnableToCreateObjectException {
        try {
            objectClass().cast(o);

            // generate UUID
            if (o instanceof EntityModelRegistry) {
                ((EntityModelRegistry) o).getUuid();
            }

            // save object
            if (o.getId() != null) {
                o = em.merge(o);
                if (o instanceof EntityModelEntry) {
                    ((EntityModelEntry) o).setDateModifiedAt(new Date());
                }
            } else {
                if (o instanceof EntityModelEntry) {
                    ((EntityModelEntry) o).setDateCreatedAt(new Date());
                }
                em.persist(o);
            }

            return o;
        } catch (ClassCastException e) {
            throw new DaoUnableToCreateObjectException(o, "Incompatible object class " + o.getClass().getName() + " for " + this.getClass().getSimpleName());
        }
    }

    @Transactional
    protected void doDelete(T o) throws DaoUnableToDeleteObjectException {
        o = em.merge(o);
        if (o instanceof EntityModelEntry) {
            ((EntityModelEntry) o).setDateDeletedAt(new Date());
        } else {
            em.remove(o);
        }
    }

    public T getObject(Long id) {
        return (T) em.find(objectClass(), id);
    }

    @Transactional(readOnly = true)
    public T getObjectFetchedAll(Long id) {
        T o = em.find(objectClass(), id);
        if (o != null) {
            fetchAllProperties(o);
        }
        return o;
    }

    @Transactional(readOnly = true)
    public <Z extends T> Z getObjectFetchedAll(Z object) {
        return (Z) getObjectFetchedAll(object.getId());
    }

    protected abstract void fetchAllProperties(T object);

    public abstract Long count();

    public abstract List<T> list(int offset, int size);

    public Long count(Class<? extends T> from, List<WhereClause> where) {

        // from class
        if (from == null) {
            from = objectClass();
        }

        // conditions
        List<String> qWhere = new LinkedList<>();
        List<Object> qParam = new LinkedList<>();
        buildQueryWhere(where, qWhere, qParam);

        // query
        Query q = em.createQuery("select count(o) from " + from.getSimpleName() + " as o"
                + (!qWhere.isEmpty() ? " where (" + StringUtils.join(qWhere, ") and (") + ")" : ""));

        for (int i = 0; i < qParam.size(); i++) {
            q.setParameter(i, qParam.get(i));
        }

        return (Long) q.getSingleResult();
    }

    public List<T> list(Class<? extends T> from, List<WhereClause> where, int offset, int size, OrderBy... orderBy) {
        return list(null, from, null, where, offset, size, orderBy);
    }

    public List<T> list(String select, Class<? extends T> from, String joinRelations, List<WhereClause> where, int offset, int size, OrderBy... orderBy) {

        // from class
        if (from == null) {
            from = objectClass();
        }

        // conditions
        final List<String> qWhere = new LinkedList<>();
        final List<Object> qParam = new LinkedList<>();
        buildQueryWhere(where, qWhere, qParam);

        // query
        final String hql = "select " + (StringUtils.isNotBlank(select) ? select : "o")
                + " from " + from.getSimpleName() + " as o "
                + (StringUtils.isNotBlank(joinRelations) ? joinRelations : "")
                + (!qWhere.isEmpty() ? " where  (" + StringUtils.join(qWhere, ") and (") + ")" : "")
                + " order by " + (orderBy != null ? StringUtils.join(ArrayUtils.add(orderBy, ORDER_BY_ID), ", ") : ORDER_BY_ID);

        Query q = em.createQuery(hql);
        for (int i = 0; i < qParam.size(); i++) {
            q.setParameter(i, qParam.get(i));
        }
        if (size > 0 && offset >= 0) {
            q.setFirstResult(offset).setMaxResults(size);
        }

        return SetUniqueList.decorate(q.getResultList());
    }
}
