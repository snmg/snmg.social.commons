/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

/**
 *
 * @author ky6uHETc
 */
public class WhereClauseNumber extends WhereClause<Number>{
    
    public WhereClauseNumber(String property, Number value) {
        super(property, Operator.eq, value);
    }
    
    public WhereClauseNumber(String property, Operator criteria, Number value) {
        super(property, criteria, value);
    }
    
}
