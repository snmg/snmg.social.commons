/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import ru.snetwork.social.exception.DaoUnsafeReadonlySql;

/**
 *
 * @author ky6uHETc
 */
public final class DaoQueryUtil {

    public static List<WhereClause> where(WhereClause... clauses) {
        List<WhereClause> where = new LinkedList<>();
        for (WhereClause c : clauses) {
            if (c != null) {
                where.add(c);
            }
        }
        return where;
    }

    // TODO: Check for valid tables
    public static boolean checkSqlSafety(String sql, String... allowedTables) throws DaoUnsafeReadonlySql {
        sql = StringUtils.trimToEmpty(sql).toLowerCase();
        if (!sql.startsWith("select ")) {
            throw new DaoUnsafeReadonlySql(sql);
        }
        return true;
    }
}
