/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

/**
 *
 * @author ky6uHETc
 */
public class OrderBy {

    final String property;
    final boolean descending;
    final Boolean nullsLast;

    public OrderBy(String property, boolean descending) {
        this.property = property;
        this.descending = descending;
        this.nullsLast = null;
    }

    public OrderBy(String property, boolean descending, boolean nullsLast) {
        this.property = property;
        this.descending = descending;
        this.nullsLast = nullsLast;
    }

    public String getProperty() {
        return property;
    }

    public boolean isDescending() {
        return descending;
    }

    @Override
    public String toString() {
        if (nullsLast != null) {
            return property + (descending ? " desc" : " asc") + (nullsLast ? " nulls last" : " nulls first");
        } else {
            return property + (descending ? " desc" : "");
        }
    }

}
