/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import org.apache.commons.collections.list.SetUniqueList;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.exception.DaoUnableToCreateObjectException;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.model.EntityUserTrace;

/**
 *
 * @author ky6uHETc
 * @param <T> класс сущности модели данных БД (должен наследовать базовый класс
 * сущности EntityModelBasic)
 */
public abstract class AbstractEntriesDaoBean<T extends EntityModelEntry, U extends EntityModelSystemUser, D extends AbstractSystemLogDaoBean> extends AbstractDaoBean<T> {

    public static final OrderBy ORDER_BY_ID = new OrderBy("o.id", false);

    protected D systemLogDaoBean = null;
    protected boolean useDateDeleted = true;

    public abstract void setSystemLogDaoBean(D systemLogDaoBean);

    @Transactional
    public void doRestore(T o, U u) {
        if (useDateDeleted) {
            o = em.merge(o);
            o.setDateDeletedAt(null);

            if (systemLogDaoBean != null && u != null && !u.isSystem()) {
                try {
                    systemLogDaoBean.doLog(o.getClass().getSimpleName(), o, u);
                } catch (DaoUnableToCreateObjectException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
    }

    @Transactional
    public T doSave(T o, U u) throws DaoUnableToCreateObjectException {
        if (o.getId() != null) {
            o.setDateModifiedAt(new Date());
            if (o instanceof EntityUserTrace) {
                ((EntityUserTrace) o).setUserModified(u);
            }
        } else {
            o.setDateCreatedAt(new Date());
            if (o instanceof EntityUserTrace) {
                ((EntityUserTrace) o).setUserCreated(u);
            }
        }

        o = super.doSave(o);

        if (systemLogDaoBean != null && u != null && !u.isSystem()) {
            try {
                systemLogDaoBean.doLog(o.getClass().getSimpleName(), o, u);
            } catch (DaoUnableToCreateObjectException ex) {
                log.error(ex.getMessage(), ex);
            }
        }

        return o;
    }

    @Transactional
    public void doDelete(T o, U u) {
        if (useDateDeleted) {
            o = em.merge(o);

            o.setDateDeletedAt(new Date());
            if (o instanceof EntityUserTrace) {
                ((EntityUserTrace) o).setUserDeleted(u);
            }

            if (systemLogDaoBean != null && u != null && !u.isSystem()) {
                try {
                    systemLogDaoBean.doLog(o.getClass().getSimpleName(), o, u);
                } catch (DaoUnableToCreateObjectException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        } else {
            o = em.merge(o);
            em.remove(o);
        }
    }

    /**
     * Returns list of entries from database.
     *
     * @param offset entries offset
     * @param size entries limit
     * @return list of entries.
     *
     * <br>
     * <br><b>NOTE:</b> to get all entries call "list(0,0)".
     *
     */
    @Override
    public List<T> list(int offset, int size) {
        return list(null, offset, size);
    }

    public List<T> list(List<WhereClause> where, int offset, int size, OrderBy... orderBy) {
        return list(null, where, offset, size, orderBy);
    }

    @Override
    public List<T> list(Class<? extends T> from, List<WhereClause> where, int offset, int size, OrderBy... orderBy) {
        return list(from, null, where, null, offset, size, orderBy);
    }

    public List<T> list(Class<? extends T> from, List<WhereClause> where, Boolean isShowDeleted, int offset, int size, OrderBy... orderBy) {
        return list(null, from, null, where, isShowDeleted, offset, size, orderBy);
    }

    public List<T> list(Class<? extends T> from, String joinRelations, List<WhereClause> where, Boolean isShowDeleted, int offset, int size, OrderBy... orderBy) {
        return list(null, from, joinRelations, where, isShowDeleted, offset, size, orderBy);
    }

    public List<T> list(Class<? extends T> from, String joinRelations, List<WhereClause> where, int offset, int size, OrderBy... orderBy) {
        return list(null, from, joinRelations, where, false, offset, size, orderBy);
    }

    @Override
    public List list(String select, Class<? extends T> from, String joinRelations, List<WhereClause> where, int offset, int size, OrderBy... orderBy) {
        return list(select, from, joinRelations, where, false, offset, size, orderBy);
    }

    public List list(String select, Class<? extends T> from, String joinRelations, List<WhereClause> where, Boolean isShowDeleted, int offset, int size, OrderBy... orderBy) {

        // from class
        if (from == null) {
            from = objectClass();
        }

        if (isShowDeleted == null) {
            isShowDeleted = false;
        }
        if (!useDateDeleted) {
            isShowDeleted = true;
        }

        // conditions
        final List<String> qWhere = new LinkedList<>();
        final List<Object> qParam = new LinkedList<>();
        buildQueryWhere(where, qWhere, qParam);

        // query
        final String hql = "select " + (StringUtils.isNotBlank(select) ? select : (StringUtils.isNotBlank(joinRelations) ? "distinct o" : "o"))
                + " from " + from.getSimpleName() + " as o"
                + (StringUtils.isNotBlank(joinRelations) ? " " + joinRelations : "")
                + (!qWhere.isEmpty() || !isShowDeleted ? " where " : "")
                + (!isShowDeleted ? " (o.dateDeletedAt is null) " : "")
                + (!isShowDeleted && !qWhere.isEmpty() ? " and " : "")
                + (!qWhere.isEmpty() ? " (" + StringUtils.join(qWhere, ") and (") + ")" : "")
                + " order by " + (orderBy != null && orderBy.length > 0 ? StringUtils.join(orderBy, ", ") : ORDER_BY_ID);
        Query q = em.createQuery(hql);
        for (int i = 0; i < qParam.size(); i++) {
            q.setParameter(i, qParam.get(i));
        }
        if (size > 0 && offset >= 0) {
            q.setFirstResult(offset).setMaxResults(size);
        }

        return StringUtils.isNotBlank(select) ? q.getResultList() : SetUniqueList.decorate(q.getResultList());
    }

    @Override
    public Long count() {
        return count(null, null);
    }

    public Long count(List<WhereClause> where) {
        return count(null, where);
    }

    @Override
    public Long count(Class<? extends T> from, List<WhereClause> where) {
        return count(from, where, null);
    }

    public Long count(Class<? extends T> from, List<WhereClause> where, Boolean isShowDeleted) {
        return count(from, null, where, isShowDeleted);
    }

    public Long count(Class<? extends T> from, String joinRelations, List<WhereClause> where, Boolean isShowDeleted) {

        // from class
        if (from == null) {
            from = objectClass();
        }

        if (isShowDeleted == null) {
            isShowDeleted = false;
        }
        if (!useDateDeleted) {
            isShowDeleted = true;
        }

        // conditions
        List<String> qWhere = new LinkedList<>();
        List<Object> qParam = new LinkedList<>();
        buildQueryWhere(where, qWhere, qParam);

        // query
        Query q = em.createQuery("select count(distinct o) from " + from.getSimpleName() + " as o"
                + (StringUtils.isNotBlank(joinRelations) ? " " + joinRelations : "")
                + (!qWhere.isEmpty() || !isShowDeleted ? " where " : "")
                + (!isShowDeleted ? " (o.dateDeletedAt is null) " : "")
                + (!isShowDeleted && !qWhere.isEmpty() ? " and " : "")
                + (!qWhere.isEmpty() ? " (" + StringUtils.join(qWhere, ") and (") + ")" : ""));

        for (int i = 0; i < qParam.size(); i++) {
            q.setParameter(i, qParam.get(i));
        }

        return (Long) q.getSingleResult();
    }

    public Long countTotal(Class<? extends T> entityClass) {
        Metamodel metaModel = em.getMetamodel();
        EntityType entityType = metaModel.entity(entityClass);
        Table table = entityClass.getAnnotation(Table.class);
        String tableName = table != null ? table.name() : entityType.getName().toLowerCase();
        Query q = em.createNativeQuery("SELECT reltuples AS estimate FROM pg_class where relname=:tableName");
        q.setParameter("tableName", tableName);
        Number total = (Number) q.getSingleResult();
        return total.longValue();
    }

    @Transactional
    public int updateByNativeQuery(String tableName, String setColumn, Object setValue,
            String conditionColumn, Object conditionValue) {

        return em.createNativeQuery(
                "update " + tableName
                + " set " + setColumn + " = :setValue"
                + " where " + conditionColumn + " in :conditionValue")
                .setParameter("setValue", setValue)
                .setParameter("conditionValue", conditionValue)
                .executeUpdate();

    }

}
