/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ky6uHETc
 */
public class JoinClauses extends WhereClause<List<WhereClause>> {

    Logic logic;

    public JoinClauses(Logic logic, WhereClause... clauses) {
        super(null, null, new LinkedList<>());
        this.logic = logic;
        if (clauses != null) {
            for (WhereClause clause : clauses) {
                value.add(clause);
            }
        }
    }

    public Logic getLogic() {
        return logic;
    }

    public enum Logic {

        AND("and"), OR("or");
        private String operand;

        private Logic(String operand) {
            this.operand = operand;
        }

        public String getOperand() {
            return operand;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.logic);
        hash = 89 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JoinClauses other = (JoinClauses) obj;
        if (this.logic != other.logic) {
            return false;
        }
        if(!(this.value.containsAll(other.value) && other.value.containsAll(this.value))){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JoinClauses{" + "logic=" + logic + ", value=" + value.toString() + '}';
    }

}
