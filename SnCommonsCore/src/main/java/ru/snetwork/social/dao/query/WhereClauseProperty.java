/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

/**
 *
 * @author ky6uHETc
 */
public class WhereClauseProperty extends WhereClause<String> {

    public WhereClauseProperty(String property, String value) {
        super(property, Operator.eq, value);
    }

    public WhereClauseProperty(String property, Operator criteria, String value) {
        super(property, criteria, value);
    }

}
