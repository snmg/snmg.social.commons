package ru.snetwork.social.dao;

import org.springframework.transaction.annotation.Transactional;
import ru.snetwork.social.exception.DaoUnableToCreateObjectException;
import ru.snetwork.social.model.EntityModelBasic;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.model.EntityModelSystemUser;

public abstract class AbstractSystemLogDaoBean<T extends EntityModelBasic, U extends EntityModelSystemUser> extends AbstractDaoBean<T> {
    
    @Transactional
    public abstract <O extends EntityModelEntry> T doLog(String objectClassName, O object, U user) throws DaoUnableToCreateObjectException;
    
}
