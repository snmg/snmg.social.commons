/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

/**
 *
 * @author ky6uHETc
 */
public class WhereClauseClass extends WhereClause<Class> {

    public WhereClauseClass(Class value) {
        super(null, Operator.eq, value);
    }

    public WhereClauseClass(Operator criteria, Class value) {
        super(null, criteria, value);
    }

    public WhereClauseClass(String property, Operator criteria, Class value) {
        super(property, criteria, value);
    }

}
