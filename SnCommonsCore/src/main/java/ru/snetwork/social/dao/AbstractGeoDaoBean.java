/*
 * Базовый абстрактный класс DAO всех типов данных хранимых в БД.
 * Определяет основные свойства и методы по работе с сущностями модели данных БД,
 * наследующих класс EntityModelBasic.
 */
package ru.snetwork.social.dao;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import ru.snetwork.social.model.EntityModelGeo;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.exception.DaoUnableToCreateObjectException;
import ru.snetwork.social.exception.DaoUnableToDeleteObjectException;

/**
 *
 * @author evgeniy
 * @param <T>
 */
public abstract class AbstractGeoDaoBean<T extends EntityModelGeo> extends AbstractDaoBean<T> {

    @Transactional
    @Override
    public void doDelete(T o) throws DaoUnableToDeleteObjectException {
        em.remove(em.contains(o) ? o : em.merge(o));
    }

    @Override
    @Transactional
    public T doSave(T o) throws DaoUnableToCreateObjectException {
        return super.doSave(o);
    }

    @Override
    public Long count() {
        return count(null, null);
    }

    public Long count(List<WhereClause> where) {
        return count(null, null, where);
    }

    @Override
    public Long count(Class<? extends T> from, List<WhereClause> where) {
        return count(from, null, where);
    }

    public Long count(Class<? extends T> from, String joinRelations, List<WhereClause> where) {

        // from class
        if (from == null) {
            from = objectClass();
        }

        // conditions
        List<String> qWhere = new LinkedList<>();
        List<Object> qParam = new LinkedList<>();
        buildQueryWhere(where, qWhere, qParam);

        // query
        Query q = em.createQuery("select count(distinct o) from " + from.getSimpleName() + " as o"
                + (StringUtils.isNotBlank(joinRelations) ? " " + joinRelations : "")
                + (!qWhere.isEmpty() ? " where (" + StringUtils.join(qWhere, ") and (") + ")" : ""));

        for (int i = 0; i < qParam.size(); i++) {
            q.setParameter(i, qParam.get(i));
        }

        return (Long) q.getSingleResult();
    }
    
    public Long countTotal(Class<? extends T> entityClass) {
        Metamodel metaModel = em.getMetamodel();
        EntityType entityType = metaModel.entity(entityClass);
        Table table = entityClass.getAnnotation(Table.class);
        String tableName = table != null ? table.name() : entityType.getName().toLowerCase();
        Query q = em.createNativeQuery("SELECT reltuples AS estimate FROM pg_class where relname=:tableName");
        q.setParameter("tableName", tableName);
        Number total = (Number) q.getSingleResult();
        return total.longValue();
    }

    @Override
    public List<T> list(int offset, int size) {
        return list(null, offset, size);
    }

    public List<T> list(List<WhereClause> where, int offset, int size) {
        List<String> qWhere = new LinkedList<>();
        List<Object> qParam = new LinkedList<>();
        buildQueryWhere(where, qWhere, qParam);
        Query q = em.createQuery("from " + objectClass().getSimpleName() + " as o"
                + (!qWhere.isEmpty() ? " where (" + StringUtils.join(qWhere, ") and (") + ")" : "")
                + " order by o.nameRu, o.nameEn, o.id");

        for (int i = 0; i < qParam.size(); i++) {
            q.setParameter(i, qParam.get(i));
        }
        if (size > 0 && offset >= 0) {
            q.setFirstResult(offset).setMaxResults(size);
        }

        return q.getResultList();
    }
}
