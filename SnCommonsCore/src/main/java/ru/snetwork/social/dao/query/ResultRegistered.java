/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

import java.util.Date;

/**
 *
 * @author ky6uHETc
 */
public class ResultRegistered {

    final Long id;
    final Date timestamp;

    public ResultRegistered(Long id, Date timestamp) {
        this.id = id;
        this.timestamp = timestamp;
    }

    public Long getId() {
        return id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

}
