/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

/**
 *
 * @author ky6uHETc
 */
public class SubqueryClause extends WhereClause {

    final String subquery;
    final String[] subqueryParams;
    final Object[] subqueryValues;

    public SubqueryClause(String property, Operator criteria, String subquery, String[] subqueryParams, Object[] subqueryValues) {
        super(property, criteria, null);

        this.subquery = subquery;
        this.subqueryParams = subqueryParams;
        this.subqueryValues = subqueryValues;
    }

    public String getSubquery() {
        return subquery;
    }

    public String[] getParams() {
        return subqueryParams;
    }

    public Object[] getValues() {
        return subqueryValues;
    }

}
