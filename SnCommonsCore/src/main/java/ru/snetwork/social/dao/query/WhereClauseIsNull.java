/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

/**
 *
 * @author ky6uHETc
 */
public class WhereClauseIsNull extends WhereClause<String> {

    public WhereClauseIsNull(String property) {
        super(property, Operator.isNull, null);
    }

}
