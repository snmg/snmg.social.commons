/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao;

import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.transaction.annotation.Transactional;
import ru.snetwork.social.model.EntityModelRegistry;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.exception.DaoUnableToCreateObjectException;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.model.EntityOrderable;

/**
 *
 * @author ky6uHETc
 * @param <T> класс сущности модели данных БД (должен наследовать базовый класс
 * сущности EntityModelBasic)
 * @param <U> класс системного пользователя
 * @param <D> класс DAO системного журнала
 */
public abstract class AbstractRegistryDaoBean<T extends EntityModelRegistry, U extends EntityModelSystemUser, D extends AbstractSystemLogDaoBean> extends AbstractEntriesDaoBean<T, U, D> {

    public static final OrderBy ORDER_BY_NAME = new OrderBy("o.name", false);
    public static final OrderBy ORDER_BY_ORDERNO = new OrderBy("o.orderNo", false);

    @Transactional
    public void doDisable(T o, U u) {
        o = em.merge(o);
        o.setDateDisabledAt(new Date());

        if (systemLogDaoBean != null && !u.isSystem()) {
            try {
                systemLogDaoBean.doLog(o.getClass().getSimpleName(), o, u);
            } catch (DaoUnableToCreateObjectException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Transactional
    public void doEnable(T o, U u) {
        o = em.merge(o);
        o.setDateDisabledAt(null);

        if (systemLogDaoBean != null && !u.isSystem()) {
            try {
                systemLogDaoBean.doLog(o.getClass().getSimpleName(), o, u);
            } catch (DaoUnableToCreateObjectException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public List<T> list(String select, Class<? extends T> from, String joinRelations, List<WhereClause> where, Boolean isShowDeleted, int offset, int size, OrderBy... orderBy) {
        if (orderBy == null || orderBy.length == 0) {
            orderBy = EntityOrderable.class.isAssignableFrom(objectClass())
                    ? new OrderBy[]{ORDER_BY_ORDERNO}
                    : new OrderBy[]{ORDER_BY_NAME};
        }
        return super.list(select, from, joinRelations, where, isShowDeleted, offset, size, orderBy); //To change body of generated methods, choose Tools | Templates.
    }

    public T getObjectByUUID(String uuid) throws NoResultException {
        return (T) em.createNamedQuery(objectClass().getSimpleName() + ".getByUUID").setParameter("uuid", uuid).getSingleResult();
    }

}
