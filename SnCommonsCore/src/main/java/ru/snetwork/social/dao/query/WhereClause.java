/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao.query;

import java.util.Objects;

/**
 *
 * @author ky6uHETc
 */
public class WhereClause<T> {

    String property;
    Operator operator;
    T value;

    public WhereClause(String property, Operator criteria, T value) {
        this.property = property;
        this.operator = criteria;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public Operator getOperator() {
        return operator;
    }

    public T getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.property);
        hash = 59 * hash + Objects.hashCode(this.operator);
        hash = 59 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WhereClause<?> other = (WhereClause<?>) obj;
        if (!Objects.equals(this.property, other.property)) {
            return false;
        }
        if (this.operator != other.operator) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WhereClause{" + "property=" + property + ", operator=" + operator + ", value=" + value + '}';
    }

    public enum Operator {
        like(" like "),
        ilike(" like "), // ilike  uses lowercase in AbstractdaoBean
        notLike (" not like "),
        eq(" = "),
        ne(" != "),
        isNull(" is null "),
        notNull(" is not null "),
        isEmpty(" is empty "),
        notEmpty(" is not empty "),
        exists(" exists elements "),
        gt(" > "),
        lt(" < "),
        ge(" >= "),
        le(" <= "),
        in(" in "),
        notIn(" not in "),
        objectInElements(" in elements "),
        objectNotInElements(" not in elements ");

        String operator;

        private Operator(String operator) {
            this.operator = operator;
        }

        public String getOperator() {
            return operator;
        }

    }
}
