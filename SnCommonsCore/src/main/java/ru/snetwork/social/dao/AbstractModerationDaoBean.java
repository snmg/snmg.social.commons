/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.dao;

import java.util.Date;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import ru.snetwork.social.Moderation;
import ru.snetwork.social.model.EntityModelModeration;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.exception.DaoUnableToCreateObjectException;
import ru.snetwork.social.model.EntityModelSystemUser;

/**
 *
 * @author evgeniy
 * @param <T> extends EntityModelModeration
 */
public abstract class AbstractModerationDaoBean<T extends EntityModelModeration, U extends EntityModelSystemUser, D extends AbstractSystemLogDaoBean> extends AbstractEntriesDaoBean<T, U, D> {

    public static final OrderBy ORDER_BY_NAME = new OrderBy("o.name", false);

    @Override
    public List<T> list(List<WhereClause> where, int offset, int size, OrderBy... orderBy) {
        if (orderBy == null) {
            orderBy = new OrderBy[]{ORDER_BY_NAME};
        }
        return super.list(where, offset, size, orderBy);
    }

    @Transactional
    public void doModerate(T o, Moderation moderationStatus, U u) {
        switch (moderationStatus) {
            case APPROVED:
                o = em.merge(o);
                o.setModerationStatus(Moderation.APPROVED);
                o.setDateModeratedAt(new Date());
                break;
            case DECLINED:
                o = em.merge(o);
                o.setModerationStatus(Moderation.DECLINED);
                o.setDateModeratedAt(new Date());
                break;
            case PENDING:
                o = em.merge(o);
                o.setModerationStatus(Moderation.PENDING);
                o.setDateModeratedAt(new Date());
                break;
            case DRAFT:
                o = em.merge(o);
                o.setModerationStatus(Moderation.DRAFT);
                o.setDateModeratedAt(new Date());
                break;
        }

        if (systemLogDaoBean != null && !u.isSystem()) {
            try {
                systemLogDaoBean.doLog(o.getClass().getSimpleName(), o, u);
            } catch (DaoUnableToCreateObjectException ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

}
