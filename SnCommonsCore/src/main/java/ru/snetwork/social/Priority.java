package ru.snetwork.social;

public enum Priority {

    HIGH,
    NORMAL,
    LOW,
    EVERY_HOUR_1,
    EVERY_HOUR_6,
    EVERY_HOUR_12,
    DAILY,
    WEEKLY;

    public static Priority valueByName(String name) {
        return valueOf(name.toUpperCase());
    }
}
