package ru.snetwork.social;

public enum WebhookActivity {

    COMMENTS,
    INSIGHTS,
    MENTIONS,
    POSTS;

    public static WebhookActivity valueByName(String name) {
        return valueOf(name.toUpperCase());
    }
}
