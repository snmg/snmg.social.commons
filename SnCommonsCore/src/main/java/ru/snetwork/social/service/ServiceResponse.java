package ru.snetwork.social.service;

public class ServiceResponse {

    private Status status;
    private String message;

    public ServiceResponse() {
    }

    public ServiceResponse(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    public enum Status {
        OK,
        WARNING,
        ERROR
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
