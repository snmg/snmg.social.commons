/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.service;

import ru.snetwork.social.Product;

/**
 * @author ky6uHETc
 *
 * Topic constants naming scheme: {APP_GROUP}_{PUBLISHER}_{TOPIC_.._.._..}
 *
 * Topic naming hints:
 * <ul>
 * <li>*.Commands - immediate exec</li>
 * <li>*.Tasks - background processes</li>
 * <li>*.Status - service ping + state</li>
 * </ul>
 *
 */
public class JmsTopic {

    public static final String[] TRUSTED_PACKAGES = new String[]{
        "java.lang",
        "java.sql",
        "java.util",
        "ru.snetwork.social",
        "ru.snmg.messaging",
        "ru.snmg.monitoring"
    };

    public static final Long TIME_TO_LIVE_SHORT = 60000l; // 1 min
    public static final Long TIME_TO_LIVE_MEDIUM = 300000l; // 5 min
    public static final Long TIME_TO_LIVE_LONG = 900000l; // 15 min

    /**
     * Monitor module
     */
    // outer
    public static final String SOCIAL_MONITOR_STATUS = Product.SOCIAL_MONITOR + ".Status"; // SnSocial.Monitor.Status
    public static final String SOCIAL_MONITOR_TASKS_STREAMING = Product.SOCIAL_MONITOR + ".Tasks.Streaming"; // SnSocial.Monitor.Tasks.Streaming
    public static final String SOCIAL_MONITOR_TASKS_MESSAGING = Product.SOCIAL_MONITOR + ".Tasks.Messaging"; // SnSocial.Monitor.Tasks.Messaging
    public static final String SOCIAL_MONITOR_COMMANDS_MESSAGING = Product.SOCIAL_MONITOR + ".Commands.Messaging";  // SnSocial.Monitor.Commands.Messaging

    /**
     * Monitoring module
     */
    // outer
    public static final String SOCIAL_MONITORING_STATUS = Product.SOCIAL_MONITORING + ".Status"; // SnSocial.Monitoring.Status
    public static final String SOCIAL_MONITORING_TASKS_MESSAGING = Product.SOCIAL_MONITORING + ".Tasks.Messaging";  // SnSocial.Monitoring.Tasks.Messaging
    public static final String SOCIAL_MONITORING_COMMANDS_MESSAGING = Product.SOCIAL_MONITORING + ".Commands.Messaging";  // SnSocial.Monitoring.Commands.Messaging

    /**
     * Streaming module
     */
    // outer
    public static final String SOCIAL_STREAMING_STATUS = Product.SOCIAL_STREAMING + ".Status"; // SnSocial.Streaming.Status
    public static final String SOCIAL_STREAMING_MEDIAITEMS = Product.SOCIAL_STREAMING + ".MediaItems";  // SnSocial.Streaming.MediaItems
    public static final String SOCIAL_STREAMING_MEDIAITEMS_BONES = Product.SOCIAL_STREAMING + ".MediaItemsBones";  // SnSocial.Streaming.MediaItemsBones

    /**
     * Messaging module
     */
    // outer
    public static final String MESSAGING_CONTROLLER_STATUS = Product.MESSAGING_CONTROLLER + ".Status";
    public static final String MESSAGING_CONTROLLER_MEDIAITEMS = Product.MESSAGING_CONTROLLER + ".MediaItems";
    public static final String MESSAGING_CONTROLLER_COMMAND_RESULTS = Product.MESSAGING_CONTROLLER + ".CommandResults";  // SnMsg.Controller.CommandResults
    // inner
    public static final String MESSAGING_CONTROLLER_COMMANDS = Product.MESSAGING_CONTROLLER + ".Commands";
    public static final String MESSAGING_TELEGRAM_STATUS = Product.MESSAGING_TELEGRAM + ".Status";
    public static final String MESSAGING_TELEGRAM_MESSAGES = Product.MESSAGING_TELEGRAM + ".Messages";
    public static final String MESSAGING_VKONTAKTE_STATUS = Product.MESSAGING_VKONTAKTE + ".Status";
    public static final String MESSAGING_VKONTAKTE_MESSAGES = Product.MESSAGING_VKONTAKTE + ".Messages";
    public static final String MESSAGING_FACEBOOK_STATUS = Product.MESSAGING_FACEBOOK + ".Status";
    public static final String MESSAGING_FACEBOOK_MESSAGES = Product.MESSAGING_FACEBOOK + ".Messages";

}
