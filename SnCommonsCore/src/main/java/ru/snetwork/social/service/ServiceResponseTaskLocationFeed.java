/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.service;

import java.util.List;
import ru.snetwork.social.model.TaskLocationFeed;

/**
 *
 * @author evgeniy
 */
public class ServiceResponseTaskLocationFeed extends ServiceResponse {

    List<TaskLocationFeed> tasks;

    public List<TaskLocationFeed> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskLocationFeed> tasks) {
        this.tasks = tasks;
    }

}
