package ru.snetwork.social.service;

import ru.snetwork.social.model.StatisticsAccount;

public class ServiceResponseStatisticsAccount extends ServiceResponse {

    private StatisticsAccount statistics;

    public ServiceResponseStatisticsAccount() {
    }

    public ServiceResponseStatisticsAccount(Status status, String message) {
        super(status, message);
    }

    public ServiceResponseStatisticsAccount(Status status, StatisticsAccount statistics) {
        super(status, null);
        this.statistics = statistics;
    }

    public StatisticsAccount getStatistics() {
        return statistics;
    }

    public void setStatistics(StatisticsAccount statistics) {
        this.statistics = statistics;
    }

}
