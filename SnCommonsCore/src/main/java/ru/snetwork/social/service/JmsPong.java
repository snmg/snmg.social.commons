package ru.snetwork.social.service;

import ru.snetwork.social.Product;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class JmsPong implements Serializable {

    private static final long serialVersionUID = 1l;

    private final Long id;
    private final Product sender;
    private final Date created;
    private final JmsPing ping;

    public JmsPong(Long id, Product sender, JmsPing ping) {
        this.id = id;
        this.sender = sender;
        this.created = new Date();
        this.ping = ping;
    }

    public Long getId() {
        return id;
    }

    public Product getSender() {
        return sender;
    }

    public Date getCreated() {
        return created;
    }

    public JmsPing getPing() {
        return ping;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.sender);
        hash = 53 * hash + Objects.hashCode(this.created);
        hash = 53 * hash + Objects.hashCode(this.ping);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JmsPong other = (JmsPong) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.sender, other.sender)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.ping, other.ping)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{"
                + "id:" + this.id + "; "
                + "sender:" + this.sender.toString() + "; "
                + "created:" + this.created + "; "
                + "ping:" + this.ping.toString()
                + "}";
    }

}
