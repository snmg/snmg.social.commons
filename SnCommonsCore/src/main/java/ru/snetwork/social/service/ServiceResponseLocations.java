package ru.snetwork.social.service;

import java.util.List;
import ru.snetwork.social.model.Location;

public class ServiceResponseLocations extends ServiceResponse {

    List<Location> locations;

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
