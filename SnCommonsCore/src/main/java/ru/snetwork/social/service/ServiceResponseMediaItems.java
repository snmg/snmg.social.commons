/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.service;

import ru.snetwork.social.model.MediaItem;
import java.util.List;
import ru.snetwork.social.model.MediaAttachment;
import ru.snetwork.social.model.MediaAuthor;

/**
 *
 * @author evgeniy
 */
public class ServiceResponseMediaItems extends ServiceResponse {

    Integer count;
    Long minId;
    Long maxId;
    List<MediaItem<MediaAuthor, MediaAttachment>> mediaItems;
    Boolean hasUnreachableMedaiItems;

    public Long getMinId() {
        return minId;
    }

    public void setMinId(Long minId) {
        this.minId = minId;
    }

    public Long getMaxId() {
        return maxId;
    }

    public void setMaxId(Long maxId) {
        this.maxId = maxId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<MediaItem<MediaAuthor, MediaAttachment>> getMediaItems() {
        return mediaItems;
    }

    public void setMediaItems(List<MediaItem<MediaAuthor, MediaAttachment>> mediaItems) {
        this.mediaItems = mediaItems;
    }

    public Boolean getHasUnreachableMedaiItems() {
        return hasUnreachableMedaiItems;
    }

    public void setHasUnreachableMedaiItems(Boolean hasUnreachableMedaiItems) {
        this.hasUnreachableMedaiItems = hasUnreachableMedaiItems;
    }

}
