/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.service;

import ru.snetwork.social.Product;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author ky6uHETc
 */
public class JmsPing implements Serializable {

    private static final long serialVersionUID = 1l;

    private final Long id;
    private final Product sender;
    private final Date created;

    public Long getId() {
        return id;
    }

    public Product getSender() {
        return sender;
    }

    public Date getCreated() {
        return created;
    }

    public JmsPing(Long id, Product sender) {
        this.id = id;
        this.sender = sender;
        this.created = new Date();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.sender);
        hash = 29 * hash + Objects.hashCode(this.created);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JmsPing other = (JmsPing) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.sender, other.sender)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{"
                + "id:" + this.id + "; "
                + "sender:" + this.sender.toString() + "; "
                + "created:" + this.created
                + "}";
    }

}
