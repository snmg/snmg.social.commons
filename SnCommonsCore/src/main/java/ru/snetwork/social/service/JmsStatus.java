package ru.snetwork.social.service;

import java.io.Serializable;
import java.util.Date;

public class JmsStatus implements Serializable {

    private static final long serialVersionUID = 1l;

    private final Date date;
    private final Object content;

    public JmsStatus(Object content) {
        this.date = new Date();
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public Object getContent() {
        return content;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{"
                + "date:" + this.date + "; "
                + "content:" + this.content.toString()
                + "}";
    }

}
