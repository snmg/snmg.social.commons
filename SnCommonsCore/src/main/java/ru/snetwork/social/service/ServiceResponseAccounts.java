package ru.snetwork.social.service;

import java.util.List;
import ru.snetwork.social.model.Account;

public class ServiceResponseAccounts extends ServiceResponse {

    List<Account> accounts;

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

}
