package ru.snetwork.social.service;

import java.util.List;
import ru.snetwork.social.model.TaskChatListen;


public class ServiceResponseTaskChatListen extends ServiceResponse {

    List<TaskChatListen> tasks;

    public List<TaskChatListen> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskChatListen> tasks) {
        this.tasks = tasks;
    }

}
