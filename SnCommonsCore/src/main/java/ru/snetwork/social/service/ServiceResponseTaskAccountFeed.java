/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.service;

import java.util.List;
import ru.snetwork.social.model.TaskAccountFeed;

/**
 *
 * @author evgeniy
 */
public class ServiceResponseTaskAccountFeed extends ServiceResponse {

    List<TaskAccountFeed> tasks;

    public List<TaskAccountFeed> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskAccountFeed> tasks) {
        this.tasks = tasks;
    }

}
