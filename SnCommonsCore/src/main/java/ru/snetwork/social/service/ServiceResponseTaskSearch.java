/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.service;

import ru.snetwork.social.model.TaskSearch;

/**
 *
 * @author evgeniy
 */
public class ServiceResponseTaskSearch extends ServiceResponse {

    TaskSearch task;

    public TaskSearch getTask() {
        return task;
    }

    public void setTask(TaskSearch task) {
        this.task = task;
    }

}
