package ru.snetwork.social.service;

import java.util.List;
import ru.snetwork.social.model.Chat;

public class ServiceResponseChats extends ServiceResponse {

    List<Chat> chats;

    public List<Chat> getChats() {
        return chats;
    }

    public void setChats(List<Chat> chats) {
        this.chats = chats;
    }

}
