package ru.snetwork.social.service;

public class JmsConstants {

    public static final String PARAMS_ACCOUNT = "paramsAccount";
    public static final String PARAMS_ACCESS_TOKEN = "paramsAccessToken";
    public static final String PARAMS_CHAT_ID = "paramsChatId";
    public static final String PARAMS_CHAT_URL = "paramsChatUrl";
    public static final String PARAMS_MESSAGE_URL = "paramsMessageUrl";
    public static final String PARAMS_MESSAGES_IDS = "paramsMessagesIds";

    public static final String PARAMS_VK_ACCOUNT_ID = "paramsVkAccountId";
    public static final String PARAMS_VK_SEL_USER_ID = "paramsVkSelUserId";
    public static final String PARAMS_VK_MSG_ID = "paramsVkMsgId";
    public static final String PARAMS_VK_MESSAGE_TEXT = "paramsVkMessageText";
}
