package ru.snetwork.social.service;

import ru.snetwork.social.model.TaskAccountStatistics;

public class ServiceResponseTaskAccountStatistics extends ServiceResponse {

    private TaskAccountStatistics task;

    public ServiceResponseTaskAccountStatistics() {
    }

    public ServiceResponseTaskAccountStatistics(Status status, String message) {
        super(status, message);
    }

    public ServiceResponseTaskAccountStatistics(Status status, TaskAccountStatistics task) {
        super(status, null);
        this.task = task;
    }

    public TaskAccountStatistics getTask() {
        return task;
    }

    public void setTask(TaskAccountStatistics task) {
        this.task = task;
    }

}
