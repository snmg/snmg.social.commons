/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.exception;

import ru.snetwork.social.model.EntityModelBasic;

/**
 *
 * @author ky6uHETc
 */
public class DaoUnableToCreateObjectException extends DaoException {


    public DaoUnableToCreateObjectException(EntityModelBasic entityObject, String cause) {
        super("Entity object typeof " + entityObject.getClass().getName() + " is not illegal to create. Cause: " + cause);
    }

}
