/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.exception;

import ru.snetwork.social.exception.SystemException;

/**
 *
 * @author ky6uHETc
 */
public class ControllerException extends SystemException {

    public ControllerException(String message) {
        super(message);
    }

    public ControllerException(String message, Throwable cause) {
        super(message, cause);
    }

}
