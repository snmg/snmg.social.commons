/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.exception;

import org.springframework.security.authentication.BadCredentialsException;

/**
 *
 * @author ky6uHETc
 */
public class AuthException extends SecurityException {

    public AuthException(BadCredentialsException cause) {
        super("Invalid username or password", cause);
    }

    public AuthException(String message) {
        super(message, null);
    }
}
