/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.exception;

/**
 *
 * @author ky6uhetc
 */
public class DaoUnsafeReadonlySql extends DaoException {

    public DaoUnsafeReadonlySql(String sql) {
        super("Unsafe readonly SQL query that might change data " + sql);
    }

}
