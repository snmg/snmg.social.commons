/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.snetwork.social.exception;


/**
 *
 * @author ky6uHETc
 */
public class DaoException extends SystemException {

    public DaoException(Throwable cause) {
        super(cause);
    }

    public DaoException(String message) {
        super(message);
    }

}
