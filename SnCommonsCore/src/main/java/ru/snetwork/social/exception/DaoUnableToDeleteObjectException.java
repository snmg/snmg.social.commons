/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.exception;

import ru.snetwork.social.model.EntityModelBasic;

/**
 *
 * @author ky6uHETc
 */
public class DaoUnableToDeleteObjectException extends DaoException {

    public DaoUnableToDeleteObjectException(EntityModelBasic entityObject, String cause) {
        super("Entity object typeof " + entityObject.getClass().getName() + " cannot be deleted. Cause: " + cause);
    }
}
