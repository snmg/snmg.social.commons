package ru.snetwork.social.model;

import java.util.Date;
import java.util.List;

public class TaskLocationFeed extends Task {

    List<Location> locations;
    Date dateSearchContentNewer;

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Date getDateSearchContentNewer() {
        return dateSearchContentNewer;
    }

    public void setDateSearchContentNewer(Date dateSearchContentNewer) {
        this.dateSearchContentNewer = dateSearchContentNewer;
    }

}
