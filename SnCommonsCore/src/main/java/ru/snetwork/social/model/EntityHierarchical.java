/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

/**
 *
 * @author ky6uHETc
 * @param <E> self class should be passed
 */
public interface EntityHierarchical<E extends EntityModelBasic> {

    public E getParent();

    public void setParent(E parent);
}
