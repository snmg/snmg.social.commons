/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.io.Serializable;

/**
 *
 * @author evgeniy
 */
public class MediaAttachment implements Serializable {

    private static final long serialVersionUID = 1l;

    protected final AccessData accessData;
    protected MediaContentType.AttachmentType contentType;
    protected String contentSubType;
    protected String attachmentId;
    protected String url;
    protected String resourceUrl;
    protected String resourceSecureUrl;
    protected String textData;

    public MediaAttachment() {
        this.accessData = null;
    }

    public MediaAttachment(AccessData accessData) {
        this.accessData = accessData;
    }

    public AccessData getAccessData() {
        return accessData;
    }

    public MediaContentType.AttachmentType getContentType() {
        return contentType;
    }

    public void setContentType(MediaContentType.AttachmentType contentType) {
        this.contentType = contentType;
    }

    public String getContentSubType() {
        return contentSubType;
    }

    public void setContentSubType(String contentSubType) {
        this.contentSubType = contentSubType;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getResourceSecureUrl() {
        return resourceSecureUrl;
    }

    public void setResourceSecureUrl(String resourceSecureUrl) {
        this.resourceSecureUrl = resourceSecureUrl;
    }

    public String getTextData() {
        return textData;
    }

    public void setTextData(String textData) {
        this.textData = textData;
    }

    public static class AccessData {

        private final String id;
        private final String accessHash;
        private final String accessUserId;
        private final String datacenterId;
        private final String version;
        private final int size = 0;

        public AccessData(String id, String accessHash, String accessUserId, String datacenterId, String version) {
            this.id = id;
            this.accessHash = accessHash;
            this.accessUserId = accessUserId;
            this.datacenterId = datacenterId;
            this.version = version;
        }

        public String getId() {
            return id;
        }

        public String getAccessHash() {
            return accessHash;
        }

        public String getAccessUserId() {
            return accessUserId;
        }

        public String getDatacenterId() {
            return datacenterId;
        }

        public String getVersion() {
            return version;
        }

        public int getSize() {
            return size;
        }

    }

}
