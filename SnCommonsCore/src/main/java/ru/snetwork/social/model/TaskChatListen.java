package ru.snetwork.social.model;

import java.util.Date;

public class TaskChatListen extends Task {

    private static final long serialVersionUID = 1l;

    private Chat chat;
    private Account accessAccount;
    private String listenFromMsgId;
    private Date listenFromMsgDate;

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public Account getAccessAccount() {
        return accessAccount;
    }

    public void setAccessAccount(Account accessAccount) {
        this.accessAccount = accessAccount;
    }

    public String getListenFromMsgId() {
        return listenFromMsgId;
    }

    public void setListenFromMsgId(String listenFromMsgId) {
        this.listenFromMsgId = listenFromMsgId;
    }

    public Date getListenFromMsgDate() {
        return listenFromMsgDate;
    }

    public void setListenFromMsgDate(Date listenFromMsgDate) {
        this.listenFromMsgDate = listenFromMsgDate;
    }

}
