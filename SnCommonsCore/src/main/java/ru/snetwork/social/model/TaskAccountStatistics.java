package ru.snetwork.social.model;

import java.util.Date;
import ru.snetwork.social.Priority;

public class TaskAccountStatistics extends Task {

    private Account account;

    public TaskAccountStatistics() {
    }

    /**
     * Constructor for object used as input param
     */
    public TaskAccountStatistics(TaskType taskType, Date dateActiveFrom, Date dateActiveTill, String ownerTaskKey, Priority priority, Account account) {
        super(taskType, dateActiveFrom, dateActiveTill, ownerTaskKey, priority);
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

}
