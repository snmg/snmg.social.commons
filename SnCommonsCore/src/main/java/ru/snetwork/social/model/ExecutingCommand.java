package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class ExecutingCommand implements Serializable {

    final String guid =  UUID.randomUUID().toString();
    Date sended;
    String senderId;
    Command command;
    CommandResult commandResult;
    Date received;

    public ExecutingCommand() {
    }

    public ExecutingCommand(Command command) {
        this.command = command;
    }

    public ExecutingCommand( String senderId, Command command) {
        this.senderId = senderId;
        this.command = command;
    }

    public String getGuid() {
        return guid;
    }

    public Date getSended() {
        return sended;
    }

    public void setSended(Date sended) {
        this.sended = sended;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public CommandResult getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(CommandResult commandResult) {
        this.commandResult = commandResult;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    @Override
    public String toString() {
        return "ExecutingCommand{" +
                "guid='" + guid + '\'' +
                ", sended=" + sended +
                ", senderId='" + senderId + '\'' +
                ", command=" + command +
                ", commandResult=" + commandResult +
                ", received=" + received +
                '}';
    }
}
