package ru.snetwork.social.model;

import java.io.Serializable;
import ru.snetwork.social.SocialNetwork;

public class Location implements Serializable{
    
    private static final long serialVersionUID = 1l;

    SocialNetwork socialNetwork;
    String locationId;
    String locationPlatformName;
    String locationAddress;
    Double lat;
    Double lng;

    public SocialNetwork getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(SocialNetwork socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationPlatformName() {
        return locationPlatformName;
    }

    public void setLocationPlatformName(String locationPlatformName) {
        this.locationPlatformName = locationPlatformName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

}
