package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ConnectedInstagramBusinessAccount implements Serializable {

    private static final long serialVersionUID = 2l;

    private List<Long> accessTokensIds;
    private String businessAccountId;
    private Account facebookPageAccount;
    private Map<Long, Account> accessTokenUsers;

    public ConnectedInstagramBusinessAccount() {
    }

    public List<Long> getAccessTokensIds() {
        return accessTokensIds;
    }

    public void setAccessTokensIds(List<Long> accessTokensIds) {
        this.accessTokensIds = accessTokensIds;
    }

    public String getBusinessAccountId() {
        return businessAccountId;
    }

    public void setBusinessAccountId(String businessAccountId) {
        this.businessAccountId = businessAccountId;
    }

    public Account getFacebookPageAccount() {
        return facebookPageAccount;
    }

    public void setFacebookPageAccount(Account facebookPageAccount) {
        this.facebookPageAccount = facebookPageAccount;
    }

    public Map<Long, Account> getAccessTokenUsers() {
        return accessTokenUsers;
    }

    public void setAccessTokenUsers(Map<Long, Account> accessTokenUsers) {
        this.accessTokenUsers = accessTokenUsers;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.accessTokensIds);
        hash = 29 * hash + Objects.hashCode(this.businessAccountId);
        hash = 29 * hash + Objects.hashCode(this.facebookPageAccount);
        hash = 29 * hash + Objects.hashCode(this.accessTokenUsers);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConnectedInstagramBusinessAccount other = (ConnectedInstagramBusinessAccount) obj;
        if (!Objects.equals(this.businessAccountId, other.businessAccountId)) {
            return false;
        }
        if (!Objects.equals(this.accessTokensIds, other.accessTokensIds)) {
            return false;
        }
        if (!Objects.equals(this.facebookPageAccount, other.facebookPageAccount)) {
            return false;
        }
        if (!Objects.equals(this.accessTokenUsers, other.accessTokenUsers)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ConnectedInstagramBusinessAccount{" + "accessTokensIds=" + accessTokensIds + ", businessAccountId=" + businessAccountId + ", facebookPageAccount=" + facebookPageAccount + ", accessTokenUsers=" + accessTokenUsers + '}';
    }

}
