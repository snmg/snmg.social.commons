package ru.snetwork.social.model;

import java.io.Serializable;

public class CommandResult implements Serializable {

    private Object data;

    public CommandResult() {
    }

    public CommandResult(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
