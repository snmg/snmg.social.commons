/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.util.Comparator;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author ky6uHETc
 */
@MappedSuperclass
public abstract class EntityModelRegistry extends EntityModelEntry {

    public static Comparator<EntityModelRegistry> COMPARATOR_BY_NAME = new Comparator<EntityModelRegistry>() {
        @Override
        public int compare(EntityModelRegistry o1, EntityModelRegistry o2) {
            if (o1.getName() != null && o2.getName() != null) {
                return o1.getName().compareTo(o2.getName());
            } else {
                return (o1.getName() != null) ? 1 : (o2.getName() != null) ? -1 : 0;
            }
        }
    };

    @Column(unique = true, length = 36)
    private String uuid;

    public boolean isDisabled() {
        return getDateDisabledAt() != null;
    }

    public abstract boolean isSystem();

    public abstract void setName(String value);

    public abstract String getName();

    public abstract void setDescription(String value);

    public abstract String getDescription();

    public abstract Date getDateDisabledAt();

    public abstract void setDateDisabledAt(Date date);

    public String getUuid() {
        if (uuid == null) {
            if (getId() != null) {
                uuid = UUID.nameUUIDFromBytes(toString().concat(RandomStringUtils.random(16)).getBytes()).toString();
            } else {
                uuid = UUID.nameUUIDFromBytes((getClass().getName() + new Date().getTime()).concat(RandomStringUtils.random(16)).getBytes()).toString();
            }
        }
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
