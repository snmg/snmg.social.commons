/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import ru.snetwork.social.SocialNetwork;

/**
 *
 * @author evgeniy
 * @param <AUTHOR>
 * @param <ATTACHMENT>
 */
public class MediaItem<AUTHOR extends MediaAuthor, ATTACHMENT extends MediaAttachment> implements Serializable {

    private static final long serialVersionUID = 1l;

    protected final SocialNetwork socialNetwork;
    protected final String id;
    protected final String url;
    protected final String scopeGroupId;
    protected final Date dateCreated;
    protected final Date dateModified;
    protected final MediaContentType.EntryType type;
    protected final String text;
    protected final AUTHOR author;
    protected final List<String> hashtags;
    protected final List<ATTACHMENT> attachments;
    protected final Boolean repost;
    protected final String repostEntryId;
    protected final String replyToEntryId;
    protected final Integer countComments;
    protected final Integer countLikes;
    protected final Integer countShares;
    protected final Long countViews;
    protected final Long countTotalReactions;

    public MediaItem(
            SocialNetwork socialNetwork,
            String id,
            String url,
            String scopeGroupId,
            Date dateCreated,
            Date dateModified,
            MediaContentType.EntryType type,
            String text,
            AUTHOR author,
            Collection<String> hashTags,
            Collection<? extends ATTACHMENT> attachments,
            Boolean repost, String repostEntryId, String replyToEntryId,
            Integer countComments, Integer countLikes, Integer countShares, Long countViews) {
        this.socialNetwork = socialNetwork;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        this.type = type;
        this.id = id;
        this.url = url;
        this.scopeGroupId = scopeGroupId;
        this.text = text;
        this.author = author;
        this.hashtags = hashTags != null ? new LinkedList<>(hashTags) : new LinkedList<>();
        this.attachments = attachments != null ? new LinkedList<>(attachments) : new LinkedList<>();
        this.repost = repost;
        this.repostEntryId = repostEntryId;
        this.replyToEntryId = replyToEntryId;
        this.countComments = countComments;
        this.countLikes = countLikes;
        this.countShares = countShares;
        this.countViews = countViews;
        this.countTotalReactions = Long.valueOf(
                (countComments != null ? countComments : 0)
                + (countLikes != null ? countLikes : 0)
                + (countShares != null ? countShares : 0));

    }

    public MediaItem(
            SocialNetwork socialNetwork,
            String id,
            String url,
            String scopeGroupId,
            Date dateCreated,
            Date dateModified,
            MediaContentType.EntryType type,
            String text,
            AUTHOR author,
            Collection<String> hashTags,
            Collection<ATTACHMENT> attachments,
            Boolean repost, String repostEntryId, String parentEntryId,
            Long countReactions, Long countViews) {
        this.socialNetwork = socialNetwork;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        this.type = type;
        this.id = id;
        this.url = url;
        this.scopeGroupId = scopeGroupId;
        this.text = text;
        this.author = author;
        this.hashtags = hashTags != null ? new LinkedList<>(hashTags) : new LinkedList<>();
        this.attachments = attachments != null ? new LinkedList<>(attachments) : new LinkedList<>();
        this.repost = repost;
        this.repostEntryId = repostEntryId;
        this.replyToEntryId = parentEntryId;
        this.countComments = null;
        this.countLikes = null;
        this.countShares = null;
        this.countViews = countViews;
        this.countTotalReactions = countReactions;

    }

    public final SocialNetwork getSocialNetwork() {
        return socialNetwork;
    }

    public final Date getDateCreated() {
        return dateCreated;
    }

    public final Date getDateModified() {
        return dateModified;
    }

    public final MediaContentType.EntryType getType() {
        return type;
    }

    public final String getId() {
        return id;
    }

    public final String getUrl() {
        return url;
    }

    public String getScopeGroupId() {
        return scopeGroupId;
    }

    public final String getText() {
        return text;
    }

    public final AUTHOR getAuthor() {
        return author;
    }

    public final List<String> getHashtags() {
        return hashtags;
    }

    public final List<ATTACHMENT> getAttachments() {
        return attachments;
    }

    public final boolean isRepost() {
        return Boolean.TRUE.equals(repost);
    }

    public final String getRepostEntryId() {
        return repostEntryId;
    }

    public final String getReplyToEntryId() {
        return replyToEntryId;
    }

    public Integer getCountComments() {
        return countComments;
    }

    public Integer getCountLikes() {
        return countLikes;
    }

    public Integer getCountShares() {
        return countShares;
    }

    public Long getCountViews() {
        return countViews;
    }

    public Long getCountTotalReactions() {
        return countTotalReactions;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.socialNetwork);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaItem other = (MediaItem) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.socialNetwork != other.socialNetwork) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{"
                + "socialNetwork=" + socialNetwork
                + ", id=" + id
                + ", url=" + url
                + ", scopeGroupId=" + scopeGroupId
                + ", dateCreated=" + dateCreated
                + ", dateModified=" + dateModified
                + ", type=" + type
                + ", text=" + text
                + ", author=" + author
                + ", hashtags=" + hashtags
                + ", attachments=" + attachments
                + ", repost=" + repost
                + ", repostEntryId=" + repostEntryId
                + ", replyToEntryId=" + replyToEntryId
                + ", countComments=" + countComments
                + ", countLikes=" + countLikes
                + ", countShares=" + countShares
                + ", countViews=" + countViews
                + ", countTotalReactions=" + countTotalReactions
                + '}';
    }

}
