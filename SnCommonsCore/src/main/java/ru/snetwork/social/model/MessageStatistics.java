package ru.snetwork.social.model;

import java.io.Serializable;

public class MessageStatistics implements Serializable {

    private static final long serialVersionUID = 1l;

    private final String messageId;
    private final Integer views;
    private final Integer likes;
    private final Integer replies;
    private final Integer shares;

    public MessageStatistics(String messageId, Integer views, Integer likes, Integer replies, Integer shares) {
        this.messageId = messageId;
        this.views = views;
        this.likes = likes;
        this.replies = replies;
        this.shares = shares;
    }

    public String getMessageId() {
        return messageId;
    }

    public Integer getViews() {
        return views;
    }

    public Integer getLikes() {
        return likes;
    }

    public Integer getReplies() {
        return replies;
    }

    public Integer getShares() {
        return shares;
    }

    @Override
    public String toString() {
        return "MessageStatistics{" + "messageId=" + messageId + ", views=" + views + ", likes=" + likes + ", replies=" + replies + ", shares=" + shares + '}';
    }

}
