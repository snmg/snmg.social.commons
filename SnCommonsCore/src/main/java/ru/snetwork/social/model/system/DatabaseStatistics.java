/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model.system;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ky6uHETc
 */
public class DatabaseStatistics {

    final Map<String, Map<String, TableStatistic>> tables = new LinkedHashMap<>();
    final List<String> tableNames = new LinkedList<String>();
    long totalRows = 0l;
    long totalBytes = 0l;

    public void addTableStatistics(TableStatistic table) {
        if (!tables.containsKey(table.getScheme())) {
            tables.put(table.getScheme(), new LinkedHashMap<>());
        }
        tables.get(table.getScheme()).put(table.getName(), table);
        if ("public".equalsIgnoreCase(table.getScheme()) || StringUtils.isBlank(table.getScheme())) {
            tableNames.add(table.getName());
        }
        totalRows += table.getRowEstimate();
        totalBytes += table.getTotalBytes();
    }

    public long getTotalRows() {
        return totalRows;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public TableStatistic getTable(String tableName) {
        for (String scheme : tables.keySet()) {
            if (tables.get(scheme).containsKey(tableName)) {
                return tables.get(scheme).get(tableName);
            }
        }
        return null;
    }

    public List<String> getTableNames() {
        return Collections.unmodifiableList(tableNames);
    }

    public static class TableStatistic {

        long oid;
        String tableScheme;
        String tableName;
        long rowEstimate;
        long totalBytes;
        long indexBytes;
        long toastBytes;
        long tableBytes;

        public TableStatistic(long oid, String tableScheme, String tableName, long rowEstimate, long totalBytes, long indexBytes, long toastBytes, long tableBytes) {
            this.oid = oid;
            this.tableScheme = tableScheme;
            this.tableName = tableName;
            this.rowEstimate = rowEstimate;
            this.totalBytes = totalBytes;
            this.indexBytes = indexBytes;
            this.toastBytes = toastBytes;
            this.tableBytes = tableBytes;
        }

        public long getOid() {
            return oid;
        }

        public String getScheme() {
            return tableScheme;
        }

        public String getName() {
            return tableName;
        }

        public long getRowEstimate() {
            return rowEstimate;
        }

        public long getTotalBytes() {
            return totalBytes;
        }

        public long getIndexBytes() {
            return indexBytes;
        }

        public long getToastBytes() {
            return toastBytes;
        }

        public long getTableBytes() {
            return tableBytes;
        }

    }

}
