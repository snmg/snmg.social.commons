package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Date;
import ru.snetwork.social.Priority;
import ru.snetwork.social.Product;

public class TaskState implements Serializable {

    private static final long serialVersionUID = 1l;

    private Product owner;
    private Long ownerTaskId;
    private Date dateActiveFrom;
    private Date dateActiveTill;
    private Priority priority;

    public Product getOwner() {
        return owner;
    }

    public void setOwner(Product owner) {
        this.owner = owner;
    }

    public Long getOwnerTaskId() {
        return ownerTaskId;
    }

    public void setOwnerTaskId(Long ownerTaskId) {
        this.ownerTaskId = ownerTaskId;
    }

    public Date getDateActiveFrom() {
        return dateActiveFrom;
    }

    public void setDateActiveFrom(Date dateActiveFrom) {
        this.dateActiveFrom = dateActiveFrom;
    }

    public Date getDateActiveTill() {
        return dateActiveTill;
    }

    public void setDateActiveTill(Date dateActiveTill) {
        this.dateActiveTill = dateActiveTill;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

}
