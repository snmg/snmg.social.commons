package ru.snetwork.social.model;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import ru.snetwork.social.SocialNetwork;

public class Account implements Serializable {

    private static final long serialVersionUID = 2l;

    String accountId;
    String accountName;
    String accountPlatformName;
    String accountUrl;
    String accountPhoto;
    AccountStatus status;

    public Account() {
    }

    public Account(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public Account(String accountId, String accountPlatformName, String accountUrl, AccountStatus status) {
        this.accountId = accountId;
        this.accountPlatformName = accountPlatformName;
        this.accountUrl = accountUrl;
        this.status = status;
    }

    public Account(String accountId, String accountPlatformName, String accountUrl, String accountPhoto, AccountStatus status) {
        this.accountId = accountId;
        this.accountPlatformName = accountPlatformName;
        this.accountUrl = accountUrl;
        this.accountPhoto = accountPhoto;
        this.status = status;
    }

    public Account(String accountId, String accountName, String accountPlatformName, String accountUrl, String accountPhoto, AccountStatus status) {
        this.accountId = accountId;
        this.accountName = accountName;
        this.accountPlatformName = accountPlatformName;
        this.accountUrl = accountUrl;
        this.accountPhoto = accountPhoto;
        this.status = status;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountPlatformName() {
        return accountPlatformName;
    }

    public void setAccountPlatformName(String accountPlatformName) {
        this.accountPlatformName = accountPlatformName;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {
        this.accountUrl = accountUrl;
    }

    public String getAccountPhoto() {
        return accountPhoto;
    }

    public SocialNetwork getSocialNetwork() throws MalformedURLException {
        URL url = new URL(accountUrl);
        return SocialNetwork.valueByDomain(url.getHost());
    }

    public AccountStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Account{" + "accountId=" + accountId + ", accountPlatformName=" + accountPlatformName + ", accountUrl=" + accountUrl + '}';
    }

}
