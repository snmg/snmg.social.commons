package ru.snetwork.social.model;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import ru.snetwork.social.SocialNetwork;

public class Chat implements Serializable {

    private static final long serialVersionUID = 1l;

    String chatId;
    String chatCode;
    String chatName;
    String chatUrl;
    Integer chatParticipantsCount;

    public Chat() {
    }

    public Chat(String chatId, String chatCode, String chatName, String chatUrl, Integer chatParticipantsCount) {
        this.chatId = chatId;
        this.chatCode = chatCode;
        this.chatName = chatName;
        this.chatUrl = chatUrl;
        this.chatParticipantsCount = chatParticipantsCount;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatCode() {
        return chatCode;
    }

    public void setChatCode(String chatCode) {
        this.chatCode = chatCode;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public String getChatUrl() {
        return chatUrl;
    }

    public void setChatUrl(String chatUrl) {
        this.chatUrl = chatUrl;
    }

    public Integer getChatParticipantsCount() {
        return chatParticipantsCount;
    }

    public void setChatParticipantsCount(Integer chatParticipantsCount) {
        this.chatParticipantsCount = chatParticipantsCount;
    }

    public SocialNetwork getSocialNetwork() throws MalformedURLException {
        URL url = new URL(chatUrl);
        return SocialNetwork.valueByDomain(url.getHost());
    }

    @Override
    public String toString() {
        return "Chat{" + "chatId=" + chatId + ", chatCode=" + chatCode + ", chatName=" + chatName + ", chatUrl=" + chatUrl + ", chatParticipantsCount=" + chatParticipantsCount + '}';
    }

}
