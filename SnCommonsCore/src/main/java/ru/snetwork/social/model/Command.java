package ru.snetwork.social.model;

import ru.snetwork.social.SocialNetwork;

import java.io.Serializable;
import java.util.Map;

public class Command implements Serializable {

    String id;
    Long internalId;
    CommandType commandType;
    SocialNetwork socialNetwork;
    Map<String, Object> parameters;

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getInternalId() {
        return internalId;
    }

    public void setInternalId(Long internalId) {
        this.internalId = internalId;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public SocialNetwork getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(SocialNetwork socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public enum CommandType {
        FETCH_MESSAGE,
        SEND_MESSAGE,
        FETCH_MESSAGES_STATISTICS,
        FETCH_CHAT,
        FETCH_CHAT_STATISTICS
    }

}
