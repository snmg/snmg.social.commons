package ru.snetwork.social.model;

import java.util.Date;
import java.util.List;

public class TaskAccountFeed extends Task {

    List<Account> accounts;
    Date dateSearchContentNewer;

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Date getDateSearchContentNewer() {
        return dateSearchContentNewer;
    }

    public void setDateSearchContentNewer(Date dateSearchContentNewer) {
        this.dateSearchContentNewer = dateSearchContentNewer;
    }

}
