package ru.snetwork.social.model;

public interface EntityOrderable {
    Integer getOrderNo();
    void setOrderNo(Integer orderNo);
}
