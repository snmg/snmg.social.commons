/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Objects;
import ru.snetwork.social.SocialNetwork;

/**
 *
 * @author evgeniy
 */
public class MediaAuthor implements Serializable{
    
    private static final long serialVersionUID = 1l;

    protected final SocialNetwork socialNetwork;
    protected final String id;
    protected final String fullName;
    protected final String firstName;
    protected final String lastName;
    protected final String profileName;
    protected final String profilePictureUrl;
    protected final MediaAuthorType type;
    protected final Integer followers;

    public MediaAuthor(
            SocialNetwork socialNetwork,
            String profileId,
            String fullName,
            String firstName,
            String lastName,
            String nickName,
            String profilePictureUrl,
            MediaAuthorType type) {
        this.socialNetwork = socialNetwork;
        this.id = profileId;
        this.fullName = fullName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profileName = nickName;
        this.profilePictureUrl = profilePictureUrl;
        this.type = type;
        this.followers = null;
    }
    
    public MediaAuthor(
            SocialNetwork socialNetwork,
            String profileId,
            String fullName,
            String firstName,
            String lastName,
            String nickName,
            String profilePictureUrl,
            MediaAuthorType type,
            Integer followers) {
        this.socialNetwork = socialNetwork;
        this.id = profileId;
        this.fullName = fullName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profileName = nickName;
        this.profilePictureUrl = profilePictureUrl;
        this.type = type;
        this.followers = followers;
    }

    public final SocialNetwork getSocialNetwork() {
        return socialNetwork;
    }

    public final String getId() {
        return id;
    }

    public final String getFullName() {
        return fullName;
    }

    public final String getFirstName() {
        return firstName;
    }

    public final String getLastName() {
        return lastName;
    }

    public final String getProfileName() {
        return profileName;
    }

    public final String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public MediaAuthorType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.socialNetwork);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaAuthor other = (MediaAuthor) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.socialNetwork != other.socialNetwork) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{"
                + "socialNetwork=" + socialNetwork
                + ", id=" + id
                + ", fullName=" + fullName
                + ", firstName=" + firstName
                + ", lastName=" + lastName
                + ", profileName=" + profileName
                + ", profilePictureUrl=" + profilePictureUrl + '}';
    }

    public static enum Gender {
        UNKNOWN,
        MALE,
        FEMALE,
        OTHER
    }
    
    public static enum MediaAuthorType {
        PERSON,
        GROUP,
        PAGE
    }

    public Integer getFollowers() {
        return followers;
    }
}
