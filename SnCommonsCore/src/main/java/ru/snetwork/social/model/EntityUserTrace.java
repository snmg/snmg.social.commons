/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

/**
 *
 * @author ky6uHETc
 * @param <U>
 */
public interface EntityUserTrace<U extends EntityModelSystemUser> {

    public abstract U getUserCreated();

    public abstract void setUserCreated(U user);

    public abstract U getUserModified();

    public abstract void setUserModified(U user);

    public abstract U getUserDeleted();

    public abstract void setUserDeleted(U user);
}
