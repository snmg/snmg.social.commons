/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.Random;
import javax.persistence.Transient;

/**
 *
 * @author ky6uHETc
 */
public abstract class EntityModelBasic implements Serializable {

    public static Random RANDOM = new Random();
    public static Comparator<EntityModelBasic> COMPARATOR_BY_ID = new Comparator<EntityModelBasic>() {
        @Override
        public int compare(EntityModelBasic o1, EntityModelBasic o2) {
            if (o1.getId() != null && o2.getId() != null) {
                return o1.getId().compareTo(o2.getId());
            } else {
                return (o1.getId() != null) ? 1 : (o2.getId() != null) ? -1 : 0;
            }
        }
    };

    @Transient
    private final transient String hashcodeId = new Date().getTime() + "_" + RANDOM.nextLong();

    public abstract Long getId();

    public abstract void setId(Long id);

    @Override
    public final int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : Objects.hashCode(this.hashcodeId));
        return hash;
    }

    @Override
    public final boolean equals(Object object) {

        if (!(object instanceof EntityModelBasic)) {
            return false;
        }
        if (!this.getClass().isInstance(object)) {
            return false;
        }
        EntityModelBasic other = (EntityModelBasic) object;
        if (this.getId() != null) {
            return Objects.equals(this.getId(), other.getId());
        } else {
            return Objects.equals(this.hashcodeId, other.hashcodeId);
        }
    }

    @Override
    public String toString() {
        return getClass().getName() + "[ id=" + getId() + " ]";
}
}
