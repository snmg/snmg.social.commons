package ru.snetwork.social.model;

import java.io.Serializable;

public class StatisticsAccount implements Serializable {

    private static final long serialVersionUID = 1l;

    private Account account;
    private Integer friends;
    private Integer followers;
    private Integer following;
    private Integer subscribers;
    private Integer likes;
    private Integer shares;
    private Integer publishedPosts;
    private Integer publishedReposts;
    private Integer publishedReplies;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getFriends() {
        return friends;
    }

    public void setFriends(Integer friends) {
        this.friends = friends;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public Integer getFollowing() {
        return following;
    }

    public void setFollowing(Integer following) {
        this.following = following;
    }

    public Integer getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Integer subscribers) {
        this.subscribers = subscribers;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getShares() {
        return shares;
    }

    public void setShares(Integer shares) {
        this.shares = shares;
    }

    public Integer getPublishedPosts() {
        return publishedPosts;
    }

    public void setPublishedPosts(Integer publishedPosts) {
        this.publishedPosts = publishedPosts;
    }

    public Integer getPublishedReposts() {
        return publishedReposts;
    }

    public void setPublishedReposts(Integer publishedReposts) {
        this.publishedReposts = publishedReposts;
    }

    public Integer getPublishedReplies() {
        return publishedReplies;
    }

    public void setPublishedReplies(Integer publishedReplies) {
        this.publishedReplies = publishedReplies;
    }

    @Override
    public String toString() {
        return "StatisticsAccount{" + "account=" + account + ", friends=" + friends + ", followers=" + followers + ", following=" + following + ", subscribers=" + subscribers + ", likes=" + likes + ", shares=" + shares + ", publishedPosts=" + publishedPosts + ", publishedReposts=" + publishedReposts + ", publishedReplies=" + publishedReplies + '}';
    }

}
