/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.util.Locale;

/**
 *
 * @author ky6uHETc
 */
public abstract class EntityModelSystemUser extends EntityModelRegistry {

    public abstract String getUserLogin();

    public abstract String getUserPassword();
    
    public abstract Locale getLocale();
    
    public abstract boolean isGuest();

}
