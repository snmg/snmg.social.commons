/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import ru.snetwork.social.SocialNetwork;

/**
 *
 * @author evgeniy
 */
public class TaskSearch extends Task {

    private static final long serialVersionUID = 2l;

    Set<SearchType> searchTypes;
    Set<SocialNetwork> socialNetworks;
    Set<String> searchHashtags;
    Set<String> searchPhrases;
    Date dateSearchContentNewer;
    ConnectedInstagramBusinessAccount connectedInstagramBusinessAccount;

    public Set<SearchType> getSearchTypes() {
        if (searchTypes == null) {
            searchTypes = new HashSet<>();
        }
        return searchTypes;
    }

    public void setSearchTypes(Set<SearchType> searchTypes) {
        this.searchTypes = searchTypes;
    }

    public Set<SocialNetwork> getSocialNetworks() {
        if (socialNetworks == null) {
            socialNetworks = new HashSet<>();
        }
        return socialNetworks;
    }

    public void setSocialNetworks(Set<SocialNetwork> socialNetworks) {
        this.socialNetworks = socialNetworks;
    }

    public Set<String> getSearchHashtags() {
        if (searchHashtags == null) {
            searchHashtags = new HashSet<>();
        }
        return searchHashtags;
    }

    public void setSearchHashtags(Set<String> searchHashtags) {
        this.searchHashtags = searchHashtags;
    }

    public Set<String> getSearchPhrases() {
        if (searchPhrases == null) {
            searchPhrases = new HashSet<>();
        }
        return searchPhrases;
    }

    public void setSearchPhrase(Set<String> searchPhrases) {
        this.searchPhrases = searchPhrases;
    }

    public Date getDateSearchContentNewer() {
        return dateSearchContentNewer;
    }

    public void setDateSearchContentNewer(Date dateSearchContentNewer) {
        this.dateSearchContentNewer = dateSearchContentNewer;
    }

    public ConnectedInstagramBusinessAccount getConnectedInstagramBusinessAccount() {
        return connectedInstagramBusinessAccount;
    }

    public void setConnectedInstagramBusinessAccount(ConnectedInstagramBusinessAccount connectedInstagramBusinessAccount) {
        this.connectedInstagramBusinessAccount = connectedInstagramBusinessAccount;
    }

    public enum SearchType {
        HASHTAG,
        PHRASE
    }

    @Override
    public String toString() {
        return "TaskSearch{"
                + "internalId=" + internalId
                + ", searchTypes=" + searchTypes
                + ", socialNetworks=" + socialNetworks
                + ", searchHashtags=" + searchHashtags
                + ", searchPhrase=" + searchPhrases
                + ", dateSearchContentNewer=" + dateSearchContentNewer + '}';
    }

}
