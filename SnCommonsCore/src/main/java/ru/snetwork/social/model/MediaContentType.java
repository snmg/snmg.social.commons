/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.io.Serializable;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ky6uHETc
 */
public class MediaContentType implements Serializable{
    
    private static final long serialVersionUID = 1l;

    private static final String[] ENTRY_AUDIO_TYPES = new String[]{"audio"};
    private static final String[] ENTRY_VIDEO_TYPES = new String[]{"video"};
    private static final String[] ENTRY_IMAGE_TYPES = new String[]{"image", "photo"};
    
    private static final String[] ATTACHMENT_AUDIO_TYPES = new String[]{"audio"};
    private static final String[] ATTACHMENT_VIDEO_TYPES = new String[]{"video"};
    private static final String[] ATTACHMENT_IMAGE_TYPES = new String[]{"image", "photo"};

    public static EntryType valueOfEntryType(String type) {
        if (StringUtils.isNotBlank(type)) {
            if (StringUtils.startsWithAny(type.toLowerCase(), ENTRY_IMAGE_TYPES)) {
                return EntryType.PHOTO;
            } else if (StringUtils.startsWithAny(type.toLowerCase(), ENTRY_VIDEO_TYPES)) {
                return EntryType.VIDEO;
            } else if (StringUtils.startsWithAny(type.toLowerCase(), ENTRY_AUDIO_TYPES)) {
                return EntryType.AUDIO;
            }
        }
        return EntryType.POST;
    }
    
    public static AttachmentType valueOfAttachmentType(String type) {
        if (StringUtils.isNotBlank(type)) {
            if (StringUtils.startsWithAny(type.toLowerCase(), ATTACHMENT_IMAGE_TYPES)) {
                return AttachmentType.IMAGE;
            } else if (StringUtils.startsWithAny(type.toLowerCase(), ATTACHMENT_VIDEO_TYPES)) {
                return AttachmentType.VIDEO;
            } else if (StringUtils.startsWithAny(type.toLowerCase(), ATTACHMENT_AUDIO_TYPES)) {
                return AttachmentType.AUDIO;
            }
        }
        return AttachmentType.UNKNOWN;
    }

    public static enum EntryType {
        STATUS,     // Twitter status
        REPLY,      // Twitter reply
        POST,       // Most used content entry type (Facebook, VK, Ok posts or Telegram Public Channel Post)
        COMMENT,
        SHARE,
        PHOTO,
        VIDEO,
        AUDIO,      
        TOPIC,       // Post without any data, that used for discussion trheads like VK topics (forum topics)
        PRIVATE_MESSAGE
    }

    public static enum AttachmentType {
        NONE,
        IMAGE,
        VIDEO,
        AUDIO,
        STREAM,
        EMBED,
        URL,
        UNKNOWN,
        MULTIPLE
    }
    
    public static enum WorkWithAppeal {
        Q_AND_A,
        ALARM_CHAT,
        SUPPORT_RP,
        LOGISTIC_CHAT,
        LETTER,
        ISO,
        FIVE_POINTS,
        NONE
    }
}
