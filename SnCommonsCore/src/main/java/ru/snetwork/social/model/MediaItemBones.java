package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Objects;

import ru.snetwork.social.SocialNetwork;

public class MediaItemBones implements Serializable {

    private static final long serialVersionUID = 1l;

    private final SocialNetwork socialNetwork;
    private final String id;
    private final String someRelationToApplicationObject;
    private final String someRelationToUserObject;

    public MediaItemBones(SocialNetwork socialNetwork, String id, String someRelationToApplicationObject, String someRelationToUserObject) {
        this.socialNetwork = socialNetwork;
        this.id = id;
        this.someRelationToApplicationObject = someRelationToApplicationObject;
        this.someRelationToUserObject = someRelationToUserObject;
    }

    public MediaItemBones(SocialNetwork socialNetwork, String id) {
        this.socialNetwork = socialNetwork;
        this.id = id;
        this.someRelationToApplicationObject = null;
        this.someRelationToUserObject = null;
    }

    public MediaItemBones(SocialNetwork socialNetwork, String id, String someRelationToObject, boolean relationToObjectUser) {
        this.socialNetwork = socialNetwork;
        this.id = id;
        if (relationToObjectUser) {
            this.someRelationToApplicationObject = null;
            this.someRelationToUserObject = someRelationToObject;
        } else {
            this.someRelationToApplicationObject = someRelationToObject;
            this.someRelationToUserObject = null;
        }
    }

    public SocialNetwork getSocialNetwork() {
        return socialNetwork;
    }

    public String getId() {
        return id;
    }

    public String getSomeRelationToApplicationObject() {
        return someRelationToApplicationObject;
    }

    public String getSomeRelationToUserObject() {
        return someRelationToUserObject;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.socialNetwork);
        hash = 11 * hash + Objects.hashCode(this.id);
        hash = 11 * hash + Objects.hashCode(this.someRelationToApplicationObject);
        hash = 11 * hash + Objects.hashCode(this.someRelationToUserObject);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaItemBones other = (MediaItemBones) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.someRelationToApplicationObject, other.someRelationToApplicationObject)) {
            return false;
        }
        if (!Objects.equals(this.someRelationToUserObject, other.someRelationToUserObject)) {
            return false;
        }
        if (this.socialNetwork != other.socialNetwork) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MediaItemBones{" + "socialNetwork=" + socialNetwork + ", id=" + id + ", someRelationToApplicationObject=" + someRelationToApplicationObject + ", someRelationToUserObject=" + someRelationToUserObject + '}';
    }

}
