package ru.snetwork.social.model;

public class CommandResultError extends CommandResult {
    
    private final String error;
    
    public CommandResultError(String error) {
        super(null);
        this.error = error;
    }
    
    public String getError() {
        return error;
    }
    
}
