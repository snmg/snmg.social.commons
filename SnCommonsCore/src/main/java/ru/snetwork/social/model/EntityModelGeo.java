/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ky6uHETc
 */
@MappedSuperclass
public abstract class EntityModelGeo extends EntityModelBasic implements Serializable {

    private String nameRu;
    private String nameEn;
    private String detailedNameRu;
    private String baseNameRu;
    private String fiasId;

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = StringUtils.trim(nameRu);
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = StringUtils.trim(nameEn);
    }

    @Override
    public String toString() {
        return detailedNameRu + " [" + nameEn + "]";
    }

    public String getDetailedNameRu() {
        return detailedNameRu;
    }

    //extended name in russian included names of parent object
    //use pattern cityNameRu (subRegionNameRu, regionNameRu)
    public void setDetailedNameRu(String detailedNameRu) {
        this.detailedNameRu = detailedNameRu;
    }

    public String getBaseNameRu() {
        return baseNameRu;
    }

    public void setBaseNameRu(String baseNameRu) {
        this.baseNameRu = baseNameRu;
    }

    public String getFiasId() {
        return fiasId;
    }

    public void setFiasId(String fiasId) {
        this.fiasId = fiasId;
    }
}
