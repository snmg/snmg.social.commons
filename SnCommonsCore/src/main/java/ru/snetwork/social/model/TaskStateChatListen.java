package ru.snetwork.social.model;

import java.util.Date;

public class TaskStateChatListen extends TaskState {

    private String latestMsgId;
    private Date latestMsgDate;

    public String getLatestMsgId() {
        return latestMsgId;
    }

    public void setLatestMsgId(String latestMsgId) {
        this.latestMsgId = latestMsgId;
    }

    public Date getLatestMsgDate() {
        return latestMsgDate;
    }

    public void setLatestMsgDate(Date latestMsgDate) {
        this.latestMsgDate = latestMsgDate;
    }

}
