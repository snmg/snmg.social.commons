/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Comparator;
import java.util.Date;
import ru.snetwork.social.Moderation;

/**
 *
 * @author evgeniy
 */
public abstract class EntityModelModeration extends EntityModelEntry {

    public abstract String getName();

    public abstract void setName(String value);

    public abstract Date getDateModeratedAt();

    public abstract void setDateModeratedAt(Date date);

    public abstract Moderation getModerationStatus();

    public abstract void setModerationStatus(Moderation value);

    public abstract String getModerationComment();

    public abstract void setModerationComment(String value);

    @JsonIgnore
    public boolean isNew() {
        return getModerationStatus() == null || Moderation.NEW.equals(getModerationStatus());
    }

    @JsonIgnore
    public boolean isApproved() {
        return Moderation.APPROVED.equals(getModerationStatus());
    }

    @JsonIgnore
    public boolean isDeclined() {
        return Moderation.DECLINED.equals(getModerationStatus());
    }

    @JsonIgnore
    public boolean isPending() {
        return Moderation.PENDING.equals(getModerationStatus());
    }

    @JsonIgnore
    public boolean isDraft() {
        return Moderation.DRAFT.equals(getModerationStatus());
    }

    public static Comparator<EntityModelRegistry> COMPARATOR_BY_NAME = new Comparator<EntityModelRegistry>() {
        @Override
        public int compare(EntityModelRegistry o1, EntityModelRegistry o2) {
            if (o1.getName() != null && o2.getName() != null) {
                return o1.getName().compareTo(o2.getName());
            } else {
                return (o1.getName() != null) ? 1 : (o2.getName() != null) ? -1 : 0;
            }
        }
    };
}
