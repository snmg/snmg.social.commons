package ru.snetwork.social.model;

import java.io.Serializable;
import java.util.Date;
import ru.snetwork.social.Priority;

public class Task implements Serializable {

    private static final long serialVersionUID = 1l;

    Long internalId;
    TaskType taskType;
    Date dateActiveFrom;
    Date dateActiveTill;
    String ownerTaskKey;
    Priority priority;

    public Task() {
    }

    public Task(TaskType taskType, Date dateActiveFrom, Date dateActiveTill, String ownerTaskKey, Priority priority) {
        this.taskType = taskType;
        this.dateActiveFrom = dateActiveFrom;
        this.dateActiveTill = dateActiveTill;
        this.ownerTaskKey = ownerTaskKey;
        this.priority = priority;
    }

    public Long getInternalId() {
        return internalId;
    }

    public void setInternalId(Long internalId) {
        this.internalId = internalId;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public Date getDateActiveFrom() {
        return dateActiveFrom;
    }

    public void setDateActiveFrom(Date dateActiveFrom) {
        this.dateActiveFrom = dateActiveFrom;
    }

    public Date getDateActiveTill() {
        return dateActiveTill;
    }

    public void setDateActiveTill(Date dateActiveTill) {
        this.dateActiveTill = dateActiveTill;
    }

    public String getOwnerTaskKey() {
        return ownerTaskKey;
    }

    public void setOwnerTaskKey(String ownerTaskKey) {
        this.ownerTaskKey = ownerTaskKey;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public enum TaskType {
        SEARCH,
        ACCOUNT_FEED,
        ACCOUNT_STATISTICS,
        LOCATION_FEED,
        CHAT_LISTEN,
        CHAT_SEARCH,
        WEBHOOK
    }
}
