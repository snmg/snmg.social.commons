package ru.snetwork.social.model;

import java.util.Map;
import java.util.Set;
import ru.snetwork.social.SocialNetwork;
import ru.snetwork.social.WebhookActivity;

public class TaskWebhook extends Task {

    SocialNetwork socialNetwork;
    Set<WebhookActivity> subscription;
    String applicationId;
    Map<String, String> applicationCredentials;

    public SocialNetwork getSocialNetwork() {
        return socialNetwork;
    }

    public void setSocialNetwork(SocialNetwork socialNetwork) {
        this.socialNetwork = socialNetwork;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public Set<WebhookActivity> getSubscription() {
        return subscription;
    }

    public void setSubscription(Set<WebhookActivity> subscription) {
        this.subscription = subscription;
    }

    public Map<String, String> getApplicationCredentials() {
        return applicationCredentials;
    }

    public void setApplicationCredentials(Map<String, String> applicationCredentials) {
        this.applicationCredentials = applicationCredentials;
    }

}
