/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;

/**
 *
 * @author ky6uHETc
 */
public abstract class EntityModelEntry extends EntityModelBasic {

    @JsonIgnore
    public boolean isDeleted() {
        return getDateDeletedAt() != null;
    }

    public abstract Date getDateCreatedAt();

    public abstract void setDateCreatedAt(Date date);

    public abstract Date getDateDeletedAt();

    public abstract void setDateDeletedAt(Date date);

    public abstract Date getDateModifiedAt();

    public abstract void setDateModifiedAt(Date date);

}
