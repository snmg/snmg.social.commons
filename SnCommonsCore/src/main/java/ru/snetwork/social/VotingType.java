/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social;

/**
 *
 * @author evgeniy
 */
public enum VotingType {

    POINTS,
    LIKES,
    SHARES,
    VIEWS;

    public static VotingType valueByName(String name) {
        return valueOf(name.toUpperCase());
    }
}
