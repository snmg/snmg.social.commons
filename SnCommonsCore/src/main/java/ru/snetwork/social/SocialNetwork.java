/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ky6uHETc
 */
public enum SocialNetwork {

    /**
     * ENUM and NAME Uppercase Values should be equal. Caused by method "public
     * static SocialNetwork valueByName(String name)"
     */
    LINKEDIN(
            "LinkedIn",
            "linkedin.com",
            "API Limits: https://developer.linkedin.com/docs/guide/v2/best-practices/throttle-limits",
            null, // new RateLimit(200, 3600),
            null,
            null,
            null, // new RateLimit(6, 3600),
            new JobRunNorms(160000, 380000, 220000),
            new JobRunNorms(160000, 710000, 550000),
            new JobRunNorms(160000, 2360000, 2200000),
            false, false,
            Feature.MONITORING
    ),
    FACEBOOK(
            "Facebook",
            "facebook.com",
            "API Limits: https://developers.facebook.com/docs/graph-api/advanced/rate-limiting",
            new RateLimit(200, 3600),
            null,
            null,
            new RateLimit(12, 3600),
            new JobRunNorms(160000, 380000, 220000),
            new JobRunNorms(160000, 710000, 550000),
            new JobRunNorms(160000, 2360000, 2200000),
            false, false,
            Feature.EXCLUSIVE, Feature.MONITORING, Feature.MESSAGING, Feature.WEBHOOK
    ),
    INSTAGRAM(
            "Instagram",
            "instagram.com",
            "API Limits: https://www.instagram.com/developer/limits/",
            new RateLimit(50, 900),
            new RateLimit(50, 900),
            null,
            new RateLimit(10, 3600),
            new JobRunNorms(30000, 230000, 200000),
            new JobRunNorms(30000, 400000, 370000),
            new JobRunNorms(30000, 800000, 770000),
            false, false,
            Feature.EXCLUSIVE, Feature.MONITORING, Feature.WEBHOOK
    ),
    TWITTER(
            "Twitter",
            "twitter.com",
            "API Limits: https://dev.twitter.com/rest/public/rate-limiting",
            new RateLimit(15, 900),
            null,
            new RateLimit(180, 900),
            null,
            new JobRunNorms(10000, 20000, 10000),
            new JobRunNorms(10000, 200000, 190000),
            new JobRunNorms(10000, 600000, 590000),
            true, false,
            Feature.MONITORING, Feature.POSTING, Feature.STREAMING
    ),
    TIKTOK(
            "TikTok",
            "tiktok.com",
            "API Limits: unknown",
            new RateLimit(15, 900),
            null,
            new RateLimit(180, 900),
            null,
            new JobRunNorms(10000, 20000, 10000),
            new JobRunNorms(10000, 200000, 190000),
            new JobRunNorms(10000, 600000, 590000),
            true, false,
            Feature.MONITORING
    ),
    VKONTAKTE(
            "Vkontakte",
            "vk.com",
            "API Limits: https://vk.com/dev/api_requests",
            new RateLimit(2700, 900),
            null,
            null,
            null,
            new JobRunNorms(10000, 230000, 200000),
            new JobRunNorms(10000, 400000, 370000),
            new JobRunNorms(10000, 800000, 770000),
            true, false,
            Feature.MONITORING, Feature.MESSAGING, Feature.POSTING, Feature.STREAMING
    ),
    ODNOKLASSNIKI(
            "Odnoklassniki",
            "ok.ru",
            "API Limits: https://apiok.ru/wiki/display/api/Application+limits",
            new RateLimit(18000, 900), // max 20 queries per second
            null,
            null,
            null,
            new JobRunNorms(10000, 230000, 200000),
            new JobRunNorms(10000, 400000, 370000),
            new JobRunNorms(10000, 800000, 770000),
            true, false,
            Feature.MONITORING, Feature.MESSAGING, Feature.POSTING
    ),
    LIVEJOURNAL(
            "LiveJournal",
            "livejournal.com",
            "",
            new RateLimit(600, 3600), // max 10 queries per minute,
            null,
            null,
            new RateLimit(60, 3600),
            new JobRunNorms(60000, 360000, 330000),
            new JobRunNorms(60000, 660000, 660000),
            new JobRunNorms(60000, 1260000, 1220000),
            false, false,
            Feature.MONITORING
    ),
    FLICKR(
            "Flickr",
            "flickr.com",
            "API: https://www.flickr.com/services/api/",
            new RateLimit(3600, 3600), // max 3600 queries per hour,
            null,
            null,
            null,
            new JobRunNorms(60000, 360000, 330000),
            new JobRunNorms(60000, 660000, 660000),
            new JobRunNorms(60000, 1260000, 1220000),
            false, false,
            Feature.MONITORING
    ),
    YOUTUBE(
            "YouTube",
            "youtube.com",
            "API: https://developers.google.com/youtube/v3/",
            new RateLimit(69, 600),
            null,
            new RateLimit(69, 600),
            null,
            new JobRunNorms(60000, 360000, 330000),
            new JobRunNorms(60000, 660000, 660000),
            new JobRunNorms(60000, 1260000, 1220000),
            true, false,
            Feature.MONITORING
    ),
    GOOGLEPLUS(
            "GooglePlus",
            "plus.google.com",
            "API: https://developers.google.com/+/web/api/rest/index",
            new RateLimit(69, 600),
            null,
            new RateLimit(69, 600),
            null,
            new JobRunNorms(20000, 170000, 150000),
            new JobRunNorms(20000, 320000, 300000),
            new JobRunNorms(20000, 620000, 600000),
            true, false,
            Feature.MONITORING
    ),
    PIKABU(
            "Pikabu",
            "pikabu.ru",
            "no API, parse only",
            null,
            null,
            null,
            null,
            new JobRunNorms(60000, 360000, 330000),
            new JobRunNorms(60000, 660000, 660000),
            new JobRunNorms(60000, 1260000, 1220000),
            false, true,
            Feature.MONITORING
    ),
    TUMBLR(
            "Tumblr",
            "tumblr.com",
            "API: https://www.tumblr.com/docs/en/api/v2",
            new RateLimit(200, 3600), // Rate Limits: 1000 requests per hour, 5000 requests per day
            null,
            null,
            null,
            new JobRunNorms(60000, 360000, 330000),
            new JobRunNorms(60000, 660000, 660000),
            new JobRunNorms(60000, 1260000, 1220000),
            false, false,
            Feature.MONITORING
    ),
    TELEGRAM(
            "Telegram",
            "telegram.org",
            "Messaging Services used",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            false, false,
            Feature.MESSAGING
    ),
    GOOGLESHEETS(
            "GoogleSheets",
            "docs.google.com",
            "Google Sheets",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            false, false,
            Feature.DATATRANSFER
    ),
    RUSSIANPOST(
            "RussianPost",
            "pochta.ru",
            "Russian Post Tracking Platform",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            false, false,
            Feature.DATATRANSFER
    ),
    LIKEE(
            "Likee",
            "like.video",
            "API Limits: unknown",
            new RateLimit(15, 900),
            null,
            new RateLimit(180, 900),
            null,
            new JobRunNorms(10000, 20000, 10000),
            new JobRunNorms(10000, 200000, 190000),
            new JobRunNorms(10000, 600000, 590000),
            true, false,
            Feature.MONITORING
    ),
    APPLE(
            "Apple",
            "apple.com",
            "API Limits: last 500 app reviews",
            null,
            null,
            null,
            null,
            new JobRunNorms(60000, 360000, 330000),
            new JobRunNorms(60000, 660000, 660000),
            new JobRunNorms(60000, 1260000, 1220000),
            false, false,
            Feature.MONITORING
    );

    public static enum RateTypes {
        LIVE,
        SANDBOX,
        SEARCH,
        USERSESSION
    }
    public static final String RATE_LIVE = "rateLive";
    public static final String RATE_SANDBOX = "rateSandbox";
    public static final String RATE_SEARCH = "rateSearch";

    private final String name;
    private final String domain;
    private final String description;
    private final RateLimit rateLimitLiveRequests;
    private final RateLimit rateLimitSandboxRequests;
    private final RateLimit rateLimitSearchRequests;
    private final RateLimit rateLimitUserSessions;
    private final JobRunNorms jobRunNormsPriorityHigh;
    private final JobRunNorms jobRunNormsPriorityNorm;
    private final JobRunNorms jobRunNormsPriorityLow;
    private final boolean multiThreadSearchAllowed;
    private final boolean httpsNotSupported;
    private final Feature[] features;

    SocialNetwork(
            String name,
            String domain,
            String description,
            RateLimit rateLimitLiveMode,
            RateLimit rateLimitSandboxMode,
            RateLimit rateLimitSearchRequests,
            RateLimit rateLimitUserSessions,
            JobRunNorms jobRunNormsPriorityHigh,
            JobRunNorms jobRunNormsPriorityNorm,
            JobRunNorms jobRunNormsPriorityLow,
            boolean multiThreadSearchAllowed,
            boolean httpsNotSupported,
            Feature... features) {
        this.name = name;
        this.domain = domain;
        this.description = description;
        this.rateLimitLiveRequests = rateLimitLiveMode;
        this.rateLimitSandboxRequests = rateLimitSandboxMode;
        this.rateLimitSearchRequests = rateLimitSearchRequests;
        this.rateLimitUserSessions = rateLimitUserSessions;
        this.jobRunNormsPriorityHigh = jobRunNormsPriorityHigh;
        this.jobRunNormsPriorityNorm = jobRunNormsPriorityNorm;
        this.jobRunNormsPriorityLow = jobRunNormsPriorityLow;
        this.multiThreadSearchAllowed = multiThreadSearchAllowed;
        this.httpsNotSupported = httpsNotSupported;
        this.features = features;
    }

    public String getName() {
        return name;
    }

    public String getDomain() {
        return domain;
    }

    public String getDescription() {
        return description;
    }

    public RateLimit getRateLimitLiveRequests() {
        return rateLimitLiveRequests;
    }

    public RateLimit getRateLimitSandboxRequests() {
        return rateLimitSandboxRequests;
    }

    public RateLimit getRateLimitSearchRequests() {
        return rateLimitSearchRequests;
    }

    public RateLimit getRateLimitUserSessions() {
        return rateLimitUserSessions;
    }

    public JobRunNorms getJobRunNorms(Priority priority) {
        switch (priority) {
            case HIGH:
                return jobRunNormsPriorityHigh;
            case NORMAL:
                return jobRunNormsPriorityNorm;
            case LOW:
                return jobRunNormsPriorityLow;
        }
        return jobRunNormsPriorityHigh;
    }

    public boolean isMultiThreadSearchAllowed() {
        return multiThreadSearchAllowed;
    }

    public boolean isHttpsNotSupported() {
        return httpsNotSupported;
    }

    public boolean isFeaturedWith(Feature feature) {
        return ArrayUtils.contains(features, feature);
    }

    public boolean isFeaturedWithMessaging() {
        return isFeaturedWith(Feature.MESSAGING);
    }

    public boolean isFeaturedWithDataTransfer() {
        return isFeaturedWith(Feature.DATATRANSFER);
    }

    public boolean isFeaturedWithMonitoring() {
        return isFeaturedWith(Feature.MONITORING);
    }

    public boolean isFeaturedWithPosting() {
        return isFeaturedWith(Feature.POSTING);
    }

    public boolean isFeaturedWithStreaming() {
        return isFeaturedWith(Feature.STREAMING);
    }
    
    public boolean isFeaturedWithWebhook() {
        return isFeaturedWith(Feature.WEBHOOK);
    }

    public static SocialNetwork valueByName(String name) {
        return valueOf(name.toUpperCase());
    }

    public static SocialNetwork valueByDomain(String domain) {
        //domain = StringUtils.startsWith(domain, "www.") ? domain.substring(4) : domain;
        if (StringUtils.countMatches(domain, ".") == 2 && !StringUtils.contains(domain, "google")) {
            domain = StringUtils.substringAfter(domain, "."); //for LJ urls like http://zavodfoto.livejournal.com/5599748.html or domains with 'www.'
        }
        if (domain.equals(SocialNetwork.LINKEDIN.getDomain())) {
            return SocialNetwork.LINKEDIN;
        } else if (domain.equals(SocialNetwork.FACEBOOK.getDomain())) {
            return SocialNetwork.FACEBOOK;
        } else if (domain.equals(SocialNetwork.INSTAGRAM.getDomain())) {
            return SocialNetwork.INSTAGRAM;
        } else if (domain.equals(SocialNetwork.TWITTER.getDomain())) {
            return SocialNetwork.TWITTER;
        } else if (domain.equals(SocialNetwork.TIKTOK.getDomain())) {
            return SocialNetwork.TIKTOK;
        } else if (domain.equals(SocialNetwork.VKONTAKTE.getDomain())) {
            return SocialNetwork.VKONTAKTE;
        } else if (domain.equals(SocialNetwork.ODNOKLASSNIKI.getDomain())) {
            return SocialNetwork.ODNOKLASSNIKI;
        } else if (domain.equals(SocialNetwork.LIVEJOURNAL.getDomain())) {
            return SocialNetwork.LIVEJOURNAL;
        } else if (domain.equals(SocialNetwork.YOUTUBE.getDomain())) {
            return SocialNetwork.YOUTUBE;
        } else if (domain.equals(SocialNetwork.GOOGLEPLUS.getDomain())) {
            return SocialNetwork.GOOGLEPLUS;
        } else if (domain.equals(SocialNetwork.TUMBLR.getDomain())) {
            return SocialNetwork.TUMBLR;
        } else if (domain.equals(SocialNetwork.TELEGRAM.getDomain()) || domain.equals("t.me")) {
            return SocialNetwork.TELEGRAM;
        } else if (domain.equals(SocialNetwork.GOOGLESHEETS.getDomain())) {
            return SocialNetwork.GOOGLESHEETS;
        } else if (domain.equals(SocialNetwork.PIKABU.getDomain())) {
            return SocialNetwork.PIKABU;
        } else if (domain.equals(SocialNetwork.RUSSIANPOST.getDomain())) {
            return SocialNetwork.RUSSIANPOST;
        } else if (domain.equals(SocialNetwork.LIKEE.getDomain())) {
            return SocialNetwork.LIKEE;
        } else if (domain.equals(SocialNetwork.APPLE.getDomain())) {
            return SocialNetwork.APPLE;
        }
        return null;
    }

    public static class RateLimit {

        int limit;
        int interval;

        /**
         * Social Networks API Rate Limits per Access Token or Access User
         *
         * @param rateLimit max API requests per interval
         * @param interval interval in seconds
         */
        public RateLimit(int rateLimit, int interval) {
            this.limit = rateLimit;
            this.interval = interval;
        }

        public int getLimit() {
            return limit;
        }

        public int getInterval() {
            return interval;
        }

    }

    public static class JobRunNorms {

        int jobDuration;
        int jobInterval;
        int jobWaiting;

        public JobRunNorms(int jobDuration, int jobInterval, int jobWaiting) {
            this.jobDuration = jobDuration;
            this.jobInterval = jobInterval;
            this.jobWaiting = jobWaiting;
        }

        public int getJobDuration() {
            return jobDuration;
        }

        public int getJobInterval() {
            return jobInterval;
        }

        public int getJobWaiting() {
            return jobWaiting;
        }

    }

    public enum Feature {
        DATATRANSFER,
        EXCLUSIVE,
        MESSAGING,
        MONITORING,
        POSTING,
        STREAMING,
        WEBHOOK
    }
}
