/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.util;

import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

/**
 *
 * @author ky6uHETc
 */
public class HbUtil {

    public static <T> T unwrapImplementation(T obj) {
        if (obj != null) {
            if (obj instanceof HibernateProxy) {
                // Unwrap Proxy;
                //      -- loading, if necessary.
                HibernateProxy proxy = (HibernateProxy) obj;
                LazyInitializer li = proxy.getHibernateLazyInitializer();
                return (T) li.getImplementation();
            }
            return obj;
        }
        return null;
    }
}
