package ru.snetwork.social.util;

import java.security.Key;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import ru.snetwork.social.BuildEnviroment;
import ru.snetwork.social.Product;
import ru.snetwork.social.exception.SystemException;

public class ExternalFileUrlUtil {

    public static class EncodeResult {

        private final String externalFileUrl;

        public EncodeResult(String externalFileUrl) {
            this.externalFileUrl = externalFileUrl;
        }

        public String getExternalFileUrl() {
            return externalFileUrl;
        }

    }

    public static class DecodeResult {

        private final Product product;
        private final BuildEnviroment buildEnviroment;
        private final String serverFilePath;

        public DecodeResult(Product product, BuildEnviroment buildEnviroment, String serverFilePath) {
            this.product = product;
            this.buildEnviroment = buildEnviroment;
            this.serverFilePath = serverFilePath;
        }

        public Product getProduct() {
            return product;
        }

        public BuildEnviroment getBuildEnviroment() {
            return buildEnviroment;
        }

        public String getServerFilePath() {
            return serverFilePath;
        }

    }

    private final static String CRYPTO_ALG = "AES";
    private final static Key CRYPTO_KEY = new SecretKeySpec("n2r4u7x!A%D*G-Ka".getBytes(), CRYPTO_ALG);
    private final static Pattern PATTERN_EXTERNAL_FILE_URL = Pattern.compile("^([\\w\\-.\\/:]*)\\/file\\/([\\w\\-]*)$", Pattern.CASE_INSENSITIVE);

    /**
     * @param product
     * @param buildEnviroment
     * @param serverFilePath /srv/data/snmsgcontroller/{..}...
     * @return
     * @throws SystemException
     */
    public static EncodeResult encodeServerFilePath(Product product, BuildEnviroment buildEnviroment, String serverFilePath) throws SystemException {
        try {
            Cipher encryptor = Cipher.getInstance(CRYPTO_ALG);
            encryptor.init(Cipher.ENCRYPT_MODE, CRYPTO_KEY);
            String encryptedServerFilePath = Base64.encodeBase64URLSafeString(encryptor.doFinal(serverFilePath.getBytes()));
            String externalUrl = product.getBaseURL(buildEnviroment) + "/file/" + encryptedServerFilePath;
            return new EncodeResult(externalUrl);
        } catch (Exception ex) {
            throw new SystemException(ex.getMessage(), ex);
        }
    }

    /**
     * @param externalFileUrl
     * <pre>http|https :// group1{productBaseURL} / file / group2{encryptedServerFilePath}</pre>
     *
     * @return ExternalFileUrlUtil.DecodeResult.class
     * @throws SystemException
     */
    public static DecodeResult decodeExternalFileUrl(String externalFileUrl) throws SystemException {
        try {
            Matcher matcher = PATTERN_EXTERNAL_FILE_URL.matcher(externalFileUrl);
            if (matcher.find()) {
                String productBaseURL = matcher.group(1);
                String encryptedServerFilePath = matcher.group(2);
                Cipher decryptor = Cipher.getInstance(CRYPTO_ALG);
                decryptor.init(Cipher.DECRYPT_MODE, CRYPTO_KEY);
                String serverFilePath = new String(decryptor.doFinal(Base64.decodeBase64(encryptedServerFilePath)));
                BuildEnviroment buildEnviroment = Product.getBuildEnviromentByBaseURL(productBaseURL);
                Product product = Product.getByBaseURL(productBaseURL, buildEnviroment);
                return new DecodeResult(product, buildEnviroment, serverFilePath);
            } else {
                throw new SystemException("Invalid external file URL: " + externalFileUrl);
            }
        } catch (Exception ex) {
            throw new SystemException(ex.getMessage(), ex);
        }
    }

}
