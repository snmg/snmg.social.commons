/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.util;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ky6uHETc
 */
public class TextFormatUtil {

    static Pattern PATTERN_TIMING_SECONDS = Pattern.compile("(\\d{1,10}):(\\d{1,2}):(\\d{1,2})");

    public static String formatTimingSeconds(Integer seconds) {
        if (seconds != null) {
            int hh = seconds / 3600;
            int mm = (seconds % 3600) / 60;
            int ss = seconds % 60;
            return hh
                    + ":"
                    + (mm < 10 ? "0" + mm : mm)
                    + ":"
                    + (ss < 10 ? "0" + ss : ss);
        }
        return "0:00:00";
    }

    public static int parseTimingSeconds(String timing) {
        if (StringUtils.isNotBlank(timing)) {
            Matcher m = PATTERN_TIMING_SECONDS.matcher(timing.trim());
            if (m.matches()) {
                int hh = Integer.valueOf(m.group(1));
                int mm = Integer.valueOf(m.group(2));
                int ss = Integer.valueOf(m.group(3));
                return hh * 3600 + mm * 60 + ss;
            }
        }
        return 0;
    }

    public static String shortedText(String text, int maxLength) {
        return StringUtils.length(text) > maxLength && maxLength > 5
                ? StringUtils.substring(text, maxLength - 4) + " ..."
                : text != null
                        ? text
                        : "";
    }
}
