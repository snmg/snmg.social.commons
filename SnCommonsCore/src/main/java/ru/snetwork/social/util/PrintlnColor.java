package ru.snetwork.social.util;

public class PrintlnColor {

    public enum Color {
        NORMAL("\u001B[0m"),
        WHITE("\u001B[1m"),
        BLACK("\u001B[30m"),
        GRAY("\u001B[37m"),
        GRAY_DARK("\u001B[1;30m"),
        GRAY_LIGHT("\u001B[1;37m"),
        RED("\u001B[31m"),
        RED_LIGHT("\u001B[1;31m"),
        GREEN("\u001B[32m"),
        GREEN_LIGHT("\u001B[1;32m"),
        YELLOW("\u001B[33m"),
        YELLOW_LIGHT("\u001B[1;33m"),
        BLUE("\u001B[34m"),
        BLUE_LIGHT("\u001B[1;34m"),
        MAGENTA("\u001B[35m"),
        MAGENTA_LIGHT("\u001B[1;35m"),
        CYAN("\u001B[36m"),
        CYAN_LIGHT("\u001B[1;36m");

        private final String color;

        private Color(String color) {
            this.color = color;
        }

        public String getColor() {
            return color;
        }

    }

    public static final void out(Color color, String string) {
        System.out.println(color.getColor() + string + Color.NORMAL.getColor());
    }

    public static final void err(Color color, String string) {
        System.err.println(color.getColor() + string + Color.NORMAL.getColor());
    }

}
