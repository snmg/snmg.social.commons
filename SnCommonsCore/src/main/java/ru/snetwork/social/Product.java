package ru.snetwork.social;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Product implements Serializable {

    private static final long serialVersionUID = 1l;

    private static Log log = LogFactory.getLog(Product.class);

    private static class BaseURL {

        private final String local;
        private final String dev;
        private final String prod;

        public BaseURL(String local, String dev, String prod) {
            this.local = local;
            this.dev = dev;
            this.prod = prod;
        }

        public String getLocal() {
            return local;
        }

        public String getDev() {
            return dev;
        }

        public String getProd() {
            return prod;
        }

    }

    public static final String SOCIAL_MONITOR = "SnSocial.Monitor";
    public static final String SOCIAL_MONITORING = "SnSocial.Monitoring";
    public static final String SOCIAL_STREAMING = "SnSocial.Streaming";
    public static final String SOCIAL_CONTEST = "SnSocial.Contest";
    public static final String MESSAGING_CONTROLLER = "SnMsg.Controller";
    public static final String MESSAGING_TELEGRAM = "SnMsg.Telegram";
    public static final String MESSAGING_VKONTAKTE = "SnMsg.Vkontakte";
    public static final String MESSAGING_FACEBOOK = "SnMsg.Facebook";
    public static final String[] PRODUCTS_IDS = {
        SOCIAL_MONITOR,
        SOCIAL_MONITORING,
        SOCIAL_STREAMING,
        SOCIAL_CONTEST,
        MESSAGING_CONTROLLER,
        MESSAGING_TELEGRAM,
        MESSAGING_VKONTAKTE,
        MESSAGING_FACEBOOK
    };
    private static final BaseURL SOCIAL_MONITOR_BASE_URL = new BaseURL(
            "http://localhost:8084/SnSocialMonitor",
            "http://social.dev.snetwork.ru/SnSocialMonitor",
            "https://snmonitor.snwall.ru"
    );
    private static final BaseURL SOCIAL_MONITORING_BASE_URL = new BaseURL(
            "http://localhost:8084/SnMonitoring",
            "http://social.dev.snetwork.ru/SnMonitoring",
            "https://monitoring.sn-mg.ru"
    );
    private static final BaseURL SOCIAL_STREAMING_BASE_URL = new BaseURL(
            "http://localhost:8084/SnSocialStreaming",
            "http://sncontest.dev.snetwork.ru/SnSocialStream",
            "https://snstream.snwall.ru"
    );
    private static final BaseURL SOCIAL_CONTEST_BASE_URL = new BaseURL(
            "http://localhost:8084/SnSocialContest",
            "http://social.dev.snetwork.ru/SnSocialContest",
            "https://sncontest.snwall.ru"
    );
    private static final BaseURL MESSAGING_CONTROLLER_BASE_URL = new BaseURL(
            "http://localhost:8084/SnMsgController",
            "http://social.dev.snetwork.ru/SnMsgControler",
            "https://messaging.sn-mg.ru"
    );
    private static final BaseURL MESSAGING_TELEGRAM_BASE_URL = new BaseURL(
            null,
            null,
            null
    );
    private static final BaseURL MESSAGING_VKONTAKTE_BASE_URL = new BaseURL(
            null,
            null,
            null
    );
    private static final BaseURL MESSAGING_FACEBOOK_BASE_URL = new BaseURL(
            null,
            null,
            null
    );

    private final String id;

    public Product(String id) {
        if (!ArrayUtils.contains(PRODUCTS_IDS, id)) {
            throw new UnsupportedOperationException("Unsupported ProductId: " + id);
        }
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getBaseURL(BuildEnviroment buildEnviroment) {
        BaseURL baseURL = null;
        switch (id) {
            case SOCIAL_MONITOR:
                baseURL = SOCIAL_MONITOR_BASE_URL;
                break;
            case SOCIAL_MONITORING:
                baseURL = SOCIAL_MONITORING_BASE_URL;
                break;
            case SOCIAL_STREAMING:
                baseURL = SOCIAL_STREAMING_BASE_URL;
                break;
            case SOCIAL_CONTEST:
                baseURL = SOCIAL_CONTEST_BASE_URL;
                break;
            case MESSAGING_CONTROLLER:
                baseURL = MESSAGING_CONTROLLER_BASE_URL;
                break;
            case MESSAGING_TELEGRAM:
                baseURL = MESSAGING_TELEGRAM_BASE_URL;
                break;
            case MESSAGING_VKONTAKTE:
                baseURL = MESSAGING_VKONTAKTE_BASE_URL;
                break;
            case MESSAGING_FACEBOOK:
                baseURL = MESSAGING_FACEBOOK_BASE_URL;
                break;
        }
        if (baseURL != null) {
            switch (buildEnviroment) {
                case LOCAL:
                    return baseURL.getLocal();
                case DEV:
                    return baseURL.getDev();
                case PROD:
                    return baseURL.getProd();
            }
        }
        return null;
    }

    public static Product getById(String id) {
        return new Product(id);
    }

    public static Product getByBaseURL(String baseURL) {
        BuildEnviroment buildEnviroment = getBuildEnviromentByBaseURL(baseURL);
        return getByBaseURL(baseURL, buildEnviroment);
    }

    public static Product getByBaseURL(String baseURL, BuildEnviroment buildEnviroment) {
        try {
            switch (buildEnviroment) {
                case LOCAL:
                    if (isBaseURLsMatching(baseURL, SOCIAL_MONITOR_BASE_URL.getLocal())) {
                        return new Product(SOCIAL_MONITOR);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_MONITORING_BASE_URL.getLocal())) {
                        return new Product(SOCIAL_MONITORING);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_STREAMING_BASE_URL.getLocal())) {
                        return new Product(SOCIAL_STREAMING);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_CONTEST_BASE_URL.getLocal())) {
                        return new Product(SOCIAL_CONTEST);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_CONTROLLER_BASE_URL.getLocal())) {
                        return new Product(MESSAGING_CONTROLLER);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_TELEGRAM_BASE_URL.getLocal())) {
                        return new Product(MESSAGING_TELEGRAM);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_VKONTAKTE_BASE_URL.getLocal())) {
                        return new Product(MESSAGING_VKONTAKTE);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_FACEBOOK_BASE_URL.getLocal())) {
                        return new Product(MESSAGING_FACEBOOK);
                    }
                    break;
                case DEV:
                    if (isBaseURLsMatching(baseURL, SOCIAL_MONITOR_BASE_URL.getDev())) {
                        return new Product(SOCIAL_MONITOR);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_MONITORING_BASE_URL.getDev())) {
                        return new Product(SOCIAL_MONITORING);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_STREAMING_BASE_URL.getDev())) {
                        return new Product(SOCIAL_STREAMING);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_CONTEST_BASE_URL.getDev())) {
                        return new Product(SOCIAL_CONTEST);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_CONTROLLER_BASE_URL.getDev())) {
                        return new Product(MESSAGING_CONTROLLER);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_TELEGRAM_BASE_URL.getDev())) {
                        return new Product(MESSAGING_TELEGRAM);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_VKONTAKTE_BASE_URL.getDev())) {
                        return new Product(MESSAGING_VKONTAKTE);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_FACEBOOK_BASE_URL.getDev())) {
                        return new Product(MESSAGING_FACEBOOK);
                    }
                    break;
                case PROD:
                    if (isBaseURLsMatching(baseURL, SOCIAL_MONITOR_BASE_URL.getProd())) {
                        return new Product(SOCIAL_MONITOR);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_MONITORING_BASE_URL.getProd())) {
                        return new Product(SOCIAL_MONITORING);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_STREAMING_BASE_URL.getProd())) {
                        return new Product(SOCIAL_STREAMING);
                    } else if (isBaseURLsMatching(baseURL, SOCIAL_CONTEST_BASE_URL.getProd())) {
                        return new Product(SOCIAL_CONTEST);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_CONTROLLER_BASE_URL.getProd())) {
                        return new Product(MESSAGING_CONTROLLER);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_TELEGRAM_BASE_URL.getProd())) {
                        return new Product(MESSAGING_TELEGRAM);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_VKONTAKTE_BASE_URL.getProd())) {
                        return new Product(MESSAGING_VKONTAKTE);
                    } else if (isBaseURLsMatching(baseURL, MESSAGING_FACEBOOK_BASE_URL.getProd())) {
                        return new Product(MESSAGING_FACEBOOK);
                    }
                    break;
            }

        } catch (MalformedURLException ex) {
            log.error(ex);
        }
        return null;
    }

    private static boolean isBaseURLsMatching(String baseURL1, String baseURL2) throws MalformedURLException {
        URL url1 = new URL(baseURL1);
        URL url2 = new URL(baseURL2);

        return url1.getHost().equalsIgnoreCase(url2.getHost())
                && url1.getPath().startsWith(url2.getPath());
    }

    public static BuildEnviroment getBuildEnviromentByBaseURL(String baseURL) {
        boolean isLocal = baseURL.startsWith("http://localhost");
        boolean isProd = baseURL.startsWith("https://");
        boolean isDev = !isLocal && !isProd;
        if (isLocal) {
            return BuildEnviroment.LOCAL;
        } else if (isDev) {
            return BuildEnviroment.DEV;
        } else if (isProd) {
            return BuildEnviroment.PROD;
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getId();
    }

}
