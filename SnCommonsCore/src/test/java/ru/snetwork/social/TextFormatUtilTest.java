package ru.snetwork.social;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.snetwork.social.util.TextFormatUtil;

/**
 *
 * @author ky6uHETc
 */
public class TextFormatUtilTest {

    public TextFormatUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTimingSeconds() {
        /*
        System.out.println("--- TextFormatUtil test format seconds ---");

        System.out.println(" - format null seconds: " + TextFormatUtil.formatTimingSeconds(null));
        System.out.println(" - format 0 seconds: " + TextFormatUtil.formatTimingSeconds(0));
        System.out.println(" - format 193 seconds: " + TextFormatUtil.formatTimingSeconds(193));
        System.out.println(" - format 6193 seconds: " + TextFormatUtil.formatTimingSeconds(6193));
        System.out.println(" - parse 0a0:00:00: " + TextFormatUtil.parseTimingSeconds("0a0:00:00"));
        System.out.println(" - parse 00:00:00: " + TextFormatUtil.parseTimingSeconds("00:00:00"));
        System.out.println(" - parse 0:13:13: " + TextFormatUtil.parseTimingSeconds("0:13:13"));
        System.out.println(" - parse 00:13:13: " + TextFormatUtil.parseTimingSeconds("00:13:13"));
        System.out.println(" - parse 1:5:13: " + TextFormatUtil.parseTimingSeconds("1:5:13"));
        System.out.println(" - parse 1:05:13: " + TextFormatUtil.parseTimingSeconds("1:05:13"));
        System.out.println(" - parse 6:45:13: " + TextFormatUtil.parseTimingSeconds("6:45:13"));
        System.out.println(" - parse 06:45:13: " + TextFormatUtil.parseTimingSeconds("06:45:13"));
         */
    }
}
