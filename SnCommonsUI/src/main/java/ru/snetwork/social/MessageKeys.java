/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social;

/**
 *
 * @author ky6uhetc
 */
public class MessageKeys {

    public static final String _ENTRIES = ".entries";
    public static final String _EDIT = ".edit";
    public static final String _NEW = ".new";
    public static final String ADD = "Add";
    public static final String ADD_CHILD_ENTRY = "Add-child-entry";
    public static final String ALL = "All";
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String APPLY = "Apply";
    public static final String AUTO_REFRESH = "Auto-refresh";
    public static final String BACK = "Back";
    public static final String CANCEL = "Cancel";
    public static final String CHECK_INPUT_FIELDS = "Check-input-fields";
    public static final String CLEAR = "Clear";
    public static final String CLOSE = "Close";
    public static final String COLUMN_X = "column.";
    public static final String CONFIRM_DELETE = "confirm.delete";
    public static final String CONFIRM_DELETE_ENTRIES = "confirm.delete-entries";
    public static final String CONFIRM_DISABLE = "confirm.disable";
    public static final String CONFIRM_DISABLE_ENTRIES = "confirm.disable-entries";
    public static final String CONFIRM_RESTORE = "confirm.restore";
    public static final String CONFIRM_RESTORE_ENTRIES = "confirm.restore-entries";
    public static final String CREATE = "Create";
    public static final String DASHBOARD = "Dashboard";
    public static final String DATA_SAVED = "Data-saved";
    public static final String DATA_EXPORTING = "Data-exporting";
    public static final String DATA_PREPARING = "Data-preparing";
    public static final String DATA_PROCESSING = "Data-processing";
    public static final String DELETE = "Delete";
    public static final String DELETE_TASK = "Delete-task";
    public static final String DETAILS = "Details";
    public static final String DISABLE = "Disable";
    public static final String DOWNLOAD = "Download";
    public static final String EDIT = "Edit";
    public static final String EDIT_PROFILE = "Edit-profile";
    public static final String ENABLE = "Enable";
    public static final String ENTRY_DELETED = "Entry-deleted";
    public static final String ENTRY_RESTORED = "Entry-restored";
    public static final String ERROR_INCORRECT_USERAUTHTOKEN = "error.incorrect-userauthtoken";
    public static final String ERROR_INCORRECT_USERLOGIN = "error.incorrect-userlogin";
    public static final String ERROR_INCORRECT_PASSWORD = "error.incorrect-password";
    public static final String ERROR_CONFIRM_PASSWORD_DOES_NOT_MATCH = "error.confirm-password-does-not-match";
    public static final String ERROR_CONSTRAINT_VIOLATION = "error.constraint-violation";
    public static final String ERROR_CONSTRAINT_VIOLATION_DESCRIPTION = "error.constraint-violation-description";
    public static final String ERROR_CONSTRAINT_VIOLATION_DUPLICATE = "error.constraint-violation-duplicate";
    public static final String ERROR_CONSTRAINT_VIOLATION_DUPLICATE_DESCRIPTION = "error.constraint-violation-duplicate-description";
    public static final String ERROR_INVALID_INPUT_DATA = "error.invalid-input-data";
    public static final String ERROR_OCCURED = "Error-occured";
    public static final String ERROR_VALUE_REQUIRED = "error.value-required";
    public static final String EXPORT_COMPLETE = "Export-complete";
    public static final String EXPORT_DOWLOAND_AUTO_OR_LINK = "Export-dowload-auto-or-link";
    public static final String EXPORT_OUTPUTING = "Export-outputing";
    public static final String EXPORT_TO_XLS = "Export-to-XLS";
    public static final String FORBIDDEN = "Forbidden";
    public static final String GENERATE_ACCESS_TOKEN = "Generate-access-token";
    public static final String REGISTER_ACCESS_TOKEN = "Register-access-token";
    public static final String REQUESTED_ENTRY_NOT_FOUND = "Requested-entry-not-found";
    public static final String REQUESTED_RESOURCE_NOT_FOUND = "Requested-resource-not-found";
    public static final String MAIN_MENU_X = "MainMenu.";
    public static final String MONTH_X = "month-";
    public static final String MORE = "More";
    public static final String NEXT = "Next";
    public static final String NOT_FOUND = "Not-found";
    public static final String OPEN = "Open";
    public static final String OPEN_IN_NEW_TAB = "Open-in-new-tab";
    public static final String PASSWORD = "Password";
    public static final String PROPERTY_ = "property.";
    public static final String REFRESH = "Refresh";
    public static final String RESTORE = "Restore";
    public static final String SAVE = "Save";
    public static final String SAVE_FOR_ALL = "Save-for-all";
    public static final String SEARCH = "Search";
    public static final String SEARCH_BY_FULL_NAME = "Search-by-full-name";
    public static final String SEARCH_BY_KEYWORDS = "Search-by-keywords";
    public static final String SEARCH_BY_NAME = "Search-by-name";
    public static final String SIGN_OUT = "Sign-out";
    public static final String SIGN_IN = "Sign-in";
    public static final String SHOW = "Show";
    public static final String SHOWING_X = "Showing-x";
    public static final String SHOW_DISABLED = "Show-disabled";
    public static final String SHOW_X = "Show.";
    public static final String SYSTEM_INFORMATION = "System-information";
    public static final String SYSTEM_SETTINGS = "System-settings";
    public static final String SYSTEM_LOG = "System-log";
    public static final String TITLE_X = "title.";
    public static final String USERNAME = "Username";
    public static final String USER_ROLE = "User-role";
    public static final String USER_ROLE_X = "user-role.";
    public static final String VIEW = "View";
    public static final String WELCOME = "Welcome";
    public static final String X_ENTRIES = "x-entries";
    public static final String X_OF_TOTAL_X = "x-of-total-x";
    public static final String YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_REQUESTED_RESOURCE = "You-do-not-have-enough-privileges-to-the-requested-resource";
    public static final String YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION = "You-do-not-have-enough-privileges-to-perform-the-action";

}
