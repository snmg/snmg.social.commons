/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social;

/**
 *
 * @author ky6uhetc
 */
public class EntityPropertyKeys {

    public static final String ACCOUNT_NON_EXPIRED = "accountNonExpired";
    public static final String ACCOUNT_NON_LOCKED = "accountNonLocked";
    public static final String AUTHORITIES = "authorities";
    public static final String CREDENTIALS_NON_EXPIRED = "credentialsNonExpired";
    public static final String DATE_CREATED_AT = "dateCreatedAt";
    public static final String DATE_DELETED_AT = "dateDeletedAt";
    public static final String DATE_DISABLED_AT = "dateDisabledAt";
    public static final String DATE_MODIFIED_AT = "dateModifiedAt";
    public static final String DELETED = "deleted";
    public static final String DESCRIPTION = "description";
    public static final String DISABLED = "disabled";
    public static final String DOMAIN = "domain";
    public static final String ENABLED = "enabled";
    public static final String GENDER = "gender";
    public static final String GUEST = "guest";
    public static final String FIRST_NAME = "firstName";
    public static final String FULL_NAME = "fullName";
    public static final String ID = "id";
    public static final String LAST_NAME = "lastName";
    public static final String LOCALE = "locale";
    public static final String NAME = "name";
    public static final String ORDER_NO = "orderNo";
    public static final String PARENT = "parent";
    public static final String STATE = "state";
    public static final String SYSTEM = "system";
    public static final String TIMEZONE = "timezone";
    public static final String USER_AUTH_SKIP_PROVIDER = "userAuthSkipProvider";
    public static final String USER_AUTH_TOKEN = "userAuthToken";
    public static final String USER_CREATED = "userCreated";
    public static final String USER_DELETED = "userDeleted";
    public static final String USER_LOGIN = "userLogin";
    public static final String USER_MODIFIED = "userModified";
    public static final String USER_PASSWORD = "userPassword";
    public static final String USER_ROLES = "userRoles";
    public static final String USERNAME = "username";
    public static final String UUID = "uuid";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_CHANGE = "passwordChange";
    public static final String PASSWORD_CONFIRM = "passwordConfirm";
    public static final String DETAILED_NAME_RU = "detailedNameRu";
    public static final String GEO_FIAS_ID = "fiasId";

}
