/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.event;

import com.vaadin.navigator.View;
import com.vaadin.server.VaadinRequest;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.view.AbstractEntriesComponent;

/**
 *
 * @author ky6uHETc
 */
public abstract class SnSocialUIEvent {

    final boolean general;

    public SnSocialUIEvent(boolean crossSession) {
        this.general = crossSession;
    }

    public static class BrowserRefresh extends SnSocialUIEvent {

        private final VaadinRequest request;

        public BrowserRefresh(VaadinRequest request) {
            super(false);
            this.request = request;
        }

        public VaadinRequest getRequest() {
            return request;
        }

    }

    public static class BrowserResize extends SnSocialUIEvent {

        private final Integer height;
        private final Integer width;

        public BrowserResize(Integer width, Integer height) {
            super(false);
            this.width = width;
            this.height = height;
        }

        public Integer getHeight() {
            return height;
        }

        public Integer getWidth() {
            return width;
        }

    }

    public static final class UserEnteredViewEvent extends SnSocialUIEvent {

        private final EntityModelSystemUser user;
        private final Class<? extends View> viewType;
        private final String scopeId;

        public UserEnteredViewEvent(EntityModelSystemUser user, Class<? extends View> viewType, String scopeId) {
            super(true);

            this.user = user;
            this.viewType = viewType;
            this.scopeId = scopeId;
        }

        public EntityModelSystemUser getUser() {
            return user;
        }

        public Class<? extends View> getViewType() {
            return viewType;
        }

        public String getScopeId() {
            return scopeId;
        }

    }

    public static final class UserLeftViewEvent extends SnSocialUIEvent {

        private final EntityModelSystemUser user;
        private final Class<? extends View> viewType;
        private final String scopeId;

        public UserLeftViewEvent(EntityModelSystemUser user, Class<? extends View> viewType, String scopeId) {
            super(true);

            this.user = user;
            this.viewType = viewType;
            this.scopeId = scopeId;
        }

        public EntityModelSystemUser getUser() {
            return user;
        }

        public Class<? extends View> getViewType() {
            return viewType;
        }

        public String getScopeId() {
            return scopeId;
        }

    }

    public static final class UserLoggedInEvent extends SnSocialUIEvent {

        private final EntityModelSystemUser user;

        public UserLoggedInEvent(EntityModelSystemUser user) {
            super(false);
            this.user = user;
        }

        public EntityModelSystemUser getUser() {
            return user;
        }

    }

    public static class UserLoggedOutEvent extends SnSocialUIEvent {

        private final EntityModelSystemUser user;

        public UserLoggedOutEvent(EntityModelSystemUser user) {
            super(true);
            this.user = user;
        }

        public EntityModelSystemUser getUser() {
            return user;
        }

    }

    public static final class UserUpdatedEvent extends SnSocialUIEvent {

        private final EntityModelSystemUser user;

        public UserUpdatedEvent(EntityModelSystemUser user) {
            super(false);
            this.user = user;
        }

        public EntityModelSystemUser getUser() {
            return user;
        }

    }

    public static class UserLogOutRequestedEvent extends SnSocialUIEvent {

        public UserLogOutRequestedEvent() {
            super(false);
        }

    }

    public static class SidebarDialogDataRequest extends SnSocialUIEvent {

        private final Long userId;
        private SnSocialUI sender;
        
        public SidebarDialogDataRequest(Long userId, SnSocialUI sender) {
            super(true);
            this.userId = userId;
            this.sender = sender;
        }

        public Long getUserId() {
            return userId;
        }

        public SnSocialUI getSender() {
            return sender;
        }
        
    }
}
