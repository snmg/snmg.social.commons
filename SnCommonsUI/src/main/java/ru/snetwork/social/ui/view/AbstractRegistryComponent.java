/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.addon.contextmenu.Menu;
import com.vaadin.addon.contextmenu.MenuItem;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.hene.popupbutton.PopupButton;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.dao.AbstractRegistryDaoBean;
import ru.snetwork.social.dao.query.JoinClauses;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.dao.query.WhereClauseIsNotNull;
import ru.snetwork.social.dao.query.WhereClauseIsNull;
import ru.snetwork.social.dao.query.WhereClauseNumber;
import ru.snetwork.social.dao.query.WhereClauseString;
import ru.snetwork.social.exception.DaoUnableToDeleteObjectException;
import ru.snetwork.social.model.EntityHierarchical;
import ru.snetwork.social.model.EntityModelRegistry;
import ru.snetwork.social.model.EntityOrderable;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.SnSocialUIComponent;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.exception.NoDialogForEntityClassException;
import ru.snetwork.social.ui.field.TreeComboBox;
import ru.snetwork.social.ui.helper.ShowOptions;

/**
 *
 * @author ky6uHETc
 * @param <T> Entity class
 */
public abstract class AbstractRegistryComponent<T extends EntityModelRegistry, U extends SnSocialUI> extends VerticalLayout implements Button.ClickListener, SnSocialUIComponent {

    protected final Log log = LogFactory.getLog(getClass());

    protected boolean refreshing = false;

    protected final Class<T> registryClass;

    // detect registryClass
    {
        if (this.getClass().getGenericSuperclass() instanceof ParameterizedType) {
            registryClass = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } else if (this.getClass().getGenericSuperclass() instanceof Class
                && ((Class) this.getClass().getGenericSuperclass()).getGenericSuperclass() instanceof ParameterizedType) {
            registryClass = (Class) ((ParameterizedType) ((Class) this.getClass().getGenericSuperclass()).getGenericSuperclass()).getActualTypeArguments()[0];
        } else {
            registryClass = null;
        }
    }

    protected final U ui;
    protected final AbstractRegistryDaoBean dao;
    protected String captionKey;

    protected final HorizontalLayout layoutHeader = new HorizontalLayout();
    private final HorizontalLayout layoutHeaderFilter = new HorizontalLayout();
    private final VerticalLayout layoutHeaderFilterMore = new VerticalLayout();
    protected final HorizontalLayout layoutHeaderButtons = new HorizontalLayout();
    private final Label layoutHeaderButtonsSpacer = new Label(" ");
    protected final TextField fSearch = new TextField();
    private final PopupButton bFilterMore = new PopupButton(MessageKeys.MORE);
    private final Button bFilterReset = new Button();
    protected final OptionGroup ogShowDisabled = new OptionGroup();
    protected final Button bEdit = new Button(MessageKeys.EDIT, this);
    protected final Button bDisable = new Button(MessageKeys.DISABLE, this);
    protected final Button bEnable = new Button(MessageKeys.ENABLE, this);
    protected final Button bDelete = new Button(MessageKeys.DELETE, this);
    protected final Button[] actionButtons = new Button[]{bEdit, bDisable, bEnable, bDelete};
    protected final Button bAdd = new Button(MessageKeys.ADD, this);

    private final BeanItemContainer<T> containerEntries = new BeanItemContainer<>(registryClass);
    protected final Table tableEntries;
    protected boolean preventTableEntriesFullResetPageBuffer = false;
    protected final Set<T> tableEntriesSelection = new LinkedHashSet<>();
    private final ContextMenu tableEntriesContextMenu;
    private final Menu.Command tableEntriesContextMenuCommand = new Menu.Command() {
        @Override
        public void menuSelected(MenuItem selectedItem) {
            contextMenuSelected(selectedItem);
        }
    };
    protected final MenuItem tableEntriesContextMenuItemEdit;
    protected final MenuItem tableEntriesContextMenuItemDisable;
    protected final MenuItem tableEntriesContextMenuItemEnable;
    protected final MenuItem tableEntriesContextMenuItemDelete;
    protected final MenuItem tableEntriesContextMenuSeparator1;
    protected final MenuItem tableEntriesContextMenuItemAddChild;
    private boolean tableEntriesSortAscending = true;
    private Object tableEntriesSortPropertyId = EntityPropertyKeys.ID;

    protected final HorizontalLayout layoutFooter = new HorizontalLayout();
    protected final Label labelCount = new Label();
    protected final Button bRefresh = new Button(MessageKeys.REFRESH, this);
    protected final CheckBox cbAutoRefresh = new CheckBox(MessageKeys.AUTO_REFRESH);

    public AbstractRegistryComponent(U ui) throws NoDaoBeanForEntityClassException {
        this.ui = ui;
        this.dao = (AbstractRegistryDaoBean) ui.getDaoBean(registryClass);
        this.captionKey = registryClass.getSimpleName() + MessageKeys._ENTRIES;
        this.tableEntries = (EntityHierarchical.class.isAssignableFrom(registryClass))
                ? new TreeTable(null, containerEntries) {
            @Override
            protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
                String result = formatTablePropertyValue(rowId, colId, property);
                return result != null ? result : super.formatPropertyValue(rowId, colId, property);
            }

            @Override
            public boolean isSortAscending() {
                return tableEntriesSortAscending;
            }

            @Override
            public Object getSortContainerPropertyId() {
                return tableEntriesSortPropertyId;
            }

            @Override
            public void setSortAscending(boolean ascending) {
                boolean refresh = tableEntriesSortAscending != ascending;
                tableEntriesSortAscending = ascending;
                if (refresh) {
                    refreshData();
                }
            }

            @Override
            public void setSortContainerPropertyId(Object propertyId) {
                boolean refresh = !Objects.equals(tableEntriesSortPropertyId, propertyId);
                tableEntriesSortPropertyId = propertyId;
                if (refresh) {
                    refreshData();
                }
            }

        }
                : new RegistryTable(null, containerEntries) {
            @Override
            protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
                String result = formatTablePropertyValue(rowId, colId, property);
                return result != null ? result : super.formatPropertyValue(rowId, colId, property);
            }

        };
        this.tableEntriesContextMenu = new ContextMenu(tableEntries, false);
        this.tableEntriesContextMenuItemEdit = tableEntriesContextMenu.addItem(MessageKeys.EDIT, FontAwesome.EDIT, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuItemDisable = tableEntriesContextMenu.addItem(MessageKeys.DISABLE, FontAwesome.LOCK, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuItemEnable = tableEntriesContextMenu.addItem(MessageKeys.ENABLE, FontAwesome.UNLOCK, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuItemDelete = tableEntriesContextMenu.addItem(MessageKeys.DELETE, FontAwesome.TRASH, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuSeparator1 = tableEntriesContextMenu.addSeparator();
        this.tableEntriesContextMenuItemAddChild = tableEntriesContextMenu.addItem(MessageKeys.ADD_CHILD_ENTRY, FontAwesome.PLUS, tableEntriesContextMenuCommand);

        // component settings
        setId(registryClass.getSimpleName().toLowerCase());
        setSizeFull();
        setCaption(captionKey);
        setStyleName("registry-entries");
        addStyleName(registryClass.getSimpleName());

        // content
        buildHeader();
        buildTable();
        buildFooter();
 
    }

    protected String formatTablePropertyValue(Object rowId, Object colId, Property<?> property) {
        return ui.formatPropertyValue(SnSocialUI.DATETIME_PATTERN, (String) colId, property.getType(), property.getValue());
    }

    protected void actionCreate() throws InstantiationException, IllegalAccessException, NoDaoBeanForEntityClassException, NoDialogForEntityClassException {
        if (bAdd.isEnabled()) {
            ui.dialog(registryClass.newInstance());
        }
    }

    protected void actionEdit(T o) throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException {
        if (bEdit.isEnabled()) {
            ui.dialog(o);
        }
    }

    protected void actionDelete(T o) throws DaoUnableToDeleteObjectException {
        if (bDelete.isEnabled()) {
            ConfirmDialog.show(ui,
                    ui.getMessage(MessageKeys.DELETE),
                    ui.getMessage(MessageKeys.CONFIRM_DELETE_ENTRIES),
                    ui.getMessage(MessageKeys.DELETE),
                    ui.getMessage(MessageKeys.CANCEL),
                    new ConfirmDialog.Listener() {
                @Override
                public void onClose(ConfirmDialog cd) {
                    try {
                        if (cd.isConfirmed()) {
                            dao.doDelete((EntityModelRegistry) dao.getObjectFetchedAll(o), ui.getUser());
                            refresh();
                        }
                    } catch (Exception ex) {
                        ui.handleException(ex);
                    }
                }
            }).getOkButton().setStyleName(ValoTheme.BUTTON_DANGER);
        }
    }

    protected void actionDisable(T o, boolean recursiveCall) {
        if (bEdit.isEnabled()) {
            if (tableEntries instanceof TreeTable && ((TreeTable) tableEntries).hasChildren(o)) {
                ((TreeTable) tableEntries).getChildren(o).forEach((child) -> {
                    actionDisable((T) child, true);
                });
            }
            dao.doDisable((EntityModelRegistry) dao.getObjectFetchedAll(o), ui.getUser());
        }
    }

    protected void actionEnable(T o, boolean recursiveCall) {
        if (bEdit.isEnabled()) {
            if (tableEntries instanceof TreeTable && ((TreeTable) tableEntries).hasChildren(o)) {
                ((TreeTable) tableEntries).getChildren(o).forEach((child) -> {
                    actionEnable((T) child, true);
                });
            }
            dao.doEnable((EntityModelRegistry) dao.getObjectFetchedAll(o), ui.getUser());
        }
    }

    protected void addHeaderFilterComponent(Component c) {
        layoutHeaderFilter.addComponent(c, layoutHeaderFilter.getComponentIndex(bFilterMore));
        layoutHeaderFilter.setComponentAlignment(c, Alignment.MIDDLE_LEFT);
    }

    protected void removeHeaderFilterComponent(Component c) {
        layoutHeaderFilter.removeComponent(c);
    }

    protected void addHeaderMoreComponent(Component c) {
        layoutHeaderFilterMore.addComponent(c);
        bFilterMore.setVisible(true);
    }

    protected void removeHeaderMoreComponent(Component c) {
        layoutHeaderFilterMore.removeComponent(c);
        bFilterMore.setVisible(layoutHeaderFilterMore.getComponentCount() > 0);
    }

    protected void removeHeaderMoreAllComponents() {
        layoutHeaderFilterMore.removeAllComponents();
        bFilterMore.setVisible(false);
    }

    protected void addHeaderButtonComponent(Component c) {
        addHeaderButtonComponent(c, false);
    }

    protected void addHeaderButtonComponent(Component c, boolean afterAddButton) {
        if (!afterAddButton && bAdd.getParent() != null && bAdd.getParent() == layoutHeaderButtons) {
            layoutHeaderButtons.addComponent(c, layoutHeaderButtons.getComponentIndex(bAdd));
        } else {
            layoutHeaderButtons.addComponent(c);
        }
    }

    protected void removeHeaderButtonComponent(Component c) {
        layoutHeaderButtons.removeComponent(c);
    }

    private void buildHeader() {
        layoutHeader.setStyleName("viewheader");
        layoutHeader.setWidth("100%");
        layoutHeader.setSpacing(true);
        layoutHeader.addComponent(layoutHeaderFilter);
        layoutHeader.addComponent(layoutHeaderButtons);
        addComponent(layoutHeader);

        // Filter
        layoutHeaderFilter.setSpacing(true);
        fSearch.setIcon(FontAwesome.SEARCH);
        fSearch.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        fSearch.setImmediate(true);
        fSearch.setWidth("300px");
        fSearch.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                refreshData();
            }
        });
        layoutHeaderFilter.addComponent(fSearch);
        layoutHeaderFilter.setComponentAlignment(fSearch, Alignment.MIDDLE_LEFT);
        layoutHeaderFilterMore.setSpacing(true);
        layoutHeaderFilterMore.setMargin(true);
        layoutHeaderFilterMore.setWidth("350px");
        layoutHeaderFilterMore.setStyleName("viewheader");
        bFilterMore.setContent(layoutHeaderFilterMore);
        bFilterMore.setStyleName(ValoTheme.BUTTON_SMALL);
        bFilterMore.setVisible(false);
        layoutHeaderFilter.addComponent(bFilterMore);
        layoutHeaderFilter.setComponentAlignment(bFilterMore, Alignment.MIDDLE_LEFT);
        ogShowDisabled.setStyleName("horizontal small");
        ogShowDisabled.addItems(ShowOptions.valuesDisabled());
        ogShowDisabled.setValue(ShowOptions.ONLY_ACTIVE);
        ogShowDisabled.setImmediate(true);
        ogShowDisabled.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                refreshData();
            }
        });
        layoutHeaderFilter.addComponent(ogShowDisabled);
        layoutHeaderFilter.setComponentAlignment(ogShowDisabled, Alignment.MIDDLE_LEFT);
        bFilterReset.addClickListener(this);
        bFilterReset.setIcon(FontAwesome.TIMES);
        bFilterReset.setStyleName(ValoTheme.BUTTON_SMALL);
        bFilterReset.setVisible(false);
        layoutHeaderFilter.addComponent(bFilterReset);
        layoutHeaderFilter.setComponentAlignment(bFilterMore, Alignment.MIDDLE_LEFT);
        layoutHeader.setExpandRatio(layoutHeaderFilter, 1.0f);

        // Buttons
        layoutHeaderButtons.setSpacing(true);
        bEdit.setStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bEdit);
        bEnable.setStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bEnable);
        bDisable.setStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bDisable);
        bDelete.setStyleName(ValoTheme.BUTTON_DANGER);
        bDelete.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bDelete);
        layoutHeaderButtons.addComponent(layoutHeaderButtonsSpacer);
        bAdd.setStyleName(ValoTheme.BUTTON_PRIMARY);
        bAdd.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bAdd);
    }

    private void buildTable() {
        tableEntries.setPageLength(30);
        tableEntries.setStyleName("viewentries");
        tableEntries.setSizeFull();
        if (EntityOrderable.class.isAssignableFrom(registryClass)) {
            tableEntries.setVisibleColumns(
                    EntityPropertyKeys.ID,
                    EntityPropertyKeys.NAME,
                    EntityPropertyKeys.ORDER_NO,
                    EntityPropertyKeys.DESCRIPTION);
            tableEntries.setColumnWidth(EntityPropertyKeys.ORDER_NO, 75);
        } else {
            tableEntries.setVisibleColumns(
                    EntityPropertyKeys.ID,
                    EntityPropertyKeys.NAME,
                    EntityPropertyKeys.DESCRIPTION);
        }
        tableEntries.setColumnAlignment(EntityPropertyKeys.ID, Table.Align.RIGHT);
        tableEntries.setColumnWidth(EntityPropertyKeys.ID, 75);
        tableEntries.setColumnExpandRatio(EntityPropertyKeys.NAME, 0.4f);
        tableEntries.setColumnExpandRatio(EntityPropertyKeys.DESCRIPTION, 0.6f);
        tableEntries.setSelectable(true);
        tableEntries.setMultiSelect(true);
        tableEntries.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                try {
                    if (event.isDoubleClick()) {
                        actionEdit((T) event.getItemId());
                    } else if (MouseEventDetails.MouseButton.RIGHT.equals(event.getButton())) {
                        if (!tableEntriesSelection.contains(event.getItemId())) {
                            if (event.isCtrlKey()) {
                                tableEntries.select(event.getItemId());
                            } else {
                                tableEntries.setValue(null);
                                tableEntries.select(event.getItemId());
                            }
                        }
                        tableEntriesContextMenu.open(event.getClientX(), event.getClientY());
                        contextMenuOpened((T) event.getItemId(), event.getItem(), event.getPropertyId());
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
        tableEntries.setCellStyleGenerator(new Table.CellStyleGenerator() {
            @Override
            public String getStyle(Table source, Object itemId, Object propertyId) {
                StringBuilder sb = new StringBuilder();
                T entry = (T) itemId;
                Item item = source.getItem(itemId);
                if (propertyId == null) {
                    if (item != null
                            && item.getItemProperty(EntityPropertyKeys.STATE) != null
                            && item.getItemProperty(EntityPropertyKeys.STATE).getValue() != null) {
                        sb.append(item.getItemProperty(EntityPropertyKeys.STATE).getValue().toString().toLowerCase());
                    }
                    if (entry.isDisabled()) {
                        sb.append(" disabled");
                    }
                    if (entry.isDeleted()) {
                        sb.append(" deleted");
                    }
                } else {
                    return "column-" + propertyId.toString();
                }
                return sb.toString();
            }
        });
        tableEntries.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object value = event.getProperty().getValue();
                tableEntriesSelection.clear();
                if (value != null && value instanceof Collection) {
                    ((Collection) value).iterator().forEachRemaining(new Consumer() {
                        @Override
                        public void accept(Object o) {
                            if (o != null && registryClass.isInstance(o)) {
                                tableEntriesSelection.add((T) o);
                            }
                        }
                    });
                } else if (value != null && registryClass.isInstance(value)) {
                    tableEntriesSelection.add((T) value);
                }
                refreshActions();
            }
        });
//        tableEntries.addContextClickListener(listener);
        tableEntries.setSortEnabled(true);
        tableEntries.setSortContainerPropertyId(EntityPropertyKeys.NAME);
        addComponent(tableEntries);
        setExpandRatio(tableEntries, 1.0f);
        if (tableEntries instanceof TreeTable) {
            ((TreeTable) tableEntries).setHierarchyColumn(EntityPropertyKeys.NAME);
        }
    }

    private void buildFooter() {
        layoutFooter.setStyleName("viewfooter");
        layoutFooter.setWidth("100%");
        layoutFooter.setSpacing(true);
        layoutFooter.addComponent(labelCount);
        layoutFooter.setComponentAlignment(labelCount, Alignment.MIDDLE_LEFT);
        layoutFooter.setExpandRatio(labelCount, 1.0f);
        cbAutoRefresh.setImmediate(true);
        layoutFooter.addComponent(cbAutoRefresh);
        layoutFooter.setComponentAlignment(cbAutoRefresh, Alignment.MIDDLE_RIGHT);
        bRefresh.setImmediate(true);
        bRefresh.setStyleName(ValoTheme.BUTTON_TINY);
        layoutFooter.addComponent(bRefresh);
        layoutFooter.setComponentAlignment(bRefresh, Alignment.MIDDLE_RIGHT);
        addComponent(layoutFooter);
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        try {
            if (event.getButton() == bAdd) {
                actionCreate();
            } else if (event.getButton() == bEdit && tableEntriesSelection.size() == 1) {
                actionEdit(tableEntriesSelection.iterator().next());
            } else if (event.getButton() == bDelete && tableEntriesSelection.size() == 1) {
                actionDelete((T) tableEntriesSelection.iterator().next());
            } else if (event.getButton() == bEnable) {
                tableEntriesSelection.iterator().forEachRemaining(new Consumer<T>() {
                    @Override
                    public void accept(T t) {
                        actionEnable(t, false);
                    }
                });
                tableEntries.setValue(null);
                refresh();
            } else if (event.getButton() == bDisable) {
                ConfirmDialog.show(ui,
                        ui.getMessage(MessageKeys.DISABLE),
                        ui.getMessage(MessageKeys.CONFIRM_DISABLE_ENTRIES),
                        ui.getMessage(MessageKeys.DISABLE),
                        ui.getMessage(MessageKeys.CANCEL),
                        new ConfirmDialog.Listener() {
                    @Override
                    public void onClose(ConfirmDialog cd) {
                        if (cd.isConfirmed()) {
                            tableEntriesSelection.iterator().forEachRemaining(new Consumer<T>() {
                                @Override
                                public void accept(T t) {
                                    actionDisable(t, false);
                                }
                            });
                            tableEntries.setValue(null);
                            refresh();
                        }
                    }
                });
            } else if (event.getButton() == bFilterReset) {
                resetFilters();
            } else if (event.getButton() == bRefresh) {
                refresh();
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    protected void contextMenuAddItemBefore(String caption, Resource icon) {
        tableEntriesContextMenu.addItem(caption, icon, tableEntriesContextMenuCommand);
    }

    protected void contextMenuAddItemBefore(String caption, Resource icon, MenuItem addBefore) {
        tableEntriesContextMenu.addItemBefore(caption, icon, tableEntriesContextMenuCommand, addBefore);
    }

    protected void contextMenuOpened(T tableItemId, Item tableItem, Object tablePropertyId) {

        tableEntriesContextMenuItemEdit.setVisible(true);
        tableEntriesContextMenuItemEnable.setVisible(tableItemId.isDisabled());
        tableEntriesContextMenuItemDisable.setVisible(!tableItemId.isDisabled());
        tableEntriesContextMenuItemDelete.setVisible(tableItemId.isDisabled());
        tableEntriesContextMenuSeparator1.setVisible(tableEntries instanceof TreeTable);
        tableEntriesContextMenuItemAddChild.setVisible(tableEntries instanceof TreeTable);

    }

    protected void contextMenuSelected(MenuItem menuItem) {
        try {
            if (tableEntriesContextMenuItemEdit == menuItem && tableEntriesSelection.size() == 1) {
                actionEdit((T) tableEntriesSelection.iterator().next());
            } else if (tableEntriesContextMenuItemDelete == menuItem && tableEntriesSelection.size() == 1) {
                actionDelete((T) tableEntriesSelection.iterator().next());
            } else if (tableEntriesContextMenuItemEnable == menuItem) {
                tableEntriesSelection.iterator().forEachRemaining(new Consumer<T>() {
                    @Override
                    public void accept(T t) {
                        actionEnable(t, false);
                    }
                });
                tableEntries.setValue(null);
                refresh();
            } else if (tableEntriesContextMenuItemDisable == menuItem) {
                ConfirmDialog.show(ui, ui.getMessage(MessageKeys.CONFIRM_DISABLE_ENTRIES), new ConfirmDialog.Listener() {
                    @Override
                    public void onClose(ConfirmDialog cd) {
                        if (cd.isConfirmed()) {
                            tableEntriesSelection.iterator().forEachRemaining(new Consumer<T>() {
                                @Override
                                public void accept(T t) {
                                    actionDisable(t, false);
                                }
                            });
                            tableEntries.setValue(null);
                            refresh();
                        }
                    }
                });
            } else if (tableEntriesContextMenuItemAddChild == menuItem) {
                T o = registryClass.newInstance();
                ((EntityHierarchical) o).setParent((T) tableEntriesSelection.iterator().next());
                actionEdit(o);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    @Override
    public final void localize() {
        setCaption(ui.getMessage(captionKey));
        fSearch.setInputPrompt(ui.getMessage(MessageKeys.SEARCH));
        ogShowDisabled.setDescription(ui.getMessage(MessageKeys.SHOW_DISABLED));
        for (Object o : ogShowDisabled.getItemIds()) {
            ogShowDisabled.setItemCaption(o, ui.getMessage(MessageKeys.SHOW_X.concat(String.valueOf(o))));
        }
        bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE));
        bAdd.setCaption(ui.getMessage(MessageKeys.ADD));
        bEdit.setCaption(ui.getMessage(MessageKeys.EDIT));
        bEnable.setCaption(ui.getMessage(MessageKeys.ENABLE));
        bDelete.setCaption(ui.getMessage(MessageKeys.DELETE));
        bDisable.setCaption(ui.getMessage(MessageKeys.DISABLE));
        bRefresh.setCaption(ui.getMessage(MessageKeys.REFRESH));
        tableEntriesContextMenuItemEdit.setText(ui.getMessage(MessageKeys.EDIT));
        tableEntriesContextMenuItemEnable.setText(ui.getMessage(MessageKeys.ENABLE));
        tableEntriesContextMenuItemDisable.setText(ui.getMessage(MessageKeys.DISABLE));
        tableEntriesContextMenuItemDelete.setText(ui.getMessage(MessageKeys.DELETE));
        tableEntriesContextMenuItemAddChild.setText(ui.getMessage(registryClass.getSimpleName() + "." + MessageKeys.ADD_CHILD_ENTRY));
        for (Object columnId : tableEntries.getVisibleColumns()) {
            tableEntries.setColumnHeader(columnId, ui.getMessage(MessageKeys.COLUMN_X.concat(String.valueOf(columnId))));
        }
        localizeComponents();
    }

    protected abstract void localizeComponents();

    @Override
    public final void refresh() {

        if (!refreshing) {
            refreshing = true;
            try {
                refreshFilterControls();
                refreshData();
                refreshActions();

            } catch (Exception ex) {
                ui.handleException(ex);
            }
        }
        refreshing = false;

    }

    protected final void refreshFilterButtons() {
        boolean showResetButton = false;
        Iterator iterator1 = layoutHeaderFilter.iterator();
        while (iterator1.hasNext()) {
            Component c = (Component) iterator1.next();
            if (c instanceof Field
                    && !((Field) c).isEmpty()
                    && !MessageKeys.ALL.equals(((Field) c).getValue())
                    && !ShowOptions.ONLY_ACTIVE.equals(((Field) c).getValue())) {
                showResetButton = true;
                break;
            }
        }
        int countMoreFilters = 0;
        Iterator iterator2 = layoutHeaderFilterMore.iterator();
        while (iterator2.hasNext()) {
            Component c = (Component) iterator2.next();
            if (c instanceof Field
                    && !((Field) c).isEmpty()
                    && !MessageKeys.ALL.equals(((Field) c).getValue())
                    && !ShowOptions.ONLY_ACTIVE.equals(((Field) c).getValue())) {
                countMoreFilters++;
            }
        }
        if (countMoreFilters > 0) {
            bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE) + " (" + countMoreFilters + ")");
            bFilterMore.addStyleName(ValoTheme.BUTTON_PRIMARY);
            showResetButton = true;
        } else {
            bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE));
            bFilterMore.removeStyleName(ValoTheme.BUTTON_PRIMARY);
        }
        bFilterReset.setVisible(showResetButton && bFilterMore.isVisible());
    }

    /**
     * Refresh filter controls is a subclass has such controls.
     */
    protected abstract void refreshFilterControls();

    protected void refreshData() {

        // refresh filters buttons
        refreshFilterButtons();

        // total 
        int total = dao.count(null).intValue();

        // filter
        List<WhereClause> where = refreshWhereClauses();
        where.add(new WhereClauseNumber("o.id", WhereClause.Operator.gt, 0l));
        if (ShowOptions.ONLY_ACTIVE.equals(ogShowDisabled.getValue())) {
            where.add(new WhereClauseIsNull("o.dateDisabledAt"));
        } else if (ShowOptions.ONLY_DISABLED.equals(ogShowDisabled.getValue())) {
            where.add(new WhereClauseIsNotNull("o.dateDisabledAt"));
        }

        // search 
        if (StringUtils.isNotBlank(fSearch.getValue())) {
            String search = "%" + fSearch.getValue() + "%";
            where.add(new JoinClauses(JoinClauses.Logic.OR,
                    new WhereClauseString("name", WhereClause.Operator.ilike, search),
                    new WhereClauseString("description", WhereClause.Operator.ilike, search)));
        }

        // order
        OrderBy orderBy = null;
        if (tableEntries.getSortContainerPropertyId() != null) {
            orderBy = new OrderBy("o." + tableEntries.getSortContainerPropertyId(), !tableEntries.isSortAscending());
        }

        // retrieve list
        int count = dao.count(where).intValue();
        List<T> list = dao.list(where, 0, count, orderBy);

        // refresh components
        refreshTableItems(list);
        refreshCountLabel(count, total);

    }

    protected void refreshTableItems(List<T> list) throws Property.ReadOnlyException, UnsupportedOperationException {
        int tablePageStartIndex = tableEntries.getCurrentPageFirstItemIndex();
        int tablePageEndIndex = tablePageStartIndex + tableEntries.getPageLength();

        if (tableEntries instanceof TreeTable) {

            // refresh data
            containerEntries.removeAllItems();

            // add root items
            for (T entry : list) {
                if (((EntityHierarchical) entry).getParent() == null) {
                    containerEntries.addBean(entry);
                    ((TreeTable) tableEntries).setChildrenAllowed(entry, false);
                }
            }
            // add child items
            int loops = 0;
            boolean loopAgain = true;
            while (loopAgain && loops < list.size()) {
                loops++;
                loopAgain = false;
                for (T entry : list) {
                    if (!containerEntries.containsId(entry)
                            && containerEntries.containsId(((EntityHierarchical) entry).getParent())) {
                        ((TreeTable) tableEntries).setChildrenAllowed(((EntityHierarchical) entry).getParent(), true);
                        containerEntries.addBean(entry);
                        ((TreeTable) tableEntries).setParent(entry, ((EntityHierarchical) entry).getParent());
                        ((TreeTable) tableEntries).setChildrenAllowed(entry, false);
                    } else if (!containerEntries.containsId(entry)) {
                        loopAgain = true;
                    }
                }
            }
            // if there are entries not added to container
            if (loopAgain) {
                for (T entry : list) {
                    if (!containerEntries.containsId(entry)) {
                        containerEntries.addBean(entry);
                        ((TreeTable) tableEntries).setChildrenAllowed(entry, false);
                    }
                }
            }
        } else if (tablePageEndIndex > 0 && tablePageEndIndex < list.size()) {
            // Workaround to prevent table scroll-position jumping in Chrome
            preventTableEntriesFullResetPageBuffer = true;
            System.out.println("--- preventTableEntriesResetPageBuffer " + preventTableEntriesFullResetPageBuffer);
            boolean sort = false;
            // update entries retrieved from DB
            for (T registryEntry : list) {
                // 
                if (containerEntries.getItem(registryEntry) != null) {
                    Item foundItem = containerEntries.getItem(registryEntry);
                    BeanItem entryItem = new BeanItem(registryEntry, registryClass);
                    for (Object propertyId : entryItem.getItemPropertyIds()) {
                        Property property = foundItem.getItemProperty(propertyId);
                        boolean readonly = property.isReadOnly();
                        property.setReadOnly(false);
                        if (!property.isReadOnly()) {
                            foundItem.getItemProperty(propertyId).setValue(entryItem.getItemProperty(propertyId).getValue());
                            property.setReadOnly(readonly);
                        }
                    }
                } else {
                    containerEntries.addBean(registryEntry);
                    sort = true;
                }
            }
            tableEntries.refreshRowCache();
            // remove deleted entries
            Collection removedEntries = CollectionUtils.subtract(containerEntries.getItemIds(), list);
            if (!removedEntries.isEmpty()) {
                preventTableEntriesFullResetPageBuffer = false;
                for (Object r : removedEntries) {
                    containerEntries.removeItem(r);
                }
            }
            // if sort required
            if (sort) {
                tableEntries.sort();
            }
        } else {
            // refresh whole data
            containerEntries.removeAllItems();
            containerEntries.addAll(list);
        }
    }

    protected List<WhereClause> refreshWhereClauses() {
        return new LinkedList<WhereClause>();
    }

    protected void refreshActions() {

        for (Button b : actionButtons) {
            removeHeaderButtonComponent(b);
        }
        if (!tableEntriesSelection.isEmpty()) {
            if (tableEntriesSelection.size() == 1) {
                T o = tableEntriesSelection.iterator().next();
                addHeaderButtonComponent(bEdit);
                if (o.isDisabled()) {
                    addHeaderButtonComponent(bEnable);
                    bEnable.setVisible(!o.isSystem());
                    addHeaderButtonComponent(bDelete);
                    bDelete.setVisible(!o.isSystem());
                } else {
                    addHeaderButtonComponent(bDisable);
                    bDisable.setVisible(!o.isSystem());
                }
            } else {
                addHeaderButtonComponent(bDisable);
                bDelete.setVisible(true);
                addHeaderButtonComponent(bEnable);
                bEnable.setVisible(true);
            }
        }
    }

    protected void refreshCountLabel(int count, int total) {
        labelCount.setValue(
                count < total
                        ? ui.getMessage(MessageKeys.SHOWING_X) + ": "
                        + count + " " + ui.getMessage(MessageKeys.X_OF_TOTAL_X) + " "
                        + total + ui.getMessage(MessageKeys.X_ENTRIES)
                        : ui.getMessage(MessageKeys.SHOWING_X) + ": "
                        + total + ui.getMessage(MessageKeys.X_ENTRIES));
    }

    private void resetFilters() {
        Iterator iterator1 = layoutHeaderFilter.iterator();
        while (iterator1.hasNext()) {
            Component c = (Component) iterator1.next();
            resetFilterField(c);
        }
        Iterator iterator2 = layoutHeaderFilterMore.iterator();
        while (iterator2.hasNext()) {
            Component c = (Component) iterator2.next();
            resetFilterField(c);
        }
        refresh();
    }

    private void resetFilterField(Component c) throws Converter.ConversionException, Property.ReadOnlyException {
        if (c instanceof AbstractField && !((AbstractField) c).isEmpty()) {
            AbstractField field = (AbstractField) c;
            List<Property.ValueChangeListener> listeners = new LinkedList<>();
            for (Object l : field.getListeners(Property.ValueChangeEvent.class)) {
                if (l instanceof Property.ValueChangeListener) {
                    listeners.add((Property.ValueChangeListener) l);
                    field.removeValueChangeListener((Property.ValueChangeListener) l);
                }
            }
            if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(MessageKeys.ALL)) {
                field.setValue(MessageKeys.ALL);
            } else if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(ShowOptions.ONLY_ACTIVE)) {
                field.setValue(ShowOptions.ONLY_ACTIVE);
            } else {
                field.setValue(null);
            }
            for (Property.ValueChangeListener l : listeners) {
                field.addValueChangeListener(l);
            }
        } else if (c instanceof TreeComboBox && !((TreeComboBox) c).isEmpty()) {
            TreeComboBox tree = (TreeComboBox) c;
            List<Property.ValueChangeListener> listeners = new LinkedList<>();
            for (Object l : tree.getListeners(Property.ValueChangeEvent.class)) {
                if (l instanceof Property.ValueChangeListener) {
                    listeners.add((Property.ValueChangeListener) l);
                    tree.removeValueChangeListener((Property.ValueChangeListener) l);
                }
            }
            tree.setValue(null);
            for (Property.ValueChangeListener l : listeners) {
                tree.addValueChangeListener((Property.ValueChangeListener) l);
            }
        }
    }

    @Override
    public void permissions() {
        for (Button b : new Button[]{bAdd, bDelete, bDisable, bEdit, bEnable}) {
            b.setEnabled(ui.isUserAdministrator()
                    || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                    ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                    : "");
        }
        for (MenuItem b : new MenuItem[]{
            tableEntriesContextMenuItemAddChild,
            tableEntriesContextMenuItemDelete,
            tableEntriesContextMenuItemDisable,
            tableEntriesContextMenuItemEdit,
            tableEntriesContextMenuItemEnable}) {
            b.setEnabled(ui.isUserAdministrator()
                    || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                    ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                    : "");
        }
    }

    @Override
    public final boolean push() {
        if (!refreshing && Boolean.TRUE.equals(cbAutoRefresh.getValue())) {
            refreshData();
            return true;
        }
        return false;
    }

    public String getCaptionKey() {
        return captionKey;
    }

    public void setCaptionKey(String captionKey) {
        this.captionKey = captionKey;
    }

    public class RegistryTable extends Table {

        public RegistryTable() {
        }

        public RegistryTable(String caption) {
            super(caption);
        }

        public RegistryTable(String caption, Container dataSource) {
            super(caption, dataSource);
        }

        @Override
        public void refreshRowCache() {
            if (!preventTableEntriesFullResetPageBuffer) {
                resetPageBuffer();
            } else {
                resetPageBufferSoft();
            }
            refreshRenderedCells();
        }

        @Override
        public void setVisibleColumns(Object... visibleColumns) {
            preventTableEntriesFullResetPageBuffer = false;
            super.setVisibleColumns(visibleColumns);
            preventTableEntriesFullResetPageBuffer = true;
        }

    }

}
