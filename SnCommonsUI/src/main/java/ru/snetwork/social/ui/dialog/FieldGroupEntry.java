/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.dialog;

import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.model.MediaAuthor;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.SnSocialUI;

/**
 *
 * @author ky6uHETc
 * @param <D>
 */
public class FieldGroupEntry<D extends AbstractDialogEntry, SU extends EntityModelSystemUser> extends FieldGroupBasic<D> {

    static final Pattern PATTERN_JOB_ID = Pattern.compile("Job\\[ id=(\\d+) \\]\\:.*");

    public FieldGroupEntry(Class beanType) {
        super(beanType);
    }

    public <E extends EntityModelEntry> FieldGroupEntry(E entry) {
        super(entry.getClass());
    }

    @Override
    public Field buildAndBind(String caption, Object propertyId, Class fieldType) throws FieldGroup.BindException {
        final SnSocialUI ui = (SnSocialUI) dialog.getUi();
        try {
            Property property = dialog.getEntryItem().getItemProperty(propertyId);
            if (EntityPropertyKeys.USER_CREATED.equals(propertyId) || EntityPropertyKeys.USER_MODIFIED.equals(propertyId)) {
                TextField f = new TextField(caption);
                f.setConverter(new Converter<String, SU>() {

                    private SU originalValue;

                    @Override
                    public SU convertToModel(String value, Class<? extends SU> targetType, Locale locale) throws Converter.ConversionException {
                        return originalValue;
                    }

                    @Override
                    public String convertToPresentation(SU value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
                        originalValue = value;
                        return (value != null) ? value.getName() : "";
                    }

                    @Override
                    public Class<SU> getModelType() {
                        return ui.getUserClass();
                    }

                    @Override
                    public Class<String> getPresentationType() {
                        return String.class;
                    }

                });
                f.setReadOnly(true);
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.GENDER.equals(propertyId)) {
                NativeSelect f = new NativeSelect(caption, Arrays.asList(MediaAuthor.Gender.values()));
                f.setMultiSelect(false);
                f.setNullSelectionAllowed(true);
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.LOCALE.equals(propertyId)) {
                OptionGroup f = new OptionGroup(caption);
                f.setStyleName("buttonsbar with-checkboxes");
                for (Locale l : ui.getLocales()) {
                    f.addItem(l);
                }
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.TIMEZONE.equals(propertyId)) {
                final List<TimeZone> timeZones = dialog.ui.getTimeZones();
                ComboBox f = new ComboBox(caption, timeZones);
                for (TimeZone tz : timeZones) {
                    String offset;
                    int offsetMinutes = tz.getRawOffset() / 1000 / 60;
                    int offsetHours = 0;
                    if (offsetMinutes < 0) {
                        offsetMinutes = Math.abs(offsetMinutes);
                        offsetHours = offsetMinutes / 60;
                        offsetMinutes = offsetMinutes % 60;
                        offset = "-" + (offsetHours < 10 ? "0" + offsetHours : offsetHours)
                                + ":" + (offsetMinutes < 10 ? "0" + offsetMinutes : offsetMinutes);
                    } else {
                        offsetHours = offsetMinutes / 60;
                        offsetMinutes = offsetMinutes % 60;
                        offset = "+" + (offsetHours < 10 ? "0" + offsetHours : offsetHours)
                                + ":" + (offsetMinutes < 10 ? "0" + offsetMinutes : offsetMinutes);
                    }
                    f.setFilteringMode(FilteringMode.CONTAINS);
                    f.setItemCaption(tz, offset + " " + tz.getID());
                }
                f.setImmediate(true);
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.USER_LOGIN.equals(propertyId)) {
                TextField f = new TextField(caption);
                f.setStyleName("userlogin");
                f.addValidator(new RegexpValidator("[\\w\\-\\.]{5,}", dialog.ui.getMessage(MessageKeys.ERROR_INCORRECT_USERLOGIN)));
                f.setRequired(true);
                f.setNullRepresentation("");
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.USER_AUTH_SKIP_PROVIDER.equals(propertyId)) {
                CheckBox f = new CheckBox(caption);
                f.setImmediate(true);
                super.bind(f, propertyId);
                return f;
            } 
        } catch (Exception ex) {
            ui.handleException(ex);
        }
        Field f = super.buildAndBind(caption, propertyId, fieldType);
        if (f instanceof DateField) {
            ((DateField) f).setDateFormat(SnSocialUI.DATETIMESEC_PATTERN);
            ((DateField) f).setResolution(Resolution.SECOND);
            ((DateField) f).setTimeZone(ui.getUserTimeZone());;
        }
        return f;
    }

}
