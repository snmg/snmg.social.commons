/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import com.vaadin.ui.themes.ValoTheme;

/**
 *
 * @author ky6uHETc
 */
public class SnSocialTheme extends ValoTheme {

    public static final String OPTION_GROUP_BUTTONSBAR = "buttonsbar";
    public static final String OPTION_GROUP_BUTTONSBAR_WITH_CHECKBOXES = OPTION_GROUP_BUTTONSBAR + " with-checkboxes";
    public static final String LAYOUT_ENTRIES = "entries";
    public static final String LAYOUT_ENTRY_FIELDS = "entry-fields";
}
