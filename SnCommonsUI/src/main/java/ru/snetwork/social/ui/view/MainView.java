/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.View;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.SnSocialMainMenu;
import ru.snetwork.social.ui.SnSocialMainMenuItem;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.SnSocialUIComponent;
import ru.snetwork.social.ui.event.SnSocialUIEvent;

/**
 *
 * @author ky6uHETc
 */
public class MainView extends HorizontalLayout implements SnSocialUIComponent {

    protected final SnSocialUI ui;
    protected final SnSocialMainMenu menu;
    protected final CssLayout contentWrapper;
    protected final CssLayout activeUsers;
    protected final CssLayout content;
    protected final Label labelForbidden = new Label("<h1>Forbidden</h1><p>You don't have enuph permissions to the requested resource</p>", ContentMode.HTML);
    private Button.ClickListener avatarClickLostener = new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent event) {
//            System.out.println("((EntityModelSystemUser) event.getButton().getData()).getId(): " + ((EntityModelSystemUser) event.getButton().getData()).getId());
            ui.getEventBus().post(
                new SnSocialUIEvent.SidebarDialogDataRequest(
                    ((EntityModelSystemUser) event.getButton().getData()).getId(), ui));
        }
    };

    public MainView(SnSocialUI ui, SnSocialMainMenu mainMenu) {
        this.ui = ui;
        setSizeFull();
        addStyleName("mainview");

        // add menu
        menu = mainMenu;
        addComponent(menu);

        // add content
        contentWrapper = new CssLayout();
        contentWrapper.setSizeFull();
        contentWrapper.setStyleName("view-content-wrapper");
        addComponent(contentWrapper);

        activeUsers = new CssLayout();
        activeUsers.setStyleName("active-users");
        contentWrapper.addComponent(activeUsers);

        content = new CssLayout();
        content.setSizeFull();
        content.setStyleName("view-content");
        contentWrapper.addComponent(content);

        // set the content area position on screen
        setExpandRatio(contentWrapper, 1.0f);

        labelForbidden.setStyleName("forbidden");

    }

    public void addBeforeContent(Component c) {
        contentWrapper.addComponent(c, contentWrapper.getComponentIndex(c));
    }

    public void addBeforeContentAsFirst(Component c) {
        contentWrapper.addComponentAsFirst(c);
    }

    public CssLayout getContent() {
        return content;
    }

    public SnSocialMainMenu getMenu() {
        return menu;
    }

    public boolean addActiveUser(EntityModelSystemUser user) {
        if (user != null && !user.equals(ui.getUser())) {
            boolean add = true;
            Iterator<Component> iterator = this.activeUsers.iterator();
            while (iterator.hasNext() && add) {
                Component c = iterator.next();
                if (c instanceof Button && user.equals(((Button) c).getData())) {
                    add = false;
                    break;
                }
            }
            if (add) {
                Button userItem = new Button();
                userItem.setData(user);
                userItem.setStyleName("user-item");
                userItem.setIcon(new ExternalResource(
                        VaadinServlet.getCurrent().getServletContext().getContextPath()
                        + "/userphoto?userId=" + user.getId()));
                userItem.setCaption(user.getName());
                userItem.addClickListener(avatarClickLostener);
                this.activeUsers.addComponent(userItem);
                ui.push();
            }
            return add;
        }
        return false;
    }

    public void putActiveUsers(Set<? extends EntityModelSystemUser> users) {
        users = new HashSet<>(users);
        for (int i = 0; i < this.activeUsers.getComponentCount(); i++) {
            final Component c = activeUsers.getComponent(i);
            if (c instanceof Button
                    && ((Button) c).getData() != null
                    && ((Button) c).getData() instanceof EntityModelSystemUser
                    && !users.contains(((Button) c).getData())) {
                this.activeUsers.removeComponent(c);
            }
        }
        for (EntityModelSystemUser u : users) {
            addActiveUser(u);
        }
    }

    public <SU extends EntityModelSystemUser> boolean removeActiveUser(SU user) {
        Component remove = null;
        Iterator<Component> iterator = this.activeUsers.iterator();
        while (iterator.hasNext()) {
            Component c = iterator.next();
            if (c instanceof Button && user.equals(((Button) c).getData())) {
                remove = c;
            }
        }
        if (remove != null) {
            this.activeUsers.removeComponent(remove);
            ui.push();
        }
        return remove != null;
    }

    public void resetActiveUsers() {
        this.activeUsers.removeAllComponents();
    }

    @Override
    public void localize() {
        labelForbidden.setValue(
                "<h1>" + ui.getMessage(MessageKeys.FORBIDDEN) + "</h1><p>"
                + ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_REQUESTED_RESOURCE)
                + "</p>");
        menu.localize();
        Iterator<Component> contentComponents = content.iterator();
        while (contentComponents.hasNext()) {
            Component c = contentComponents.next();
            if (c instanceof SnSocialUIComponent) {
                ((SnSocialUIComponent) c).localize();
            }
        }
    }

    @Override
    public void permissions() {
        menu.permissions();
        Iterator<Component> contentComponents = content.iterator();
        while (contentComponents.hasNext()) {
            Component c = contentComponents.next();
            if (c instanceof SnSocialUIComponent) {
                ((SnSocialUIComponent) c).permissions();
            }
        }

        // check current view permissions using mainMenuItem visibility
        View currentView = ui.getNavigator().getCurrentView();
        permissionForView(currentView);
    }

    public boolean permissionForView(View checkPermissionsOnView) {
        boolean granted = true;
                for (Object item : menu.getMainMenuItems()) {
                    SnSocialMainMenuItem mmItem = (SnSocialMainMenuItem) item;
            if (mmItem.getView().getViewType().isInstance(checkPermissionsOnView)) {
                granted = mmItem.hasViewPermission();
                break;
            }
        }
        if (!granted) {
            getContent().removeAllComponents();
            getContent().addComponent(labelForbidden);
        }
        return granted;
    }

    @Override
    public void refresh() {
        menu.refresh();
        Iterator<Component> contentComponents = content.iterator();
        while (contentComponents.hasNext()) {
            Component c = contentComponents.next();
            if (c instanceof SnSocialUIComponent) {
                ((SnSocialUIComponent) c).refresh();
            }
        }
    }

    @Override
    public boolean push() {
        boolean push = false;
        Iterator<Component> contentComponents = content.iterator();
        while (contentComponents.hasNext()) {
            Component c = contentComponents.next();
            if (c instanceof SnSocialUIComponent && ((SnSocialUIComponent) c).push()) {
                push = true;
            }
        }
        if (menu.push()) {
            push = true;
        }
        return push;
    }

    public boolean isCurrentViewForbidden() {
        return labelForbidden.getParent() != null && labelForbidden.getParent() == getContent();
    }
}
