/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import com.vaadin.navigator.View;
import com.vaadin.ui.Component;

/**
 *
 * @author ky6uHETc
 */
public interface SnSocialUIView extends View, SnSocialUIComponent {

    public Component getParent();

    public String getUriFragmentId();

}
