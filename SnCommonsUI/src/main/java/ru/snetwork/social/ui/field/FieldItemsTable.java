/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.field;

import com.vaadin.data.*;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.util.ReflectTools;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.apache.commons.lang.StringUtils;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.ui.SnSocialUI;

/**
 *
 * @author evgeniy
 * @param <BEANTYPE> - table itemIds type
 */
public class FieldItemsTable<BEANTYPE> extends CustomComponent implements Field, Property.ReadOnlyStatusChangeListener, Property.ReadOnlyStatusChangeNotifier {

    private static final Method METHOD_ITEM_ADDED = ReflectTools.findMethod(FieldItemsTable.ItemAddedListener.class, "itemAdded", FieldItemsTable.ItemAddedEvent.class);

    private final SnSocialUI ui;
    private final VerticalLayout layoutMain = new VerticalLayout();
    private final HorizontalLayout layoutButtons = new HorizontalLayout();
    private final Label labelButtons = new Label(" ");
    private final BeanItemContainer<BEANTYPE> containerItems;
    private final Table tableItems;
    private final Panel panelItems = new Panel();
    protected final Set<Object> tableEntriesSelection = new LinkedHashSet<>();
    private final HashMap<Object, HashMap<Object, Field>> tableItemsFields = new HashMap<Object, HashMap<Object, Field>>() {
        @Override
        public HashMap<Object, Field> get(Object key) {
            if (super.get(key) == null) {
                super.put(key, new HashMap<>());
            }
            return super.get(key);
        }

    };
    private final Button buttonAdd = new Button();
    private final Button buttonRemove = new Button();

    private Collection value = null;
    private Property dataSource = null;
    private boolean invalidAllowed = false;
    private boolean invalidCommitted = false;
    private boolean modified = false;
    private boolean required = false;
    private String requiredError = null;
    private boolean committingValueToDataSource = false;
    private boolean valueWasModifiedByDataSourceDuringCommit;
    private LinkedList<Validator> validators = null;
    private Buffered.SourceException currentBufferedSourceException = null;

    public FieldItemsTable(SnSocialUI contestUI, Class<? super BEANTYPE> itemType, TableFieldFactory fieldFactory) {

        final FieldItemsTable self = this;
        ui = contestUI;
        setStyleName(FieldItemsTable.class.getSimpleName());

        containerItems = new BeanItemContainer<BEANTYPE>(itemType);
        tableItems = new Table(null, containerItems) {
            @Override
            protected Object getPropertyValue(Object rowId, Object colId,
                    Property property) {
                Object o = super.getPropertyValue(rowId, colId, property);
                if (o instanceof Field) {
                    tableItemsFields.get(rowId).put(colId, (Field) o);
                }
                return o;
            }

            @Override
            protected void unregisterComponent(Component component) {
                super.unregisterComponent(component);
                if (component instanceof Field) {
                    for (HashMap<Object, Field> map : tableItemsFields.values()) {
                        map.remove((Field) component);
                    }
                }
            }

        };
        tableItems.setTableFieldFactory(fieldFactory);
        tableItems.setNullSelectionAllowed(true);
        tableItems.setWidth("100%");
        tableItems.setHeight("100%");
        tableItems.setSelectable(true);
        tableItems.setMultiSelect(true);
        tableItems.setImmediate(true);
        tableItems.setEditable(true);
        tableItems.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object v = event.getProperty().getValue();
                buttonRemove.setEnabled((v != null && v instanceof Set && ((Set) v).size() > 0));
                tableEntriesSelection.clear();
                if (v != null && v instanceof Collection) {
                    ((Collection) v).iterator().forEachRemaining(new Consumer() {
                        @Override
                        public void accept(Object o) {
                            if (o != null) {
                                tableEntriesSelection.add(o);
                            }
                        }
                    });
                } else if (v != null) {
                    tableEntriesSelection.add(v);
                }
            }
        });

        buttonAdd.setDescription(ui.getMessage(MessageKeys.ADD));
        buttonAdd.setStyleName(ValoTheme.BUTTON_TINY);
        buttonAdd.setIcon(FontAwesome.PLUS);
        buttonAdd.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Object o = containerItems.getBeanType().newInstance();
                    addBean((BEANTYPE) o);
                    tableItems.setCurrentPageFirstItemId(o);
                    for (Object colId : tableItems.getVisibleColumns()) {
                        if (tableItemsFields.get(o).get(colId) instanceof Field
                                && !((Field) tableItemsFields.get(o).get(colId)).isReadOnly()) {
                            ((Field) tableItemsFields.get(o).get(colId)).focus();
                            break;
                        }
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }

        });

        buttonRemove.setDescription(ui.getMessage(MessageKeys.DELETE));
        buttonRemove.setStyleName(ValoTheme.BUTTON_TINY);
        buttonRemove.setIcon(FontAwesome.MINUS);
        buttonRemove.setEnabled(false);
        buttonRemove.setDisableOnClick(true);
        buttonRemove.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Object v = getValue();
                    if (v == null || !(v instanceof Collection)) {
                        v = new LinkedList<BEANTYPE>();
                        setValue(v);
                    }
                    if (tableItems.getValue() != null && tableItems.getValue() instanceof Set) {
                        ((Collection) v).removeAll((Set) tableItems.getValue());
                    }
                    renderValueToTable();
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });

        layoutButtons.addComponent(buttonAdd);
        layoutButtons.setComponentAlignment(buttonAdd, Alignment.MIDDLE_CENTER);
        layoutButtons.addComponent(buttonRemove);
        layoutButtons.setComponentAlignment(buttonRemove, Alignment.MIDDLE_CENTER);
        layoutButtons.setStyleName("buttons-comment spacer");
        layoutButtons.addComponent(labelButtons);
        layoutButtons.setExpandRatio(labelButtons, 1.0f);
        layoutButtons.setSpacing(true);
        layoutButtons.setWidth("100%");

        layoutMain.addComponent(tableItems);
        layoutMain.addComponent(layoutButtons);
        layoutMain.setSpacing(true);
        layoutMain.setSizeFull();
        layoutMain.setExpandRatio(tableItems, 1.0f);

        super.setCompositionRoot(layoutMain);

    }

    private void renderValueToTable() {
        tableItemsFields.clear();
        Object v = getValue();
        containerItems.removeAllItems();
        if (v instanceof Collection) {
            for (Object item : (Collection) v) {
                containerItems.addItem(item);
            }
        }
        tableItems.setCurrentPageFirstItemIndex(containerItems.size());
    }

    public void setCaption(String caption, String captionAdd, String captionRemove) {
        setCaption(caption);
        setCaptionAdd(captionAdd);
        setCaptionRemove(captionRemove);
    }

    public void setCaptionAdd(String caption) {
        buttonAdd.setCaption(caption);
    }

    public void setCaptionRemove(String caption) {
        buttonRemove.setCaption(caption);
    }

    public void addNestedContainerProperty(String propertyId) {
        containerItems.addNestedContainerProperty(propertyId);
    }

    public void addTableGeneratedColumn(Object id, Table.ColumnGenerator column) {
        tableItems.addGeneratedColumn(id, column);
    }

    @Override
    public void setId(String id) {
        super.setId(id);
        tableItems.setId(id + "-table");
        panelItems.setId(id + "-panel");
    }

    public Set<BEANTYPE> getTableSelectedItems() {
        if (tableItems.getValue() != null) {
            Set selection = new LinkedHashSet();
            if (tableItems.getValue() instanceof Collection) {
                selection.addAll((Collection) tableItems.getValue());
            } else {
                selection.add(tableItems.getValue());
            }
            return selection;
        }
        return null;
    }

    public Object[] getTableVisibleColumns() {
        return tableItems.getVisibleColumns();
    }

    public Item getTableItem(Object itemId) {
        return tableItems.getItem(itemId);
    }

    public void setTableSelectedItems(Set<BEANTYPE> selection) {
        tableItems.setValue(selection);
    }

    public boolean isTableReadonly() {
        return tableItems.isReadOnly();
    }

    public void setTableReadOnly(boolean readonly) {
        tableItems.setReadOnly(readonly);
        if (readonly) {
            tableItems.addStyleName("readonly");
        } else {
            tableItems.addStyleName("readonly");
        }
    }

    public void setTableVisibleColumns(String[] visibleColumns) {
        tableItems.setVisibleColumns((Object[]) visibleColumns);
        for (Object colId : visibleColumns) {
            tableItems.setColumnHeader(colId, ui.getMessage(MessageKeys.COLUMN_X.concat((String) colId)));
        }
    }

    public void setTableColumnHeaders(String[] columnHeaders) {
        tableItems.setColumnHeaders(columnHeaders);
    }

    public void setTableColumnHeader(Object columnId, String columnHeader) {
        tableItems.setColumnHeader(columnId, columnHeader);
    }

    public void setTableColumnHeaderMode(Table.ColumnHeaderMode headerMode) {
        tableItems.setColumnHeaderMode(headerMode);
    }

    public void setTableColumnWidth(Object columnId, int width) {
        tableItems.setColumnWidth(columnId, width);
    }

    public void setTableColumnExpandRatio(Object columnId, float ratio) {
        tableItems.setColumnExpandRatio(columnId, ratio);
    }

    public void setTableItemIconPropertyId(Object propertyId) {
        tableItems.setItemIconPropertyId(propertyId);
        tableItems.setRowHeaderMode(Table.RowHeaderMode.ICON_ONLY);
    }

    public void setTableHeight(String height) {
        tableItems.setHeight(height);
    }

    public void setTablePageLength(int pageLength) {
        tableItems.setPageLength(pageLength);
    }

    public void setTableSortPropertyId(Object propertyId) {
        tableItems.setSortEnabled(true);
        tableItems.setSortContainerPropertyId(propertyId);
        tableItems.sort();
    }

    public void sortTable() {
        if (tableItems.isSortEnabled() && tableItems.getSortContainerPropertyId() != null) {
            tableItems.sort();
        }
    }

    public MarginInfo getMargin() {
        return layoutMain.getMargin();
    }

    public void setMargin(boolean enabled) {
        layoutMain.setMargin(enabled);
    }

    public void setMargin(MarginInfo marginInfo) {
        layoutMain.setMargin(marginInfo);
    }

    public HorizontalLayout getLayoutButtons() {
        return layoutButtons;
    }

    @Override
    public String getStyleName() {
        return StringUtils.remove(super.getStyleName(), FieldItemsTable.class.getSimpleName()).trim();
    }

    @Override
    public void setStyleName(String styleName) {
        super.setStyleName(FieldItemsTable.class.getSimpleName() + " " + styleName);
    }

    public void setButtonsComment(String text) {
        labelButtons.setValue(text);
    }

    public void addButtonsComponent(Component component) {
        layoutButtons.addComponent(component);
        layoutButtons.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
    }

    public void removeButtonsComponent(Component component) {
        layoutButtons.removeComponent(component);
    }

    public void addButtonsComponent(Component component, int index) {
        layoutButtons.addComponent(component, index);
        layoutButtons.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void focus() {
        tableItems.focus();
    }

    @Override
    public boolean isInvalidAllowed() {
        return invalidAllowed;
    }

    @Override
    public void setInvalidAllowed(boolean invalidAllowed) {
        this.invalidAllowed = invalidAllowed;
    }

    @Override
    public boolean isInvalidCommitted() {
        return invalidCommitted;
    }

    @Override
    public void setInvalidCommitted(boolean invalidCommitted) {
        this.invalidCommitted = invalidCommitted;
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public boolean isRequired() {
        return required;
    }

    @Override
    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        tableItems.setSelectable(!readonly);
        tableItems.setEditable(!readonly);
        buttonAdd.setVisible(!readonly);
        buttonRemove.setVisible(!readonly);
    }

    @Override
    public String getRequiredError() {
        return requiredError;
    }

    @Override
    public void setRequiredError(String requiredError) {
        this.requiredError = requiredError;
    }

    @Override
    public void commit() throws Buffered.SourceException, Validator.InvalidValueException {
        if (dataSource != null && !dataSource.isReadOnly()) {
            if ((isInvalidCommitted() || isValid())) {
                final Object newValue = getValue();
                try {
                    // Commits the value to datasource.
                    valueWasModifiedByDataSourceDuringCommit = false;
                    committingValueToDataSource = true;
                    dataSource.setValue(newValue);

                } catch (final Throwable e) {
                    // Sets the buffering state.
                    currentBufferedSourceException = new Buffered.SourceException(this, e);
                    // Throws the source exception.
                    throw currentBufferedSourceException;

                } finally {
                    committingValueToDataSource = false;
                }
            } else {
                /*
                 * An invalid value and we don't allow them, throw the exception
                 */
                validate();
            }
        }

        // If successful, remove set the buffering state to be ok
        if (currentBufferedSourceException != null) {
            currentBufferedSourceException = null;
        }

        if (valueWasModifiedByDataSourceDuringCommit) {
            valueWasModifiedByDataSourceDuringCommit = false;
            fireValueChange();
        }
    }

    @Override
    public void discard() throws Buffered.SourceException {
        if (dataSource != null) {

            // Gets the correct value from datasource
            Collection newValue;
            try {
                // Discards buffer by overwriting from datasource
                newValue = (Collection) dataSource.getValue();

                // If successful, remove set the buffering state to be ok
                if (currentBufferedSourceException != null) {
                    currentBufferedSourceException = null;
                }

            } catch (final Throwable e) {
                // Sets the buffering state
                currentBufferedSourceException = new Buffered.SourceException(this, e);

                // Throws the source exception
                throw currentBufferedSourceException;
            }

            // If the new value differs from the previous one
            if ((newValue == null && value != null) || (newValue != null && !newValue.equals(value))) {
                setInternalValue(newValue);
                fireValueChange();
            }
        }
    }

    @Override
    public void addValidator(Validator validator) {
        if (validators == null) {
            validators = new LinkedList<Validator>();
        }
        validators.add(validator);
    }

    @Override
    public Collection<Validator> getValidators() {
        if (validators == null || validators.isEmpty()) {
            return null;
        }
        return Collections.unmodifiableCollection(validators);
    }

    @Override
    public void removeValidator(Validator validator) {
        if (validators != null) {
            validators.remove(validator);
        }
    }

    @Override
    public boolean isEmpty() {
        return containerItems.size() == 0;
    }

    @Override
    public boolean isValid() {
        try {
            validate();
            return true;
        } catch (Validator.InvalidValueException ex) {

        }
        return false;
    }

    @Override
    public void validate() throws Validator.InvalidValueException {
        if (isEmpty() && isRequired()) {
            throw new Validator.EmptyValueException(ui.getMessage(MessageKeys.ERROR_VALUE_REQUIRED));
        }
        if (!tableItemsFields.isEmpty()) {
            for (HashMap<Object, Field> fields : tableItemsFields.values()) {
                for (Field f : fields.values()) {
                    f.validate();
                }
            }
        }
        if (validators != null) {
            if (isEmpty() && isRequired()) {
                throw new Validator.EmptyValueException(ui.getMessage(MessageKeys.ERROR_VALUE_REQUIRED));
            }
            for (Validator v : validators) {
                v.validate(getValue());
            }
        }
    }

    @Override
    public Object getValue() {
        // Give the value from abstract buffers if the field if possible
        if (dataSource == null || isModified()) {
            return value;
        }

        return dataSource.getValue();

    }

    public void addBean(BEANTYPE o) throws ReadOnlyException {
        Object v = getValue();
        if (v == null || !(v instanceof Collection)) {
            v = dataSource != null && Set.class.isAssignableFrom(dataSource.getType()) ? new LinkedHashSet<BEANTYPE>() : new LinkedList<BEANTYPE>();
            setValue(v);
        }
        if (!((Collection) v).contains(o)) {
            ((Collection) v).add(o);
            fireEvent(new ItemAddedEvent(o, this));
        }
        renderValueToTable();
        fireValueChange();
    }

    public void addBeans(Collection<BEANTYPE> l) {
        if (l != null && !l.isEmpty()) {
            Object v = getValue();
            if (v == null || !(v instanceof Collection)) {
                v = new LinkedList<BEANTYPE>();
                setValue(v);
            }
            for (BEANTYPE o : l) {
                if (!((Collection) v).contains(o)) {
                    ((Collection) v).add(o);
                    fireEvent(new ItemAddedEvent(o, this));
                }
            }
            renderValueToTable();
            fireValueChange();
        }
    }

    public Collection<BEANTYPE> getItems() {
        return containerItems.getItemIds();
    }

    public Property getItemProperty(Object itemId, Object propertyId) {
        return containerItems.getContainerProperty(itemId, propertyId);
    }

    @Override
    public void setValue(Object newValue) throws Property.ReadOnlyException {
        if ((newValue == null && value != null) || (newValue != null && !newValue.equals(value))) {

            // Read only fields can not be changed
            if (isReadOnly()) {
                throw new Property.ReadOnlyException();
            }

            // If invalid values are not allowed, the value must be checked
            if (!isInvalidAllowed()) {
                final Collection<Validator> v = getValidators();
                if (v != null) {
                    for (final Iterator<Validator> i = v.iterator(); i.hasNext();) {
                        (i.next()).validate(newValue);
                    }
                }
            }

            // Changes the value
            setInternalValue((Collection) newValue);
            modified = true;

            valueWasModifiedByDataSourceDuringCommit = false;
            // In write through mode , try to commit
            if (dataSource != null && (isInvalidCommitted() || isValid())) {
                try {
                    // Commits the value to datasource
                    committingValueToDataSource = true;
                    dataSource.setValue(newValue);

                } catch (final Throwable e) {
                    // Sets the buffering state
                    currentBufferedSourceException = new Buffered.SourceException(this, e);

                    // Throws the source exception
                    throw currentBufferedSourceException;
                } finally {
                    committingValueToDataSource = false;
                }
            }

            // If successful, remove set the buffering state to be ok
            if (currentBufferedSourceException != null) {
                currentBufferedSourceException = null;
            }

            if (valueWasModifiedByDataSourceDuringCommit) {
                /*
                 * Value was modified by datasource. Force repaint even if
                 * repaint was not requested.
                 */
                valueWasModifiedByDataSourceDuringCommit = false;
            }

            // Fires the value change
            fireValueChange();

        }

    }

    @Override
    public Class<?> getType() {
        return Collection.class;
    }
    private static final Method VALUE_CHANGE_METHOD;

    static {
        try {
            VALUE_CHANGE_METHOD = Property.ValueChangeListener.class.getDeclaredMethod("valueChange", new Class[]{Property.ValueChangeEvent.class});
        } catch (final java.lang.NoSuchMethodException e) {
            // This should never happen
            throw new java.lang.RuntimeException("Internal error finding methods in FieldSelectWithCombo");
        }
    }

    @Override
    public void addListener(Property.ValueChangeListener listener) {
        addListener(Property.ValueChangeEvent.class, listener, VALUE_CHANGE_METHOD);

    }

    @Override
    public void removeListener(Property.ValueChangeListener listener) {
        removeListener(Property.ValueChangeEvent.class, listener,
                VALUE_CHANGE_METHOD);
    }

    @Override
    public void valueChange(Property.ValueChangeEvent event) {
        tableItems.valueChange(event);
        if (committingValueToDataSource) {
            boolean propertyNotifiesOfTheBufferedValue = event.getProperty().getValue() == value
                    || (value != null && value.equals(event.getProperty().getValue()));

            if (!propertyNotifiesOfTheBufferedValue) {
                /*
                     * Property (or chained property like PropertyFormatter) now
                     * reports different value than the one the field has just
                     * committed to it. In this case we respect the property
                     * value.
                     *
                     * Still, we don't fire value change yet, but instead
                     * postpone it until "commit" is done. See setValue(Object,
                     * boolean) and commit().
                 */
                readValueFromProperty(event);
                valueWasModifiedByDataSourceDuringCommit = true;
            }
        } else if (!isModified()) {
            readValueFromProperty(event);
            fireValueChange();
        }
    }

    public void wrapTableWithPanel() {
        if (panelItems.getParent() == null) {
            tableItems.setPageLength(0);
            tableItems.setSizeUndefined();
            tableItems.setWidth("100%");
            panelItems.setSizeFull();
            panelItems.setContent(tableItems);
            layoutMain.addComponent(panelItems, 0);
            layoutMain.setExpandRatio(panelItems, 1.0f);
        }
    }

    @Override
    public void setPropertyDataSource(Property newDataSource) {
        // Saves the old value
        final Property oldDataSource = this.dataSource;

        // Stops listening the old data source changes
        if (dataSource != null && Property.ValueChangeNotifier.class.isAssignableFrom(dataSource.getClass())) {
            ((Property.ValueChangeNotifier) dataSource).removeValueChangeListener(this);
        }
        if (dataSource != null && Property.ReadOnlyStatusChangeNotifier.class.isAssignableFrom(dataSource.getClass())) {
            ((Property.ReadOnlyStatusChangeNotifier) dataSource).removeReadOnlyStatusChangeListener(this);
        }

        // Sets the new data source
        dataSource = newDataSource;

        // Gets the value from source
        try {
            if (dataSource != null) {
                setInternalValue((Collection) dataSource.getValue());
            }
        } catch (final Throwable e) {
            currentBufferedSourceException = new Buffered.SourceException(this, e);
        }

        // Listens the new data source if possible
        if (dataSource instanceof Property.ValueChangeNotifier) {
            ((Property.ValueChangeNotifier) dataSource).removeValueChangeListener(this);
        }
        if (dataSource instanceof Property.ReadOnlyStatusChangeNotifier) {
            ((Property.ReadOnlyStatusChangeNotifier) dataSource).removeReadOnlyStatusChangeListener(this);
        }

        // Copy the validators from the data source
        if (dataSource instanceof Validatable) {
            final Collection<Validator> validators = ((Validatable) dataSource).getValidators();
            if (validators != null) {
                for (final Iterator<Validator> i = validators.iterator(); i.hasNext();) {
                    addValidator(i.next());
                }
            }
        }

        // Fires value change if the value has changed
        if ((oldDataSource == null && newDataSource != null)
                || (oldDataSource != null && newDataSource == null)
                || oldDataSource != newDataSource) {
            fireValueChange();
        }

    }

    @Override
    public Property getPropertyDataSource() {
        return dataSource;
    }

    @Override
    public int getTabIndex() {
        return tableItems.getTabIndex();
    }

    @Override
    public void setTabIndex(int tabIndex) {
        tableItems.setTabIndex(tabIndex);
    }

    protected void fireValueChange() {
        fireEvent(new ValueChangeEvent(this));
    }

    private void readValueFromProperty(Property.ValueChangeEvent event) {
        if (event.getProperty().getValue() instanceof Collection) {
            setInternalValue((List) event.getProperty().getValue());
        }
    }

    protected void setInternalValue(Collection newValue) {
        value = newValue;
        renderValueToTable();
    }

    /*
     * Read-only status change events
     */
    private static final Method READ_ONLY_STATUS_CHANGE_METHOD;

    static {
        try {
            READ_ONLY_STATUS_CHANGE_METHOD = Property.ReadOnlyStatusChangeListener.class.getDeclaredMethod("readOnlyStatusChange", new Class[]{Property.ReadOnlyStatusChangeEvent.class});
        } catch (final java.lang.NoSuchMethodException ex) {
            // This should never happen
            throw new java.lang.RuntimeException("Internal error finding methods in AbstractField");
        }
    }

    @Override
    public void readOnlyStatusChange(Property.ReadOnlyStatusChangeEvent event) {
    }

    @Override
    public void addListener(Property.ReadOnlyStatusChangeListener listener) {
        addListener(Property.ReadOnlyStatusChangeEvent.class, listener, READ_ONLY_STATUS_CHANGE_METHOD);
    }

    @Override
    public void removeListener(Property.ReadOnlyStatusChangeListener listener) {
        removeListener(Property.ReadOnlyStatusChangeEvent.class, listener, READ_ONLY_STATUS_CHANGE_METHOD);
    }

    public void addListener(ItemAddedListener listener) {
        super.addListener(FieldItemsTable.ItemAddedEvent.class, listener, METHOD_ITEM_ADDED);
    }

    @Override
    public void clear() {
        containerItems.removeAllItems();
    }

    @Override
    public void setBuffered(boolean buffered) {
        tableItems.setBuffered(buffered);
    }

    @Override
    public boolean isBuffered() {
        return tableItems.isBuffered();
    }

    @Override
    public void removeAllValidators() {
        tableItems.removeAllValidators();
    }

    @Override
    public void addValueChangeListener(ValueChangeListener listener) {
        addListener(listener);
    }

    @Override
    public void removeValueChangeListener(ValueChangeListener listener) {
        tableItems.removeValueChangeListener(listener);
    }

    @Override
    public void addReadOnlyStatusChangeListener(ReadOnlyStatusChangeListener listener) {
        tableItems.addReadOnlyStatusChangeListener(listener);
    }

    @Override
    public void removeReadOnlyStatusChangeListener(ReadOnlyStatusChangeListener listener) {
        tableItems.removeReadOnlyStatusChangeListener(listener);
    }

    public class ItemAddedEvent extends Component.Event {

        final Object added;
        final Property property;

        public ItemAddedEvent(Object added, Component source) {
            super(source);
            this.added = added;
            this.property = dataSource != null ? dataSource : new ObjectProperty<Collection>(value);
        }

        public Object getAdded() {
            return added;
        }

        public Property getProperty() {
            return property;
        }

    }

    public class ValueChangeEvent extends Component.Event implements Property.ValueChangeEvent {

        final Property property;

        public ValueChangeEvent(Component source) {
            super(source);
            this.property = dataSource != null ? dataSource : new ObjectProperty<Collection>(value);
        }

        @Override
        public Property getProperty() {
            return property;
        }

    }

    public interface ItemAddedListener {

        public void itemAdded(FieldItemsTable.ItemAddedEvent event);
    }

    public Button getButtonAdd() {
        return buttonAdd;
    }

    public Button getButtonRemove() {
        return buttonRemove;
    }
    
    public void addTableItemsClickListener(ItemClickEvent.ItemClickListener listeter) {
        tableItems.addItemClickListener(listeter);
    }
}
