/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.VerticalLayout;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.SnSocialUIComponent;

/**
 *
 * @author ky6uHETc
 */
public class LoginView extends VerticalLayout implements SnSocialUIComponent {

    final SnSocialUI ui;
    final LoginForm loginForm;

    public LoginView(SnSocialUI ui, String uiCaption) {
        this.ui = ui;
        setSizeFull();
        setStyleName("loginview");

        loginForm = new LoginForm(this.ui, uiCaption);
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void localize() {
        loginForm.localize();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void permissions() {

    }

    @Override
    public boolean push() {
        return false;
    }

}
