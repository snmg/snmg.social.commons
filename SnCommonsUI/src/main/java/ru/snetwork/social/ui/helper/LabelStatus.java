/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.helper;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ky6uHETc
 */
public class LabelStatus extends Label {

    public LabelStatus() {
        super("", ContentMode.HTML);
    }

    public void setStatusValues(StatusValue mainValue, StatusValue... otherValues) {
        StringBuilder sb = new StringBuilder();
        sb.append("<div class='main-value ").append(mainValue.className).append("'>")
                .append("<span class='value-caption'>").append(mainValue.caption).append("</span>")
                .append("<span class='value-value'>").append(mainValue.value).append("</span>")
                .append(mainValue.progressPercentage != null && mainValue.progressPercentage >= 0 && mainValue.progressPercentage <= 100
                        ? "<span class='value-progress'><span class='value-progress-percentage' style='width: " + mainValue.progressPercentage + "%'>&nbsp;</span></span>"
                        : "")
                .append("</div>");
        sb.append("<div class='other-values'>");
        if (otherValues != null) {
            for (StatusValue ov : otherValues) {
                sb.append("<div class='other-value ").append(ov.className).append("'>")
                        .append("<span class='value-caption'>").append(ov.caption).append(":</span>")
                        .append("<span class='value-value'>").append(
                        StringUtils.isNotBlank(ov.link)
                        ? "<a href='" + ov.link + "'>" + ov.value + "</a>"
                        : ov.value
                ).append("</span>")
                        .append("</div>");
            }
        }
        sb.append("</div>");
        setValue(sb.toString());
    }

    public static class StatusValue {

        String className;
        String caption;
        String value;
        String link;
        Integer progressPercentage;

        public StatusValue(String className, String caption, String value) {
            this.className = className;
            this.caption = caption;
            this.value = value;
            this.link = null;
            this.progressPercentage = null;
        }

        public StatusValue(String className, String caption, String value, String link) {
            this.className = className;
            this.caption = caption;
            this.value = value;
            this.link = link;
            this.progressPercentage = null;
        }

        public StatusValue(String className, String caption, String value, int progressPercentage) {
            this.className = className;
            this.caption = caption;
            this.value = value;
            this.link = null;
            this.progressPercentage = progressPercentage;
        }

    }

}
