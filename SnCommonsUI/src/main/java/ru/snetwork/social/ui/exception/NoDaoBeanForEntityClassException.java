/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.exception;

import ru.snetwork.social.exception.SystemException;

/**
 *
 * @author ky6uHETc
 */
public class NoDaoBeanForEntityClassException extends SystemException {

    public NoDaoBeanForEntityClassException(Class entityClass) {
        super("Unable to determine DAO Bean for class " + entityClass.getName());

    }

    public NoDaoBeanForEntityClassException(String entitySimpleName) {
        super("Unable to determine DAO Bean for class " + entitySimpleName);

    }

}
