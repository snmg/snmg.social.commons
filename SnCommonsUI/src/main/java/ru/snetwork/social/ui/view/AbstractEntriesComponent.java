/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.addon.contextmenu.Menu;
import com.vaadin.addon.contextmenu.MenuItem;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanUtil;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.vaadin.addons.lazyquerycontainer.AbstractBeanQuery;
import org.vaadin.addons.lazyquerycontainer.BeanQueryFactory;
import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.hene.popupbutton.PopupButton;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.exception.DaoUnableToDeleteObjectException;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.dao.AbstractEntriesDaoBean;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.dao.query.WhereClauseNumber;
import ru.snetwork.social.model.EntityModelGeo;
import ru.snetwork.social.model.EntityModelRegistry;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.exception.NoDialogForEntityClassException;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.field.TreeComboBox;
import ru.snetwork.social.ui.helper.ShowOptions;
import ru.snetwork.social.ui.SnSocialUIComponent;

/**
 *
 * @author ky6uHETc
 * @param <T> Entity Class
 * @param <U> Vaadin UI class
 */
public abstract class AbstractEntriesComponent<T extends EntityModelEntry, D extends AbstractEntriesDaoBean, U extends SnSocialUI>
        extends VerticalLayout
        implements Button.ClickListener, SnSocialUIComponent, SnSocialUIComponent.DisplayedSensing, SnSocialUIComponent.UriFragmentSensing {

    protected boolean displayed = false;
    protected boolean refreshing = false;
    protected Log log = LogFactory.getLog(getClass());

    protected final Class<T> entriesClass;

    private Thread exportThread;
    private XlsExportWindow exportWindow = null;

    // detect entriesClass
    {
        if (this.getClass().getGenericSuperclass() instanceof ParameterizedType) {
            entriesClass = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } else if (this.getClass().getGenericSuperclass() instanceof Class
                && ((Class) this.getClass().getGenericSuperclass()).getGenericSuperclass() instanceof ParameterizedType) {
            entriesClass = (Class) ((ParameterizedType) ((Class) this.getClass().getGenericSuperclass()).getGenericSuperclass()).getActualTypeArguments()[0];
        } else {
            entriesClass = null;
        }
    }

    protected final U ui;
    protected final D dao;
    protected String captionKey;

    protected final HorizontalLayout layoutHeader = new HorizontalLayout();
    protected final HorizontalLayout layoutHeaderFilter = new HorizontalLayout();
    protected final VerticalLayout layoutHeaderFilterMore = new VerticalLayout();
    protected final HorizontalLayout layoutHeaderButtons = new HorizontalLayout();
    protected final Label layoutHeaderButtonsSpacer = new Label(" ");
    protected final TextField fSearch = new TextField();
    protected final PopupButton bFilterMore = new PopupButton(MessageKeys.MORE);
    protected final Button bFilterReset = new Button();
    protected final Button bEdit = new Button(MessageKeys.EDIT, this);
    protected final Button bDelete = new Button(MessageKeys.DELETE, this);
    protected final Button bRestore = new Button(MessageKeys.RESTORE, this);
    protected final Button bAdd = new Button(MessageKeys.CREATE, this);
    protected Button[] actionButtons = new Button[]{bEdit, bDelete};

    protected OrderBy entriesPreOrderBy = null;
    protected final HorizontalLayout layoutTable = new HorizontalLayout();
    protected final BeanQueryFactory<EntriesQuery> entriesQueryFactory = new BeanQueryFactory<>(EntriesQuery.class);
    protected final LazyQueryContainer containerEntries = new LazyQueryContainer(entriesQueryFactory, EntityPropertyKeys.ID, 100, true);
    protected final EntriesTable tableEntries = new EntriesTable(null, containerEntries);

    protected String tableEntriesDateFormat = SnSocialUI.DATETIMESEC_PATTERN;
    protected final Set<Long> tableEntriesSelection = new LinkedHashSet<>();

    protected final ContextMenu tableEntriesContextMenu;
    protected final Menu.Command tableEntriesContextMenuCommand = new Menu.Command() {
        @Override
        public void menuSelected(MenuItem selectedItem) {
            contextMenuSelected(selectedItem);
        }
    };
    protected final MenuItem tableEntriesContextMenuItemEdit;
    protected final MenuItem tableEntriesContextMenuItemDelete;
    protected final MenuItem tableEntriesContextMenuItemRestore;

    protected final HorizontalLayout layoutFooter = new HorizontalLayout();
    protected final Label labelCount = new Label();
    protected final CheckBox cbAutoRefresh = new CheckBox(MessageKeys.AUTO_REFRESH);
    protected final Button bRefresh = new Button(MessageKeys.REFRESH, this);
    protected final Button bExportToExcel = new Button(MessageKeys.EXPORT_TO_XLS, this);

    protected String querySelect = null;
    protected Class<? extends T> querySubclass = null;
    protected String queryJoinRelations = null;
    private List<WhereClause> queryWhere = null;
    protected String queryTotalJoinRelations = null;
    protected List<WhereClause> queryTotalWhere = null;
    protected Integer entriesCount = null;
    protected Integer entriesTotal = null;
    protected boolean queryShowDeleted = false;

    protected Window.CloseListener entryDialogCloseListener = new Window.CloseListener() {
        @Override
        public void windowClose(Window.CloseEvent e) {
            refreshEntryIdUriFragment(null);
        }
    };

    private Long tableEntriesSelectOnload = null;

    public AbstractEntriesComponent(U ui) throws NoDaoBeanForEntityClassException, IntrospectionException {
        this.ui = ui;
        this.dao = (D) ui.getDaoBean(entriesClass);
        this.captionKey = entriesClass.getSimpleName() + MessageKeys._ENTRIES;

        // container configuration
        Map<String, Object> qc = new HashMap<>();
        qc.put("component", this);
        entriesQueryFactory.setQueryConfiguration(qc);
        for (PropertyDescriptor pd : BeanUtil.getBeanPropertyDescriptor(entriesClass)) {
            boolean sortable
                    = Number.class.isAssignableFrom(pd.getPropertyType())
                    || Date.class.isAssignableFrom(pd.getPropertyType())
                    || String.class.isAssignableFrom(pd.getPropertyType())
                    || EntityModelRegistry.class.isAssignableFrom(pd.getPropertyType())
                    || EntityModelGeo.class.isAssignableFrom(pd.getPropertyType());
            containerEntries.addContainerProperty(pd.getName(), pd.getPropertyType(), null, true, sortable);
        }

        this.tableEntriesContextMenu = new ContextMenu(tableEntries, false);
        this.tableEntriesContextMenuItemEdit = tableEntriesContextMenu.addItem(MessageKeys.EDIT, FontAwesome.EDIT, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuItemDelete = tableEntriesContextMenu.addItem(MessageKeys.DELETE, FontAwesome.TRASH, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuItemRestore = tableEntriesContextMenu.addItem(MessageKeys.RESTORE, FontAwesome.REFRESH, tableEntriesContextMenuCommand);

        // component settings
        setId(entriesClass.getSimpleName().toLowerCase());
        setSizeFull();
        setCaption(captionKey);
        setStyleName("entries");
        addStyleName(entriesClass.getSimpleName());

        // content
        buildHeader();
        buildTable();
        buildFooter();

    }

    protected void actionAdd() throws NoDialogForEntityClassException, NoDaoBeanForEntityClassException, IllegalAccessException, InstantiationException {
        ui.dialog(entriesClass.newInstance());
    }

    protected void actionEdit(Long id) throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException {
        ui.dialog(entriesClass, id).addCloseListener(entryDialogCloseListener);
        refreshEntryIdUriFragment(id);
    }

    protected final void actionDelete(Long id) throws DaoUnableToDeleteObjectException {
        dao.doDelete((EntityModelEntry) dao.getObject(id), ui.getUser());
    }

    protected void runExportXls(String... parameters) {

        if (exportWindow != null) {
            exportWindow.close();
            exportWindow = null;
        }
        exportWindow = new XlsExportWindow();
        ui.addWindow(exportWindow);

        exportThread = new Thread(new AbstractEntriesComponent.XlsExportThread(parameters));
        exportThread.start();
    }

    protected abstract File actionExportXls(ru.snetwork.social.ui.view.AbstractEntriesComponent.XlsExportWindow window, String... parameters);

    protected void actionDeletePrompt(final Collection<Long> selection) {
        ConfirmDialog cd = ConfirmDialog.show(ui,
                ui.getMessage(MessageKeys.DELETE),
                ui.getMessage(MessageKeys.CONFIRM_DELETE_ENTRIES),
                ui.getMessage(MessageKeys.DELETE),
                ui.getMessage(MessageKeys.CANCEL),
                new ConfirmDialog.Listener() {
            @Override
            public void onClose(ConfirmDialog cd) {
                try {
                    if (cd.isConfirmed()) {
                        Iterator<Long> iterator = selection.iterator();
                        while (iterator.hasNext()) {
                            actionDelete((Long) iterator.next());
                        }
                        refresh();
                        tableEntries.setValue(null);
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
        cd.getOkButton().setStyleName(ValoTheme.BUTTON_DANGER);
    }

    protected final void actionRestore(Long id) {
        dao.doRestore((EntityModelEntry) dao.getObject(id), ui.getUser());

    }

    protected void actionRestorePrompt(final Collection<Long> selection) {
        ConfirmDialog.show(ui, ui.getMessage(MessageKeys.CONFIRM_RESTORE_ENTRIES), new ConfirmDialog.Listener() {
            @Override
            public void onClose(ConfirmDialog cd) {
                try {
                    if (cd.isConfirmed()) {
                        Iterator<Long> iterator = selection.iterator();
                        while (iterator.hasNext()) {
                            actionRestore((Long) iterator.next());
                        }
                        refresh();
                        tableEntries.setValue(null);
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
    }

    protected void addHeaderFilterComponent(Component c) {
        layoutHeaderFilter.addComponent(c, layoutHeaderFilter.getComponentIndex(bFilterMore));
        layoutHeaderFilter.setComponentAlignment(c, Alignment.MIDDLE_LEFT);
    }

    protected boolean isHeaderFilterComponent(Component c) {
        return c.getParent() == layoutHeaderFilter;
    }

    protected void removeHeaderFilterComponent(Component c) {
        layoutHeaderFilter.removeComponent(c);
    }

    protected void addHeaderMoreComponent(Component c) {
        layoutHeaderFilterMore.addComponent(c);
        bFilterMore.setVisible(true);
    }

    protected void addHeaderMoreComponent(Component c, int index) {
        layoutHeaderFilterMore.addComponent(c, index);
        bFilterMore.setVisible(true);
    }

    protected boolean isHeaderMoreComponent(Component c) {
        return c.getParent() == layoutHeaderFilterMore;
    }

    protected void removeHeaderMoreComponent(Component c) {
        layoutHeaderFilterMore.removeComponent(c);
        bFilterMore.setVisible(layoutHeaderFilterMore.getComponentCount() > 0);
    }

    protected void removeHeaderMoreAllComponents() {
        layoutHeaderFilterMore.removeAllComponents();
        bFilterMore.setVisible(false);
    }

    protected void setHeaderMorePopupVisibility(boolean popupVisible) {
        bFilterMore.setPopupVisible(popupVisible);
    }

    protected void setHeaderMorePopupWidth(String width) {
        layoutHeaderFilterMore.setWidth(width);
    }

    protected void addHeaderButtonComponent(Component c) {
        addHeaderButtonComponent(c, null);
    }

    protected void addHeaderButtonComponent(Component c, Component beforeComponent) {
        if (beforeComponent != null && beforeComponent.getParent() != null && beforeComponent.getParent() == layoutHeaderButtons) {
            layoutHeaderButtons.addComponent(c, layoutHeaderButtons.getComponentIndex(beforeComponent));
        } else {
            layoutHeaderButtons.addComponent(c);
        }
    }

    protected void addHeaderButtonComponentAsFirst(Component c) {
        layoutHeaderButtons.addComponentAsFirst(c);
    }

    protected void removeHeaderButtonComponent(Component c) {
        layoutHeaderButtons.removeComponent(c);
    }

    private void buildHeader() {
        layoutHeader.setStyleName("viewheader");
        layoutHeader.setWidth("100%");
        layoutHeader.setSpacing(true);
        layoutHeader.addComponent(layoutHeaderFilter);
        layoutHeader.addComponent(layoutHeaderButtons);
        addComponent(layoutHeader);

        // Filter
        layoutHeaderFilter.setSpacing(true);
        fSearch.setIcon(FontAwesome.SEARCH);
        fSearch.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        fSearch.setImmediate(true);
        fSearch.setWidth("300px");
        fSearch.setNullRepresentation("");
        fSearch.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                refreshData();
            }
        });
        layoutHeaderFilter.addComponent(fSearch);
        layoutHeaderFilter.setComponentAlignment(fSearch, Alignment.MIDDLE_LEFT);
        layoutHeaderFilterMore.setSpacing(true);
        layoutHeaderFilterMore.setMargin(true);
        layoutHeaderFilterMore.setWidth("350px");
        layoutHeaderFilterMore.setStyleName("viewheader");
        layoutHeaderFilterMore.addStyleName("filtermore");
        bFilterMore.setContent(layoutHeaderFilterMore);
        bFilterMore.setStyleName(ValoTheme.BUTTON_SMALL);
        bFilterMore.setVisible(false);
        layoutHeaderFilter.addComponent(bFilterMore);
        layoutHeaderFilter.setComponentAlignment(bFilterMore, Alignment.MIDDLE_LEFT);
        bFilterReset.addClickListener(this);
        bFilterReset.setIcon(FontAwesome.TIMES);
        bFilterReset.setStyleName(ValoTheme.BUTTON_SMALL);
        bFilterReset.setVisible(false);
        layoutHeaderFilter.addComponent(bFilterReset);
        layoutHeaderFilter.setComponentAlignment(bFilterMore, Alignment.MIDDLE_LEFT);
        layoutHeader.setExpandRatio(layoutHeaderFilter, 1.0f);

        // Buttons
        layoutHeaderButtons.setSpacing(true);
        bEdit.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bEdit);
        bDelete.setStyleName(ValoTheme.BUTTON_DANGER);
        bDelete.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bDelete);
        bRestore.setStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(layoutHeaderButtonsSpacer);
        bAdd.setStyleName(ValoTheme.BUTTON_PRIMARY);
        bAdd.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bAdd);

    }

    private void buildTable() {

        // table configuration
        tableEntries.setId(this.getClass().getSimpleName() + "-entries");
        tableEntries.setPageLength(30);
        tableEntries.setStyleName("viewentries");
        tableEntries.setSizeFull();
        tableEntries.setVisibleColumns(
                EntityPropertyKeys.ID,
                EntityPropertyKeys.DATE_CREATED_AT,
                EntityPropertyKeys.DATE_MODIFIED_AT);
        tableEntries.setColumnAlignment(EntityPropertyKeys.ID, Table.Align.RIGHT);
        tableEntries.setColumnWidth(EntityPropertyKeys.ID, 75);
        tableEntries.setSelectable(true);
        tableEntries.setMultiSelect(true);
        tableEntries.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                try {
                    if (event.isDoubleClick()) {
                        actionEdit((Long) event.getItemId());
                    } else if (MouseEventDetails.MouseButton.RIGHT.equals(event.getButton())) {
                        if (!tableEntriesSelection.contains(event.getItemId())) {
                            if (event.isCtrlKey()) {
                                tableEntries.select(event.getItemId());
                            } else {
                                tableEntries.setValue(null);
                                tableEntries.select(event.getItemId());
                            }
                        }
                        tableEntriesContextMenu.open(event.getClientX(), event.getClientY());
                        contextMenuOpened((Long) event.getItemId(), event.getItem(), event.getPropertyId());
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
        tableEntries.setCellStyleGenerator(new Table.CellStyleGenerator() {
            @Override
            public String getStyle(Table source, Object itemId, Object propertyId) {
                StringBuilder sb = new StringBuilder();
                Item item = source.getItem(itemId);
                if (propertyId == null) {
                    if (item != null
                            && item.getItemProperty(EntityPropertyKeys.STATE) != null
                            && item.getItemProperty(EntityPropertyKeys.STATE).getValue() != null) {
                        sb.append(item.getItemProperty(EntityPropertyKeys.STATE).getValue().toString().toLowerCase());
                    }
                    if (item.getItemProperty(EntityPropertyKeys.DISABLED) != null
                            && Boolean.TRUE.equals(item.getItemProperty(EntityPropertyKeys.DISABLED).getValue())) {
                        sb.append(" deleted");
                    }
                    if (item.getItemProperty(EntityPropertyKeys.DELETED) != null
                            && Boolean.TRUE.equals(item.getItemProperty(EntityPropertyKeys.DELETED).getValue())) {
                        sb.append(" deleted");
                    }
                } else {
                    return propertyId.toString();
                }
                return sb.toString();
            }
        });
        tableEntries.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object value = event.getProperty().getValue();
                tableEntriesSelection.clear();
                if (value != null && value instanceof Collection) {
                    ((Collection) value).iterator().forEachRemaining(new Consumer() {
                        @Override
                        public void accept(Object o) {
                            if (o != null && o instanceof Long) {
                                tableEntriesSelection.add((Long) o);
                            }
                        }
                    });
                } else if (value != null && value instanceof Long) {
                    tableEntriesSelection.add((Long) value);
                }
                refreshActions();
            }
        });

        layoutTable.setSizeFull();
        layoutTable.addComponent(tableEntries);
        layoutTable.setExpandRatio(tableEntries, 1.0f);
        addComponent(layoutTable);
        setExpandRatio(layoutTable, 1.0f);
    }

    public void buildFooter() {
        layoutFooter.setStyleName("viewfooter");
        layoutFooter.setWidth("100%");
        layoutFooter.setSpacing(true);
        layoutFooter.addComponent(labelCount);
        layoutFooter.setExpandRatio(labelCount, 1.0f);
        layoutFooter.setComponentAlignment(labelCount, Alignment.MIDDLE_LEFT);
        cbAutoRefresh.setImmediate(true);
        layoutFooter.addComponent(cbAutoRefresh);
        layoutFooter.setComponentAlignment(cbAutoRefresh, Alignment.MIDDLE_RIGHT);
        bRefresh.setIcon(FontAwesome.REFRESH);
        bRefresh.setImmediate(true);
        bRefresh.setStyleName(ValoTheme.BUTTON_TINY);
        layoutFooter.addComponent(bRefresh);
        addComponent(layoutFooter);

        bExportToExcel.setIcon(FontAwesome.FILE_EXCEL_O);
        bExportToExcel.setStyleName(ValoTheme.BUTTON_TINY);
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        try {
            if (event.getButton() == bAdd) {
                actionAdd();
            } else if (event.getButton() == bEdit && tableEntriesSelection.size() == 1) {
                actionEdit(tableEntriesSelection.iterator().next());
            } else if (event.getButton() == bDelete && tableEntriesSelection.size() >= 1) {
                actionDeletePrompt(tableEntriesSelection);
            } else if (event.getButton() == bRestore && tableEntriesSelection.size() >= 1) {
                actionRestorePrompt(tableEntriesSelection);
            } else if (event.getButton() == bFilterReset) {
                resetFilters();
            } else if (event.getButton() == bRefresh) {
                refresh();
            } else if (event.getButton() == bExportToExcel) {
                runExportXls(null);
//                actionExportXls(null);
                bExportToExcel.setEnabled(true);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    protected void contextMenuOpened(Long tableItemId, Item tableItem, Object tablePropertyId) {

        tableEntriesContextMenuItemEdit.setVisible(tableEntriesSelection.size() == 1);
        boolean showDelete = false;
        boolean showRestore = false;
        Iterator<Long> iterator = tableEntriesSelection.iterator();
        while (iterator.hasNext()) {
            Long itemId = iterator.next();
            if (tableEntries.getItem(itemId) != null) {
                if (tableEntries.getItem(itemId).getItemProperty(EntityPropertyKeys.DATE_DELETED_AT).getValue() != null) {
                    showRestore = true;
                } else {
                    showDelete = true;
                }
            }
        }
        tableEntriesContextMenuItemDelete.setVisible(showDelete);
        tableEntriesContextMenuItemRestore.setVisible(showRestore);
    }

    protected void contextMenuSelected(MenuItem menuItem) {
        try {
            if (tableEntriesContextMenuItemEdit == menuItem && tableEntriesSelection.size() == 1) {
                actionEdit((Long) tableEntriesSelection.iterator().next());
            } else if (tableEntriesContextMenuItemDelete == menuItem && tableEntriesSelection.size() > 0) {
                actionDeletePrompt(tableEntriesSelection);
            } else if (tableEntriesContextMenuItemRestore == menuItem && tableEntriesSelection.size() > 0) {
                actionRestorePrompt(tableEntriesSelection);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    @Override
    public boolean isDisplayed() {
        return displayed;
    }

    @Override
    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    protected String formatTablePropertyValue(Object colId, Property<?> property) {
        return formatTablePropertyValue(null, colId, property);
    }

    protected String formatTablePropertyValue(Object rowId, Object colId, Property<?> property) {
        String value
                = ui.formatPropertyValue(
                        tableEntriesDateFormat,
                        (String) colId,
                        property.getType(),
                        property.getValue());
        return value;
    }

    public String getCaptionKey() {
        return captionKey;
    }

    public void setCaptionKey(String captionKey) {
        this.captionKey = captionKey;
    }

    public int getEntriesCount() {
        // otherwise count size
        if (entriesCount == null) {

//            // -- Debug --
//            Date start = new Date();
//            System.out.println("\n\n--- " + getClass().getSimpleName() + " -> size : start " + queryTotalWhere);
//            System.out.println(" - queryTotalWhere: " + queryTotalWhere);
//            System.out.println(" - queryWhere: " + queryWhere);
//            System.out.println(" - queryTotalWhere & queryWhere: "
//                    + (queryTotalWhere != null && queryWhere != null
//                    && queryTotalWhere.containsAll(queryWhere)
//                    && queryWhere.containsAll(queryTotalWhere)));
//            // -- Debug --
            boolean countApproximate = false;
            boolean totalApproximate = false;

            if ((queryTotalWhere == null || queryTotalWhere.isEmpty())
                    && (queryWhere == null || queryWhere.isEmpty())
                    && querySubclass == null) {
                totalApproximate = true;
                entriesTotal = dao.countTotal(entriesClass).intValue();
                if (entriesTotal < 10000) {
                    entriesCount = dao.count(null, queryTotalJoinRelations, queryTotalWhere, queryShowDeleted).intValue();
                } else {
                    countApproximate = true;
                    entriesCount = entriesTotal;
                }
            } else {
                entriesCount = dao.count(querySubclass,
                        StringUtils.replace(queryJoinRelations, " fetch ", " "),
                        queryWhere, queryShowDeleted).intValue();
                if (queryTotalWhere == null && queryWhere == null
                        || queryTotalWhere != null && queryWhere != null
                        && queryTotalWhere.containsAll(queryWhere)
                        && queryWhere.containsAll(queryTotalWhere)) {
                    entriesTotal = entriesCount;
                }
//                else {
//                    entriesTotal = dao.countTotal(entriesClass).intValue();
//                    if (entriesTotal < 10000 || queryTotalWhere != null) {
//                        entriesTotal = dao.count(
//                                querySubclass,
//                                queryTotalJoinRelations,
//                                queryTotalWhere,
//                                queryShowDeleted).intValue();
//                    } else {
//                        totalApproximate = true;
//                    }
//                }
                else { //для увеличения производительности
                    entriesTotal = 0;
                    totalApproximate = true;
                }
            }
            refreshCountLabel(entriesCount, countApproximate, entriesTotal, totalApproximate);

//                // -- Debug --
//                System.out.println("  - " + component.getClass().getSimpleName() + " -> size : " + (new Date().getTime() - start.getTime()));
//                System.out.println("\n\n");
//                // -- Debug --
        }

        return entriesCount;
    }

    @Override
    public final void localize() {
        setCaption(ui.getMessage(captionKey));
        fSearch.setInputPrompt(ui.getMessage(MessageKeys.SEARCH));
        bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE));
        bAdd.setCaption(ui.getMessage(MessageKeys.ADD));
        bEdit.setCaption(ui.getMessage(MessageKeys.EDIT));
        bDelete.setCaption(ui.getMessage(MessageKeys.DELETE));
        bRestore.setCaption(ui.getMessage(MessageKeys.RESTORE));
        bRefresh.setDescription(ui.getMessage(MessageKeys.REFRESH));
        bRefresh.setCaption("");
        bExportToExcel.setDescription(ui.getMessage(MessageKeys.EXPORT_TO_XLS));
        bExportToExcel.setCaption("");
        cbAutoRefresh.setCaption(ui.getMessage(MessageKeys.AUTO_REFRESH));
        tableEntriesContextMenuItemEdit.setText(ui.getMessage(MessageKeys.EDIT));
        tableEntriesContextMenuItemDelete.setText(ui.getMessage(MessageKeys.DELETE));
        tableEntriesContextMenuItemRestore.setText(ui.getMessage(MessageKeys.RESTORE));
        for (Object columnId : tableEntries.getVisibleColumns()) {
            tableEntries.setColumnHeader(columnId, ui.getMessage(MessageKeys.COLUMN_X + String.valueOf(columnId)));
        }
        localizeComponents();
    }

    protected abstract void localizeComponents();

    /**
     * Refresh all visible controls and data
     */
    @Override
    public final void refresh() {

        // refresh only if not already refreshing 
        if (!refreshing) {

            // toggle refreshing flag
            refreshing = true;

            try {

                refreshFilterControls();
                refreshData();
                refreshActions();

                if (tableEntriesSelectOnload != null) {
                    Long selectId = tableEntriesSelectOnload;
                    tableEntriesSelectOnload = null;
                    selectEntryId(selectId);
                    actionEdit(selectId);
                }

            } catch (Exception ex) {
                ui.handleException(ex);
            } finally {
                refreshing = false;
            }
        }

    }

    /**
     * Refresh action controls
     */
    protected void refreshActions() {
        for (Button b : actionButtons) {
            layoutHeaderButtons.removeComponent(b);
        }
        if (!tableEntriesSelection.isEmpty()) {
            layoutHeaderButtons.addComponent(bDelete, layoutHeader.getComponentCount() - 2);
            if (tableEntriesSelection.size() == 1) {
                layoutHeaderButtons.addComponent(bEdit, layoutHeader.getComponentCount() - 2);
            }
        }
    }

    protected void refreshCountLabel(int count, boolean countApproximate, int total, boolean totalApproximate) {
        labelCount.setValue(
                count < total
                        ? ui.getMessage(MessageKeys.SHOWING_X) + ": "
                        + count + " " + ui.getMessage(MessageKeys.X_OF_TOTAL_X) + " "
                        + (totalApproximate ? "~" : "")
                        + total //+ ui.getMessage(MessageKeys.X_ENTRIES)
                        : ui.getMessage(MessageKeys.SHOWING_X) + ": "
                        + (countApproximate ? "~" : "")
                        + count
        ); //+ ui.getMessage(MessageKeys.X_ENTRIES))

    }

    /**
     * Refresh table data
     */
    protected final void refreshData() {

        int pageStartIndex = tableEntries.getCurrentPageFirstItemIndex();
        int pageEndIndex = pageStartIndex + tableEntries.getPageLength();

        // refresh filters buttons
        refreshFilterButtons();

        // where clause
        queryWhere = refreshWhereClauses();

        // refresh table columns
        refreshTableColumns();

        // retrieve list
        entriesCount = null;

        if (getEntriesCount() < pageEndIndex) {
            containerEntries.refresh();
        } else {
            // Workaround to prevent table scroll-position jumping in Chrome
            // Debugging rows -69295870_238093 55983
            containerEntries.getQueryView().refresh();
            tableEntries.refreshRowCache(false);
        }
        if (tableEntries.size() > 0 && tableEntries.size() < 65535) {
            bExportToExcel.setEnabled(true);
            bExportToExcel.setDisableOnClick(true);
        } else {
            bExportToExcel.setEnabled(false);
        }

    }

    protected void refreshEntryIdUriFragment(Long entryId) {

        try {
            String oldUri = Page.getCurrent().getUriFragment();
            String newUri;
            if (entryId != null) {
                String oldPath = oldUri.replaceAll("\\-entry\\-\\/\\d+", "");
                ArrayList<String> newPath = new ArrayList(Arrays.asList(oldPath.split("\\/+")));
                newPath.add("-entry-");
                newPath.add(String.valueOf(entryId));
                newUri = StringUtils.join(newPath, "/");
            } else {
                newUri = oldUri.replaceAll("\\/\\-entry\\-\\/\\d+", "");
            }
            if (!StringUtils.equalsIgnoreCase(oldUri, newUri)) {
                Page.getCurrent().setUriFragment(newUri, false);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    private void refreshFilterButtons() {
        boolean showResetButton = false;
        Iterator iterator1 = layoutHeaderFilter.iterator();
        while (iterator1.hasNext()) {
            Component c = (Component) iterator1.next();
            if (c instanceof Field
                    && !((Field) c).isEmpty()
                    && !MessageKeys.ALL.equals(((Field) c).getValue())
                    && !ShowOptions.ONLY_ACTIVE.equals(((Field) c).getValue())
                    && !ShowOptions.UNREVIEWED.equals(((Field) c).getValue())) {
                showResetButton = true;
                break;
            }
        }
        int countMoreFilters = 0;
        Iterator iterator2 = layoutHeaderFilterMore.iterator();
        while (iterator2.hasNext()) {
            Component c = (Component) iterator2.next();
            if (c instanceof Field
                    && !((Field) c).isEmpty()
                    && !MessageKeys.ALL.equals(((Field) c).getValue())
                    && !ShowOptions.ONLY_ACTIVE.equals(((Field) c).getValue())
                    && !ShowOptions.UNREVIEWED.equals(((Field) c).getValue())) {
                countMoreFilters++;
            }
        }
        if (countMoreFilters > 0) {
            bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE) + " (" + countMoreFilters + ")");
            bFilterMore.addStyleName(ValoTheme.BUTTON_PRIMARY);
            showResetButton = true;
        } else {
            bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE));
            bFilterMore.removeStyleName(ValoTheme.BUTTON_PRIMARY);
        }
        bFilterReset.setVisible(showResetButton && bFilterMore.isVisible());
    }

    /**
     * Refresh filter controls is a subclass has such controls.
     */
    protected abstract void refreshFilterControls();

    protected void refreshTableColumns() {
        // nothing to do - for future use
    }

    protected abstract List<WhereClause> refreshWhereClauses();

    protected void resetFilters() {
        Iterator iterator1 = layoutHeaderFilter.iterator();
        while (iterator1.hasNext()) {
            Component c = (Component) iterator1.next();
            resetFilterField(c);
        }
        Iterator iterator2 = layoutHeaderFilterMore.iterator();
        while (iterator2.hasNext()) {
            Component c = (Component) iterator2.next();
            resetFilterField(c);
        }
        refresh();
    }

    private void resetFilterField(Component c) throws Converter.ConversionException, Property.ReadOnlyException {
        if (c instanceof AbstractField && !((AbstractField) c).isEmpty()) {
            AbstractField field = (AbstractField) c;
            List<Property.ValueChangeListener> listeners = new LinkedList<>();
            for (Object l : field.getListeners(Property.ValueChangeEvent.class)) {
                if (l instanceof Property.ValueChangeListener) {
                    listeners.add((Property.ValueChangeListener) l);
                    field.removeValueChangeListener((Property.ValueChangeListener) l);
                }
            }
            if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(MessageKeys.ALL)) {
                field.setValue(MessageKeys.ALL);
            } else if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(ShowOptions.ONLY_ACTIVE)) {
                field.setValue(ShowOptions.ONLY_ACTIVE);
            } else if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(ShowOptions.UNREVIEWED)) {
                field.setValue(ShowOptions.UNREVIEWED);
            } else {
                field.setValue(null);
            }
            for (Property.ValueChangeListener l : listeners) {
                field.addValueChangeListener(l);
            }
        } else if (c instanceof TreeComboBox && !((TreeComboBox) c).isEmpty()) {
            TreeComboBox tree = (TreeComboBox) c;
            List<Property.ValueChangeListener> listeners = new LinkedList<>();
            for (Object l : tree.getListeners(Property.ValueChangeEvent.class)) {
                if (l instanceof Property.ValueChangeListener) {
                    listeners.add((Property.ValueChangeListener) l);
                    tree.removeValueChangeListener((Property.ValueChangeListener) l);
                }
            }
            tree.setValue(null);
            for (Property.ValueChangeListener l : listeners) {
                tree.addValueChangeListener((Property.ValueChangeListener) l);
            }
        }
    }

    @Override
    public void permissions() {
        for (Button b : new Button[]{bAdd, bDelete, bEdit, bRestore}) {
            b.setEnabled(ui.isUserAdministrator() || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                            ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                            : "");
        }
        for (MenuItem b : new MenuItem[]{
            tableEntriesContextMenuItemDelete,
            tableEntriesContextMenuItemEdit,
            tableEntriesContextMenuItemRestore}) {
            b.setEnabled(ui.isUserAdministrator() || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                            ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                            : "");
        }
    }

    @Override
    public boolean push() {
        if (!refreshing && Boolean.TRUE.equals(cbAutoRefresh.getValue())) {
            containerEntries.refresh();
            return true;
        }
        return false;
    }

    protected boolean selectEntryId(Long selectId) {

        if (tableEntries.getVisibleItemIds().contains(selectId)) {

            tableEntries.select(selectId);
            return true;

        } else {

            tableEntries.setSortContainerPropertyId("id");
            tableEntries.setSortAscending(false);

            List<WhereClause> checkWhereClauses = refreshWhereClauses();
            if (checkWhereClauses == null) {
                checkWhereClauses = new ArrayList<>(1);
            }
            checkWhereClauses.add(new WhereClauseNumber("o.id", selectId));
            List checkSelectedId = dao.list(null, querySubclass, queryJoinRelations, checkWhereClauses, 0, 1);

            if (checkSelectedId != null && checkSelectedId.isEmpty()) {

                SnSocialUI.notificationShow(
                        ui.getMessage(MessageKeys.NOT_FOUND),
                        ui.getMessage(MessageKeys.REQUESTED_ENTRY_NOT_FOUND),
                        Notification.Type.WARNING_MESSAGE);

                return false;

            } else {

                boolean foundSelected = false;
                int i = 0;
                int max = tableEntries.size();
                int min = 0;
                int offset = 0;
                OrderBy orderBy = new OrderBy("o.id", true);
                while (!foundSelected && i < 100) {
                    int shift = (max - min) / 2;
                    List foundCurrentId = dao.list(null, querySubclass, queryJoinRelations, refreshWhereClauses(), offset + shift, 1, orderBy);
                    Long currentId = foundCurrentId.isEmpty() ? 0l : ((EntityModelEntry) foundCurrentId.get(0)).getId().longValue();
                    if (currentId < selectId) {
                        max = max - shift;
                    } else if (currentId > selectId) {
                        offset += shift;
                        min = min + shift;
                    } else {
                        foundSelected = true;
                    }

                    if (shift < tableEntries.getPageLength() / 2) {
                        foundSelected = true;
                    }
                    i++;
                }

                if (foundSelected) {
                    tableEntries.setCurrentPageFirstItemIndex(offset);
                    tableEntries.select(selectId);
                    return true;
                }

                return false;

            }

        }
    }

    @Override
    public void senseUriFragment(String params) {

        try {
            if (params.matches(".*\\-entry\\-\\/\\d+$")) {
                String itemId = params.split("\\-entry\\-\\/")[1];
                tableEntriesSelectOnload = Long.valueOf(itemId);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    protected static class EntriesQuery extends AbstractBeanQuery<EntityModelEntry> {

        AbstractEntriesComponent component;
        AbstractEntriesDaoBean dao;
        SnSocialUI ui;
        List<OrderBy> orderBy;
        List<String> orderByJoin;
        List<String> orderBySelect;

        public EntriesQuery(QueryDefinition queryDefinition, Map<String, Object> queryConfiguration, Object[] sortPropertyIds, boolean[] sortStates) {
            super(queryDefinition, queryConfiguration, sortPropertyIds, sortStates);
            component = (AbstractEntriesComponent) queryConfiguration.get("component");
            dao = component.dao;
            ui = component.ui;
            orderBy = new ArrayList<>();
            orderBySelect = new ArrayList<>();
            orderByJoin = new ArrayList<>();
            if (component.entriesPreOrderBy != null) {
                orderBy.add(component.entriesPreOrderBy);
            }
            if (sortPropertyIds != null && sortPropertyIds.length > 0) {
                //orderBy = new ArrayList<>(sortPropertyIds.length);
                for (int i = 0; i < sortPropertyIds.length; i++) {
                    try {
                        if (EntityModelRegistry.class.isAssignableFrom(BeanUtil.getPropertyType(component.entriesClass, sortPropertyIds[i].toString()))) {
                            orderBy.add(new OrderBy("o_" + sortPropertyIds[i] + ".name", sortStates.length > i ? !sortStates[i] : false));
                            orderByJoin.add(" left join o." + sortPropertyIds[i] + " as o_" + sortPropertyIds[i]);
                            orderBySelect.add("o_" + sortPropertyIds[i]);
                        } else if (EntityModelGeo.class.isAssignableFrom(BeanUtil.getPropertyType(component.entriesClass, sortPropertyIds[i].toString()))) {
                            orderBy.add(new OrderBy("o_" + sortPropertyIds[i] + ".nameRu", sortStates.length > i ? !sortStates[i] : false));
                            orderByJoin.add(" left join o." + sortPropertyIds[i] + " as o_" + sortPropertyIds[i]);
                            orderBySelect.add("o_" + sortPropertyIds[i]);
                        } else if (Date.class.isAssignableFrom(BeanUtil.getPropertyType(component.entriesClass, sortPropertyIds[i].toString()))) {
                            Boolean desc = sortStates.length > i ? !sortStates[i] : false;
                            orderBy.add(new OrderBy("o." + sortPropertyIds[i], desc, true)); //nullsLast for date columns
                        } else {
                            orderBy.add(new OrderBy("o." + sortPropertyIds[i], sortStates.length > i ? !sortStates[i] : false));
                        }
                    } catch (Exception ex) {
                        component.log.error("IntrospectionException in EntriesQuery: " + ex);
                    }

                }
            }

            if (orderBy.isEmpty()) {
                orderBy.add(new OrderBy("o.id", true));
            }
        }

        @Override
        protected EntityModelEntry constructBean() {
            try {
                return (EntityModelEntry) component.entriesClass.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                // ignore Exception
//                ex.printStackTrace(System.out);
                component.log.error(ex, ex);
                return null;
            }
        }

        @Override
        public int size() {

            // if not displayed and not explicitly refreshing pretend size 0
            if (!component.displayed && !component.refreshing) {
                return 0;
            }

            return component.getEntriesCount();
        }

        @Override
        protected List<EntityModelEntry> loadBeans(int i, int i1) {

//            /* -- Debug -- */
//            Date start = new Date();
//            System.out.println("\n\n--- " + component.getClass().getSimpleName() + " -> loadBeans " + i + " " + i1 + " : start");
//            System.out.println("  - " + component.queryJoinRelations);
//            System.out.println("  - " + component.queryWhere);
//            /* -- Debug -- */
            String select = component.querySelect;
            String join = component.queryJoinRelations;
            if (!orderByJoin.isEmpty()) {
                select = StringUtils.join(orderBySelect, ", ");
                if (StringUtils.isNotBlank(component.querySelect)) {
                    select = component.querySelect + ", " + select;
                } else {
                    select = "o, " + select;
                }
                join = StringUtils.join(orderByJoin, " ");
                if (StringUtils.isNotBlank(component.queryJoinRelations)) {
                    join = component.queryJoinRelations + " " + join;
                }
            }

            List list = dao.list(select,
                    component.querySubclass,
                    join,
                    component.queryWhere,
                    component.queryShowDeleted, i, i1,
                    orderBy.toArray(new OrderBy[]{}));

            List result;

            if (list != null && !list.isEmpty() && !orderByJoin.isEmpty()) {
                result = new ArrayList(list.size());
                int resultEndIndex = StringUtils.isNotBlank(component.querySelect)
                        ? component.querySelect.split(",").length
                        : 0;
                for (Object row : list) {
                    Object[] r = (Object[]) row;
                    if (resultEndIndex > 1) {
                        result.add(ArrayUtils.subarray(r, 0, resultEndIndex));
                    } else {
                        result.add(r[0]);
                    }
                }
            } else {
                result = list;
            }

//            /* -- Debug -- */
//            System.out.println("  - " + component.getClass().getSimpleName() + " -> loadBeans " + i + " " + i1 + " : " + (new Date().getTime() - start.getTime()));
//            System.out.println("\n\n");
//            /* -- Debug -- */
            return result;
        }

        @Override
        protected void saveBeans(List<EntityModelEntry> list, List<EntityModelEntry> list1, List<EntityModelEntry> list2) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public class EntriesTable extends Table {

        public EntriesTable(String caption, Container dataSource) {
            super(caption, dataSource);
        }

        @Override
        protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
            String value = formatTablePropertyValue(rowId, colId, property);
            return value != null ? value : super.formatPropertyValue(rowId, colId, property);
        }

        public void refreshRowCache(boolean fullResetPageBuffer) {
            if (fullResetPageBuffer) {
                resetPageBuffer();
            } else {
                resetPageBufferSoft();
            }
            refreshRenderedCells();
        }

    };

    protected class XlsExportThread implements Runnable { //extends Thread

        private File file;
        private final String[] parameters;

        public XlsExportThread(String[] parameters) {
            this.parameters = parameters;
        }

        @Override
        public void run() {
            file = actionExportXls(exportWindow, parameters);
            if (file != null) {
                exportWindow.progressBar.setCaption(ui.getMessage(MessageKeys.EXPORT_COMPLETE));
                String fileKey = RandomStringUtils.randomAlphanumeric(16);
                ui.setHttpSessionAttribute(fileKey + ":download", file.getName());
                ui.setHttpSessionAttribute(fileKey + ":filename", entriesClass.getSimpleName() + "." + StringUtils.substringAfter(file.getName(), "."));

                exportWindow.labelLink.setValue(exportWindow.labelLink.getValue()
                        + " <a href='"
                        + VaadinServlet.getCurrent().getServletContext().getContextPath()
                        + "/download?fileKey=" + fileKey
                        + "' target='_blank'>"
                        + entriesClass.getSimpleName() + "." + StringUtils.substringAfter(file.getName(), ".")
                        + "</a>");
                exportWindow.toggleControls();

                Page.getCurrent().open(
                        VaadinServlet.getCurrent().getServletContext().getContextPath()
                        + "/download?fileKey=" + fileKey, fileKey, true);
            } else {
                exportWindow.close();
            }
        }

        public File getFile() {
            return file;
        }

    }

    protected class XlsExportWindow extends Window implements Button.ClickListener {

        private final VerticalLayout layoutRoot = new VerticalLayout();
        private final HorizontalLayout layoutButtons = new HorizontalLayout();
        private final Button buttonCancel = new Button(ui.getMessage(MessageKeys.CANCEL), this);
        private final Button buttonClose = new Button(ui.getMessage(MessageKeys.CLOSE), this);
        private final ProgressBar progressBar = new ProgressBar();
        private final Label labelLink = new Label(ui.getMessage(MessageKeys.EXPORT_DOWLOAND_AUTO_OR_LINK), ContentMode.HTML);

        public XlsExportWindow() {
            this.setWidth("450px");
            this.setCaption(ui.getMessage(MessageKeys.EXPORT_TO_XLS));
            this.setModal(true);
            this.setClosable(false);
            this.setResizable(false);
            this.setDraggable(false);
            this.center();
            layoutRoot.setStyleName("xls-export-window");
            layoutRoot.setMargin(true);
            layoutRoot.setSpacing(true);
            progressBar.setWidth("100%");
            progressBar.setCaption(ui.getMessage(MessageKeys.DATA_PREPARING));
            progressBar.setVisible(true);
            labelLink.setVisible(false);
            layoutRoot.addComponent(progressBar);
            layoutRoot.addComponent(labelLink);
            buttonCancel.setVisible(true);
            buttonClose.setVisible(false);
            layoutButtons.setWidth("100%");
            layoutButtons.addComponent(buttonCancel);
            layoutButtons.setComponentAlignment(buttonCancel, Alignment.BOTTOM_RIGHT);
            layoutButtons.addComponent(buttonClose);
            layoutButtons.setComponentAlignment(buttonClose, Alignment.BOTTOM_RIGHT);
            layoutRoot.addComponent(layoutButtons);
            setContent(layoutRoot);

            ui.setPollInterval(300);
            addCloseListener(new Window.CloseListener() {
                @Override
                public void windowClose(CloseEvent e) {
                    ui.setPollInterval(-1);
                    System.gc();
                    bExportToExcel.setEnabled(true);
                }
            });
        }

        @Override
        public void buttonClick(Button.ClickEvent event) {
            if (event.getButton().equals(buttonCancel)) {
                if (exportThread.isAlive()) {
                    exportThread.interrupt();
                    this.close();
                }
            } else if (event.getButton().equals(buttonClose)) {
                this.close();
            }
        }

        public void setProgressBarValue(int current, int total) {
            float value = total != 0 ? ((float) current < total ? current : total) / total : 0;
            ui.getSession().lock();
            progressBar.setCaption(ui.getMessage(MessageKeys.DATA_PREPARING) + " (" + current + " / " + total + ")");
            progressBar.setValue(Float.valueOf(value));
            ui.getSession().unlock();
        }

        public void toggleControls() {
            labelLink.setVisible(true);
            buttonCancel.setVisible(false);
            buttonClose.setVisible(true);
        }

    }
}
