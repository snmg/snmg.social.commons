/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.vaadin.data.Property;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.lang.StringUtils;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.exception.AuthException;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.event.SnSocialUIEvent;
import ru.snetwork.social.ui.event.SnSocialUIEventBus;

/**
 *
 * @author ky6uHETc
 */
public class LoginForm extends VerticalLayout implements Button.ClickListener {

    private final SnSocialUI ui;
    private final CssLayout layoutHeader;
    private final HorizontalLayout layoutLogin;
    private final CssLayout layoutFooter;
    private final Label labelWelcome;
    private final Label labelSnMonitor;
    private final TextField fieldUsername;
    private final PasswordField fieldPassword;
    private final Button buttonSignIn;

    public LoginForm(SnSocialUI ui, String uiCaption) {
        this.ui = ui;

        setSpacing(true);
        setSizeUndefined();
        setStyleName("login-panel");

        // header
        layoutHeader = new CssLayout();
        layoutHeader.setStyleName("header");
        layoutHeader.setWidth("100%");
        labelWelcome = new Label("Welcome");
        labelWelcome.setStyleName(ValoTheme.LABEL_H4);
        labelWelcome.addStyleName(ValoTheme.LABEL_COLORED);
        labelWelcome.setSizeUndefined();
        layoutHeader.addComponent(labelWelcome);
        labelSnMonitor = new Label(uiCaption);
        labelSnMonitor.setStyleName(ValoTheme.LABEL_H3);
        labelSnMonitor.setSizeUndefined();
        layoutHeader.addComponent(labelSnMonitor);
        addComponent(layoutHeader);

        // login
        layoutLogin = new HorizontalLayout();
        layoutLogin.setStyleName("login-form");
        layoutLogin.setSpacing(true);

        fieldUsername = new TextField("Username");
        fieldUsername.setIcon(FontAwesome.USER);
        fieldUsername.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        fieldUsername.setImmediate(true);
        layoutLogin.addComponent(fieldUsername);
        fieldPassword = new PasswordField("Password");
        fieldPassword.setIcon(FontAwesome.LOCK);
        fieldPassword.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        fieldPassword.setImmediate(true);

        fieldPassword.addFocusListener(new FieldEvents.FocusListener() {
            @Override
            public void focus(FieldEvents.FocusEvent event) {
                buttonSignIn.setClickShortcut(ShortcutAction.KeyCode.ENTER);
            }
        });
        fieldPassword.addBlurListener(new FieldEvents.BlurListener() {
            @Override
            public void blur(FieldEvents.BlurEvent event) {
                buttonSignIn.removeClickShortcut();
            }
        });

        layoutLogin.addComponent(fieldPassword);

        buttonSignIn = new Button("Sign In", this);
        buttonSignIn.setStyleName(ValoTheme.BUTTON_PRIMARY);
        layoutLogin.addComponent(buttonSignIn);
        layoutLogin.setComponentAlignment(buttonSignIn, Alignment.BOTTOM_LEFT);
        addComponent(layoutLogin);

        // footer
        layoutFooter = new CssLayout();
        layoutFooter.setStyleName("footer");
        addComponent(layoutFooter);
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        try {
            if (event.getButton() == buttonSignIn) {
                login();
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    public void localize() {
        labelWelcome.setValue(ui.getMessage(MessageKeys.WELCOME));
        fieldUsername.setCaption(ui.getMessage(MessageKeys.USERNAME));
        fieldPassword.setCaption(ui.getMessage(MessageKeys.PASSWORD));
        buttonSignIn.setCaption(ui.getMessage(MessageKeys.SIGN_IN));
    }

    protected void login() throws Property.ReadOnlyException {
        if (StringUtils.isNotBlank(fieldUsername.getValue()) && StringUtils.isNotBlank(fieldPassword.getValue())) {
            try {
                EntityModelSystemUser u = ui.authenticate(fieldUsername.getValue().toLowerCase(), fieldPassword.getValue());
                fieldPassword.setValue(null);
                ui.getEventBus().post(new SnSocialUIEvent.UserLoggedInEvent(u));
            } catch (AuthException ex) {
                SnSocialUI.notificationShow(ui.getMessage(MessageKeys.ERROR_OCCURED), ex.getMessage(), Notification.Type.WARNING_MESSAGE);
                fieldPassword.setValue("");
            }
        }
    }

}
