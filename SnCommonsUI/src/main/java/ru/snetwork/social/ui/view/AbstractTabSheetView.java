/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.event.SnSocialUIEvent;
import ru.snetwork.social.ui.SnSocialUIComponent;
import ru.snetwork.social.ui.SnSocialUIView;

/**
 *
 * @author ky6uHETc
 * @param <U>
 */
public abstract class AbstractTabSheetView<U extends SnSocialUI> extends TabSheet implements SnSocialUIView {
    
    public static Map<Class<? extends AbstractTabSheetView>, Map<String, Map<EntityModelSystemUser, Date>>> ACTIVE_SCOPE_USERS
            = new ConcurrentHashMap<Class<? extends AbstractTabSheetView>, Map<String, Map<EntityModelSystemUser, Date>>>() {
        @Override
        public Map<String, Map<EntityModelSystemUser, Date>> get(Object key) {
            if (super.get(key) == null) {
                super.put((Class) key, new ConcurrentHashMap<String, Map<EntityModelSystemUser, Date>>() {
                    @Override
                    public Map<EntityModelSystemUser, Date> get(Object key) {
                        if (super.get(key) == null) {
                            super.put((String) key, new ConcurrentHashMap<EntityModelSystemUser, Date>());
                        }
                        return super.get(key);
                    }
                });
            }
            return super.get(key);
        }

    };
    private static final int ACTIVE_SCOPE_USERS_TIME_LIMIT = 60000;
    protected final Log log = LogFactory.getLog(getClass());
    protected final U ui;
    private boolean subview = false;
    private boolean ignoreSelectTabListener = false;

    public static Map<String, Date> activeScopes() {
        Map<String, Date> activeScopes = new HashMap<>();
        for (Class<? extends AbstractTabSheetView> key : ACTIVE_SCOPE_USERS.keySet()) {
            final Map<String, Map<EntityModelSystemUser, Date>> scopes = ACTIVE_SCOPE_USERS.get(key);
            for (String scopeKey : scopes.keySet()) {
                if (scopes.get(scopeKey) != null) {
                    for (EntityModelSystemUser user : scopes.get(scopeKey).keySet()) {
                        Date lastActive = scopes.get(scopeKey).get(user);
                        if (lastActive != null
                                && (new Date().getTime() - lastActive.getTime()) < ACTIVE_SCOPE_USERS_TIME_LIMIT) {
                            activeScopes.put(scopeKey + ":user" + user.getId(), lastActive);
                        }
                    }
                }
            }
        }
        return activeScopes;
    }

    public static Map<EntityModelSystemUser, Date> activeScopeUsers() {
        Map<EntityModelSystemUser, Date> activeScopeUsers = new HashMap<>();
        for (Class<? extends AbstractTabSheetView> key : ACTIVE_SCOPE_USERS.keySet()) {
            final Map<String, Map<EntityModelSystemUser, Date>> scopes = ACTIVE_SCOPE_USERS.get(key);
            for (String scopeKey : scopes.keySet()) {
                if (scopes.get(scopeKey) != null) {
                    for (EntityModelSystemUser user : scopes.get(scopeKey).keySet()) {
                        Date lastActive = scopes.get(scopeKey).get(user);
                        if (lastActive != null
                                && (new Date().getTime() - lastActive.getTime()) < ACTIVE_SCOPE_USERS_TIME_LIMIT
                                && (activeScopeUsers.get(user) == null || lastActive.after(activeScopeUsers.get(user)))) {
                            activeScopeUsers.put(user, lastActive);
                        }
                    }
                }
            }
        }
        return activeScopeUsers;
    }

    public AbstractTabSheetView() throws NoDaoBeanForEntityClassException {
        this(false);
    }

    public AbstractTabSheetView(boolean subview) throws NoDaoBeanForEntityClassException {
        ui = (U) UI.getCurrent();
        this.subview = subview;
        setSizeFull();
        setStyleName(subview ? "sub-view" : "main-view");
        setTabCaptionsAsHtml(true);

        addSelectedTabChangeListener(new SelectedTabChangeListener() {
            @Override
            public void selectedTabChange(SelectedTabChangeEvent event) {

                if (!ignoreSelectTabListener) {

                    // refresh page uri
                    refreshPageUriBySelectedTab();

                    // refresh current tab
                    if (AbstractTabSheetView.this == ui.getNavigator().getCurrentView()
                            || (subview
                            && AbstractTabSheetView.this.getParent() != null
                            && AbstractTabSheetView.this.getParent() == ui.getNavigator().getCurrentView())) {
                        refreshCurrentTab();
                        refreshPageTitle();
                    }

                }
            }

        });

        ui.getEventBus().register(this, true);
        final EntityModelSystemUser user = ui.getUser();
        this.addDetachListener(new DetachListener() {
            @Override
            public void detach(DetachEvent event) {
                ui.getEventBus().unregister(AbstractTabSheetView.this);
                ui.getEventBus().post(new SnSocialUIEvent.UserLeftViewEvent(
                        user,
                        AbstractTabSheetView.this.getClass(),
                        getScopeId()));
            }
        });
    }

    @Override
    public final void enter(ViewChangeListener.ViewChangeEvent event) {

        // if logged in perform actions for refreshing
        if (ui.isLoggedIn()) {

            // if parameters passed, select current tab using the parameters value
            if (StringUtils.isNotBlank(event.getParameters())) {
                selectTabByUri(event.getParameters());
            } else {
                refreshPageUriBySelectedTab();
            }

            localize();
            refreshCountIndicators();
            refresh();
            permissions();
            ui.getEventBus().post(new SnSocialUIEvent.UserEnteredViewEvent(
                    ui.getUser(),
                    getClass(),
                    getScopeId()));
        }

    }

    protected void selectTabByUri(String uriParameters) {

        if (StringUtils.isNotBlank(uriParameters)) {

            final String[] params = uriParameters.split("/", 2);
            String currentTabId = params[0];
            boolean selectedTabById = false;

            for (int i = 0; i < this.getComponentCount(); i++) {

                Component tabComponent = this.getTab(i).getComponent();
                String tabId = StringUtils.isNotBlank(tabComponent.getId())
                        ? tabComponent.getId()
                        : tabComponent.getClass().getSimpleName();

                // if it's the selected tab
                if (tabId.equals(currentTabId)) {

                    this.ignoreSelectTabListener = true;
                    this.setSelectedTab(i);
                    selectedTabById = true;

                    // if there are more parameters and selected tab sensible to URI fragment parameters
                    if (params.length > 1 && StringUtils.isNotBlank(params[1])) {
                        if (this.getSelectedTab() instanceof SnSocialUIComponent.UriFragmentSensing) {

                            // applying parameters
                            ((SnSocialUIComponent.UriFragmentSensing) this.getSelectedTab()).senseUriFragment(params[1]);

                        } else if (this.getSelectedTab() instanceof AbstractTabSheetView) {

                            // entering to subtab
                            ((AbstractTabSheetView) this.getSelectedTab()).selectTabByUri(params[1]);

                        }
                    }

                    this.ignoreSelectTabListener = false;

                    break;

                }
            }

            if (!selectedTabById && StringUtils.equals(currentTabId, this.getSelectedTab().getId())) {

                if (this.getSelectedTab() instanceof SnSocialUIComponent.UriFragmentSensing) {

                    // applying parameters
                    ((SnSocialUIComponent.UriFragmentSensing) this.getSelectedTab()).senseUriFragment(uriParameters);

                } else if (this.getSelectedTab() instanceof AbstractTabSheetView
                        && ((AbstractTabSheetView) this.getSelectedTab()).getSelectedTab() instanceof SnSocialUIComponent.UriFragmentSensing) {

                    // entering to subtab
                    ((SnSocialUIComponent.UriFragmentSensing) ((AbstractTabSheetView) this.getSelectedTab()).getSelectedTab()).senseUriFragment(uriParameters);

                }
            }

        }
    }

    public void refreshActiveUsers() {
        if (!subview) {
            final Map<EntityModelSystemUser, Date> activeUsers = ACTIVE_SCOPE_USERS.get(getClass()).get(getScopeId());
            for (EntityModelSystemUser key : activeUsers.keySet()) {
                Date date = activeUsers.get(key);
                if (date == null || (new Date().getTime() - date.getTime()) > ACTIVE_SCOPE_USERS_TIME_LIMIT) {
                    activeUsers.remove(key);
                }
            }
            ui.getMainView().putActiveUsers(activeUsers.keySet());
        }
    }

    @Subscribe
    public void refreshActiveUsers(SnSocialUIEvent.UserEnteredViewEvent event) {
        if (!subview) {
            log.debug("--- refreshActiveUsers: entered view " + event.getUser());
            if (getScopeId().equals(event.getScopeId())
                    && getClass().equals(event.getViewType())
                    && event.getUser() != null
                    && !event.getUser().isGuest()
                    && !event.getUser().isSystem()) {
                ui.getMainView().addActiveUser(event.getUser());
                ACTIVE_SCOPE_USERS.get(getClass()).get(getScopeId()).put((EntityModelSystemUser) event.getUser(), new Date());
            }
        }
    }

    @Subscribe
    public void refreshActiveUsers(SnSocialUIEvent.UserLeftViewEvent event) {
        if (!subview) {
            log.debug("--- refreshActiveUsers: left view " + event.getUser());
            if (getScopeId().equals(event.getScopeId())
                    && getClass().equals(event.getViewType())
                    && event.getUser() != null
                    && !event.getUser().isGuest()
                    && !event.getUser().isSystem()) {
                ui.getMainView().removeActiveUser(event.getUser());
                ACTIVE_SCOPE_USERS.get(getClass()).get(getScopeId()).remove(event.getUser());
            }
        }
    }

    @Subscribe
    public void refreshActiveUsers(SnSocialUIEvent.UserLoggedOutEvent event) {
        if (!subview) {
            log.debug("--- refreshActiveUsers: user logged out " + event.getUser());
            if (event.getUser() != null
                    && !event.getUser().isGuest()
                    && !event.getUser().isSystem()) {
                ui.getMainView().removeActiveUser(event.getUser());
                ACTIVE_SCOPE_USERS.get(getClass()).get(getScopeId()).remove(event.getUser());
            }
        }
    }

    public void refreshCountIndicators() {
        // refresh tabs count indicator
        for (int i = 0; i < getComponentCount(); i++) {
            Component tabComponent = getTab(i).getComponent();
            if (tabComponent instanceof SnSocialUIComponent.CountIndicator) {
                Integer countIndicator = ((SnSocialUIComponent.CountIndicator) tabComponent).getCountIndicator();
                if (countIndicator != null) {
                    getTab(i).setCaption(tabComponent.getCaption()
                            + "<span class='count-indicator "
                            + ((SnSocialUIComponent.CountIndicator) tabComponent).getCountIndicatorStyleName()
                            + "'>" + countIndicator + "</span>");
                } else {
                    getTab(i).setCaption(tabComponent.getCaption());
                }
            }
        }
    }

    @Override
    public final void localize() {
        Iterator<Component> components = iterator();
        while (components.hasNext()) {
            Component c = components.next();
            if (c instanceof SnSocialUIComponent) {
                ((SnSocialUIComponent) c).localize();
                getTab(c).setCaption(c.getCaption());
            }
        }
        localizeComponents();
    }

    protected void localizeComponents() {

    }

    @Override
    public void permissions() {
        Iterator<Component> components = iterator();
        while (components.hasNext()) {
            Component c = components.next();
            if (c instanceof SnSocialUIComponent) {
                ((SnSocialUIComponent) c).permissions();
            }
        }
    }

    @Override
    public void refresh() {
        refreshActiveUsers();
        refreshCurrentTab();
        refreshPageTitle();
        refreshCountIndicators();
    }

    protected void refreshCurrentTab() {

        // turn off displayed flag for all tabs that are sensing the flag
        for (int i = 0; i < getComponentCount(); i++) {
            if (getTab(i).getComponent() instanceof SnSocialUIComponent.DisplayedSensing) {
                ((SnSocialUIComponent.DisplayedSensing) getTab(i).getComponent()).setDisplayed(false);
            }
        }

        // current tab
        Component c = getSelectedTab();

        // if current tab is sensing displayed flag
        if (c instanceof SnSocialUIComponent.DisplayedSensing) {
            ((SnSocialUIComponent.DisplayedSensing) c).setDisplayed(true);
        }

        // if current tab can be refreshed
        if (c instanceof SnSocialUIComponent) {
            ((SnSocialUIComponent) c).refresh();
        }

    }

    protected final void refreshPageTitle() {
        ui.getPage().setTitle(ui.getTitle() + " | " + getTitle() + (getSelectedTab() == null ? "" : (" | " + getSelectedTab().getCaption())));
    }

    protected final void refreshPageUriBySelectedTab() {

        // build new URI path
        ArrayList<String> newPath = new ArrayList();
        newPath.add(
                StringUtils.isNotBlank(getSelectedTab().getId())
                ? getSelectedTab().getId()
                : getSelectedTab().getClass().getSimpleName());
        SnSocialUIView uriComponent = AbstractTabSheetView.this;
        newPath.add(0, uriComponent.getUriFragmentId());
        while (uriComponent.getParent() instanceof SnSocialUIView) {
            uriComponent = (SnSocialUIView) uriComponent.getParent();
            newPath.add(0, uriComponent.getUriFragmentId());
        }
        // if currenttab is a tabsheetView
        if (AbstractTabSheetView.this.getSelectedTab() instanceof AbstractTabSheetView) {
            final AbstractTabSheetView subtabsheet = (AbstractTabSheetView) AbstractTabSheetView.this.getSelectedTab();
            newPath.add(StringUtils.isNotBlank(subtabsheet.getSelectedTab().getId())
                    ? subtabsheet.getSelectedTab().getId()
                    : subtabsheet.getSelectedTab().getClass().getSimpleName());
        }

        // set URI fragment
        ui.getPage().setUriFragment("!" + StringUtils.join(newPath, "/"), false);
    }

    @Override
    public final boolean push() {

        // check whether user is still preset in active users set
        if (!subview && ui.getUser() != null && !ui.getUser().isGuest()) {
            final Map<EntityModelSystemUser, Date> activeUsers = ACTIVE_SCOPE_USERS.get(getClass()).get(getScopeId());
            if (!activeUsers.containsKey(ui.getUser())) {
                ui.getEventBus().post(
                        new SnSocialUIEvent.UserEnteredViewEvent(
                                ui.getUser(),
                                getClass(),
                                getScopeId())
                );
            } else {
                activeUsers.put(ui.getUser(), new Date());
            }
        }

        // check push for selected tab
        Component c = getSelectedTab();
        if (c instanceof SnSocialUIComponent) {
            return ((SnSocialUIComponent) c).push();
        }

        return false;
    }

    public String getScopeId() {
        return ui.getCurrentScopeId();
    }

    public abstract String getTitle();

    @Override
    public String getUriFragmentId() {
        String fragmentId = StringUtils.isNotBlank(getId())
                ? getId()
                : getClass().getSimpleName();
        for (MainViewType mainViewType : ui.getMainViewTypes()) {
            if (mainViewType.getViewType().isInstance(this)) {
                fragmentId = mainViewType.getViewName();
            }
        }
        return fragmentId;
    }

}
