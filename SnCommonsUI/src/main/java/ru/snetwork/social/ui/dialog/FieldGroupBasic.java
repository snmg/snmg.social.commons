/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.dialog;

import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.model.EntityModelBasic;
import ru.snetwork.social.ui.SnSocialUI;
import static ru.snetwork.social.ui.dialog.AbstractDialog.STATUS_PROPERTIES;

/**
 *
 * @author ky6uHETc
 * @param <D>
 */
public class FieldGroupBasic<D extends AbstractDialog> extends BeanFieldGroup {

    protected D dialog;
    final Converter<String, Integer> converterInteger = new Converter<String, Integer>() {
        @Override
        public Integer convertToModel(String value, Class<? extends Integer> targetType, Locale locale) throws Converter.ConversionException {
            try {
                return StringUtils.isNotBlank(value) ? dialog.ui.getFormatLong().parse(value).intValue() : null;
            } catch (ParseException ex) {
                throw new Converter.ConversionException(ex.getMessage());
            }
        }

        @Override
        public String convertToPresentation(Integer value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
            return dialog.ui.formatNumber(value);
        }

        @Override
        public Class<Integer> getModelType() {
            return Integer.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }
    };
    final Converter<String, Long> converterLong = new Converter<String, Long>() {
        @Override
        public Long convertToModel(String value, Class<? extends Long> targetType, Locale locale) throws Converter.ConversionException {
            try {
                return StringUtils.isNotBlank(value) ? dialog.ui.getFormatLong().parse(value).longValue() : null;
            } catch (ParseException ex) {
                throw new Converter.ConversionException(ex.getMessage());
            }
        }

        @Override

        public String convertToPresentation(Long value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
            return dialog.ui.formatNumber(value);
        }

        @Override
        public Class<Long> getModelType() {
            return Long.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }
    };
    final Converter<String, Float> converterFloat = new Converter<String, Float>() {
        @Override
        public Float convertToModel(String value, Class<? extends Float> targetType, Locale locale) throws Converter.ConversionException {
            try {
                return StringUtils.isNotBlank(value) ? dialog.ui.getFormatNumber().parse(value).floatValue() : null;
            } catch (ParseException ex) {
                throw new Converter.ConversionException(ex.getMessage());
            }
        }

        @Override

        public String convertToPresentation(Float value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
            return dialog.ui.formatNumber(value);
        }

        @Override
        public Class<Float> getModelType() {
            return Float.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }
    };
    final Converter<String, Double> converterDouble = new Converter<String, Double>() {
        @Override
        public Double convertToModel(String value, Class<? extends Double> targetType, Locale locale) throws Converter.ConversionException {
            try {
                return StringUtils.isNotBlank(value) ? dialog.ui.getFormatNumber().parse(value).doubleValue() : null;
            } catch (ParseException ex) {
                throw new Converter.ConversionException(ex.getMessage());
            }
        }

        @Override

        public String convertToPresentation(Double value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
            return dialog.ui.formatNumber(value);
        }

        @Override
        public Class<Double> getModelType() {
            return Double.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }
    };

    public FieldGroupBasic(Class beanType) {
        super(beanType);
    }

    public <B extends EntityModelBasic> FieldGroupBasic(B bean) {
        super(bean.getClass());
    }

    public void setDialog(D dialog) {
        this.dialog = dialog;
    }

    @Override
    public void commit() throws FieldGroup.CommitException {
        Iterator<Component> components = dialog.tabsheetFields.iterator();
        while (components.hasNext()) {
            dialog.tabsheetFields.getTab(components.next()).setStyleName("");
        }
        for (Field f : getFields()) {
            f.removeStyleName("invalid-field");
        }
        try {
            super.commit();
        } catch (FieldGroup.CommitException ex) {
            for (Field f : ex.getInvalidFields().keySet()) {
                f.addStyleName("invalid-field");
                if (f.getParent().getParent() == dialog.tabsheetFields) {
                    dialog.tabsheetFields.getTab(f.getParent()).setStyleName("invalid-fields");
                }
            }
            throw ex;
        }

    }

    @Override
    public Field buildAndBind(String caption, Object propertyId, Class fieldType) throws FieldGroup.BindException {
        try {
            Property property = dialog.entryItem.getItemProperty(propertyId);
            if (EntityPropertyKeys.ID.equals(propertyId)) {
                TextField f = new TextField(caption);
                f.setReadOnly(true);
                f.setNullRepresentation("auto");
                f.setConverter(new Converter<String, Long>() {
                    @Override
                    public Long convertToModel(String value, Class<? extends Long> targetType, Locale locale) throws Converter.ConversionException {
                        return StringUtils.isNotBlank(value) ? Long.parseLong(value) : null;
                    }

                    @Override
                    public String convertToPresentation(Long value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
                        return value != null ? value.toString() : "";
                    }

                    @Override
                    public Class<Long> getModelType() {
                        return Long.class;
                    }

                    @Override
                    public Class<String> getPresentationType() {
                        return String.class;
                    }
                });
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.DESCRIPTION.equals(propertyId)) {
                TextArea f = new TextArea(caption);
                f.setNullRepresentation("");
                f.setHeight("75px");
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.UUID.equals(propertyId)) {
                TextField f = new TextField(caption);
                f.setNullRepresentation("auto");
                super.bind(f, propertyId);
                return f;
            } else if (EntityPropertyKeys.LOCALE.equals(propertyId)) {
                OptionGroup f = new OptionGroup(caption);
                f.setStyleName("buttonsbar with-checkboxes");
                for (Locale l : dialog.ui.getLocales()) {
                    f.addItem(l);
                }
                super.bind(f, propertyId);
                return f;
            } else if (ArrayUtils.contains(STATUS_PROPERTIES, propertyId)) {
                TextField f = new TextField();
                f.setCaption(caption);
                f.setReadOnly(true);
                f.setNullRepresentation("");
                f.setConverter(new Converter<String, Date>() {
                    @Override
                    public Date convertToModel(String value, Class<? extends Date> targetType, Locale locale) throws Converter.ConversionException {
                        try {
                            return StringUtils.isNotBlank(value) ? DateUtils.parseDate(value, SnSocialUI.DATE_PATTERNS) : null;
                        } catch (ParseException ex) {
                            dialog.ui.handleException(ex);
                        }
                        return null;
                    }

                    @Override
                    public String convertToPresentation(Date value, Class<? extends String> targetType, Locale locale) throws Converter.ConversionException {
                        return value != null ? DateFormatUtils.format(value, SnSocialUI.DATETIME_PATTERN_FULL, dialog.ui.getUserTimeZone()) : "";
                    }

                    @Override
                    public Class<Date> getModelType() {
                        return Date.class;
                    }

                    @Override
                    public Class<String> getPresentationType() {
                        return String.class;
                    }
                });
                super.bind(f, propertyId);
                return f;

            } else if (Date.class.isAssignableFrom(property.getType())) {
                DateField f = new DateField(caption);
                f.setDateFormat(SnSocialUI.DATETIME_PATTERN);
                f.setResolution(Resolution.MINUTE);
                f.setTimeZone(dialog.ui.getUserTimeZone());
                super.bind(f, propertyId);
                return f;
            } else if (Number.class.isAssignableFrom(property.getType())) {
                TextField f = new TextField(caption);
                if (Long.class.isAssignableFrom(property.getType())) {
                    f.setConverter(converterLong);
                } else if (Integer.class.isAssignableFrom(property.getType())) {
                    f.setConverter(converterInteger);
                } else if (Double.class.isAssignableFrom(property.getType())) {
                    f.setConverter(converterDouble);
                } else if (Float.class.isAssignableFrom(property.getType())) {
                    f.setConverter(converterFloat);
                }
                f.addStyleName("v-numberfield");
                f.setNullRepresentation("");
                f.setWidth("194px");
                super.bind(f, propertyId);
                return f;
            }
        } catch (Exception ex) {
            dialog.ui.handleException(ex);
        }
        try {
            Field f = super.buildAndBind(caption, propertyId, fieldType);
            if (f instanceof DateField) {
                ((DateField) f).setDateFormat(SnSocialUI.DATETIMESEC_PATTERN);
                ((DateField) f).setResolution(Resolution.SECOND);
                ((DateField) f).setTimeZone(dialog.ui.getUserTimeZone());;
            }
            return f;
        } catch (RuntimeException ex) {
            dialog.ui.notificationShow(
                    dialog.ui.getMessage(MessageKeys.ERROR_OCCURED),
                    "Unable to build and bind field for \"" + propertyId + "\": " + ex.getMessage(),
                    Notification.Type.WARNING_MESSAGE);
            dialog.log.error(ex.getMessage(), ex);
            return null;
        }
    }

}
