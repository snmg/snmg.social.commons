/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.view;

import com.vaadin.navigator.View;
import com.vaadin.server.Resource;
import ru.snetwork.social.ui.SnSocialUI;

/**
 *
 * @author ky6uHETc
 */
public interface MainViewType<U extends SnSocialUI> {

    public String getViewName();

    public Class<? extends View> getViewType();

    public Resource getIcon();

    public boolean hasSeparator();

    public boolean hasViewPermission(U ui);
}
