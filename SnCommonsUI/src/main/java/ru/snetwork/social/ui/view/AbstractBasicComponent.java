package ru.snetwork.social.ui.view;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.addon.contextmenu.Menu;
import com.vaadin.addon.contextmenu.MenuItem;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanUtil;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.vaadin.addons.lazyquerycontainer.AbstractBeanQuery;
import org.vaadin.addons.lazyquerycontainer.BeanQueryFactory;
import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.hene.popupbutton.PopupButton;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.dao.AbstractDaoBean;
import ru.snetwork.social.dao.query.OrderBy;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.model.EntityModelBasic;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.exception.NoDialogForEntityClassException;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.field.TreeComboBox;
import ru.snetwork.social.ui.helper.ShowOptions;
import ru.snetwork.social.ui.SnSocialUIComponent;

/**
 * @param <T> Basic Class
 * @param <U> Vaadin UI class
 */
public abstract class AbstractBasicComponent<T extends EntityModelBasic, D extends AbstractDaoBean, U extends SnSocialUI>
        extends VerticalLayout
        implements Button.ClickListener, SnSocialUIComponent, SnSocialUIComponent.DisplayedSensing {

    protected boolean displayed = false;
    protected boolean refreshing = false;
    protected Log log = LogFactory.getLog(getClass());

    protected final Class<T> entriesClass;

    // detect entriesClass
    {
        if (this.getClass().getGenericSuperclass() instanceof ParameterizedType) {
            entriesClass = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } else if (this.getClass().getGenericSuperclass() instanceof Class
                && ((Class) this.getClass().getGenericSuperclass()).getGenericSuperclass() instanceof ParameterizedType) {
            entriesClass = (Class) ((ParameterizedType) ((Class) this.getClass().getGenericSuperclass()).getGenericSuperclass()).getActualTypeArguments()[0];
        } else {
            entriesClass = null;
        }
    }

    protected final U ui;
    protected final D dao;
    protected String captionKey;

    private final HorizontalLayout layoutHeader = new HorizontalLayout();
    private final HorizontalLayout layoutHeaderFilter = new HorizontalLayout();
    private final VerticalLayout layoutHeaderFilterMore = new VerticalLayout();
    private final HorizontalLayout layoutHeaderButtons = new HorizontalLayout();
    private final Label layoutHeaderButtonsSpacer = new Label(" ");
    protected final TextField fSearch = new TextField();
    private final PopupButton bFilterMore = new PopupButton(MessageKeys.MORE);
    private final Button bFilterReset = new Button();
    protected final Button bEdit = new Button(MessageKeys.EDIT, this);
    protected final Button bDelete = new Button(MessageKeys.DELETE, this);
    protected final Button bAdd = new Button(MessageKeys.CREATE, this);
    protected Button[] actionButtons = new Button[]{bEdit, bDelete};

    protected OrderBy entriesPreOrderBy = null;
    protected final HorizontalLayout layoutTable = new HorizontalLayout();
    protected final BeanQueryFactory<EntriesQuery> entriesQueryFactory = new BeanQueryFactory<>(EntriesQuery.class);
    protected final LazyQueryContainer containerEntries = new LazyQueryContainer(entriesQueryFactory, EntityPropertyKeys.ID, 100, true);
    protected final EntriesTable tableEntries = new EntriesTable(null, containerEntries);

    protected String tableEntriesDateFormat = SnSocialUI.DATETIMESEC_PATTERN;
    protected final Set<Long> tableEntriesSelection = new LinkedHashSet<>();

    protected final ContextMenu tableEntriesContextMenu;
    protected final Menu.Command tableEntriesContextMenuCommand = new Menu.Command() {
        @Override
        public void menuSelected(MenuItem selectedItem) {
            contextMenuSelected(selectedItem);
        }
    };
    protected final MenuItem tableEntriesContextMenuItemEdit;
    protected final MenuItem tableEntriesContextMenuItemDelete;

    protected final HorizontalLayout layoutFooter = new HorizontalLayout();
    protected final Label labelCount = new Label();
    protected final CheckBox cbAutoRefresh = new CheckBox(MessageKeys.AUTO_REFRESH);
    protected final Button bRefresh = new Button(MessageKeys.REFRESH, this);
    protected final Button bExportToExcel = new Button(MessageKeys.EXPORT_TO_XLS, this);

    protected String querySelect = null;
    protected Class<? extends T> querySubclass = null;
    protected String queryJoinRelations = null;
    private List<WhereClause> queryWhere = null;
    protected String queryTotalJoinRelations = null;
    protected List<WhereClause> queryTotalWhere = null;
    protected Integer entriesCount = null;
    protected Integer entriesTotal = null;
    protected boolean queryShowDeleted = false;

    public AbstractBasicComponent(U ui) throws NoDaoBeanForEntityClassException, IntrospectionException {
        this.ui = ui;
        this.dao = (D) ui.getDaoBean(entriesClass);
        this.captionKey = entriesClass.getSimpleName() + MessageKeys._ENTRIES;

        // container configuration
        Map<String, Object> qc = new HashMap<>();
        qc.put("component", this);
        entriesQueryFactory.setQueryConfiguration(qc);
        for (PropertyDescriptor pd : BeanUtil.getBeanPropertyDescriptor(entriesClass)) {
            boolean sortable
                    = Number.class.isAssignableFrom(pd.getPropertyType())
                    || Date.class.isAssignableFrom(pd.getPropertyType())
                    || String.class.isAssignableFrom(pd.getPropertyType());
            containerEntries.addContainerProperty(pd.getName(), pd.getPropertyType(), null, true, sortable);
        }

        this.tableEntriesContextMenu = new ContextMenu(tableEntries, false);
        this.tableEntriesContextMenuItemEdit = tableEntriesContextMenu.addItem(MessageKeys.EDIT, FontAwesome.EDIT, tableEntriesContextMenuCommand);
        this.tableEntriesContextMenuItemDelete = tableEntriesContextMenu.addItem(MessageKeys.DELETE, FontAwesome.TRASH, tableEntriesContextMenuCommand);

        // component settings
        setId(entriesClass.getSimpleName().toLowerCase());
        setSizeFull();
        setCaption(captionKey);
        setStyleName("entries");
        addStyleName(entriesClass.getSimpleName());

        // content
        buildHeader();
        buildTable();
        buildFooter();

    }

    protected void actionEdit(Long id) throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException {
        ui.dialog(entriesClass, id);
    }

    protected abstract void actionDelete(Long id);

    protected abstract void actionExportXls();

    protected void actionDeletePrompt(final Collection<Long> selection) {
        ConfirmDialog cd = ConfirmDialog.show(ui,
                ui.getMessage(MessageKeys.DELETE),
                ui.getMessage(MessageKeys.CONFIRM_DELETE_ENTRIES),
                ui.getMessage(MessageKeys.DELETE),
                ui.getMessage(MessageKeys.CANCEL),
                new ConfirmDialog.Listener() {
            @Override
            public void onClose(ConfirmDialog cd) {
                try {
                    if (cd.isConfirmed()) {
                        Iterator<Long> iterator = selection.iterator();
                        while (iterator.hasNext()) {
                            actionDelete((Long) iterator.next());
                        }
                        refresh();
                        tableEntries.setValue(null);
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
        cd.getOkButton().setStyleName(ValoTheme.BUTTON_DANGER);
    }

    protected void addHeaderFilterComponent(Component c) {
        layoutHeaderFilter.addComponent(c, layoutHeaderFilter.getComponentIndex(bFilterMore));
        layoutHeaderFilter.setComponentAlignment(c, Alignment.MIDDLE_LEFT);
    }

    protected boolean isHeaderFilterComponent(Component c) {
        return c.getParent() == layoutHeaderFilter;
    }

    protected void removeHeaderFilterComponent(Component c) {
        layoutHeaderFilter.removeComponent(c);
    }

    protected void addHeaderMoreComponent(Component c) {
        layoutHeaderFilterMore.addComponent(c);
        bFilterMore.setVisible(true);
    }

    protected void addHeaderMoreComponent(Component c, int index) {
        layoutHeaderFilterMore.addComponent(c, index);
        bFilterMore.setVisible(true);
    }

    protected boolean isHeaderMoreComponent(Component c) {
        return c.getParent() == layoutHeaderFilterMore;
    }

    protected void removeHeaderMoreComponent(Component c) {
        layoutHeaderFilterMore.removeComponent(c);
        bFilterMore.setVisible(layoutHeaderFilterMore.getComponentCount() > 0);
    }

    protected void removeHeaderMoreAllComponents() {
        layoutHeaderFilterMore.removeAllComponents();
        bFilterMore.setVisible(false);
    }

    protected void setHeaderMorePopupVisibility(boolean popupVisible) {
        bFilterMore.setPopupVisible(popupVisible);
    }

    protected void setHeaderMorePopupWidth(String width) {
        layoutHeaderFilterMore.setWidth(width);
    }

    protected void addHeaderButtonComponent(Component c) {
        addHeaderButtonComponent(c, null);
    }

    protected void addHeaderButtonComponent(Component c, Component beforeComponent) {
        if (beforeComponent != null && beforeComponent.getParent() != null && beforeComponent.getParent() == layoutHeaderButtons) {
            layoutHeaderButtons.addComponent(c, layoutHeaderButtons.getComponentIndex(beforeComponent));
        } else {
            layoutHeaderButtons.addComponent(c);
        }
    }

    protected void addHeaderButtonComponentAsFirst(Component c) {
        layoutHeaderButtons.addComponentAsFirst(c);
    }

    protected void removeHeaderButtonComponent(Component c) {
        layoutHeaderButtons.removeComponent(c);
    }

    private void buildHeader() {
        layoutHeader.setStyleName("viewheader");
        layoutHeader.setWidth("100%");
        layoutHeader.setSpacing(true);
        layoutHeader.addComponent(layoutHeaderFilter);
        layoutHeader.addComponent(layoutHeaderButtons);
        addComponent(layoutHeader);

        // Filter
        layoutHeaderFilter.setSpacing(true);
        fSearch.setIcon(FontAwesome.SEARCH);
        fSearch.setStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        fSearch.setImmediate(true);
        fSearch.setWidth("300px");
        fSearch.setNullRepresentation("");
        fSearch.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                refreshData();
            }
        });
        layoutHeaderFilter.addComponent(fSearch);
        layoutHeaderFilter.setComponentAlignment(fSearch, Alignment.MIDDLE_LEFT);
        layoutHeaderFilterMore.setSpacing(true);
        layoutHeaderFilterMore.setMargin(true);
        layoutHeaderFilterMore.setWidth("350px");
        layoutHeaderFilterMore.setStyleName("viewheader");
        layoutHeaderFilterMore.addStyleName("filtermore");
        bFilterMore.setContent(layoutHeaderFilterMore);
        bFilterMore.setStyleName(ValoTheme.BUTTON_SMALL);
        bFilterMore.setVisible(false);
        layoutHeaderFilter.addComponent(bFilterMore);
        layoutHeaderFilter.setComponentAlignment(bFilterMore, Alignment.MIDDLE_LEFT);
        bFilterReset.addClickListener(this);
        bFilterReset.setIcon(FontAwesome.TIMES);
        bFilterReset.setStyleName(ValoTheme.BUTTON_SMALL);
        bFilterReset.setVisible(false);
        layoutHeaderFilter.addComponent(bFilterReset);
        layoutHeaderFilter.setComponentAlignment(bFilterMore, Alignment.MIDDLE_LEFT);
        layoutHeader.setExpandRatio(layoutHeaderFilter, 1.0f);

        // Buttons
        layoutHeaderButtons.setSpacing(true);
        bEdit.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bEdit);
        bDelete.setStyleName(ValoTheme.BUTTON_DANGER);
        bDelete.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bDelete);
        layoutHeaderButtons.addComponent(layoutHeaderButtonsSpacer);
        bAdd.setStyleName(ValoTheme.BUTTON_PRIMARY);
        bAdd.addStyleName(ValoTheme.BUTTON_SMALL);
        layoutHeaderButtons.addComponent(bAdd);

    }

    private void buildTable() {

        // table configuration
        tableEntries.setId(this.getClass().getSimpleName() + "-entries");
        tableEntries.setPageLength(30);
        tableEntries.setStyleName("viewentries");
        tableEntries.setSizeFull();
        tableEntries.setVisibleColumns(EntityPropertyKeys.ID);
        tableEntries.setColumnAlignment(EntityPropertyKeys.ID, Table.Align.RIGHT);
        tableEntries.setColumnWidth(EntityPropertyKeys.ID, 75);
        tableEntries.setSelectable(true);
        tableEntries.setMultiSelect(true);
        tableEntries.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                try {
                    if (event.isDoubleClick()) {
                        actionEdit((Long) event.getItemId());
                    } else if (MouseEventDetails.MouseButton.RIGHT.equals(event.getButton())) {
                        if (!tableEntriesSelection.contains(event.getItemId())) {
                            if (event.isCtrlKey()) {
                                tableEntries.select(event.getItemId());
                            } else {
                                tableEntries.setValue(null);
                                tableEntries.select(event.getItemId());
                            }
                        }
                        tableEntriesContextMenu.open(event.getClientX(), event.getClientY());
                        contextMenuOpened((Long) event.getItemId(), event.getItem(), event.getPropertyId());
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
        tableEntries.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object value = event.getProperty().getValue();
                tableEntriesSelection.clear();
                if (value != null && value instanceof Collection) {
                    ((Collection) value).iterator().forEachRemaining(new Consumer() {
                        @Override
                        public void accept(Object o) {
                            if (o != null && o instanceof Long) {
                                tableEntriesSelection.add((Long) o);
                            }
                        }
                    });
                } else if (value != null && value instanceof Long) {
                    tableEntriesSelection.add((Long) value);
                }
                refreshActions();
            }
        });

        layoutTable.setSizeFull();
        layoutTable.addComponent(tableEntries);
        layoutTable.setExpandRatio(tableEntries, 1.0f);
        addComponent(layoutTable);
        setExpandRatio(layoutTable, 1.0f);
    }

    public void buildFooter() {
        layoutFooter.setStyleName("viewfooter");
        layoutFooter.setWidth("100%");
        layoutFooter.setSpacing(true);
        layoutFooter.addComponent(labelCount);
        layoutFooter.setExpandRatio(labelCount, 1.0f);
        layoutFooter.setComponentAlignment(labelCount, Alignment.MIDDLE_LEFT);
        cbAutoRefresh.setImmediate(true);
        layoutFooter.addComponent(cbAutoRefresh);
        layoutFooter.setComponentAlignment(cbAutoRefresh, Alignment.MIDDLE_RIGHT);
        bRefresh.setIcon(FontAwesome.REFRESH);
        bRefresh.setImmediate(true);
        bRefresh.setStyleName(ValoTheme.BUTTON_TINY);
        layoutFooter.addComponent(bRefresh);
        addComponent(layoutFooter);

        bExportToExcel.setIcon(FontAwesome.FILE_EXCEL_O);
        bExportToExcel.setStyleName(ValoTheme.BUTTON_TINY);
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        try {
            if (event.getButton() == bAdd) {
                ui.dialog(entriesClass.newInstance());
            } else if (event.getButton() == bEdit && tableEntriesSelection.size() == 1) {
                actionEdit(tableEntriesSelection.iterator().next());
            } else if (event.getButton() == bDelete && tableEntriesSelection.size() >= 1) {
                actionDeletePrompt(tableEntriesSelection);
            } else if (event.getButton() == bFilterReset) {
                resetFilters();
            } else if (event.getButton() == bRefresh) {
                refresh();
            } else if (event.getButton() == bExportToExcel) {
                actionExportXls();
                bExportToExcel.setEnabled(true);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    protected void contextMenuOpened(Long tableItemId, Item tableItem, Object tablePropertyId) {

        tableEntriesContextMenuItemEdit.setVisible(tableEntriesSelection.size() == 1);
        boolean showDelete = false;
        boolean showRestore = false;
        Iterator<Long> iterator = tableEntriesSelection.iterator();
        while (iterator.hasNext()) {
            Long itemId = iterator.next();
            if (tableEntries.getItem(itemId) != null) {
                if (tableEntries.getItem(itemId).getItemProperty(EntityPropertyKeys.DATE_DELETED_AT).getValue() != null) {
                    showRestore = true;
                } else {
                    showDelete = true;
                }
            }
        }
        tableEntriesContextMenuItemDelete.setVisible(showDelete);
    }

    protected void contextMenuSelected(MenuItem menuItem) {
        try {
            if (tableEntriesContextMenuItemEdit == menuItem && tableEntriesSelection.size() == 1) {
                actionEdit((Long) tableEntriesSelection.iterator().next());
            } else if (tableEntriesContextMenuItemDelete == menuItem && tableEntriesSelection.size() > 0) {
                actionDeletePrompt(tableEntriesSelection);
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    @Override
    public boolean isDisplayed() {
        return displayed;
    }

    @Override
    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    protected String formatTablePropertyValue(Object colId, Property<?> property) {
        String value
                = ui.formatPropertyValue(
                        tableEntriesDateFormat,
                        (String) colId,
                        property.getType(),
                        property.getValue());
        return value;
    }

    @Override
    public final void localize() {
        setCaption(ui.getMessage(captionKey));
        fSearch.setInputPrompt(ui.getMessage(MessageKeys.SEARCH));
        bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE));
        bAdd.setCaption(ui.getMessage(MessageKeys.ADD));
        bEdit.setCaption(ui.getMessage(MessageKeys.EDIT));
        bDelete.setCaption(ui.getMessage(MessageKeys.DELETE));
        bRefresh.setDescription(ui.getMessage(MessageKeys.REFRESH));
        bRefresh.setCaption("");
        bExportToExcel.setDescription(ui.getMessage(MessageKeys.EXPORT_TO_XLS));
        bExportToExcel.setCaption("");
        cbAutoRefresh.setCaption(ui.getMessage(MessageKeys.AUTO_REFRESH));
        tableEntriesContextMenuItemEdit.setText(ui.getMessage(MessageKeys.EDIT));
        tableEntriesContextMenuItemDelete.setText(ui.getMessage(MessageKeys.DELETE));
        localizeComponents();
    }

    protected abstract void localizeComponents();

    /**
     * Refresh all visible controls and data
     */
    @Override
    public final void refresh() {

        // refresh only if not already refreshing 
        if (!refreshing) {

            // toggle refreshing flag
            refreshing = true;

            try {

                refreshFilterControls();
                refreshData();
                refreshActions();

            } catch (Exception ex) {
                ui.handleException(ex);
            } finally {
                refreshing = false;
            }
        }

    }

    /**
     * Refresh action controls
     */
    protected void refreshActions() {
        for (Button b : actionButtons) {
            layoutHeaderButtons.removeComponent(b);
        }
        if (!tableEntriesSelection.isEmpty()) {
            layoutHeaderButtons.addComponent(bDelete, layoutHeader.getComponentCount() - 2);
            if (tableEntriesSelection.size() == 1) {
                layoutHeaderButtons.addComponent(bEdit, layoutHeader.getComponentCount() - 2);
            }
        }
    }

    protected void refreshCountLabel(int count, boolean countApproximate, int total, boolean totalApproximate) {
        labelCount.setValue(
                count < total
                        ? ui.getMessage(MessageKeys.SHOWING_X) + ": "
                        + count + " " + ui.getMessage(MessageKeys.X_OF_TOTAL_X) + " "
                        + (totalApproximate ? "~" : "")
                        + total
                        : ui.getMessage(MessageKeys.SHOWING_X) + ": "
                        + (countApproximate ? "~" : "")
                        + count);

    }

    public String getCaptionKey() {
        return captionKey;
    }

    public void setCaptionKey(String captionKey) {
        this.captionKey = captionKey;
    }

    public int getEntriesCount() {
        // otherwise count size
        if (entriesCount == null) {
            boolean countApproximate = false;
            boolean totalApproximate = false;
            if ((queryTotalWhere == null || queryTotalWhere.isEmpty())
                    && (queryWhere == null || queryWhere.isEmpty())
                    && querySubclass == null) //
            {
                totalApproximate = true;
                entriesTotal = dao.count(entriesClass, null).intValue();
                if (entriesTotal < 10000) {
                    entriesCount = dao.count(null, queryTotalWhere).intValue();
                } else {
                    countApproximate = true;
                    entriesCount = entriesTotal;
                }
            } else {
                entriesCount = dao.count(querySubclass, queryWhere).intValue();
                if (queryTotalWhere == null && queryWhere == null
                        || queryTotalWhere != null && queryWhere != null && queryTotalWhere.containsAll(queryWhere) && queryWhere.containsAll(queryTotalWhere)) //f
                {
                    entriesTotal = entriesCount;
                } else {
                    entriesTotal = dao.count(entriesClass, null).intValue();
                    if (entriesTotal < 10000 || queryTotalWhere != null) {
                        entriesTotal = dao.count(querySubclass, queryTotalWhere).intValue();
                    } else {
                        totalApproximate = true;
                    }
                }
            }
            refreshCountLabel(entriesCount, countApproximate, entriesTotal, totalApproximate);
        }
        return entriesCount;
    }

    /**
     * Refresh table data
     */
    protected final void refreshData() {

        int pageStartIndex = tableEntries.getCurrentPageFirstItemIndex();
        int pageEndIndex = pageStartIndex + tableEntries.getPageLength();

        // refresh filters buttons
        refreshFilterButtons();

        // where clause
        queryWhere = refreshWhereClauses();

        // retrieve list
        entriesCount = null;

        if (getEntriesCount() < pageEndIndex) {
            containerEntries.refresh();
        } else {
            // Workaround to prevent table scroll-position jumping in Chrome
            // Debugging rows -69295870_238093 55983
            containerEntries.getQueryView().refresh();
            tableEntries.refreshRowCache(false);
        }
        if (tableEntries.size() > 0 && tableEntries.size() < 65535) {
            bExportToExcel.setEnabled(true);
            bExportToExcel.setDisableOnClick(true);
        } else {
            bExportToExcel.setEnabled(false);
        }

    }

    private void refreshFilterButtons() {
        boolean showResetButton = false;
        Iterator iterator1 = layoutHeaderFilter.iterator();
        while (iterator1.hasNext()) {
            Component c = (Component) iterator1.next();
            if (c instanceof Field
                    && !((Field) c).isEmpty()
                    && !MessageKeys.ALL.equals(((Field) c).getValue())
                    && !ShowOptions.ONLY_ACTIVE.equals(((Field) c).getValue())
                    && !ShowOptions.UNREVIEWED.equals(((Field) c).getValue())) {
                showResetButton = true;
                break;
            }
        }
        int countMoreFilters = 0;
        Iterator iterator2 = layoutHeaderFilterMore.iterator();
        while (iterator2.hasNext()) {
            Component c = (Component) iterator2.next();
            if (c instanceof Field
                    && !((Field) c).isEmpty()
                    && !MessageKeys.ALL.equals(((Field) c).getValue())
                    && !ShowOptions.ONLY_ACTIVE.equals(((Field) c).getValue())
                    && !ShowOptions.UNREVIEWED.equals(((Field) c).getValue())) {
                countMoreFilters++;
            }
        }
        if (countMoreFilters > 0) {
            bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE) + " (" + countMoreFilters + ")");
            bFilterMore.addStyleName(ValoTheme.BUTTON_PRIMARY);
            showResetButton = true;
        } else {
            bFilterMore.setCaption(ui.getMessage(MessageKeys.MORE));
            bFilterMore.removeStyleName(ValoTheme.BUTTON_PRIMARY);
        }
        bFilterReset.setVisible(showResetButton && bFilterMore.isVisible());
    }

    /**
     * Refresh filter controls is a subclass has such controls.
     */
    protected abstract void refreshFilterControls();

    private void resetFilters() {
        Iterator iterator1 = layoutHeaderFilter.iterator();
        while (iterator1.hasNext()) {
            Component c = (Component) iterator1.next();
            resetFilterField(c);
        }
        Iterator iterator2 = layoutHeaderFilterMore.iterator();
        while (iterator2.hasNext()) {
            Component c = (Component) iterator2.next();
            resetFilterField(c);
        }
        refresh();
    }

    private void resetFilterField(Component c) throws Converter.ConversionException, Property.ReadOnlyException {
        if (c instanceof AbstractField && !((AbstractField) c).isEmpty()) {
            AbstractField field = (AbstractField) c;
            List<Property.ValueChangeListener> listeners = new LinkedList<>();
            for (Object l : field.getListeners(Property.ValueChangeEvent.class)) {
                if (l instanceof Property.ValueChangeListener) {
                    listeners.add((Property.ValueChangeListener) l);
                    field.removeValueChangeListener((Property.ValueChangeListener) l);
                }
            }
            if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(MessageKeys.ALL)) {
                field.setValue(MessageKeys.ALL);
            } else if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(ShowOptions.ONLY_ACTIVE)) {
                field.setValue(ShowOptions.ONLY_ACTIVE);
            } else if (field instanceof OptionGroup && ((OptionGroup) field).getItemIds().contains(ShowOptions.UNREVIEWED)) {
                field.setValue(ShowOptions.UNREVIEWED);
            } else {
                field.setValue(null);
            }
            for (Property.ValueChangeListener l : listeners) {
                field.addValueChangeListener(l);
            }
        } else if (c instanceof TreeComboBox && !((TreeComboBox) c).isEmpty()) {
            TreeComboBox tree = (TreeComboBox) c;
            List<Property.ValueChangeListener> listeners = new LinkedList<>();
            for (Object l : tree.getListeners(Property.ValueChangeEvent.class)) {
                if (l instanceof Property.ValueChangeListener) {
                    listeners.add((Property.ValueChangeListener) l);
                    tree.removeValueChangeListener((Property.ValueChangeListener) l);
                }
            }
            tree.setValue(null);
            for (Property.ValueChangeListener l : listeners) {
                tree.addValueChangeListener((Property.ValueChangeListener) l);
            }
        }
    }

    @Override
    public void permissions() {
        for (Button b : new Button[]{bAdd, bDelete, bEdit}) {
            b.setEnabled(ui.isUserAdministrator() || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                    ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                    : "");
        }
        for (MenuItem b : new MenuItem[]{
            tableEntriesContextMenuItemDelete,
            tableEntriesContextMenuItemEdit
        }) {
            b.setEnabled(ui.isUserAdministrator() || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                    ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                    : "");
        }
    }

    @Override
    public boolean push() {
        if (!refreshing && Boolean.TRUE.equals(cbAutoRefresh.getValue())) {
            containerEntries.refresh();
            return true;
        }
        return false;
    }

    protected abstract List<WhereClause> refreshWhereClauses();

    protected static class EntriesQuery extends AbstractBeanQuery<EntityModelBasic> {

        AbstractBasicComponent component;
        AbstractDaoBean dao;
        SnSocialUI ui;
        List<OrderBy> orderBy;

        public EntriesQuery(QueryDefinition queryDefinition, Map<String, Object> queryConfiguration, Object[] sortPropertyIds, boolean[] sortStates) {
            super(queryDefinition, queryConfiguration, sortPropertyIds, sortStates);
            component = (AbstractBasicComponent) queryConfiguration.get("component");
            dao = component.dao;
            ui = component.ui;
            orderBy = new ArrayList<>();
            if (component.entriesPreOrderBy != null) {
                orderBy.add(component.entriesPreOrderBy);
            }
            if (sortPropertyIds != null && sortPropertyIds.length > 0) {
                //orderBy = new ArrayList<>(sortPropertyIds.length);
                for (int i = 0; i < sortPropertyIds.length; i++) {
                    orderBy.add(new OrderBy("o." + sortPropertyIds[i], sortStates.length > i ? !sortStates[i] : false));
                }
            }

            if (orderBy.isEmpty()) {
                orderBy.add(new OrderBy("o.id", true));
            }
        }

        @Override
        protected EntityModelBasic constructBean() {
            try {
                return (EntityModelBasic) component.entriesClass.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                // ignore Exception
                ex.printStackTrace(System.out);
                return null;
            }
        }

        @Override
        public int size() {

            // if not displayed and not explicitly refreshing pretend size 0
            if (!component.displayed && !component.refreshing) {
                return 0;
            }

            return component.getEntriesCount();
        }

        @Override
        protected List<EntityModelBasic> loadBeans(int i, int i1) {
            List list = dao.list(component.querySelect,
                    component.querySubclass,
                    component.queryJoinRelations,
                    component.queryWhere,
                    i, i1,
                    orderBy.toArray(new OrderBy[]{}));
            return list;
        }

        @Override
        protected void saveBeans(List<EntityModelBasic> list, List<EntityModelBasic> list1, List<EntityModelBasic> list2) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public class EntriesTable extends Table {

        public EntriesTable(String caption, Container dataSource) {
            super(caption, dataSource);
        }

        @Override
        protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
            String value = formatTablePropertyValue(colId, property);
            return value != null ? value : super.formatPropertyValue(rowId, colId, property);
        }

        public void refreshRowCache(boolean fullResetPageBuffer) {
            if (fullResetPageBuffer) {
                resetPageBuffer();
            } else {
                resetPageBufferSoft();
            }
            refreshRenderedCells();
        }

    };

}
