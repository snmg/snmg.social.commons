/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.dialog;

import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.vaadin.dialogs.ConfirmDialog;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.model.EntityModelRegistry;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.dao.AbstractEntriesDaoBean;
import ru.snetwork.social.dao.AbstractSystemLogDaoBean;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.SnSocialUI;

/**
 *
 * @author ky6uHETc
 * @param <T> entity type
 * @param <U> UI type
 * @param <SU> system user class
 * @param <SL> System log daoBean
 */
public abstract class AbstractDialogEntry<T extends EntityModelEntry, U extends SnSocialUI, SU extends EntityModelSystemUser, SL extends AbstractSystemLogDaoBean >
        extends AbstractDialog<AbstractEntriesDaoBean<T, SU, SL>, T, U, FieldGroupEntry>
        implements Button.ClickListener {

    protected static final Object[] UNBOUND_PROPS = new Object[]{
        EntityPropertyKeys.DATE_DELETED_AT,
        EntityPropertyKeys.DATE_DISABLED_AT,
        EntityPropertyKeys.DELETED,
        EntityPropertyKeys.ENABLED,
        EntityPropertyKeys.DISABLED,
        EntityPropertyKeys.SYSTEM,
        EntityPropertyKeys.STATE,
        EntityPropertyKeys.AUTHORITIES,
        EntityPropertyKeys.USERNAME,
        EntityPropertyKeys.USER_PASSWORD,
        EntityPropertyKeys.CREDENTIALS_NON_EXPIRED,
        EntityPropertyKeys.ACCOUNT_NON_EXPIRED,
        EntityPropertyKeys.ACCOUNT_NON_LOCKED,
        EntityPropertyKeys.GUEST,
        EntityPropertyKeys.USER_DELETED,
        EntityPropertyKeys.FULL_NAME
    };
    protected static final Object[] REQUIRED_PROPS = new Object[]{
        EntityPropertyKeys.NAME};

    protected final Button buttonDelete = new Button(ui.getMessage(MessageKeys.DELETE), this);
    protected final Button buttonRestore = new Button(ui.getMessage(MessageKeys.RESTORE), this);
    
    public AbstractDialogEntry(U ui, T entry, boolean doNotReread, String... orderPropertyIds) throws NoDaoBeanForEntityClassException {
        this(ui, entry, doNotReread, null, orderPropertyIds);
    }

    public AbstractDialogEntry(U ui, T entry, boolean doNotReread, FieldGroupEntry entryFieldGroup, String... orderedFieldsPropertyIds) throws NoDaoBeanForEntityClassException {
        super(
                ui,
                entry,
                doNotReread,
                entryFieldGroup != null ? entryFieldGroup : new FieldGroupEntry(entry),
                orderedFieldsPropertyIds);

        setStyleName("dialog-entry");
        addStyleName("Dialog" + entry.getClass().getSimpleName());

        localize();

        // disable controls if entry is system
        if (entry.getId() != null && entry instanceof EntityModelRegistry && ((EntityModelRegistry) entry).isSystem()) {
            buttonSave.setEnabled(false);
            for (Field f : entryFields.getFields()) {
                f.setReadOnly(true);
            }
        } else if (entry.getId() != null) {
            if (entry.getDateDeletedAt() != null) {
                buttonRestore.setStyleName(ValoTheme.BUTTON_FRIENDLY);
                layoutButtons.addComponent(buttonRestore, 0);
                layoutButtons.removeComponent(buttonSave);
            } else {
                buttonDelete.setStyleName(ValoTheme.BUTTON_LINK);
                buttonDelete.addStyleName(ValoTheme.BUTTON_DANGER);
                buttonDelete.setIcon(FontAwesome.TRASH);
                layoutFooter.addComponent(buttonDelete, 0);
                layoutFooter.setComponentAlignment(buttonDelete, Alignment.MIDDLE_LEFT);
            }
        }
    }

    @Override
    protected void initEntryFields(String[] orderedFieldsPropertyIds) {
        for (Object propertyId : entryItem.getItemPropertyIds()) {
            if (!ArrayUtils.contains(UNBOUND_PROPS, propertyId)) {
                entryFields.buildAndBind(ui.getMessage(MessageKeys.PROPERTY_ + propertyId), propertyId);
            }
        }

        // attach fields
        for (Object propertyId : STATUS_PROPERTIES) {
            if (entryFields.getField(propertyId) != null) {
                attachField(propertyId, entryFields.getField(propertyId), true);
            }
        }
        for (Object propertyId : orderedFieldsPropertyIds) {
            if (entryFields.getField(propertyId) != null
                    && entryFields.getField(propertyId).getParent() == null) {
                attachField(propertyId, entryFields.getField(propertyId), false);
            }
        }
        for (Object propertyId : entryFields.getBoundPropertyIds()) {
            if (entryFields.getField(propertyId) != null
                    && entryFields.getField(propertyId).getParent() == null) {
                attachField(propertyId, entryFields.getField(propertyId), false);
            }
        }

        // required fields
        for (Object propertyId : REQUIRED_PROPS) {
            if (entryFields.getField(propertyId) != null) {
                entryFields.getField(propertyId).setRequired(true);
            }
        }
    }

    public void addLayoutComponent(Component l, Float expandoRation) {
        layoutContent.addComponent(l, layoutContent.getComponentIndex(layoutFooter));
        if (expandoRation != null) {
            layoutContent.setExpandRatio(l, expandoRation);
        }
    }

    public void addTabFieldsComponent(Component c) {
        tabsheetFields.setVisible(true);
        tabsheetFields.addComponent(c);
    }

    public void addTabFieldsComponent(Component c, int position) {
        addTabFieldsComponent(c);
        tabsheetFields.setTabPosition(tabsheetFields.getTab(c), position);
    }

    public Collection<Component> getTabFieldsComponents() {
        List<Component> tabsheetComponents = new LinkedList<>();
        for (int i = 0; i < tabsheetFields.getComponentCount(); i++) {
            tabsheetComponents.add(tabsheetFields.getTab(i).getComponent());
        }
        return tabsheetComponents;
    }

    protected void attachField(Object propertyId, Field field, boolean statusField) {
        if (statusField) {

            field.setWidth(EntityPropertyKeys.UUID.equals(propertyId) ? "200px" : EntityPropertyKeys.ID.equals(propertyId) ? "100px" : "140px");
            field.setReadOnly(true);
            layoutHeader.addComponent(field);
            if (field.getValue() == null || (field.getValue() instanceof String && StringUtils.isBlank((String) field.getValue()))) {
                field.setVisible(false);
                layoutHeader.setExpandRatio(field, 0.0f);
                Label label = new Label("auto");
                label.setCaption(field.getCaption());
                layoutHeader.addComponent(label);
                layoutHeader.setExpandRatio(label, EntityPropertyKeys.UUID.equals(propertyId) ? 0.3f : EntityPropertyKeys.ID.equals(propertyId) ? 0.2f : 0.25f);
            } else {
                layoutHeader.setExpandRatio(field, EntityPropertyKeys.UUID.equals(propertyId) ? 0.3f : EntityPropertyKeys.ID.equals(propertyId) ? 0.2f : 0.25f);
            }
        } else {
            layoutFields.addComponent(field);
        }
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (event.getButton() == buttonSave) {
            saveEntry();
        } else if (event.getButton() == buttonDelete) {
            deleteEntry();
        } else if (event.getButton() == buttonRestore) {
            restoreEntry();
        } else if (event.getButton() == buttonCancel) {
            this.close();
        }
    }

    protected void deleteEntry() {
        ConfirmDialog cd = ConfirmDialog.show(ui,
                ui.getMessage(MessageKeys.DELETE),
                ui.getMessage(MessageKeys.CONFIRM_DELETE),
                ui.getMessage(MessageKeys.DELETE),
                ui.getMessage(MessageKeys.CANCEL),
                new ConfirmDialog.Listener() {
            @Override
            public void onClose(ConfirmDialog cd) {
                try {
                    if (cd.isConfirmed()) {
                        dao.doDelete(entry, (SU) ui.getUser());
                        saved = dao.getObjectFetchedAll(entry);
                        fireEvent(new EntrySavedEvent(AbstractDialogEntry.this, saved));
                        ui.notificationShow(ui.getMessage(MessageKeys.ENTRY_DELETED));
                        AbstractDialogEntry.this.close();
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
        cd.getOkButton().setStyleName(ValoTheme.BUTTON_DANGER);

    }

    protected void restoreEntry() {
        ConfirmDialog.show(ui, ui.getMessage(MessageKeys.CONFIRM_RESTORE), new ConfirmDialog.Listener() {
            @Override
            public void onClose(ConfirmDialog cd) {
                try {
                    if (cd.isConfirmed()) {
                        dao.doRestore(entry, (SU) ui.getUser());
                        saved = dao.getObjectFetchedAll(entry);
                        fireEvent(new EntrySavedEvent(AbstractDialogEntry.this, saved));
                        ui.notificationShow(ui.getMessage(MessageKeys.ENTRY_RESTORED));
                        AbstractDialogEntry.this.close();
                    }
                } catch (Exception ex) {
                    ui.handleException(ex);
                }
            }
        });
    }

    protected void saveEntry() {
        try {
            entryFields.commit();
            try {
                saved = dao.doSave(entry, (SU) ui.getUser());
                fireEvent(new EntrySavedEvent(this, saved));
                ui.notificationShow(ui.getMessage(MessageKeys.DATA_SAVED));
                this.close();
            } catch (Exception ex) {
                ui.handleException(ex);
            }
        } catch (FieldGroup.CommitException ex) {

            StringBuilder sb = new StringBuilder("<div class='detailed-message'>");
            sb.append(ui.getMessage(MessageKeys.CHECK_INPUT_FIELDS));
            sb.append(":<ul>");
            for (Field f : ex.getInvalidFields().keySet()) {
                sb.append("<li>").append(f.getCaption());
                if (StringUtils.isNotBlank(ex.getInvalidFields().get(f).getMessage())) {
                    sb.append(": ").append(ex.getInvalidFields().get(f).getMessage());
                } else if (ex.getInvalidFields().get(f) instanceof Validator.EmptyValueException) {
                    sb.append(": ").append(ui.getMessage(MessageKeys.ERROR_VALUE_REQUIRED));
                }
            }
            sb.append("</ul>");
            if (ex.getCause() != null && StringUtils.isNotBlank(ex.getCause().getMessage())) {
                sb.append(ex.getCause().getMessage());
            }
            sb.append("</ul></div>");
            ui.notificationShow(ui.getMessage(MessageKeys.ERROR_INVALID_INPUT_DATA),
                    sb.toString(), Notification.Type.ERROR_MESSAGE);

        }
    }

    @Override
    public void permissions() {
        for (Button b : new Button[]{buttonSave, buttonDelete}) {
            b.setEnabled(ui.isUserAdministrator()
                    || ui.isUserManager());
            b.setDescription(
                    !b.isEnabled()
                    ? ui.getMessage(MessageKeys.YOU_DO_NOT_HAVE_ENOUGH_PRIVILEGES_TO_PERFORM_THE_ACTION)
                    : "");
        }
    }

    // Entry savd event
    private static final Method ENTRY_SAVED_METHOD;

    static {
        try {
            ENTRY_SAVED_METHOD = EntrySavedListener.class.getDeclaredMethod("entrySaved", new Class[]{EntrySavedEvent.class});
        } catch (final java.lang.NoSuchMethodException e) {
            // This should never happen
            throw new java.lang.RuntimeException("Internal error, window close method not found");
        }
    }

    public static class EntrySavedEvent extends Component.Event {

        private final EntityModelEntry savedEntry;

        public EntrySavedEvent(Component source, EntityModelEntry entry) {
            super(source);
            this.savedEntry = entry;
        }

        public EntityModelEntry getSavedEntry() {
            return savedEntry;
        }

        public Window getWindow() {
            return (Window) getSource();
        }
    }

    public interface EntrySavedListener extends Serializable {

        public void entrySaved(EntrySavedEvent e);
    }

    public void addEntrySavedListener(EntrySavedListener listener) {
        addListener(EntrySavedEvent.class, listener, ENTRY_SAVED_METHOD);
    }

    public void removeEntrySavedListener(EntrySavedListener listener) {
        removeListener(CloseEvent.class, listener, ENTRY_SAVED_METHOD);
    }

    public BeanItem<T> getEntryItem() {
        return entryItem;
    }
    
}
