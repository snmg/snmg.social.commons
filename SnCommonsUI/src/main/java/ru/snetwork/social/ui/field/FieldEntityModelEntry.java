package ru.snetwork.social.ui.field;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.lang.StringUtils;
import ru.snetwork.social.dao.AbstractDaoBean;
import ru.snetwork.social.dao.query.WhereClause;
import ru.snetwork.social.dao.query.WhereClauseString;
import ru.snetwork.social.model.EntityModelEntry;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.SnSocialUI;
import ru.snetwork.social.ui.dialog.AbstractDialogEntry;

import java.util.*;
import ru.snetwork.social.model.EntityModelBasic;

public class FieldEntityModelEntry<T extends EntityModelEntry> extends CustomField<T> implements Button.ClickListener {

    private final SnSocialUI ui;
    private final HorizontalLayout layoutRoot = new HorizontalLayout();
    private String entryLabel = "";
    private final TextField fieldName = new TextField();
    private final Button buttonSearch = new Button(null, this);
    private final Button buttonClear = new Button(null, this);

    private final Class<T> clazz;
    private final AbstractDaoBean dao;

    private T newEntryDummy;
    private String[] joinFetchProperties;
    private Map<String, Object> whereCriterias = new HashMap<>();
    private Object[] visibleColumns;
    private String searchProperty = "o.searchableContent";
    protected Button.ClickListener applyButtonClickListener;
    protected Button.ClickListener searchButtonClickListener;

    public FieldEntityModelEntry(Class<T> type, SnSocialUI ui, String caption,
            Object[] visibleColumns) throws NoDaoBeanForEntityClassException {
        setCaption(caption);
        setStyleName("entry-entity-field");
        layoutRoot.setSpacing(true);
        layoutRoot.setWidth("100%");

        this.ui = ui;
        this.visibleColumns = visibleColumns;

        clazz = type;
        dao = ui.getDaoBean(clazz);

        fieldName.setNullRepresentation("");
        fieldName.setWidth("100%");
        fieldName.addValueChangeListener((event) -> {
            if (event.getProperty().getValue() == null) {
                FieldEntityModelEntry.this.addStyleName("null-value");
            } else {
                FieldEntityModelEntry.this.removeStyleName("null-value");
            }
        });
        fieldName.setValue(null);
        fieldName.setReadOnly(true);
        layoutRoot.addComponent(fieldName);
        layoutRoot.setExpandRatio(fieldName, 1.0f);
        buttonSearch.setIcon(FontAwesome.SEARCH);
        buttonSearch.setStyleName(ValoTheme.BUTTON_TINY);
        layoutRoot.addComponent(buttonSearch);
        buttonClear.setStyleName(ValoTheme.BUTTON_TINY);
        buttonClear.setIcon(FontAwesome.TIMES);
        layoutRoot.addComponent(buttonClear);

        setReadOnly(false);
        addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                fieldName.setReadOnly(false);
                if (event.getProperty().getValue() != null
                        && clazz.isInstance(event.getProperty().getValue())) {
                    fieldName.setValue(getStringValue((T) event.getProperty().getValue()));
                } else {
                    fieldName.setValue(null);
                }
                fieldName.setReadOnly(true);
            }
        });

    }

    public void addButton(Button button) {
        layoutRoot.addComponent(button);
    }

    public void addButton(Button button, int pos) {
        layoutRoot.addComponent(button, pos);
    }

    protected String getStringValue(T o) {
        return String.valueOf(o);
    }

    protected String getSearchPhrase() {
        return getValue() != null ? String.valueOf(getValue()) : null;
    }

    public String getSearchProperty() {
        return searchProperty;
    }

    public void setSearchProperty(String searchProperty) {
        this.searchProperty = searchProperty;
    }

    public void setSearchButtonClickListener(Button.ClickListener searchButtonClickListener) {
        if (this.searchButtonClickListener != null) {
            buttonSearch.removeClickListener(searchButtonClickListener);
        }
        this.searchButtonClickListener = searchButtonClickListener;
        buttonSearch.addClickListener(searchButtonClickListener);
    }

    public void setApplyButtonClickListener(Button.ClickListener applyButtonClickListener) {
        this.applyButtonClickListener = applyButtonClickListener;
    }

    @Override
    public Class<? extends T> getType() {
        return clazz;
    }

    @Override
    public void setPropertyDataSource(Property newDataSource) {
        super.setPropertyDataSource(newDataSource);
        buttonSearch.setEnabled(!isReadOnly());
    }

    @Override
    protected Component initContent() {
        return layoutRoot;
    }

    public Button getButtonSearch() {
        return buttonSearch;
    }

    public Button getButtonClear() {
        return buttonClear;
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (buttonSearch == event.getButton()) {
            ui.addWindow(new DialogSearch());
        } else if (buttonClear == event.getButton()) {
            this.clear();
        }
    }

    public T getNewEntryDummy() {
        return newEntryDummy;
    }

    public void setNewEntryDummy(T newEntryDummy) {
        this.newEntryDummy = newEntryDummy;
    }

    public void setInputPrompt(String prompt) {
        this.fieldName.setNullRepresentation(prompt);
    }

    public void setJoinFetchProperties(String... joinFetchProperties) {
        this.joinFetchProperties = joinFetchProperties;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        buttonClear.setEnabled(!readOnly);
        buttonSearch.setEnabled(!readOnly);
    }

    public void putWhereCriteria(String property, Object value) {
        whereCriterias.put(property, value);
    }

    public void removeWhereCriteria(String property) {
        whereCriterias.remove(property);
    }

    class DialogSearch extends Window implements Button.ClickListener {

        final CheckBox searchByFullName = new CheckBox(ui.getMessage(MessageKeys.SEARCH_BY_FULL_NAME));
        final String joinRelations;
        final VerticalLayout layoutRoot = new VerticalLayout();
        final HorizontalLayout layoutSearch = new HorizontalLayout();
        final HorizontalLayout layoutButtons = new HorizontalLayout();
        final TextField fieldSearch = new TextField();
        final Button buttonSearch = new Button(ui.getMessage(MessageKeys.SEARCH), DialogSearch.this);
        final Button buttonAdd = new Button(ui.getMessage(MessageKeys.ADD), DialogSearch.this);
        final Button buttonApply = new Button(ui.getMessage(MessageKeys.APPLY), DialogSearch.this);
        final Button buttonCancel = new Button(ui.getMessage(MessageKeys.CANCEL), DialogSearch.this);
        final BeanItemContainer<T> containerOptions = new BeanItemContainer<>(clazz);
        final Table tableOptions = new Table(null, containerOptions) {
            @Override
            protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {

                if (property.getValue() instanceof EntityModelEntry) {
                    return ui.formatPropertyValue(
                            SnSocialUI.DATETIME_PATTERN,
                            (String) colId,
                            property.getType(),
                            property.getValue());
                }
                return super.formatPropertyValue(rowId, colId, property);
            }
        };

        public DialogSearch() {
            setModal(true);
            center();
            setWidth("50%");
            setHeight("60%");
            setCaption(ui.getMessage(MessageKeys.SEARCH) + " " + entryLabel);
            layoutRoot.setSizeFull();
            layoutRoot.setMargin(true);
            layoutRoot.setSpacing(true);
            setContent(layoutRoot);
            layoutSearch.setSpacing(true);
            layoutSearch.setWidth("100%");
            fieldSearch.setWidth("100%");
            fieldSearch.setValue(getSearchPhrase());
            fieldSearch.setInputPrompt(ui.getMessage(MessageKeys.SEARCH_BY_KEYWORDS));
            fieldSearch.setNullRepresentation("");
            fieldSearch.focus();
            fieldSearch.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.LAZY);
            fieldSearch.addTextChangeListener(new FieldEvents.TextChangeListener() {
                @Override
                public void textChange(FieldEvents.TextChangeEvent event) {
                    String value = (String) event.getText();
                    searchChange(value);
                }

            });
            layoutSearch.addComponent(fieldSearch);
            layoutSearch.setExpandRatio(fieldSearch, 1.0f);
            buttonSearch.setEnabled(false);
            layoutSearch.addComponent(buttonSearch);
            searchByFullName.setValue(true);
            layoutRoot.addComponent(layoutSearch);
            tableOptions.setSizeFull();
            tableOptions.setNullSelectionAllowed(false);
            tableOptions.setSelectable(true);
            tableOptions.addItemClickListener(new ItemClickEvent.ItemClickListener() {
                @Override
                public void itemClick(ItemClickEvent event) {
                    if (event.isDoubleClick()) {
                        buttonApply.click();
                    }
                }
            });
            tableOptions.setMultiSelect(false);
            tableOptions.addValueChangeListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    buttonApply.setEnabled(tableOptions.getValue() != null);
                }
            });
            tableOptions.setVisibleColumns(visibleColumns);
            for (Object columnId : tableOptions.getVisibleColumns()) {
                tableOptions.setColumnHeader(columnId, ui.getMessage(MessageKeys.PROPERTY_.concat((String) columnId)));
            }
            if (joinFetchProperties != null && joinFetchProperties.length > 0) {
                joinRelations = " left join fetch o." + StringUtils.join(joinFetchProperties, " left join fetch o.");
            } else {
                joinRelations = null;
            }

            layoutRoot.addComponent(tableOptions);
            layoutRoot.setExpandRatio(tableOptions, 1.0f);
            layoutButtons.setSpacing(true);
            layoutButtons.setWidth("100%");
            layoutButtons.setStyleName("viewfooter");
            buttonAdd.setStyleName(ValoTheme.BUTTON_LINK);
            layoutButtons.addComponent(buttonAdd);
            layoutButtons.setExpandRatio(buttonAdd, 1.0f);

            buttonApply.setStyleName(ValoTheme.BUTTON_PRIMARY);
            if (applyButtonClickListener != null) {
                buttonApply.addClickListener(applyButtonClickListener);
            }
            layoutButtons.addComponent(buttonApply);
            layoutButtons.addComponent(buttonCancel);
            layoutRoot.addComponent(layoutButtons);
            layoutRoot.setComponentAlignment(layoutButtons, Alignment.MIDDLE_RIGHT);

            searchChange(fieldSearch.getValue());
        }

        @Override
        public void buttonClick(Button.ClickEvent event) {
            try {

                if (buttonSearch == event.getButton()) {
                    searchOptions(searchByFullName.getValue());
                } else if (buttonAdd == event.getButton()) {
                    T newEntry = clazz.newInstance();

                    if (newEntryDummy != null) {
                        BeanItem<T> newItem = new BeanItem<>(newEntry);
                        BeanItem<T> dummyItem = new BeanItem<>(newEntryDummy);
                        for (Object propertyId : newItem.getItemPropertyIds()) {
                            if (!newItem.getItemProperty(propertyId).isReadOnly()
                                    && dummyItem.getItemProperty(propertyId) != null
                                    && dummyItem.getItemProperty(propertyId).getValue() != null) {
                                newItem.getItemProperty(propertyId).setValue(dummyItem.getItemProperty(propertyId).getValue());
                            }
                        }
                    }

                    // open edit dialog
                    AbstractDialogEntry d = (AbstractDialogEntry) ui.dialog(newEntry);
                    // prevent default close listeners of dialog
                    for (Object l : d.getListeners(CloseEvent.class)) {
                        d.removeCloseListener((CloseListener) l);
                    }
                    // add close-listener applying saved object to field value
                    d.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(CloseEvent e) {
                            if (d.getSaved() != null && clazz.isInstance(d.getSaved())) {
                                FieldEntityModelEntry.this.setValue((T) d.getSaved());
                                DialogSearch.this.close();
                            }
                        }
                    });
                } else if (buttonApply == event.getButton()) {
                    if (clazz.isInstance(tableOptions.getValue())) {
                        setValue((T) dao.getObjectFetchedAll((T) tableOptions.getValue()));
                        DialogSearch.this.close();
                        buttonApply.setData(tableOptions.getValue());
                    }
                } else if (buttonCancel == event.getButton()) {
                    DialogSearch.this.close();
                }

            } catch (Exception e) {
                ui.handleException(e);
            }
        }

        protected void searchChange(String value) throws ReadOnlyException {
            buttonSearch.setEnabled(value != null && value.trim().length() >= 2);
            if (buttonSearch.isEnabled()) {
                fieldSearch.setValue(value);
                searchOptions(searchByFullName.getValue());
            }
        }

        private void searchOptions(Boolean searchByFullName) {

            containerOptions.removeAllItems();
            List<WhereClause> where = new LinkedList<>();

            if (searchByFullName) {
                where.add(new WhereClauseString(
                        searchProperty,
                        WhereClause.Operator.ilike,
                        "%" + fieldSearch.getValue() + "%")
                );
            } else {
                where.add(new WhereClauseString(
                        searchProperty,
                        WhereClause.Operator.ilike,
                        fieldSearch.getValue() + "%")
                );
            }
            for (String property : whereCriterias.keySet()) {
                where.add(new WhereClause(property, WhereClause.Operator.eq, whereCriterias.get(property)));
            }
            Collection c = dao.list(null, null, joinRelations, where, 0, 0);
            for (Object o : c) {
                o = dao.getObjectFetchedAll((EntityModelBasic) o);
                containerOptions.addItem(o);
            }
//            containerOptions.addAll(c);
        }

    }

}
