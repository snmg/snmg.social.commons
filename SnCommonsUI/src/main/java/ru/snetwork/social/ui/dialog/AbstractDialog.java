/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.dialog;

import com.vaadin.data.fieldgroup.DefaultFieldGroupFieldFactory;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ru.snetwork.social.dao.AbstractDaoBean;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.model.EntityModelBasic;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.ui.SnSocialTheme;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.SnSocialUI;

/**
 *
 * @author ky6uHETc
 * @param <D> DAO bean class
 * @param <T> entity type
 * @param <U> UI type
 * @param <F> Fields group class
 */
public abstract class AbstractDialog<D extends AbstractDaoBean<T>, T extends EntityModelBasic, U extends SnSocialUI, F extends FieldGroupBasic>
        extends Window
        implements Button.ClickListener {

    protected static final Log log = LogFactory.getLog(AbstractDialog.class.getName());
    protected static final Object[] STATUS_PROPERTIES = new Object[]{
        EntityPropertyKeys.ID,
        EntityPropertyKeys.DATE_CREATED_AT,
        EntityPropertyKeys.DATE_MODIFIED_AT,
        EntityPropertyKeys.USER_CREATED,
        EntityPropertyKeys.USER_MODIFIED,
        EntityPropertyKeys.UUID
    };
    protected final U ui;
    protected final D dao;
    protected final T entry;
    protected T saved;
    protected final BeanItem<T> entryItem;
    protected final F entryFields;
    protected final EntryFieldFactory entryFieldFactory;

    protected final HorizontalLayout layoutHeader;
    protected final FormLayout layoutFields;
    protected final TabSheet tabsheetFields;
    protected final Button buttonSave;
    protected final Button buttonCancel;
    protected final HorizontalLayout layoutButtons;
    protected final HorizontalLayout layoutFooter;
    protected final VerticalLayout layoutContent;

    public AbstractDialog(U ui, T e, boolean doNotReread, F entryFieldGroup, String... orderedFieldsPropertyIds) throws NoDaoBeanForEntityClassException {
        this.ui = ui;
        this.dao = (D) ui.getDaoBean(e.getClass());

        this.entry = e.getId() != null && e.getId() > 0 && !doNotReread ? dao.getObjectFetchedAll(e) : e;
        this.entryItem = new BeanItem<T>(this.entry);

        this.setWidth("700px");
        this.setModal(true);

        // header
        layoutHeader = new HorizontalLayout();
        layoutHeader.setWidth("100%");
        layoutHeader.setSpacing(true);
        layoutHeader.setStyleName(ValoTheme.WINDOW_TOP_TOOLBAR);

        // generate fields
        entryFieldFactory = new EntryFieldFactory();
        entryFields
                = entryFieldGroup != null
                        ? entryFieldGroup
                        : (F) new FieldGroupBasic(e);
        entryFields.setDialog(this);
        entryFields.setFieldFactory(entryFieldFactory);
        entryFields.setItemDataSource(entryItem);
        layoutFields = new FormLayout();
        layoutFields.setWidth("100%");
        layoutFields.setMargin(true);
        layoutFields.setStyleName(SnSocialTheme.LAYOUT_ENTRY_FIELDS);
        tabsheetFields = new TabSheet();
        tabsheetFields.setHeight("100%");
        tabsheetFields.setWidth("100%");
        tabsheetFields.setStyleName(SnSocialTheme.LAYOUT_ENTRY_FIELDS);
        tabsheetFields.setVisible(false);
        tabsheetFields.addSelectedTabChangeListener(new TabSheet.SelectedTabChangeListener() {
            @Override
            public void selectedTabChange(TabSheet.SelectedTabChangeEvent event) {
                if (AbstractDialog.this.isModal()) {
                    center();
                }
            }
        });

        // actions
        buttonSave = new Button(entry.getId() == null ? MessageKeys.CREATE : MessageKeys.SAVE, this);
        buttonSave.setStyleName(ValoTheme.BUTTON_PRIMARY);
        buttonSave.setClickShortcut(ShortcutAction.KeyCode.ENTER, ShortcutAction.ModifierKey.CTRL);
        buttonCancel = new Button(MessageKeys.CANCEL, this);
        buttonCancel.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);
        layoutButtons = new HorizontalLayout(buttonSave, buttonCancel);
        layoutButtons.setSpacing(true);
        Label labelSpacer = new Label("");
        layoutFooter = new HorizontalLayout(labelSpacer, layoutButtons);
        layoutFooter.setWidth("100%");
        layoutFooter.setStyleName(ValoTheme.WINDOW_BOTTOM_TOOLBAR);
        layoutFooter.setExpandRatio(labelSpacer, 1.0f);
        layoutFooter.setComponentAlignment(layoutButtons, Alignment.MIDDLE_RIGHT);

        // content
        layoutContent = new VerticalLayout(layoutHeader, layoutFields, tabsheetFields, layoutFooter);
        layoutContent.setMargin(true);
        layoutContent.setExpandRatio(layoutFields, 1.0f);

        // properties and fields
        if (orderedFieldsPropertyIds == null) {
            orderedFieldsPropertyIds = new String[]{
                EntityPropertyKeys.NAME,
                EntityPropertyKeys.DESCRIPTION};
        }
        initEntryProperties();
        initEntryFields(orderedFieldsPropertyIds);

        // captions and content
        setCaption(entry.getClass().getSimpleName() + (entry.getId() == null ? MessageKeys._NEW : MessageKeys._EDIT));
        setContent(layoutContent);

        localize();

    }

    public TabSheet getTabFields() {
        return tabsheetFields;
    }

    public T getSaved() {
        return saved;
    }

    public HorizontalLayout getLayoutButtons() {
        return layoutButtons;
    }
    
    public VerticalLayout getLayoutContent(){
        return layoutContent;
    }

    public HorizontalLayout getLayoutFooter() {
        return layoutFooter;
    }

    public HorizontalLayout getLayoutHeader() {
        return layoutHeader;
    }

    protected abstract void initEntryProperties();

    protected abstract void initEntryFields(String[] orderedFieldsPropertyIds);

    public void localize() {
        buttonSave.setCaption(ui.getMessage(entry.getId() == null ? MessageKeys.CREATE : MessageKeys.SAVE));
        buttonCancel.setCaption(ui.getMessage(MessageKeys.CANCEL));
        setCaption(ui.getMessage(entry.getClass().getSimpleName() + (entry.getId() == null ? MessageKeys._NEW : MessageKeys._EDIT)));
    }

    public void permissions() {
        buttonSave.setEnabled(false);
    }

    public class EntryFieldFactory extends DefaultFieldGroupFieldFactory {

        @Override
        public <T extends Field> T createField(Class<?> type, Class<T> fieldType) {
            T f = super.createField(type, fieldType); //To change body of generated methods, choose Tools | Templates.
            if (f instanceof TextField) {
                ((TextField) f).setNullRepresentation("");
            } else if (f instanceof TextArea) {
                ((TextArea) f).setNullRepresentation("");
            }
            return f;
        }
    }

    public D getDao() {
        return dao;
    }

    public T getEntry() {
        return entry;
    }

    public BeanItem<T> getEntryItem() {
        return entryItem;
    }

    public U getUi() {
        return ui;
    }

    public F getEntryFields() {
        return entryFields;
    }
}
