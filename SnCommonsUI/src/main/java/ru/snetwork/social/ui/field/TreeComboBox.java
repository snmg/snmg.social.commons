/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.field;

import com.vaadin.data.Buffered;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.Container.ItemSetChangeListener;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.MultiSelectMode;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import com.vaadin.ui.themes.ValoTheme;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.vaadin.hene.popupbutton.PopupButton;

/**
 *
 * @author ky6uHETc
 */
public final class TreeComboBox extends CustomField implements Field {

    private final PopupButton popupButton;
    private final CssLayout popupLayout;
    private final HorizontalLayout layoutRoot;
    private final Tree treeItems;
    private final TextField fItemName;
    String parentPropertyId;
    private String filterString;
    private boolean filterOnlyMatchPrefix = true;
    private boolean parentsSelectable = true;

    private final Property dataSourceSingle;
    private final Property dataSourceMulti;

    public TreeComboBox(String caption, BeanItemContainer container) {

        setCaption(caption);

        fItemName = new TextField();
        fItemName.setWidth("100%");
        fItemName.setNullRepresentation("");
        fItemName.addShortcutListener(new ShortcutListener("Shortcut Name2", ShortcutAction.KeyCode.TAB, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                treeItems.focus();
            }
	});

        layoutRoot = new HorizontalLayout();
        popupLayout = new CssLayout();
        popupLayout.setStyleName("tree-combobox-popupcontent");
        popupLayout.setWidth("700px");
        popupLayout.setHeight("440px");
        BeanItemContainer indexedContainer = (BeanItemContainer) container;

        treeItems = new Tree(null, indexedContainer);
        treeItems.setSelectable(true);
        treeItems.setSizeFull();
        
        treeItems.addShortcutListener(new ShortcutListener("Shortcut Name", ShortcutAction.KeyCode.TAB, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                fItemName.focus();
            }
	});
                
        treeItems.addShortcutListener(new ShortcutListener("Shortcut Name", ShortcutAction.KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                final Object itemId = treeItems.getValue();
                if (itemId != null) {
                    if (!treeItems.hasChildren(itemId) || parentsSelectable) {
                        treeItems.setValue(itemId);
                        indexedContainer.removeAllContainerFilters();
                        refreshPopupButtonCaption(treeItems.getValue());
                        TreeComboBox.this.setValue(treeItems.getValue());
                    } else if (treeItems.hasChildren(itemId)) {
                        if (treeItems.isExpanded(itemId)) {
                            treeItems.collapseItem(itemId);
                        } else {
                            treeItems.expandItem(itemId);
                        }
                    }
                }
            }
	});
        treeItems.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.getButton() == MouseEventDetails.MouseButton.LEFT) {
                    final Object itemId = event.getItemId();
                    if (!treeItems.hasChildren(itemId) || parentsSelectable) {
                        indexedContainer.removeAllContainerFilters();
                        treeItems.setValue(itemId);
                        refreshPopupButtonCaption(itemId);
                        TreeComboBox.this.setValue(itemId);
                    } else if (treeItems.hasChildren(itemId)) {
                        if (treeItems.isExpanded(itemId)) {
                            treeItems.collapseItem(itemId);
                        } else {
                            treeItems.expandItem(itemId);
                        }
                    }
                }
            }
        });
        treeItems.setItemStyleGenerator(new Tree.ItemStyleGenerator() {
            @Override
            public String getStyle(Tree source, Object itemId) {
                if (source.hasChildren(itemId)) {
                    return "has-children";
                }
                return "";
            }
        });
        
        treeItems.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (StringUtils.isEmpty(fItemName.getValue())) {
                   refreshPopupButtonCaption(event.getProperty().getValue());
                }
            }
        });
        
        indexedContainer.addItemSetChangeListener(new ItemSetChangeListener() {
            @Override
            public void containerItemSetChange(ItemSetChangeEvent event) {
                BeanItemContainer container = (BeanItemContainer) event.getContainer();
                if (container.hasContainerFilters()) {
                    Collection items = indexedContainer.getItemIds();
                    treeItems.setItemCaptionMode(Select.ITEM_CAPTION_MODE_EXPLICIT);
                    try {
                        for (Iterator iterator = items.iterator(); iterator.hasNext();) {
                            Object item = iterator.next();
                            String name = item.getClass().getMethod("getName", null).invoke(item, null).toString();
                            Object parent = item.getClass().getMethod("getParent", null).invoke(item, null);
                            String parentName = null;
                            String parentName2 = null;
                            if (parent != null) {
                                parentName = parent.getClass().getMethod("getName", null).invoke(parent, null).toString();
                                Object parent2 = parent.getClass().getMethod("getParent", null).invoke(parent, null);
                                if (parent2 != null) {
                                    parentName2 = parent2.getClass().getMethod("getName", null).invoke(parent2, null).toString();
                                }
                            }
                            
                            treeItems.setItemCaption(item, (StringUtils.isNotEmpty(parentName2) ? parentName2 + " > " : "") + (StringUtils.isNotEmpty(parentName) ? parentName + " > " : "") + name);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        treeItems.setItemCaptionPropertyId(getItemCaptionPropertyId());
                    }
                } else {
                    treeItems.setItemCaptionPropertyId(getItemCaptionPropertyId());
                }
            }
        });
        popupLayout.addComponent(treeItems);

        popupButton = new PopupButton("");
        popupButton.setContent(popupLayout);
        popupButton.setStyleName(ValoTheme.BUTTON_TINY);
        popupButton.setClosePopupOnOutsideClick(true);
        fItemName.setTextChangeEventMode(TextChangeEventMode.TIMEOUT);
        fItemName.setTextChangeTimeout(300);
        fItemName.addTextChangeListener(new TextChangeListener() {
            @Override
            public void textChange(TextChangeEvent event) {
                if (!popupButton.isPopupVisible()) {
                    popupButton.setPopupVisible(true);
                }
                filterString = event.getText().toLowerCase();
                indexedContainer.removeAllContainerFilters();
                if (StringUtils.isNotBlank(filterString)) {
                    SimpleStringFilter filter = new SimpleStringFilter("name", filterString, true, filterOnlyMatchPrefix);
                    indexedContainer.addContainerFilter(filter);
//                    treeItems.focus();
                } else {
                    indexedContainer.removeAllContainerFilters();
                }
                setParentPropertyIds(parentPropertyId);
            }
        });
        popupButton.addPopupVisibilityListener(new PopupButton.PopupVisibilityListener() {
            @Override
            public void popupVisibilityChange(PopupButton.PopupVisibilityEvent event) {
                if (event.isPopupVisible()) {
                    TreeComboBox.this.addStyleName("focus");
                } else {
                    TreeComboBox.this.removeStyleName("focus");
                }
            }
        });

        setStyleName("tree-combobox");
        layoutRoot.setWidth("100%");
        layoutRoot.addComponent(fItemName);
        layoutRoot.addComponent(popupButton);
        layoutRoot.setExpandRatio(fItemName, 1.0f);

        dataSourceSingle = new ObjectProperty(null, container.getBeanType());
        dataSourceMulti = new ObjectProperty(null, Set.class);
        treeItems.setPropertyDataSource(dataSourceSingle);
        super.setPropertyDataSource(treeItems);
        
    }

    @Override
    protected Component initContent() {
        return layoutRoot;
    }

    @Override
    public void commit() throws Buffered.SourceException, Validator.InvalidValueException {
        treeItems.commit();
        validate();
    }

    @Override
    public void focus() {
        popupButton.focus();
    }

    public void getWidth(String width) {
        super.setWidth(width);
    }

    @Override
    public boolean isEmpty() {
        return treeItems.isEmpty();
    }

    public Set getValueIncludingChildren() {
        Set v = new HashSet();
        if (getValue() instanceof Collection) {
            for (Object itemId : (Collection) getValue()) {
                getValueIncludingChildren(v, itemId, 0);
            }
        } else {
            getValueIncludingChildren(v, getValue(), 0);
        }
        return v;
    }

    protected void getValueIncludingChildren(Set v, Object itemId, int depth) {
        if (depth > 10) {
            return;
        }
        v.add(itemId);
        if (treeItems.hasChildren(itemId)) {
            for (Object childId : treeItems.getChildren(itemId)) {
                getValueIncludingChildren(v, childId, depth + 1);
            }
        }
    }

    @Override
    public Class<?> getType() {
        return treeItems.getType();
    }

    @Override
    public void setPropertyDataSource(Property newDataSource) {
        treeItems.setPropertyDataSource(newDataSource);
    }

    @Override
    public Property getPropertyDataSource() {
        return treeItems.getPropertyDataSource();
    }

    @Override
    public void setValue(Object newFieldValue) throws ReadOnlyException, Converter.ConversionException {
        super.setValue(newFieldValue);
        treeItems.setValue(newFieldValue);
        refreshPopupButtonCaption(newFieldValue);
    }

    public String getParentPropertyId() {
        return parentPropertyId;
    }

    public void setParentPropertyId(String parentPropertyId) {
        if (!StringUtils.equals(parentPropertyId, this.parentPropertyId)) {
            setParentPropertyIds(parentPropertyId);
        }
        this.parentPropertyId = parentPropertyId;
    }

    protected void setParentPropertyIds(String parentPropertyId1) {
        for (Object itemId : treeItems.getItemIds()) {
            treeItems.setParent(itemId, null);
            treeItems.setChildrenAllowed(itemId, true);
        }
        if (parentPropertyId1 != null) {
            for (Object itemId : treeItems.getItemIds()) {
                Item item = treeItems.getItem(itemId);
                if (item.getItemProperty(parentPropertyId1) != null) {
                    treeItems.setParent(itemId, item.getItemProperty(parentPropertyId1).getValue());
                } else {
                    treeItems.setParent(itemId, null);
                }
            }
            for (Object itemId : treeItems.getItemIds()) {
                treeItems.setChildrenAllowed(itemId, treeItems.hasChildren(itemId));
            }
        }
    }

    public Collection<?> getItemIds() {
        return treeItems.getItemIds();
    }

    public Object getItemCaptionPropertyId() {
        return treeItems.getItemCaptionPropertyId();
    }

    public void setItemCaptionPropertyId(Object propertyId) {
        treeItems.setItemCaptionPropertyId(propertyId);
    }

    public String getItemCaption(Object itemId) {
        return treeItems.getItemCaption(itemId);
    }

    public boolean isFilterOnlyMatchPrefix() {
        return filterOnlyMatchPrefix;
    }

    public void setFilterOnlyMatchPrefix(boolean filterstringOnlyMatchPrefix) {
        this.filterOnlyMatchPrefix = filterstringOnlyMatchPrefix;
    }

    public boolean isMultiSelect() {
        return treeItems.isMultiSelect();
    }

    public void setMultiSelect(boolean multiSelect) {
        if (treeItems.getPropertyDataSource() == dataSourceSingle && multiSelect) {
            treeItems.setPropertyDataSource(dataSourceMulti);
        } else if (treeItems.getPropertyDataSource() == dataSourceMulti && !multiSelect) {
            treeItems.setPropertyDataSource(dataSourceSingle);
        }
        treeItems.setMultiSelect(multiSelect);

    }

    public void setMultiSelectMode(MultiSelectMode multiSelectMode) {
        treeItems.setMultiselectMode(multiSelectMode);
    }

    public String getNullRepresentation() {
        return fItemName.getInputPrompt();
    }

    public void setNullRepresentation(String nullRepresentation) {
        fItemName.setInputPrompt(nullRepresentation);
        refreshPopupButtonCaption(getValue());
    }

    public boolean isParentsSelectable() {
        return parentsSelectable;
    }

    public void setParentsSelectable(boolean parentsSelectable) {
        this.parentsSelectable = parentsSelectable;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        fItemName.setReadOnly(readOnly);
        popupButton.setEnabled(!readOnly);
    }

    private void refreshPopupButtonCaption(Object value) {

        if (!isMultiSelect() && value != null) {
            fItemName.setValue(treeItems.getItemCaption(value));
        } else if (value != null && (value instanceof Collection) && !((Collection) value).isEmpty()) {
            List<String> captions = new ArrayList(((Collection) value).size());
            for (Object v : (Collection) value) {
                captions.add(treeItems.getItemCaption(v));
            }
            popupButton.setStyleName("not-null");
            fItemName.setValue(StringUtils.join(captions, "; "));
        } else {
            popupButton.setStyleName("null");
            fItemName.setValue(null);
        }

        if (popupButton.isPopupVisible()) {
            popupButton.setPopupVisible(isMultiSelect());
        }
    }
    
    public String getFItemNameValue() {
        return fItemName.getValue();
    }
}
