/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import ru.snetwork.social.ui.view.MainViewType;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.ui.ComponentContainer;
import java.util.Date;

/**
 *
 * @author ky6uHETc
 */
public class SnSocialNavigator extends Navigator {

    private ViewProvider errorViewProvider;

    public SnSocialNavigator(SnSocialUI ui, ComponentContainer container) {
        super(ui, container);
        for (MainViewType viewType : ui.getMainViewTypes()) {
            ViewProvider viewProvider = new ClassBasedViewProvider(viewType.getViewName(), viewType.getViewType());
            addProvider(viewProvider);
            if (ui.getMainViewError() == viewType) {
                errorViewProvider = viewProvider;
            }
        }
        setErrorProvider(new ViewProvider() {
            @Override
            public String getViewName(String viewAndParameters) {
                return ui.getMainViewError().getViewName();
            }

            @Override
            public View getView(String viewName) {
                return errorViewProvider.getView(viewName);
            }
        }
        );

    }

    public void detach() {
        super.switchView(new ViewChangeListener.ViewChangeEvent(this, super.getCurrentView(), null, null, null));
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("===> Memory Usage Debug: SnSocialNavigator [" + this.hashCode() + "] finalize at " + new Date());
        super.finalize();
    }

}
