/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import com.vaadin.ui.UIDetachedException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ru.snetwork.social.model.EntityModelSystemUser;

/**
 *
 * @author ky6uHETc
 */
public class SnSocialUIPushThread extends Thread {

    protected static final Log LOG = LogFactory.getLog(SnSocialUIPushThread.class);

    protected static final List<SnSocialUIPushThread> ALL = new LinkedList<>();
    protected static final ConcurrentHashMap<EntityModelSystemUser, Date> USERS = new ConcurrentHashMap<EntityModelSystemUser, Date>();
    protected static final int ACTIVE_USERS_TIME_LIMIT = 60000;

    protected boolean pushing = false;
    protected boolean run = true;
    protected SnSocialUI ui;

    public SnSocialUIPushThread(SnSocialUI ui) {
        this.ui = ui;
    }

    public static Set<EntityModelSystemUser> activeUsers() {
        Date now = new Date();
        for (EntityModelSystemUser u : USERS.keySet()) {
            if ((USERS.get(u) == null
                    || now.getTime() - USERS.get(u).getTime() > 3 * ACTIVE_USERS_TIME_LIMIT)) {
                USERS.remove(u);
            }
        }

        return USERS.keySet();
    }

    @Override
    public void run() {
        LOG.info("===> Thread Debug: SnSocialUIPushThread " + this.getId() + " started at " + new Date());
        try {
            while (!isInterrupted() && run) {
                
                // sleep 10s 
                Thread.sleep(10000);

                // push
                try {
                    Date now = new Date();
                    try {
                        ui.getHttpSession();
                    } catch (Exception ex) {
                        run = false;
                        break;
                    }
                    if (ui.getUser() != null
                            && !ui.getUser().isGuest()
                            && !ui.getUser().isSystem()
                            && (USERS.get(ui.getUser()) == null
                            || now.getTime() - USERS.get(ui.getUser()).getTime() > ACTIVE_USERS_TIME_LIMIT)) {
                        USERS.put(ui.getUser(), now);
                    }
                    ui.access(new Runnable() {
                        @Override
                        public void run() {
                            if (!pushing && ui.getContent() instanceof SnSocialUIComponent && ((SnSocialUIComponent) ui.getContent()).push()) {
                                pushing = true;
                                try {
                                    ui.push();
                                } catch (IllegalStateException ex) {
                                    LOG.warn(ex.getMessage());
                                    SnSocialUIPushThread.this.interrupt();
                                }
                                pushing = false;
                            }
                        }
                    });
                } catch (IllegalStateException | UIDetachedException ex) {
                    LOG.warn(ex.getMessage());
                    run = false;
                }

            }
        } catch (InterruptedException e) {
            LOG.info("===> Thread Debug: SnSocialUIPushThread " + this.getId() + " InterruptedException at " + new Date());
        } finally {
            LOG.info("===> Thread Debug: SnSocialUIPushThread " + this.getId() + " finished run at " + new Date());
            ui = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        LOG.info("===> Thread Debug: SnSocialUIPushThread " + this.getId() + " finalizing at " + new Date());
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

}
