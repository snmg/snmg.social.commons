/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.helper;

/**
 *
 * @author ky6uHETc
 */
public enum ShowOptions {
    ONLY_ACTIVE,
    ONLY_DISABLED,
    ONLY_DELETED,
    UNREVIEWED,
    DECLINED,
    COMPLETED,
    SHOW_ALL;

    static ShowOptions[] VALUES_DISABLED = new ShowOptions[]{
        ONLY_ACTIVE,
        ONLY_DISABLED,
        SHOW_ALL
    };
    static ShowOptions[] VALUES_DELETED = new ShowOptions[]{
        ONLY_ACTIVE,
        ONLY_DELETED,
        SHOW_ALL

    };
    static ShowOptions[] VALUES_REVIEW = new ShowOptions[]{
        UNREVIEWED,
        DECLINED,
        SHOW_ALL

    };
    static ShowOptions[] VALUES_COMPLETED = new ShowOptions[]{
        ONLY_ACTIVE,
        COMPLETED,
        ONLY_DELETED,
        SHOW_ALL
    };

    public static ShowOptions[] valuesDisabled() {
        return VALUES_DISABLED;
    }

    public static ShowOptions[] valuesDeleted() {
        return VALUES_DELETED;
    }

    public static ShowOptions[] valuesCompleted() {
        return VALUES_COMPLETED;
    }

    public static ShowOptions[] valuesReview() {
        return VALUES_REVIEW;
    }
}
