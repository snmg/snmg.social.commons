/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.WrappedHttpSession;
import com.vaadin.shared.Position;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import java.lang.reflect.ParameterizedType;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import ru.snetwork.social.EntityPropertyKeys;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.dao.AbstractDaoBean;
import ru.snetwork.social.exception.AuthException;
import ru.snetwork.social.model.EntityModelBasic;
import ru.snetwork.social.model.EntityModelGeo;
import ru.snetwork.social.model.EntityModelRegistry;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.dialog.AbstractDialog;
import ru.snetwork.social.ui.event.SnSocialUIEvent;
import ru.snetwork.social.ui.event.SnSocialUIEventBus;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.exception.NoDialogForEntityClassException;
import ru.snetwork.social.ui.helper.SpringContextHelper;
import ru.snetwork.social.ui.view.LoginView;
import ru.snetwork.social.ui.view.MainView;
import ru.snetwork.social.ui.view.MainViewType;

/**
 *
 * @author ky6uhetc
 * @param <MM> MainMenu class
 * @param <SU> System user entity class
 */
public abstract class SnSocialUI<SU extends EntityModelSystemUser, MM extends SnSocialMainMenu> extends UI {

    public static final String DATE_PATTERN = "dd.MM.yy";
    public static final String DATE_PATTERN_FULL_YEAR = "dd.MM.yyyy";
    public static final String MONTH_PATTERN = "MM";
    public static final String YEAR_PATTERN = "yyyy";
    public static final String DATETIME_PATTERN = "dd.MM.yy HH:mm";
    public static final String DATETIMESEC_PATTERN = "dd.MM.yy HH:mm:ss";
    public static final String DATETIMESECMS_PATTERN = "dd.MM.yy HH:mm:ss.S";
    public static final String DATETIME_PATTERN_FULL = "dd.MM.yyyy HH:mm:ss ZZZZ";
    public static final String DATETIME_PATTERN_FULL_YEAR = "dd.MM.yyyy HH:mm";
    public static final String DATETIMESEC_PATTERN_FULL_YEAR = "dd.MM.yyyy HH:mm:ss";
    public static final String DATETIMESECMS_PATTERN_FULL_YEAR = "dd.MM.yyyy HH:mm:ss.S";
    public static final String TIME_PATTERN = "HH:mm";
    public static final String TIMESEC_PATTERN = "HH:mm:ss";
    public static final String[] DATE_PATTERNS = new String[]{
        DATETIME_PATTERN_FULL,
        DATETIMESEC_PATTERN,
        DATETIME_PATTERN,
        DATE_PATTERN,
        DATETIMESEC_PATTERN_FULL_YEAR,
        DATETIME_PATTERN_FULL_YEAR,
        DATE_PATTERN_FULL_YEAR,
        "MMM d, yyyy HH:mm:ss aaa",
        "EEE, d MMM yyyy HH:mm:ss",
        "EEE, d MMM yyyy HH:mm:ss Z"
    };

    protected DecimalFormat formatNumber = new DecimalFormat();
    protected DecimalFormat formatLong = new DecimalFormat();

    {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setGroupingSeparator(' ');
        formatSymbols.setDecimalSeparator('.');
        formatNumber.setDecimalFormatSymbols(formatSymbols);
        formatNumber.setMaximumFractionDigits(2);
        formatNumber.setGroupingUsed(false);
        formatLong.setDecimalFormatSymbols(formatSymbols);
        formatLong.setGroupingUsed(false);
    }

    protected final Log log = LogFactory.getLog(this.getClass());
    protected final Class<? extends SU> userClass;

    protected final SpringContextHelper contextHelper = new SpringContextHelper(VaadinServlet.getCurrent().getServletContext());
    protected final SnSocialUIEventBus eventBus = new SnSocialUIEventBus();
    private SnSocialUIPushThread pushDataThread = new SnSocialUIPushThread(this);

    protected MainView mainView;
    protected LoginView loginView;

    private ResourceBundle messagesCommon;
    private ResourceBundle messagesExtended;

    private boolean pushing = false;
    private Integer windowWidth;
    private Integer windowHeight;

    public SnSocialUI() {
        super();
        this.userClass
                = this.getClass().getSuperclass().getGenericSuperclass() instanceof ParameterizedType
                        ? (Class) ((ParameterizedType) this.getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0]
                        : (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public SnSocialUI(Component content) {
        super(content);
        this.userClass
                = this.getClass().getSuperclass().getGenericSuperclass() instanceof ParameterizedType
                        ? (Class) ((ParameterizedType) this.getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0]
                        : (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        eventBus.register(this, true);
        Responsive.makeResponsive(this);

        if (getUser() != null && getUser().getLocale() != null) {
            setLocale(getUser().getLocale());
        } else {
            setLocale(initDefaultLocale());
        }
        messagesCommon = ResourceBundle.getBundle("ru.snetwork.social.Messages", getLocale());
        messagesExtended = initMessagesBundle(getLocale());

        windowWidth = Page.getCurrent().getBrowserWindowWidth();
        windowHeight = Page.getCurrent().getBrowserWindowHeight();

        // initDataProperties views
        initViews();

        // initDataProperties navigator
        setNavigator(new SnSocialNavigator(this, mainView.getContent()));
        getNavigator().addViewChangeListener(new ViewChangeListener() {
            @Override
            public void afterViewChange(ViewChangeListener.ViewChangeEvent event) {
                for (Object item : mainView.getMenu().getMainMenuItems()) {
                    SnSocialMainMenuItem mmItem = (SnSocialMainMenuItem) item;
                    if (mmItem.getView().getViewType().isInstance(event.getNewView())) {
                        mmItem.addStyleName("selected");
                    } else {
                        mmItem.removeStyleName("selected");
                    }
                }
            }

            @Override
            public boolean beforeViewChange(ViewChangeListener.ViewChangeEvent event) {
                mainView.resetActiveUsers();
                return !isLoggedIn() || mainView.permissionForView(event.getNewView());
            }
        });

        // refresh view
        localize();
        resetView(true);

        // Start push the data feed thread
        // TODO after re-Run project and do some actions (may be errors), method begins to throw IllegalStateException in log
        pushDataThread.start();
        SnSocialUIPushThread.ALL.add(pushDataThread);

        // Browser close listener
        this.addDetachListener(new DetachListener() {
            @Override
            public void detach(DetachEvent event) {
                log.info("--- UI detached ---");
                stopPushDataThread();
                eventBus.unregister(SnSocialUI.this);
            }
        });

//        
//      --- DO NOT USE THIS HACK! ---
//      It breaks PreserverOnRefresh behavior
//        
//        JavaScript.getCurrent().addFunction("aboutToClose", new JavaScriptFunction() {
//
//            @Override
//            public void call(JsonArray arguments) {
//                log.debug("--- Window/Tab is closed ---");
//                SnSocialMonitorUI.this.close();
//            }
//        });
//        Page.getCurrent().getJavaScript().execute("window.onbeforeunload = function (e) { var e = e || window.event; aboutToClose(); return; };");
//        
//      --- DO NOT USE THIS HACK! ---
//
        // Some views need to be aware of browser resize events so a
        // BrowserResizeEvent gets fired to the event bus on every occasion.
        Page.getCurrent().addBrowserWindowResizeListener(new Page.BrowserWindowResizeListener() {
            @Override
            public void browserWindowResized(final Page.BrowserWindowResizeEvent event) {
                windowWidth = event.getWidth();
                windowHeight = event.getHeight();
                eventBus.post(new SnSocialUIEvent.BrowserResize(event.getWidth(), event.getHeight()));
            }
        });
    }

    protected abstract MM initMainMenu();

    protected abstract ResourceBundle initMessagesBundle(Locale locale);

    protected final void initViews() {
        mainView = new MainView(this, initMainMenu());
        loginView = new LoginView(this, getTitle());
    }

    protected abstract Locale initDefaultLocale();

    public final <T> T getBean(Class<T> beanClass) {
        return contextHelper.getBean(beanClass);
    }

    public String getCurrentScopeId() {
        return "main";
    }

    public abstract AbstractDaoBean getDaoBean(Class<? extends EntityModelBasic> entityClass) throws NoDaoBeanForEntityClassException;

    public SnSocialUIEventBus getEventBus() {
        return eventBus;
    }

    public DecimalFormat getFormatLong() {
        return formatLong;
    }

    public DecimalFormat getFormatNumber() {
        return formatNumber;
    }

    public abstract SU getGuestUser();

    public HttpSession getHttpSession() {
        HttpSession session = ((WrappedHttpSession) getSession().getSession()).getHttpSession();
        return session;
    }

    public <T extends Object> T getHttpSessionAttribute(String attributeName, T defaultValue, Class<T> valueType) {
        return getHttpSession().getAttribute(attributeName) != null
                && valueType.isInstance(getHttpSession().getAttribute(attributeName))
                ? (T) getHttpSession().getAttribute(attributeName)
                : defaultValue;
    }

    public void setHttpSessionAttribute(String attributeName, Object attributeValue) {
        getHttpSession().setAttribute(attributeName, attributeValue);
    }

    public abstract Locale[] getLocales();

    public MainView getMainView() {
        return mainView;
    }

    public abstract MainViewType getMainViewError();

    public abstract MainViewType[] getMainViewTypes();

    public String getMessage(String key) {
        if (messagesExtended.containsKey(key)) {
            return messagesExtended.getString(key);
        } else if (messagesCommon.containsKey(key)) {
            return messagesCommon.getString(key);
        } else {
            return "??? " + key + " ???";
        }
    }

    public abstract List<TimeZone> getTimeZones();

    public abstract String getTitle();

    public SU getUser() {
        return isAuthEnabled()
                ? (SU) getHttpSession().getAttribute(userClass.getName())
                : getGuestUser();
    }

    public Class<? extends SU> getUserClass() {
        return userClass;
    }

    public abstract TimeZone getUserTimeZone();

    public Integer getWindowWidth() {
        return windowWidth;
    }

    public Integer getWindowHeight() {
        return windowHeight;
    }

    public final <T extends EntityModelBasic> AbstractDialog dialog(Class<? extends EntityModelBasic> entryClass, Long entryId) throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException {
        return dialog(getDaoBean(entryClass).getObjectFetchedAll(entryId), true);
    }

    public final <T extends EntityModelBasic> AbstractDialog dialog(T entry) throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException {
        return dialog(entry, false);
    }

    protected abstract <T extends EntityModelBasic> AbstractDialog dialog(T entry, boolean doNotReread) throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException;

    public String formatDate(Date date) {
        return date != null ? DateFormatUtils.format(date, DATE_PATTERN, getUserTimeZone()) : "";
    }

    public String formatDateTime(Date date) {
        return date != null ? DateFormatUtils.format(date, DATETIME_PATTERN, getUserTimeZone()) : "";
    }

    public String formatDateTimeSec(Date date) {
        return date != null ? DateFormatUtils.format(date, DATETIMESEC_PATTERN, getUserTimeZone()) : "";
    }
    
    public String formatDateFullYear(Date date) {
        return date != null ? DateFormatUtils.format(date, DATE_PATTERN_FULL_YEAR, getUserTimeZone()) : "";
    }

    public String formatDateTimeFullYear(Date date) {
        return date != null ? DateFormatUtils.format(date, DATETIME_PATTERN_FULL_YEAR, getUserTimeZone()) : "";
    }

    public String formatDateTimeSecFullYear(Date date) {
        return date != null ? DateFormatUtils.format(date, DATETIMESEC_PATTERN_FULL_YEAR, getUserTimeZone()) : "";
    }

    public String formatDateMonth(Date date) {
        return date != null
                ? getMessage(MessageKeys.MONTH_X.concat(DateFormatUtils.format(date, MONTH_PATTERN, getUserTimeZone())))
                + " "
                + DateFormatUtils.format(date, YEAR_PATTERN, getUserTimeZone())
                : "";
    }

    public String formatFileSize(Long number) {
        return number != null ? FileUtils.byteCountToDisplaySize(number) : "0";
    }

    public String formatGeoName(EntityModelGeo o) {
        if (o != null) {
            return StringUtils.isNotBlank(o.getDetailedNameRu()) 
                    ? o.getDetailedNameRu()
                    : StringUtils.isNotBlank(o.getNameRu())
                        ? o.getNameRu()
                        : StringUtils.isNotBlank(o.getNameEn())
                            ? o.getNameEn()
                            : "";
        }
        return "-";
    }

    public String formatNumber(Number number) {
        return number instanceof Long || number instanceof Integer ? formatLong.format(number) : number instanceof Number ? formatNumber.format(number) : "";
    }

    public String formatNumber(Number number, int maxFractionDigits) {
        formatNumber.setMaximumFractionDigits(maxFractionDigits);
        String result = formatNumber(number);
        formatNumber.setMinimumFractionDigits(2);
        return result;
    }

    public String formatPropertyValue(String dateFormat, String propertyId, Class propertyType, Object propertyValue) {
        if (propertyValue instanceof Collection) {
            List<String> result = new ArrayList<>(((Collection) propertyValue).size());
            for (Object v : ((Collection) propertyValue)) {
                result.add(formatPropertyValue(dateFormat, propertyId, propertyType, v));
            }
            return StringUtils.join(result, ",\n");
        } else if (Date.class.isAssignableFrom(propertyType)) {
            return propertyValue != null
                    ? DateFormatUtils.format((Date) propertyValue, dateFormat, getUserTimeZone())
                    : "";
        } else if (propertyValue instanceof EntityModelGeo) {
            final EntityModelGeo geoObject = (EntityModelGeo) propertyValue;
            return geoObject.getNameRu();
        } else if (propertyValue instanceof EntityModelRegistry) {
            return ((EntityModelRegistry) propertyValue).getName();
        } else if (propertyId.equals(EntityPropertyKeys.ID)) {
            final Long id = (Long) propertyValue;
            return id.toString();
        } else if (propertyValue instanceof Boolean) {
            return String.valueOf(propertyValue);
        }
        return null;
    }

    public String formatTime(Date date) {
        return date != null ? DateFormatUtils.format(date, TIME_PATTERN, getUserTimeZone()) : "";
    }

    public String formatTimeSeconds(Date date) {
        return date != null ? DateFormatUtils.format(date, TIMESEC_PATTERN, getUserTimeZone()) : "";
    }

    public String formatTimeDuration(Integer miliseconds) {
        if (miliseconds != null) {
            int seconds = miliseconds / 1000;
            if (seconds < 60) {
                return formatNumber(1.0 * miliseconds / 1000, 1) + " sec";
            } else if (seconds < 3600) {
                return formatNumber(1.0 * seconds / 60, 1) + " min";
            } else {
                return (seconds / 3600) + " hr " + ((seconds % 3600) / 60) + " min";
            }
        }
        return null;
    }

    public void handleException(Throwable ex) {
        if (ex instanceof DataIntegrityViolationException) {
            DataIntegrityViolationException ex2 = (DataIntegrityViolationException) ex;
            String message = getMessage(MessageKeys.ERROR_CONSTRAINT_VIOLATION_DESCRIPTION);
            if (ex2.getCause() instanceof ConstraintViolationException) {
                ConstraintViolationException ex3 = (ConstraintViolationException) ex2.getCause();
                ex3.getConstraintName();
                if (ex3.getCause() != null && StringUtils.contains(ex3.getCause().getMessage(), "duplicate key value violates unique constraint")) {
                    message = getMessage(MessageKeys.ERROR_CONSTRAINT_VIOLATION_DUPLICATE);
                    if (StringUtils.isNotBlank(ex3.getConstraintName())) {
                        message = message
                                + ":<br/><small>" + getMessage(MessageKeys.ERROR_CONSTRAINT_VIOLATION_DUPLICATE_DESCRIPTION)
                                + " [ Constraint: " + ex3.getConstraintName() + " ]</small>";
                    }
                } else if (ex3.getCause() != null && StringUtils.isNotBlank(ex3.getCause().getMessage())) {
                    message = message + "<br/> " + ex3.getCause().getMessage();
                } else {
                    message = message + "<br/> " + ex3.getMessage();
                }
            } else if (ex2.getCause() != null && StringUtils.isNotBlank(ex2.getCause().getMessage())) {
                message = message + "<br/> " + ex2.getCause().getMessage();
            } else {
                message = message + "<br/> " + ex2.getMessage();
            }
            log.warn(ex.getMessage());
            notificationShow(getMessage(MessageKeys.ERROR_CONSTRAINT_VIOLATION), message, Notification.Type.ERROR_MESSAGE);
        } else {
            log.error(ex.getMessage(), ex);
            notificationShow(getMessage(MessageKeys.ERROR_OCCURED), ex.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    public abstract boolean isAuthEnabled();

    public boolean isLoggedIn() {
        return getUser() != null;
    }

    public abstract boolean isUserAdministrator();

    public abstract boolean isUserManager();

    public static void notificationShow(String caption) {
        notificationShow(caption, null);
    }

    public static void notificationShow(String caption, String description) {
        notificationShow(caption, description, Notification.Type.TRAY_NOTIFICATION);
    }

    public static void notificationShow(String caption, String description, Notification.Type type) {
        Notification n = new Notification(caption, description, type);
        if (type == Notification.Type.HUMANIZED_MESSAGE) {
            n.setDelayMsec(3000);
            n.setPosition(Position.BOTTOM_LEFT);
            description = (description != null ? description : "") + "<span style=\"position:fixed;top:0;left:0;width:100%;height:100%\"></span>";
        } else if (type == Notification.Type.WARNING_MESSAGE) {
            n.setDelayMsec(-1);
            description = (description != null ? description : "") + "<span style=\"position:fixed;top:0;left:0;width:100%;height:100%\"></span>";
        } else if (type == Notification.Type.ERROR_MESSAGE) {
            description = (description != null ? description : "") + "<span style=\"position:fixed;top:0;left:0;width:100%;height:100%\"></span>";
        } else if (type == Notification.Type.TRAY_NOTIFICATION) {
            n.setPosition(Position.BOTTOM_LEFT);
        }
        n.setDescription(description);
        n.setHtmlContentAllowed(true);
        n.show(Page.getCurrent());
    }

    public void localize() {
        mainView.localize();
        loginView.localize();
    }

    public abstract SU authenticate(String username, String password) throws AuthException;

    @Subscribe
    public final void login(SnSocialUIEvent.UserLoggedInEvent event) {
        login((SU) event.getUser());
    }

    protected void login(SU user) {
        getHttpSession().setAttribute(userClass.getName(), user);

        // fire event to sync currentView state
        if (getNavigator().getCurrentView() != null) {
            reset(false);
            String[] uriFragment = getPage().getUriFragment().split("/", 2);
            getNavigator().getCurrentView().enter(
                    new ViewChangeListener.ViewChangeEvent(
                            getNavigator(),
                            getNavigator().getCurrentView(),
                            getNavigator().getCurrentView(),
                            getNavigator().getState(),
                            uriFragment.length == 2 ? uriFragment[1] : ""));
        } else {
            reset(true);
        }
    }

    @Subscribe
    public void logout(SnSocialUIEvent.UserLogOutRequestedEvent event) {
        SU user = (SU) getHttpSession().getAttribute(userClass.getName());
        getHttpSession().setAttribute(userClass.getName(), null);
        if (mainView.isCurrentViewForbidden()) {
            getNavigator().navigateTo("dashboard");
        }
        reset(false);

        // fire event to sync active users in all sessions
        if (user != null && !user.isGuest() && !user.isSystem()) {
            eventBus.post(new SnSocialUIEvent.UserLoggedOutEvent(user));
        }
    }

    @Subscribe
    public void logout(SnSocialUIEvent.UserLoggedOutEvent event) {
        if (getUser() == null && getContent() != loginView) {
            if (mainView.isCurrentViewForbidden()) {
                getNavigator().navigateTo("dashboard");
            }
            reset(false);
            try {
                push();
            } catch (Exception ex) {
                // ignore push exception
                JavaScript.getCurrent().execute("window.location.reload()");
            }
        }
    }

    public void permissions() {
        mainView.permissions();
    }

    @Override
    public void push() {
        if (!pushing) {
            pushing = true;
            super.push();
            pushing = false;
        }
    }

    public void refresh() {
        mainView.refresh();
    }

    protected void resetView(boolean refreshView) {
        if (getUser() != null) {
            setContent(mainView);
            if (refreshView) {
                refresh();
            } else {
                mainView.getMenu().refresh();
            }
        } else {
            setContent(loginView);
        }
    }

    protected void reset(boolean refreshView) {
        // current locale
        if (getUser() != null && getUser().getLocale() != null) {
            setLocale(getUser().getLocale());
        } else {
            setLocale(initDefaultLocale());
        }
        // current localization bundle
        if (messagesExtended == null || !messagesExtended.getLocale().equals(getLocale())) {
            messagesCommon = ResourceBundle.getBundle("ru.snetwork.social.Messages", getLocale());
            messagesExtended = initMessagesBundle(getLocale());
        }
        // proceed reset
        localize();
        resetView(refreshView);
        permissions();
    }

    public void stopPushDataThread() {
        log.info("--- Stop pushDataThread: " + pushDataThread.getId() + " (ALL: " + SnSocialUIPushThread.ALL.size() + ") ---");
        pushDataThread.run = false;
        pushDataThread.interrupt();
        SnSocialUIPushThread.ALL.remove(pushDataThread);
        pushDataThread.ui = null;
        pushDataThread = null;
        System.gc();
        log.info("--- Stopped pushDataThread (ALL: " + SnSocialUIPushThread.ALL.size() + ") ---");
    }

    public static void stopPushDataThreads() {
        for (SnSocialUIPushThread pThread : SnSocialUIPushThread.ALL) {
            pThread.run = false;
            pThread.interrupt();
        }
        SnSocialUIPushThread.ALL.clear();
        System.gc();
    }

    @Override
    public void detach() {
        try {
            detachComponentsParent(getMainView());
            if (getNavigator() instanceof SnSocialNavigator) {
                ((SnSocialNavigator) getNavigator()).detach();
            }
            setNavigator(null);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        System.out.println("===> Memory Usage Debug: SnSocialUI [" + this.hashCode() + "]  detach at " + new Date());
        super.detach();
    }

    private void detachComponentsParent(ComponentContainer container) {
        Iterator<Component> components = container.iterator();
        while (components.hasNext()) {
            Component c = components.next();
            getEventBus().unregister(c);
            if (c instanceof ComponentContainer) {
                if (c instanceof TabSheet) {
                    TabSheet t = (TabSheet) c;
                    List l = new ArrayList(t.getListeners(TabSheet.SelectedTabChangeEvent.class));
                    for (Object o : l) {
                        t.removeSelectedTabChangeListener((TabSheet.SelectedTabChangeListener) o);
                    }
                }
                detachComponentsParent((ComponentContainer) c);
            }
        }
        container.removeAllComponents();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("===> Memory Usage Debug: SnSocialUI [" + this.hashCode() + "] finalize at " + new Date());
        super.finalize();
    }

}
