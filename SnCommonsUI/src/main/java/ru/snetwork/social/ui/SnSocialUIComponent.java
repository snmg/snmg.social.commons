/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

/**
 *
 * @author ky6uHETc
 */
public interface SnSocialUIComponent {

    public void localize();

    public boolean push();

    public void permissions();
    
    public void refresh();

    public interface UriFragmentSensing {

        public void senseUriFragment(String params);

    }

    public interface DisplayedSensing {

        public boolean isDisplayed();

        public void setDisplayed(boolean displayed);

    }

    public interface CountIndicator {

        public Integer getCountIndicator();
        
        public String getCountIndicatorStyleName();
    }
}
