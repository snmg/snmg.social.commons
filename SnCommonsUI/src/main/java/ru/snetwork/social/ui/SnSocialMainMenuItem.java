/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import java.util.Objects;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.ui.view.MainViewType;

/**
 *
 * @author ky6uHETc
 */
public class SnSocialMainMenuItem extends Button {

    private final MainViewType view;
    private final SnSocialUI ui;
    private Label separator;

    public SnSocialMainMenuItem(SnSocialMainMenu menu, MainViewType view) {
        this.view = view;
        this.ui = menu.ui;
        this.setCaption(MessageKeys.MAIN_MENU_X + view.getViewName());
        this.setDescription(MessageKeys.MAIN_MENU_X + view.getViewName());
        this.setPrimaryStyleName("valo-menu-item");
        this.setIcon(view.getIcon());
        this.setHtmlContentAllowed(true);
        this.addClickListener(menu);
        this.addContextClickListener(menu);
    }

    public Label getSeparator() {
        return separator;
    }

    public void setSeparator(Label separator) {
        this.separator = separator;
    }

    public MainViewType getView() {
        return view;
    }

    public boolean hasViewPermission() {
        return view.hasViewPermission(ui);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.view);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SnSocialMainMenuItem other = (SnSocialMainMenuItem) obj;
        if (!Objects.equals(this.view, other.view)) {
            return false;
        }
        return true;
    }

}
