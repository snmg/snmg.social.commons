/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui;

import com.vaadin.addon.contextmenu.ContextMenu;
import com.vaadin.addon.contextmenu.Menu;
import com.vaadin.event.ContextClickEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import ru.snetwork.social.MessageKeys;
import ru.snetwork.social.model.EntityModelSystemUser;
import ru.snetwork.social.ui.event.SnSocialUIEvent;
import ru.snetwork.social.ui.exception.NoDaoBeanForEntityClassException;
import ru.snetwork.social.ui.exception.NoDialogForEntityClassException;
import ru.snetwork.social.ui.view.MainViewType;

/**
 *
 * @author ky6uHETc
 * @param <U> system user
 */
public abstract class SnSocialMainMenu<U extends EntityModelSystemUser>
        extends CustomComponent implements Button.ClickListener, ContextClickEvent.ContextClickListener, MenuBar.Command, SnSocialUIComponent {

    private static final String STYLE_VISIBLE = "valo-menu-visible";
    protected final SnSocialUI ui;
    private final CssLayout menuContent = new CssLayout();
    protected final CssLayout mainMenuContainer = new CssLayout();
    private final Map<MainViewType, SnSocialMainMenuItem> mainMenuItems = new LinkedHashMap<>();
    private final ContextMenu mainMenuContextMenu = new ContextMenu(mainMenuContainer, false);
    private SnSocialMainMenuItem mainMenuContextMenuItem = null;
    private final Menu.Command mainMenuContextMenuCommand = new Menu.Command() {
        @Override
        public void menuSelected(com.vaadin.addon.contextmenu.MenuItem selectedItem) {
            if (mainMenuContextMenuItem != null) {
                if (mainMenuItemOpen == selectedItem) {
                    ui.getNavigator().navigateTo(mainMenuContextMenuItem.getView().getViewName());
                } else if (mainMenuItemOpenInNewTab == selectedItem) {
                    ui.getPage().open("#!" + mainMenuContextMenuItem.getView().getViewName(), "_blank", false);
                }

            }
        }
    };
    private final com.vaadin.addon.contextmenu.MenuItem mainMenuItemOpen = mainMenuContextMenu.addItem(MessageKeys.OPEN, mainMenuContextMenuCommand);
    private final com.vaadin.addon.contextmenu.MenuItem mainMenuItemOpenInNewTab = mainMenuContextMenu.addItem(MessageKeys.OPEN_IN_NEW_TAB, mainMenuContextMenuCommand);
    protected MenuItem usermenuSettingsItem;
    protected MenuItem usermenuEditProfile;
    protected MenuItem usermenuLogoutSeparator;
    private MenuItem usermenuLogout;

    private Date curDateTime = new Date();
    private String curDateTimeFormatted = "";
    private String curDateTimeUTC = "";
    private final Label labelLogo = new Label("SN Social <strong>Services</strong>", ContentMode.HTML);
    private final HorizontalLayout logoWrapper = new HorizontalLayout(labelLogo);
    private final CssLayout layoutActiveUsers = new CssLayout();
    private final Label labelTime = new Label();

    public SnSocialMainMenu(final SnSocialUI ui, String logoText) {
        this.ui = ui;

        ui.getEventBus().register(this);

        // 
        addStyleName("valo-menu");
        setSizeUndefined();

        // build menu
        setCompositionRoot(buildContent(logoText));
    }

    protected void addComponent(Component c) {
        menuContent.addComponent(c);
    }

    protected void removeComponent(Component c) {
        menuContent.removeComponent(c);
    }

    private Component buildContent(String logoText) {

        menuContent.addStyleName("sidebar");
        menuContent.addStyleName(ValoTheme.MENU_PART);
        menuContent.addStyleName("no-vertical-drag-hints");
        menuContent.addStyleName("no-horizontal-drag-hints");
        menuContent.setWidth(null);
        menuContent.setHeight("100%");

        menuContent.addComponent(buildTitle(logoText));
        menuContent.addComponent(buildUserMenu());
        menuContent.addComponent(buildToggleButton());
        menuContent.addComponent(buildMenuItems());
        menuContent.addComponent(buildActiveUsers());
        menuContent.addComponent(buildTimeDisplay());

        return menuContent;
    }

    private Component buildTitle(String logoText) {
        labelLogo.setSizeUndefined();
        labelLogo.setValue(logoText);
        logoWrapper.setComponentAlignment(labelLogo, Alignment.MIDDLE_CENTER);
        logoWrapper.addStyleName("valo-menu-title");

        return logoWrapper;
    }

    private Component buildUserMenu() {
        final MenuBar userMenu = new MenuBar();
        userMenu.addStyleName("user-menu");
        usermenuSettingsItem = userMenu.addItem(ui.getGuestUser().getName(), new ThemeResource("img/user_portrait.png"), null);
        usermenuEditProfile = usermenuSettingsItem.addItem(MessageKeys.EDIT_PROFILE, SnSocialMainMenu.this);
        usermenuLogoutSeparator = usermenuSettingsItem.addSeparator();
        usermenuLogout = usermenuSettingsItem.addItem("Sign Out", SnSocialMainMenu.this);
        return userMenu;
    }

    private Component buildToggleButton() {
        Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
                    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
                } else {
                    getCompositionRoot().addStyleName(STYLE_VISIBLE);
                }
            }
        });
        valoMenuToggleButton.setIcon(FontAwesome.LIST);
        valoMenuToggleButton.addStyleName("valo-menu-toggle");
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
        return valoMenuToggleButton;
    }

    private Component buildMenuItems() {
        mainMenuContainer.addStyleName("valo-menuitems");
        for (MainViewType viewType : ui.getMainViewTypes()) {
            SnSocialMainMenuItem mainMenuItem = new SnSocialMainMenuItem(this, viewType);
            mainMenuItems.put(viewType, mainMenuItem);
            mainMenuContainer.addComponent(mainMenuItem);
            if (viewType.hasSeparator()) {
                Label separator = new Label("<hr/>", ContentMode.HTML);
                separator.setStyleName("separator");
                mainMenuContainer.addComponent(separator);
                mainMenuItem.addStyleName("has-separator");
                mainMenuItem.setSeparator(separator);
            }
        }
        return mainMenuContainer;
    }

    private Component buildActiveUsers() {
        layoutActiveUsers.setStyleName("active-users");
        return layoutActiveUsers;
    }

    private Component buildTimeDisplay() {
        CssLayout timeDisplayWrapper = new CssLayout();
        Label labelText = new Label("Server Time");
        curDateTimeFormatted = DateFormatUtils.format(curDateTime, SnSocialUI.DATETIME_PATTERN, ui.getUserTimeZone());
        curDateTimeUTC = DateFormatUtils.format(curDateTime, SnSocialUI.DATETIME_PATTERN);
        labelTime.setValue(
                curDateTimeFormatted + "<br/>" + curDateTimeUTC + " (UTC)"
        );
        labelTime.setContentMode(ContentMode.HTML);
        timeDisplayWrapper.addComponent(labelText);
        timeDisplayWrapper.addComponent(labelTime);

        labelText.addStyleName("serverTimeDisplay_text");
        labelTime.addStyleName("serverTimeDisplay_time");
        timeDisplayWrapper.addStyleName("serverTimeDisplay");

        return timeDisplayWrapper;
    }

    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (event.getButton() instanceof SnSocialMainMenuItem) {
            ui.getNavigator().navigateTo(((SnSocialMainMenuItem) event.getButton()).getView().getViewName());
        }
    }

    @Override
    public void contextClick(ContextClickEvent event) {
        if (event.getComponent() instanceof SnSocialMainMenuItem) {
            mainMenuContextMenuItem = (SnSocialMainMenuItem) event.getComponent();
            if (MouseEventDetails.MouseButton.MIDDLE.getName().equals(event.getButton().getName())) {
                ui.getPage().open("#!" + mainMenuContextMenuItem.getView().getViewName(), "_blank", false);
            } else {
                mainMenuContextMenu.open(event.getClientX(), event.getClientY());
            }
        }
    }

    @Override
    public void menuSelected(MenuItem selectedItem) {
        try {
            if (usermenuLogout == selectedItem) {
                ui.getEventBus().post(new SnSocialUIEvent.UserLogOutRequestedEvent());
            } else if (usermenuEditProfile == selectedItem) {
                openProfileDialog();
            } else {
                SnSocialUI.notificationShow("Menu selected: " + selectedItem.getText());
            }
        } catch (Exception ex) {
            ui.handleException(ex);
        }
    }

    public String getLogoText() {
        return labelLogo.getValue();
    }

    public Collection<SnSocialMainMenuItem> getMainMenuItems() {
        return mainMenuItems.values();
    }

    public SnSocialMainMenuItem getMainMenuItem(MainViewType viewType) {
        for (SnSocialMainMenuItem menuItem : mainMenuItems.values()) {
            if (menuItem.getView().equals(viewType)) {
                return menuItem;
            }
        }
        return null;
    }

    public abstract void openProfileDialog() throws NoDaoBeanForEntityClassException, NoDialogForEntityClassException;

    public void updateUserName(U user) {
        if (user != null) {
            usermenuSettingsItem.setIcon(new ExternalResource(
                    VaadinServlet.getCurrent().getServletContext().getContextPath()
                    + "/userphoto?userId=" + user.getId()));
            usermenuSettingsItem.setText(user.getName());
        } else {
            usermenuSettingsItem.setIcon(new ThemeResource("img/user_portrait.png"));
            usermenuSettingsItem.setText("[ UNKNOWN ]");
        }
    }

    @Override
    public void localize() {
        usermenuEditProfile.setText(ui.getMessage(MessageKeys.EDIT_PROFILE));
        usermenuLogout.setText(ui.getMessage(MessageKeys.SIGN_OUT));
        Iterator<Component> mainMenu = mainMenuContainer.iterator();
        while (mainMenu.hasNext()) {
            Component c = mainMenu.next();
            if (c instanceof SnSocialMainMenuItem) {
                ((SnSocialMainMenuItem) c).setCaption(ui.getMessage(MessageKeys.MAIN_MENU_X + ((SnSocialMainMenuItem) c).getView().getViewName()));
                ((SnSocialMainMenuItem) c).setDescription(ui.getMessage(MessageKeys.MAIN_MENU_X + ((SnSocialMainMenuItem) c).getView().getViewName()));
            }

        }
        mainMenuItemOpen.setText(ui.getMessage(MessageKeys.OPEN));
        mainMenuItemOpenInNewTab.setText(ui.getMessage(MessageKeys.OPEN_IN_NEW_TAB));
    }

    @Override
    public void permissions() {
        for (SnSocialMainMenuItem mainMenuItem : mainMenuItems.values()) {
            mainMenuItem.setVisible(mainMenuItem.hasViewPermission());
        }
    }

    @Override
    public void refresh() {
        updateUserName((U) ui.getUser());
    }

    @Override
    public boolean push() {
        if (!DateUtils.truncatedEquals(curDateTime, new Date(), Calendar.MINUTE)) {
            curDateTime = new Date();
            curDateTimeFormatted = DateFormatUtils.format(curDateTime, SnSocialUI.DATETIME_PATTERN, ui.getUserTimeZone());
            curDateTimeUTC = DateFormatUtils.format(curDateTime, SnSocialUI.DATETIME_PATTERN);
            labelTime.setValue(
                    curDateTimeFormatted + "<br/>" + curDateTimeUTC + " (UTC)"
            );

            return true;

        } else {
            return false;
        }
    }

    public boolean addActiveUser(U user) {
        boolean add = true;
        Iterator<Component> activeUsers = layoutActiveUsers.iterator();
        while (activeUsers.hasNext() && add) {
            Component c = activeUsers.next();
            if (c instanceof Label && user.equals(((Label) c).getData())) {
                add = false;
            }
        }
        if (add) {
            Label labelUser = new Label();
            labelUser.setData(user);
            labelUser.setStyleName("user-item");
            labelUser.setIcon(new ExternalResource(
                    VaadinServlet.getCurrent().getServletContext().getContextPath()
                    + "/userphoto?userId=" + user.getId()));
            labelUser.setValue(user.getName());
            layoutActiveUsers.addComponent(labelUser);
            ui.push();
        }
        return add;
    }

    public boolean removeActiveUser(U user) {
        Component remove = null;
        Iterator<Component> activeUsers = layoutActiveUsers.iterator();
        while (activeUsers.hasNext()) {
            Component c = activeUsers.next();
            if (c instanceof Label && user.equals(((Label) c).getData())) {
                remove = c;
            }
        }
        if (remove != null) {
            layoutActiveUsers.removeComponent(remove);
            ui.push();
        }
        return remove != null;
    }

}
