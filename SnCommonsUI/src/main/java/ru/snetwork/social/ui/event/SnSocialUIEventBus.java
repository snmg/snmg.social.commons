/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.event;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import java.util.Date;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.snetwork.social.ui.exception.NotGeneralEvent;

/**
 *
 * @author ky6uHETc
 */
public class SnSocialUIEventBus implements SubscriberExceptionHandler {

    protected final static Log LOG = LogFactory.getLog(SnSocialUIEventBus.class);
    protected final static GeneralEventBus GENERAL_EVENT_BUS = new GeneralEventBus();
    protected EventBus eventBus;

    public static SnSocialUIEventBus getGeneralEventBus() {
        return GENERAL_EVENT_BUS;
    }

    public SnSocialUIEventBus() {
        eventBus = new EventBus(this);
    }

    public void post(final SnSocialUIEvent event) {
        if (event.general) {
            GENERAL_EVENT_BUS.post(event);
        } else {
            eventBus.post(event);
        }
    }

    public void register(final Object object) {
        register(object, false);
    }

    public void register(final Object object, boolean useGeneralEventBus) {
        if (useGeneralEventBus) {
            GENERAL_EVENT_BUS.register(object);
        }
        eventBus.register(object);

    }

    public void unregister(final Object object) {
        try {
            GENERAL_EVENT_BUS.unregister(object);
        } catch (IllegalArgumentException ex) {
            LOG.warn(ex.getMessage());
        }
        try {
            eventBus.unregister(object);
        } catch (IllegalArgumentException ex) {
            LOG.warn(ex.getMessage());
        }

    }

    @Override
    public final void handleException(final Throwable exception,
            final SubscriberExceptionContext context) {
        LOG.error(exception.getMessage(), exception);
    }

    static class GeneralEventBus extends SnSocialUIEventBus {

        public GeneralEventBus() {
            final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
            executor.setCorePoolSize(10);
            executor.setMaxPoolSize(50);
            executor.setQueueCapacity(100);
            executor.setKeepAliveSeconds(10);
            executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
                @Override
                public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                    LOG.warn("Rejected execution " + r + " by GeneralEventBus (" + executor.toString() + "): purge queue ");
                    executor.purge();
                }
            });
            executor.initialize();
            eventBus = new AsyncEventBus(executor);
        }

        @Override
        public void post(final SnSocialUIEvent event) {
            if (event.general) {
//                LOG.debug("--- Post crossSession event " + event + " start");
//                GeneralEventThread postEventThread = new GeneralEventThread(event);
//                postEventThread.start();
//                LOG.debug("--- Post crossSession event " + event + " finished");
                eventBus.post(event);
            } else {
                throw new NotGeneralEvent();
            }
        }

        @Override
        public void unregister(final Object object) {
            super.eventBus.unregister(object);
        }

    }

    static class GeneralEventThread extends Thread {

        final SnSocialUIEvent event;

        public GeneralEventThread(SnSocialUIEvent event) {
            this.event = event;
        }

        @Override
        public void run() {

            try {

                Date now = new Date();
                LOG.debug(" - running post crossSession event " + event);
                GENERAL_EVENT_BUS.eventBus.post(event);
                LOG.debug(" - finished posting crossSession event " + event + " " + (now.getTime() - new Date().getTime()));

            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }

        }

    }

}
