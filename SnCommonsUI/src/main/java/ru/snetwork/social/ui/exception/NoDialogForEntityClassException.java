/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.exception;

import ru.snetwork.social.exception.SystemException;

/**
 *
 * @author evgeniy
 */
public class NoDialogForEntityClassException extends SystemException {

    public NoDialogForEntityClassException(Class entityClass) {
        super("Unable to determine Dialog for class " + entityClass.getName());

    }

    public NoDialogForEntityClassException(String entitySimpleName) {
        super("Unable to determine Dialog for class " + entitySimpleName);

    }

}
