/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.snetwork.social.ui.helper;

import javax.servlet.ServletContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author ky6uHETc
 */
public class SpringContextHelper {

    private final ApplicationContext context;

    public SpringContextHelper(ServletContext servletContext) {
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    public Object getBean(final String beanRef) {
        return context.getBean(beanRef);
    }

    public <T> T getBean(final Class<T> beanClass) {
        return context.getBean(beanClass);
    }

}
